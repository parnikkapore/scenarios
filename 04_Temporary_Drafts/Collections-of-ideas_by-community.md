# Collections of ideas

A notepad of pitches and episodes ideas collected during discussions on IRC channel or other social networks.

---

**2018-11-08**

A dialog in French [from Framasky's post](https://framapiaf.org/@framasky/101032306165237316) with two undefined characters speaking about a lost dimension; the dimension of the lost objects: one is doing inventory of them; the other one -curious- ask about the content of this dimension of all the lost objects. Inside, the dimension is filled with "good ideas", "old memories", but mostly filled with socks. Single orphan sock (not the pair). 

This idea of exploring a dimension of our "lost" things is good and could fit Pepper and her exploration of dimension.

Original: (CC-0)
```
— J'ai trouvé un nouveau job !
— Ha cool, tu fais quoi ?
— Je fais l'inventaire de la dimension des objets perdus à jamais.
— Pardon ?
— Tous les trucs qu'on perd et qu'on retrouve jamais… bah en fait c'est parce qu'ils passent dans cette dimension.
— Et y a des trucs intéressants dedans ?
— Pas mal oui. J'ai vu passer le Graal, un paquet de bonnes idées, des vieux souvenirs, mais bon, franchement, le stock est surtout constitué de chaussettes. Orphelines, cela va sans dire.
\#MercrediFiction
```

---

**2018-07-17**

Perfect speechbubble tech-like text for a new invention made by Pepper:  
**17:10 cmaloney**  machine learning neural network serverless decentralized federated blockchain geospatial locator for cats

---

**2018-03-31**

**19:43 deevad** At dinner time, Pepper is very exited to show to her guest, Shichimi, Saffron and Coriander what she  prepared: we discover a table full of giant Pizzas with overload of cheese and stuff on it, big hamburgers, candies, etc... Pepper start to eat while guest are disgusted in front of this table and the way Pepper and Carrot eats. Shichimi, Saffron and Coriander struggle to tell Pepper this is not healthy food and a bit disgusting. They try to inform Pepper to eat more healthy , like...vegetables! But Pepper doesn't understand, she is sure Pizza and Hamburgers are vegetable! A conflict rise. Her friend leave her home because they cannot agree. While she see her friends leaving on the distance; Pepper is confused. With a pizza piece in the hand she goes to her garden patch and think "ok, there is still something wrong with the garden": we see the plantation of big flowers looking alike pizzas, hamburger growing like tomatoes, etc...  

---

**2018-03-29**

About [artwork from this livestream](https://youtu.be/zefmloD3KjU)

**20:47 deevad** Pepper wake up, see throught the windows ; autumn started. She is alerted, and hurry to prepare a potion all day. She finally finish the potion. She is happy. She drink it and get younger , really younger. She tell "perfect, duration estimated 4 hours". She run on the mountain. Sit on a rock, and tell Carrot " That my favorite thing in life.... watching autumn with with same vision as in my childhood"  
Note: Transforming into a children needs to have a higher cost than a single day preparing a potion.  

---

**2018-03-29**

**18:23  CalimeroTeknik:** when will Pepper have her magic cloud (which is a literal cloud) for putting all of her stuff  
**18:24  CalimeroTeknik:** and suddenly it doesn't respond anymore and she realizes she had no idea where the stuff went. uh-oh!  
**18:26  CalimeroTeknik:** some items remain lost but all is good since the most important one (of little monetary value) has been recovered  
**18:27  CalimeroTeknik:** and an important lesson is learned: even if that cloud looked convenient, think where you put your stuff  
**18:55  CalimeroTeknik:** deevad, don't forget to make the cloud emit notifications at the worst possible time, like when pepper is concentrating on her studies :p  

---

**2018-03-21**

**18:57 cmaloney:** A house on a mighty cliff that slowly gets sucked in because the cliff is next to raging waters  
**18:58 cmaloney:** but the moral is not to try to sand-bag the house, or re-locate. Rather it's to keep rebuilding and learning  
**19:03 deevad:** I could imagine well Shichimi doing land-art around her tent and have this constant issue with wind, water, climate. And just adapt or relocate. Something Pepper and Carrot can't understand  , but learn  
**19:04 cmaloney:** The joy of doing, not of permanence  
**19:05 cmaloney:** tide comes in and washes away the art  
**19:05 cmaloney:** and when it receeds she goes back out to do it again  
**19:05 cmaloney:** because the stones are new and fresh and colorful and it's a different pallete each time  

---

**2016-10-04**

**cauldron story**

Cmaloney's thought to fix a proposal for a story/storyboard cancelled by deevad.  
The original storyboard was opposing Saffron and Pepper, in competition for a young and cute blacksmith...  

**cmaloney:** Pepper successfully blows up her cauldron. She takes it to the blacksmith for repair. Saffron appears and hands off some metal goods that she has magically repaired. Saffron notifes Pepper's Cauldron (A Komona Cauldron) and wonders why it keeps cracking (That's odd. It shouldn't do that unless subjected to extreme conditions). Pepper blushes. Pepper says "well, maybe Chaosah magic is stronger than Magmah magic. Saffron freezes and says "well, maybe if Chaosah magic were more precise we wouldn't have these problems". They begin feuding, and Saffron and Pepper take magical-girl poses. They do some spells back and forth. Saffron casts a spell that makes an iron wall. Pepper smirks and retorts with a time-based spell that ages the wall and makes it rust. Saffron, realizing the implications, yells "Nooooo!". We now see the blacksmith, rusty hammer, rusty awl, aged to about 70 y/o. He is very surprised at what happened.

---

**2016-07-24**

(about future boys in the serie around Pepper)  

**deevad:** An emo cute ghost Pepper can meet only through a mirror : her impossible romantic love. A strong and muscular douchebag viking/warrior or blacksmith : hard to seduce because in competition with other womans. The sympathic young wizard, not confident , cute in a way but forever in the friendzone.  

---

