# Pepper et Carrot 3 - Le Héros de Hereva - Partie 1 
_Par Nicolas Artance, d'après l'univers d'Hereva de David Revoy ([http://www.peppercarrot.com/](http://www.peppercarrot.com/))_  

_Il y a bien longtemps de cela, un puissant et terrible dragon noir semait la terreur parmi les hommes. Depuis les cieux qu'il transperçait avec ses ailes gigantesques, son œil vif et cruel guettait le moindre mouvement sur terre, afin de réduire en cendres de son souffle incendiaire la première petite fourmi qui aurait l'audace de fouler le sol dont il était le maître absolu.  
Alors que le monde était sur le point de sombrer dans les ténèbres du désespoir, le ciel déchiré par les mouvements du dragon envoya une femme dotée de grands pouvoirs magiques. Grâce à eux, elle redonna de l'espoir au peuple d'en bas, et se mit en quête d'abattre le dragon.  
Mais ce dernier était bien plus coriace et résistant qu'il n'y paraissait. Cependant, la femme venue des cieux parvint à se protéger des terribles assauts du monstre. Afin que plus jamais les hommes ne subissent la colère du dragon et récupèrent leurs droits de naissance, elle combina l'énergie du sol avec ses pouvoirs pour former six monstres légendaires, à qui elle confia une et unique mission : attendre qu'un véritable Héros se dévoile, suffisamment sage et brave pour abattre le dragon une fois pour toute. La femme, ayant sacrifiée toute son énergie, retourna à la terre, confiant aux hommes ses dernières forces afin qu'ils puissent se protéger du monstre en attendant que le Héros de Hereva se dévoile. On appela cette femme "la Sorcière Originelle"._

"Waouh ! La Sorcière Originelle, le Héros de Hereva ! Dis, maman, elle est vraie cette histoire ?"  
  
"Certains y croient, et y voient une explication à notre existence, d'autres non. C'est à chacun de se forger son propre avis."  
  
"Et toi, maman, tu y croies ?"  
  
"J'aimerais beaucoup. Il faut avouer qu'il serait rassurant de savoir que la Sorcière Originelle ou le Héros de Hereva veillent sur nous !"  

"Ils ont l'air trop forts ! Et le Héros, il a fini par abattre le dragon ?"  

"C'est là où l'histoire fait débat. Personne n'a jamais su la suite de ce conte. Mais aucun dragon ne nous menace depuis le ciel, non ? Alors je ne sais pas. Peut-être qu'ils sont tombés dans l'oubli... et puis, aujourd'hui, les dragons sont pacifiques, donc aucun risque qu'ils viennent nous incendier. Allez, il est temps de dormir, maintenant. Bonne nuit, ma chérie."  

"Maman."

"Oui ?"

"J'aimerais bien devenir un héros, moi aussi. Comme ça, je pourrais te protéger si des vilains dragons viennent nous embêter !"  

"Ha, ha ! Contente-toi d'être une sorcière, pour l'instant, et de dormir ! Tu commences ton initiation demain."  

"D'accord ! Bonne nuit, maman !"

"Bonne nuit, ma petite Wasabi."
  

## Acte 1 - La fin de la magie 

### Scène 1 - Le Jugement

_Ascétribune, quelque part dans le ciel.  
Au cœur des nuages, une véritable forteresse géante flotte paisiblement à l'abri des regards. L'éclat des trois lunes n'est pas suffisant pour trahir sa position. Et pour cause, ce qui se déroule à l'intérieur est d'une importance si grande qu'elle échappe au bas peuple.
Dans une des salles de la forteresse, quelques personnes se sont réunies autour d'une table ovale imposante, possédant un trou au milieu. Dans ce trou, entourées de barres solides et animées par magie... des sorcières. Trois sorcières et une sorte de fakir flottant en l'air. Leur point commun ? Tous ont atteint l'honorable titre de Maître d'une des six écoles de magie d'Hereva. Visiblement très irrités de se retrouver dans cette cage magique, observées par des dizaines de paires d'yeux, tous attendent avec plus ou moins d'impatience que quelqu'un réagisse.
Les convives autour de la cage ne sont pas des gens ordinaires pour autant. Tous (ou presque) ont un peuple sous leurs ordres. Eux aussi semblent attendre quelque chose, ou quelqu'un. La dernière hypothèse est plus vraisemblable : en effet, un siège plus imposant que les autres demeure vide autour de la table. Il a plus l'allure d'un trône, avec un symbole ressemblant fortement à un petit fantôme au sommet.
Les minutes passent, mais rien ne se produit. L'un des convives commence à perdre patience._ 

_**Roi Hartru :**_ "Bon, qu'attendons-nous pour tous les brûler ? Qu'il neige ? J'ai du travail qui m'attend, moi !"  

_**Reine Aeil :**_ "Oh, mais quelle grossièreté ! Faîtes donc preuve d'un peu de patience, pour changer !"

_**Roi Hartru :**_ "On vous a demandé le chemin de la plage, à vous ? Et puis, question patience, vous êtes plutôt mal placée pour faire des leçons !"

_**Reine Aeil :**_ "Quel toupet ! Êtes-vous donc tous comme ça, à Grala ? Des malotrus malpolis et sauvages ?"

_**Roi Hartru :**_ "Et vous, à Wass Trar, vous passez tous votre temps à critiquer le voisin en essayant de paraître mieux qu'eux à longueur de journée ? Non ! Alors évitez de trop généraliser et laissez mes sujets tranquilles !"

_**Reine Aeil** (redressant sa couronne et prenant un air hautain) **:**_ "Peuh ! Ignare !"

_**Roi Einsheneim :**_ "Je vous en prie, conservez votre calme ! La réunion n'a pas encore débuté."

_**Roi Hartru :**_ "Dîtes donc, l'ingénieur ! Quand on aura besoin de vos lumières, on vous fera signe ! "

_**Roi Einsheneim :**_ "Sympa..."

_**Roi Hartru :**_ "Et encore... vu votre réputation à Qualicity dans ce domaine, on demandera peut-être à quelqu'un d'autre, ce sera plus constructif ! Pourquoi pas à votre fille chérie, par exemple ? Tout le monde sait que c'est grâce à elle qu'on ne vous enferme pas tous pour éviter de faire sauter la planète, avec vos inventions ridicules ! Oh, non, j'oubliais : c'est une sorcière, et dans peu de temps, elle sera aussi coupable que les autres..."

_**Roi Einsheneim** (se levant d'un coup) **:**_ "PARDON ?! Qu'avez-vous osé insinuer sur ma fille ?"

_**Roi Hartru** (haussant les épaules d'un air goguenard) **:**_ "Rien du tout, mon cher ami. Je ne fais qu'énoncer les faits. Bientôt son art démoniaque sera enfin vu pour ce qu'il est vraiment ! Il rend ceux qui la pratiquent complètement fous ! Regardez donc !"

_Il désigne la cage magique. Toutes les sorcières observent le roi avec un mélange entre frayeur et fureur. La plus petite, et sans doute la plus âgée d'entre elles, ne tient plus en place, et agite ses bras en sautant avec colère, hurlant des insultes heureusement inaudibles pour l'assistance en direction du roi Hartru._

_**Roi Hartru :**_ "Ces imbéciles n'ont pas encore compris qu'au sein de la Cage des Accusés, la parole ne leur est accordée que lorsqu'on leur pose une question ! Vous voyez, quelque part, vous devriez nous remercier : nous allons sauver votre petite princesse de la folie !"

_**Roi Einsheneim :**_ "Espèce de..."

_**Lord Azeirf :**_ "Allons, allons. Ne nous laissons pas emporter par des élans barbares. Nous sommes des adultes civilisés, qui plus est à la tête d'un pays ! Nous nous devons de montrer l'exemple et de rester courtois."

_**Roi Einsheneim :**_ "Oui... oui, vous avez entièrement raison."

_**Reine Aeil :**_ "Pas entièrement ! Suis-je la seule à trouver la présence de cet individu dérangeante ? Vous n'êtes même pas roi !"

_**Lord Azeirf** (s'inclinant légèrement avec un sourire) **:**_ "Majesté de Wass Trar, sachez que je suis ici sur la demande de mon maître le prince Acren, qui est à la tête de Komona, mais trop jeune pour assister à une telle "réunion"."

_La reine ne semble pas très convaincue et lance un regard suspect vers Lord Azeirf. Ce dernier remet son monocle en place avec un air malicieux._

_**Roi Hartru :**_ "Il n'aurait pas dû pouvoir entrer ici ! Seuls les dirigeants de ce monde devraient être au courant de l'affaire et avoir voix au chapitre, et non un ridicule petit lord de rien du tout ! Vous n'êtes qu'un fourbe, Lord Azeirf, et empestez le coup fourré à plein nez !"

_**Lors Azeirf :**_ "Veuillez pardonner l'odeur que je dégage, Sire Hartru. Mais il semblerait que le monde respire cette même odeur à mon contact. Et pourtant, je ne fais qu'obéir aux ordres de Sire Acren. Contrairement à vous, qui n'obéissez qu'à vous-même, ce qui doit être très pratique pour vos petites affaires."

_**Roi Hartru :**_ "Pardon ? Auriez-vous osé insinuer quelque chose en ma présence, Lord Azeirf ?!"

_**Roi Einsheneim** (murmurant à lui-même) **:**_ "Et voilà ce que ça fait..."

_**Lord Azeirf :**_ "La nature de vos "activités" n'est plus un secret pour personne, Sire. Je fais bien sûr allusion à votre sous-sol et surtout à ce qu'il contient..."

_**Roi Hartru** (se levant d'un bond) **:**_ "Un mot de plus et c'est moi qui vous fais sortir, sale..."

_**Voix jeune :**_ "Je vous en prie, arrêtez !"

_Le roi Hartru stoppe son mouvement colérique. Tous se tournent vers l'un des convives, une petite fille qui ne doit pas avoir plus de dix ans. Drapée dans une robe beaucoup trop imposante pour elle, elle est au bord des larmes._

_**Impératrice Lin Chu :**_ "Ces conflits et vagues de haine me sont insupportables ! Nous ne sommes pas ici pour régler des comptes personnels, alors je vous en supplie : arrêtez !"

_Tout le monde lance un regard assassin vers le roi Hartru. Un peu honteux, il préfère rebrousser chemin en poussant un grognement et se rasseoir en croisant les bras._

_**Roi Hartru :**_ "Il devrait y avoir une limite d'âge pour ce genre de réunion !"

_**Impératrice Lin Chu :**_ "Merci, Roi Hartru. Je vous en suis reconnaissante."

_**Reine Aeil** (roulant des yeux, agitant son éventail) **:**_ "Bon, je commence à m'impatienter ! Et il fait une de ces chaleurs ! Quand est-ce qu'elle va enfin se décider à nous honorer de sa présence, cette petite pimbêche ?"

_**Voix :**_ "Me voici ! Veuillez pardonner mon retard !"

_L'attention se tourne vers la nouvelle arrivante, qui dévale l'escalier à toute vitesse pour rejoindre les convives. Le roi Hartru lui lance un regard pétrifiant empli de haine, alors que les autres se contentent d'un air agacé. Elle arrive devant le siège imposant laissé vide._

_**Reine Aeil :**_ "On ne vous attendait plus, Miss ! Enfin, je devrais dire Maître de Ah, mais enfin, vous comprenez... étant donné les circonstances..."

_**Shichimi :**_ "Pas d'inquiétude, votre Altesse : je comprends. Et puis, je ne suis là que pour assurer l'intérim le temps que..."

_La découverte du siège sur lequel elle doit s’asseoir lui coupe la parole. Elle n'en revient pas que ce siège-là lui soit attribué._

_**Reine Aeil :**_ "Le temps que ?"

_**Shichimi** (réagissant, et s'asseyant) **:**_ "Pardonnez-moi ! Je disais donc, le temps qu'un nouveau Maître soit désigné par les Lunes Couchantes, dans quelques jours."

_Shichimi regarde autour de la table. Se retrouver en présence des chefs de toutes les puissances d'Hereva est bien plus intimidant qu'il n'y paraît. Surtout pour ce qu'elle doit y faire : être observée avec autant de mépris par tous les Maîtres des écoles de magie d'Hereva ne fait que renforcer son malaise. Elle tente de faire abstraction et de débuter la réunion._

_**Shichimi :**_ "Hum ! Donc, à présent que nous sommes tous au complet, nous pouvons débuter la réunion. Enfin, je veux dire... le jugement."

_**Roi Hartru :**_ "Avant de débuter quoi que ce soit, j'aimerais soumettre une question à tout le monde."

_**Shichimi :**_ "Ah... mais, vous savez... ce n'est pas très réglementaire, maintenant que le jugement a débuté..."

_**Roi Hartru :**_ "Je me fiche que ce soit réglementaire ou non ! Enfin, rois et reines d'Hereva, ne trouvez-vous pas honteux et déplacé que ce soit justement une sorcière qui préside cette assemblée ?"

_Des murmures commencent à s'élever autour de la table._

_**Shichimi** (désorientée) **:**_ "Euh... s'il vous plaît ! Un peu de calme, je vous prie..."

_Le silence revient. Shichimi fait des efforts surhumains pour cacher ses mains sous la table, pour que les autres ne voient pas qu'elle tremble._

_**Shichimi :**_ "Eh bien, pour répondre à votre question, sachez que les textes sont très clairs à ce sujet : *"Le Maître de Ah demeure le seul Grand Juge du Conseil de Ah, en toute circonstance, même en cas de conflit d'intérêt."* Je ne fais qu'appliquer..."

_**Roi Hartru :**_ "Cette règle n'a aucun sens ! Elle signifie juste que le Maître de Ah peut tout se permettre, y compris des crimes, sans aucun risque d'être puni !"

_**Reine Aeil :**_ "Pour une fois, nous tombons d'accord ! Il faut modifier cette règle au plus vite ! Imaginez qu'un psychopathe prenne la place du Maître de Ah ! Le monde serait à feu et à sang !"

_**Roi Einsheneim :**_ "Mais pourquoi voulez-vous qu'un psychopathe prenne la place du Maître de Ah ? Ils ne sont pas choisis au hasard, tout de même ! Maître Shichimi parlait d'une désignation par les Lunes Couchantes, tout à l'heure !"

_**Reine Aeil :**_ "Et vous croyez qu'il est rassurant de savoir que des astres désignent un dirigeant ?"

_**Roi Einsheneim :**_ "Euh... eh bien je..."

_**Roi Hartru :**_ "Eh, l'ingénieur ! Qu'est-ce que j'ai dit, tout à l'heure ? Il ne semble pas qu'on ait besoin de vos lumières !"

_**Reine Aeil :**_ "Il suffit ! J'en ai assez de vos manières de brutes ! Cessez de vous en prendre à ce pauvre homme à chaque fois qu'il ouvre la bouche !"

_Le ton monte. Les voix se renforcent de plus en plus. La bouilloire se met en marche... commence à siffler... et explose !_

_**Shichimi** (tapant du poing sur la table) **:**_ "ÇA SUFFIT !"

_Le calme revient d'un coup. Les souverains sont surpris par cette puissante voix affirmée._

_**Shichimi :**_ "Vous faîtes pleurer l'Impératrice de Zhongdong ! Vous êtes fiers de vous ?!"

_En effet, la jeune impératrice Lin Chu a plongé son visage dans ses mains. Shichimi se dirige vers elle et lui donne un mouchoir en soie. Pendant qu'elle se mouche et sèche ses larmes, Shichimi lui caresse le dos avant d'assassiner tout le monde du regard._

_**Shichimi :**_ "Pour votre gouverne, sachez que si nous attendons que les Lunes Couchantes désignent le Maître de Ah, ce n'est pas pour faire joli ! Seul quelqu'un d'intègre et droit peut mériter ce titre ! C'est pour cela que ce texte est en vigueur ici ! Et si Maître Wasabi m'a désignée comme intérimaire, c'est qu'elle avait confiance en mon jugement ! Alors je vous demande d'avoir confiance en celui-ci vous aussi ! Et de vous comporter en adulte ! C'est un jugement, pas un spectacle !"

_Les convives se regardent d'un air un peu honteux. Sans autre mot, Shichimi retourne s'asseoir avec hargne. Le roi Hartru continue de lui lancer un air méprisant._

_**Shichimi :**_ "Bien ! Revenons à notre affaire. Il s'agit d'étudier le cas de l'accident de l'Arbre de Komona qui a eu lieu le Azarday de début de cycle à 11 PinkMoon. Je rappelle les faits : le fondateur de Chaosah a réveillé l'Arbre de Komona en détournant sa fonction première de régulateur de magie pour son profit personnel. Elle a tenté un transfert d'âme complet sur une jeune sorcière. Elle a aussi..."

_Shichimi marque une pause en étouffant ce mot, ce terrible mot qui la hante depuis des jours et des nuits. Elle n'arrive toujours pas à y croire, à le rendre réel. Et pourtant elle revit cette terrible scène encore et encore lorsqu'elle ferme les yeux.  
Mais il faut passer outre. Elle l'a désignée pour que justice soit faîte. Et c'est ce qu'elle compte faire._  

_**Shichimi :**_ "Elle a aussi... assassiné... l'ancien Maître de Ah, Wasabi, avant de mettre un terme à sa propre existence. L'Arbre, perturbé, a ensuite émis plusieurs rayons d'énergie d'une portée colossale, provoquant des dégâts matériels importants à travers le monde, dont un qui a failli détruire Komona. Les sorcières présentes ont tout fait pour le contenir. L'Arbre s'est ensuite stabilisé grâce à une jeune sorcière de Chaosah du nom de Pepper. Voilà quels sont les faits. L'objectif de cette assemblée est de prendre les sanctions qui s'imposent. Concernant l'instigateur de cet incident, Chicory, ayant mis fin à ses jours, il est difficile de la punir davantage. Concernant les sorcières présentes sur les lieux, représentées ici par leur Maître respectif..."

_**Roi Hartru :**_ "Il faut les brûler !"

_**Reine Aeil :**_ "Ah, j'aurais été surprise si vous n'aviez pas coupé la parole à une personne de plus pour étaler l'étendue de votre sauvagerie !"

_**Roi Hartru :**_ "C'est ça, plaignez-vous ! Pourtant, vous aussi, vous avez subi les retombées de cette mascarade !"

_**Reine Aeil :**_ "Certes, mais de là à brûler les gens..."

_**Roi Hartru :**_ "C'est la seule solution ! Vous voyez bien ce qu'il se passe, ces sorcières sont une véritable calamité, et elles le prouvent toujours un peu plus chaque jour ! Quelle est la prochaine étape ? Elles vont détruire une ville entière ? Non, il faut les stopper maintenant ! Avant qu'il n'arrive une véritable tragédie !"

_**Roi Einsheneim :**_ "Vous surenchérissez, Roi Hartru. De ce que j'ai compris, le responsable n'est plus. De plus, les sorcières ont contribué à protéger Komona..."

_**Roi Hartru :**_ "Pour sauver leur peau, oui ! Mais de la part d'un homme qui a une sorcière pour fille, je ne m'étonne pas de votre manque de soutien à ma cause ! Quoi que... n'est-ce pas vous qui a vu son château être quasiment détruit par des sorcières ?"

_**Roi Einsheneim :**_ "Il a été établi qu'il s'agissait de Maître Wasabi, en effet. Mais elle m'a ensuite envoyé une lettre d'excuse et un dédommagement, donc..."

_**Roi Hartru :**_ "Pour se protéger, une fois de plus ! Mais voyez, voyez de quoi elles sont capables, vous, les puissants du monde ! Et n'oubliez pas que c'est une des leurs qui a pouvoir de décision sur nous tous ! Alors vous pouvez bien nous bercer avec vos jérémiades et vos lunes qui choisissent des personnes intègres, mais votre Maître a dégradé le château d'un roi ! C'est ça, pour vous, l'intégrité ?! J'appelle ça une mascarade !"

_**Shichimi :**_ "Les sorcières ne sont pas infaillibles. Tout comme vous, elles peuvent commettre des erreurs..."

_**Roi Hartru :**_ "Des erreurs ! Des personnes avec autant de pouvoirs ne devraient pas commettre d'erreurs ! Surtout lorsque le sort du monde repose entre vos mains ! Ce n'est pas normal ! Personne ne devrait avoir plus de pouvoirs qu'un autre ! C'est intolérable ! C'est pour ces raisons-là que j'appelle à l'exécution immédiate de toutes les sorcières ! Le monde sera enfin en sécurité !"

_Un silence pesant suit cette terrible déclaration. Le sérieux avec lequel il a prononcé ces mots en a effrayé plus d'un. Shichimi a de plus en plus de mal à cacher sa frayeur._

_**Shichimi :**_ "B... bon. Laissons la parole aux Maîtres."

### Scène 2 - Prohibition

_Shichimi lance un petit jet de lumière en direction de la cage. La plus petite des sorcières se jette sur les barreaux et hurle au roi Hartru :_

_**Thym :**_ "Espèce de grossier personnage ! Laissez-moi sortir de là, et je vais vous montrer toute l'étendue de la sauvagerie dont une sorcière est capable !"

_**Basilic :** (essayant de retenir Thym) **:**_ "Thym, je t'en supplie, calme-toi !"

_**Thym :**_ "Que je me calme ?! Ce gros lourdaud veut nous massacrer, et je devrais me calmer ?! Oh, j'oubliais : vous autres d'Hippiah, on pourrait vous menacer de brûler votre forêt, vous feriez du thé à vos agresseurs !"

_**Basilic** (rougissant) **:**_ "Inutile d'exagérer !"

_**Roi Hartru :**_ "Tu peux te vanter et m'insulter autant que tu veux, sorcière ! Mais dans ta cage, tu ne peux absolument rien contre moi ! Et lorsque j'aurai eu gain de cause, je te conseille de détaler le plus loin possible de moi, parce que la chasse sera ouverte et j'ai toujours été un chasseur d'exception !"

_**Thym :**_ "Essaie un peu de me pourchasser, mon gros ! C'est un déluge chaotique sans précédent qui t'attend !"

_**Shichimi :**_ "STOP ! Ce n'est pas un règlement de compte qu'on veut entendre, mais une défense ! Alors, Maîtres des écoles de magie, qu'avez-vous à déclarer ?"

_**Thym :**_ "Il y a rien à déclarer, non ?! Les faits sont là : nous avons tout fait pour protéger les gens de cet Arbre ! Sans nous, vous auriez fini désintégrés ! Et même pas un merci, en plus !"

_**Roi Hartru :**_ "Merci ?! À celles qui ont provoqué cette catastrophe ?! Vous délirez à plein régime ! Nous serions bien plus tranquilles et en sécurité sans votre présence ! Je préfère me pendre plutôt que vous devoir quoi que ce soit !"

_**Thym :**_ "Oh, ça peut très vite s'arranger, ça !"

_**Basilic :**_ "Pour notre défense, nous avons effectivement tout tenté pour éviter les dégâts trop important sur les villes voisines. Et que nous ne sommes en aucun cas responsables du réveil de l'Arbre. Tout était l’œuvre de Chicory !"

_**Roi Hartru :**_ "Pas responsables ?! Vous plaisantez ! Vous êtes tous responsables ! On m'a rapporté qu'une amulette devait être mise en contact de l'Arbre pour activer son contrôle, et qu'il devait avoir l'accord de tous les Maîtres pour fonctionner ! Je me trompe ?"

_Thym serre les dents mais ne répond pas. Aucune des sorcières ne répond. Un petit sourire satisfait se dessine sur le visage du roi._

_**Roi Hartru :**_ "C'est bien ce qu'il me semblait... en connaissant les risques encourus, vous avez donné votre accord pour qu'une folle dangereuse puisse l'utiliser ! Voyez, chers souverains, la puissance détenue par ces calamités ! Trouvez-vous ça normal, à présent ? Qu'un simple hochement de tête puisse coûter la vie de vos sujets ?"

_Un autre silence suit la plaidoirie. Les souverains échangent des regards entre eux, comme pour se concerter._

_**Capsica :**_ "Bon, vous avez terminé, les cocos ? J'ai une promo à faire, moi !"

_Tous les regards se tournent vers la diva Capsica, qui porte son éternel froufrou rose et des lunettes de soleil. Décidément, il n'y en a que pour ses spectacles. Elle s'est totalement désintéressée du jugement._

_**Roi Hartru :**_ "Je crois qu'il va falloir faire une croix sur vos spectacles de saltimbanques, Madame ! Le feu que vous utilisez pour vos performances scéniques sera celui de votre bûcher !"

_**Capsica :**_ "Oh, un bûcher... quelle bonne idée de mise en scène ! Je pense pouvoir élaborer un numéro très délicieux pour les yeux ! Merci pour votre suggestion, mon brave !"

_**Roi Hartru :**_ "Mon brave ?! Savez-vous seulement à qui vous vous adressez, espèce de..."

_**Lord Azeirf :**_ "Gardez votre sang froid, Sire Hartru. Nous avons à présent tous les éléments pour un jugement réfléchi."

_Le lord se lève et commence à marcher lentement autour de ses confrères et consœurs, les mains dans le dos._

_**Lord Azeirf :**_ "Chacun des Maîtres ici présent disposait d'une partie de l'amulette responsable de la catastrophe de Komona. Seul leur accord pouvait permettre à feue Maître Chicory d'activer l'Arbre de Komona." 

_**Thym :**_ "EH ! Il n'y avait pas que nous ! Et l'espèce de sirène d'Aquah, alors ? Elle est où, celle-là ?" 

_**Basilic :**_ "Ils n'ont pas dû réussir à la capturer... elles ne se laissent pas faire aussi facilement, ce sont en partie des bêtes..." 

_**Thym :**_ "En partie ? Encore une fois, je te trouve bien modérée dans tes propos..." 

_**Shichimi :**_ "S'il vous plaît !" 

_Thym lance un regard noir vers Shichimi. Elle le soutient avec difficulté, mais y parvient._

_**Shichimi :**_ "Merci. Poursuivez, Lord Azeirf.

_**Lord Azeirf :**_ "Merci. Je disais que leur responsabilité dans la catastrophe est indirecte mais établie. Et n'allez pas croire que le Maître d'Aquah n'est pas impliquée sous prétexte qu'elle n'est pas présente : elle est jugée au même titre que les autres. Elle a aussi donné son sceau, et était présente sur les lieux pour tenter de contrer l'Arbre. Leurs efforts combinés pour empêcher plus de dégradations jouent en leur faveur, mais personnellement, j'aimerais que ce jugement aille au-delà des limites que nous leur avons fixées."

_Une vague d’incompréhension saisit l'assistance._

_**Shichimi :**_ "Au-delà des limites ? Que voulez-vous dire, Lord Azeirf ?"

_**Lord Azeirf :**_ "Que nous devrions profiter de cette occasion pour aussi juger les actes des sorcières envers la population depuis des siècles ! À ma connaissance, très peu de sorcières ont été punies pour leurs mauvaises actions, étant donné qui a le pouvoir de décider ici. N'est-ce pas, Maître intérimaire de Ah ?"

_Shichimi est déconcertée. Elle ne sait pas quoi répondre. Qu'aurait répondu Wasabi ?_

_**Thym :**_ "Dîtes donc, vous ! Vous vous prenez pour qui pour toutes nous juger ?! Je vous préviens..."

_**Lord Azeirf :**_ "Vous n'avez pas la parole, Madame. Aucune question ne vous a été posée."

_Sans que Shichimi l'ait décidé, la cage redevient hermétique à tout son. Thym s'agite à nouveau sans que personne ne puisse profiter de ses paroles._

_**Lord Azeirf :**_ "Tous, ici, avons dû subir les retombées de la perfidie des sorcières. À commencer par mon Maître, le prince Acren, qui a été intimidé et menacé des dizaines de fois par des sorcières, au point qu'il en est à présent terrorisé. Mais ce n'est pas un cas isolé. Sire Hartru, outre les dégâts causés par l'Arbre de Komona, n'est-ce pas votre belle-fille qui a été changée en crapaud par une sorcière alors qu'elle ne faisait que se balader tranquillement dans la forêt ?"

_**Roi Hartru :**_ "Si je retrouve la responsable, il n'y aura pas de jugement, vous pouvez me croire !"

_**Lord Azeirf :**_ "Altesse de Wass Trar, on m'a un jour rapporté que votre demeure toute entière avait été couverte de saletés car vous auriez regardé une sorcière de travers. Est-ce exact ?"

_**Reine Aeil :**_ "Oh que oui ! L'odeur avait envahi toute la ville ! Nous avons mis une semaine à tout nettoyer, c'était horrible !"

_**Lord Azeirf :**_ "Ingénieur en Chef Einsheneim, les rumeurs racontent que de terribles expériences avaient été menées sur des êtres vivants dans les caves de votre château, par des sorcières. Est-ce infondé ?"

_**Roi Einsheneim :**_ "Les mesures ont été prises pour cesser ces activités illégales dont j'ignorais tout. Et ça ne concernait qu'une seule sorcière, qui a déjà été punie, donc..."

_**Lord Azeirf :**_ "Et vous, Impératrice de Zhongdong. Il s'agit sans doute d'une des pires histoires qui se soient produites pour quelqu'un de cette pièce. N'est-ce pas une sorcière qui vous a privée de vos parents ?"

_Une puissante vague de choc frappe les convives. Mêmes les Maîtres emprisonnés sont surpris. La jeune impératrice voit ses yeux se remplir de larmes à nouveau._

_**Lord Azeirf :**_ "Alors, Imminence ? Est-ce la vérité ou non ?"

_**Reine Aeil :**_ "Mais fichez-lui la paix, butor ! Ce n'est encore qu'une enfant !"

_**Roi Hartru :**_ "Mais nous devons savoir ! Répondez donc, fillette, qu'on en finisse !"

_**Reine Aeil :**_ "Vous voir faire preuve de compassion aurait été un miracle !"

_**Roi Hartru :**_ "CHUT ! Contentez-vous de répondre au Lord !"

_Après quelques gémissements, l'impératrice se remet à fondre en larmes._

_**Impératrice Lin Chu :**_ "Si ! C'est la vérité ! *Snif !* Mes parents - *snif !* - ont été massacrés - *snif !* - par une sorcière - *snif !* Mais je ne veux pas - *snif !* - que ça serve de prétexte - *snif !* - pour punir les autres - *snif !* - sorcières ! Je ne veux pas... *snif !* Je ne veux pas attiser la haine ! *Snif !* C'est contraire à toutes les croyances - *snif !* - de mon peuple ! "

_**Lord Azeirf :**_ "Malheureusement, chère Impératrice de Zhongdong, ce sont bien les sorcières qui attisent la haine... car la dernière histoire que je voulais exposer a touché le monde entier pendant plusieurs années. Je parle bien entendu de la Grande Guerre d'Hereva."

_Seuls les gémissements de l'impératrice se font encore entendre. L'air grave qu'a pris le lord est des plus inquiétants. Shichimi ne sait plus quoi faire ou quoi dire. Elle est à deux doigts de craquer._

_**Lord Azeirf :**_ "Deux écoles, Aquah et Magmah, qui entrent en conflit. Des sorcières qui se déchirent entre elles... quelle chance avaient nos peuples respectifs face à un tel déluge de puissance ? Ne parlons même pas des pratiques de résurrection, qui non seulement sont éthiquement inacceptables, mais qui de plus ont permis de prolonger ce massacre..." 

_**Roi Einsheneim :**_ "Il s'agissait de Zombification, pas de résurrection ! Et ma mère a agi sur ordre de Maître Wasabi ! Je ne peux pas vous laisser salir sa mémoire sans réagir !"

_**Lord Azeirf :**_ "Cela ne rend pas ses actions plus éthiques pour autant, Sire Einsheneim. Et au final, c'est bien Maître Wasabi qui a fait en sorte que le conflit prenne fin. Mais malheureusement, elle n'est plus là pour en parler."

_Le cœur de Shichimi se serre davantage. Il est très difficile d'endurer le discours du lord, à plus forte raison quand ce qu'il dit... est la pure vérité._

_**Lord Azeirf :**_ "Mes amis. Peu importe que nous nous apprécions ou non, nous sommes tous dans le même camp. Le camp de ceux qui subissent, de ceux qui sont oppressés, de ceux qui ont peur des sorcières. Certes, elles ne sont pas toutes mauvaises. Mais un tel pouvoir entre de mauvaises mains serait cataclysmique. Aujourd'hui, je vous propose bien plus que de régler l'affaire de l'Arbre de Komona. Je vous propose de régler l'affaire des sorcières contre le peuple."

_**Roi Hartru :**_ "Mon cher Lord, je vous avais sous-estimé ! Vous avez raison, mille fois raison, sur toute la ligne ! Et je suis d'accord pour régler cette histoire une bonne fois pour toute ! Débarrassons-nous définitivement de ce fléau !"

_**Lord Azeirf :**_ "Je vous arrête tout de suite, mon bon souverain. Si nous faisions ceci, quelle différence feriez-vous avec celles que nous critiquons ? Nous serions tout aussi fautifs qu'elles. Non, non. Je refuse tout acte de barbarie."

_**Roi Hartru :**_ "Quoi, alors ?"

_**Lord Azeirf :**_ "Je propose que la pratique de la magie soit prohibée."

_Nouvelle surprise générale. Shichimi se réveille enfin._

_**Shichimi :**_ "Pardon ? La magie... prohibée ?"

_**Lord Azeirf :**_ "Tout à fait, Maître de Ah. C'est le seul moyen pacifique de régler le problème. Si la pratique de la sorcellerie est interdite, et que nous faisons subir de très lourdes sanctions à ceux qui transgressent cette règle, l'harmonie devrait revenir très vite parmi les gens du peuple."

_**Shichimi :**_ "Mais... mais c'est impossible ! Des centaines de sorcières sont en apprentissage ! D'autres en vivent tous les jours et en ont fait leur métier ! On ne peut pas interdire la magie comme ça ! Ce seraient d'innombrables vies qui seraient brisées !"

_**Lord Azeirf :**_ "Et ce seraient encore plus de vies qui seraient sauvées et apaisées. Je pense que c'est un juste retour des choses, après tout ce qu'il s'est passé dans notre histoire."

_**Roi Hartru :**_ "Ah, laissez tomber, Lord Azeirf ! C'est cette petite peste qui a pouvoir de décision ! Elle n'approuvera jamais une telle mesure !"

_**Lord Azeirf** (avec un sourire malicieux) **:**_ "Oui, vous avez raison. À moins que..."

_Le lord change de cap et se dirige vers Shichimi, qui sent son cœur battre à toute vitesse face à un tel regard perfide._

_**Lord Azeirf :**_ "Voyez-vous, à force de travailler pour mon Maître sur les documents officiels, j'ai commencé à prendre goût à l'étude des contrats et des textes de loi. Et il se trouve que ce matin, je faisais une relecture des textes stipulant le cadre de cette assemblée (rédigés par des sorcières, cela va de soi). Et je suis tombé sur une loi assez intéressante, qui pourrait jouer en notre faveur : *"Toute décision impliquant personnellement le Grand Juge peut être contestée si une majorité absolue encourage une décision allant à son encontre."* Il s'agit bien du cas présent, n'est-ce pas ? Notre Grand Juge est directement impliqué par la mesure anti-magie que je propose ? Alors il n'y a qu'à voir. Mes chers souverains. Si vous êtes d'accord pour sauver votre peuple, pour reprendre vos droits et pour promouvoir l'interdiction de la pratique de la sorcellerie, je vous en prie, levez la main."

_Shichimi assiste, impuissante, à une levée de mains générale. Seuls le roi Einsheneim et l'impératrice Lin Chu laissent leur main baissée._

_**Roi Hartru :**_ "Eh bien, fillette ! Voulez-vous donc que d'autres parents soient abattus par ces monstres ?!"

_**Impératrice Lin Chu :**_ "Je refuse... je refuse de participer à ça ! Je refuse d'attiser plus de haine..."

_**Roi Hartru :**_ "Comme vous voudrez ! Vos parents seraient atterrés par votre comportement, jeune fille !"

_**Reine Aeil :**_ "Arrêtez d'importuner cette pauvre enfant, sauvage ! Vous voyez bien que la majorité absolue est atteinte, de toute façon !"

_**Lord Azeirf :**_ "Remarque pertinente, Altesse."

_Le lord se tourne alors vers Shichimi, qui est tétanisée._

_**Lord Azeirf :**_ "Chère Maître de Ah, nous attendons votre jugement. N'oubliez pas ce qu'impliquerait aller à l'encontre de vos propres textes."

_Shichimi est paralysée. Elle cherche du soutien vers les Maîtres, mais elle peut voir sur leur visage un désespoir et un profond choc qu'elle ne peut encaisser. Elle ne peut pas approuver une telle mesure. Mais le lord a raison : elle ne peut pas non plus ignorer la majorité absolue.  
Résignée, d'une voix tremblante, Shichimi déclare :_  

_**Shichimi :**_ "La... la... la loi anti-magie... est approuvée."

_Le roi Hartru pousse un cri de joie. Le roi Einsheneim est presque aussi terrorisé que les Maîtres. Le Lord Azeirf garde un petit sourire satisfait en coin, avant de prendre la direction de sa chaise. Les Maîtres protestent sans que l'on puisse les entendre._

_**Voix :**_ "Le Néant vous guette, à présent."

_Tout le monde se tourne, surpris, vers l'origine de cette voix. Il s'agit du fakir robotique, qui donnait l'impression de dormir en lévitant._

_**Roi Einsheneim :**_ "Maître Soumbala..."

_**Roi Hartru :**_ "Mais comment se fait-il qu'on puisse l'entendre malgré la barrière de la cage, celui-là ?! Encore une invention du démon !"

_**Soumbala :**_ "À présent, un nouveau conflit se prépare. Pas d'armes, pas de violence. Ce sera la guerre du peuple contre le peuple. Vous ne mesurez pas l'impact de votre décision."

_**Roi Hartru :**_ "C'est ce qu'on va voir, espèce de robot ! Je vous attends personnellement, et je serai prêt à vous recevoir !"

_**Shichimi :**_ "Je... la séance est levée. Vous pouvez disposer."

_Les souverains commencent à quitter leur siège pour sortir de la forteresse. Les Maîtres, dépités, sortent aussi de la pièce. Thym lance un regard noir envers Shichimi, mais cette dernière, seule sur son trône, n'arrive pas encore à bien réaliser la portée de ce qu'elle vient de décider. La fin de la magie est déclarée._

### Scène 3 - Le marché noir

_Le lendemain du Conseil de Ah, à Komona.  
La jeune sorcière Pepper arrive vers la cité volante, agrippée à son balai, en compagnie de son fidèle chat Carrot. Elle porte un large sac pour quelques commissions de nourriture et d'ingrédients pour ses potions._

_**Pepper :**_ "Et nous y voilà ! Bon, voyons la liste... où est-elle, d'ailleurs ?" 

_Carrot fouille dans le sac et sort la liste avec sa gueule._

_**Pepper :**_ "Ah, merci, partenaire ! Alors... il me faut des racines de mandragon, un kilo de noix de Kokoro, des plumes de Kapi-kappa, des yeux de merlans fris... encore ? Je pensais en avoir achetés cette sem..."

_Pepper regarde un peu autour d'elle. Elle constate que les personnes qui passent à proximité lui lancent un regard surpris, voire choqué._

_**Pepper :**_ "Hum ? Eh bien, qu'est-ce qu'ils ont ? J'ai quelque chose sur le visage, Carrot ?" 

_Le petit chat orange sort une petite loupe du sac et inspecte le visage de Pepper. Il fait part de son diagnostic final en hochant horizontalement la tête._

_**Pepper :**_ "Bon... ils sont tellement lunatiques, ces Komoniens ! En plus, c'est bientôt la triple pleine lune ! De bons moments en perspective... mais !"

_Pepper se tourne vers l'entrée de la cité._

_**Pepper :**_ "Où sont passés les gardes ? N'importe qui peut entrer, maintenant ! Qu'ils ne s'étonnent pas s'il leur arrive des bricoles... allez, viens, Carrot ! Finissons cette corvée en vitesse, je sens qu'il ne faut pas traîner plus que nécessaire, aujourd'hui..." 

_Pepper pénètre alors au cœur du marché._

_**Pepper :**_ "Quel silence... ils sont vraiment déprimants aujourd'hui ! Et ils continuent de me regarder de travers... je te préviens, Carrot : ça risque de m'agacer assez rapidement !"

_Carrot tremblote légèrement. Il se réfugie sur l'épaule de sa maîtresse, en surveillant les gens du coin de l'œil, qui continuent de les regarder bizarrement._

_**Pepper :**_ "Mais... c'est moi, où il y a moins de stands que d'habitude ?" 

_Pepper arrive à un endroit du marché où devrait se dresser un stand. Mais..._

_**Pepper :**_ "Mais... où est le stand de l'herboriste magique ?! Comment je vais faire pour avoir mes racines de mandragon, moi ?! Bon, allons voir le marchand de fruits. Lui, au moins, il est toujours là... je trouve ça de plus en plus curieux, Carrot. Et même inquiétant... et ces gens qui me regardent tout le temps..."

_Bouillonnante de rage, elle arrive au stand de fruits. Les clients autour s'écartent de Pepper, comme pris de panique._

_**Pepper :**_ "Bonjour ! Un kilo de noix de Kokoro, s'il vous plaît."

_Le commerçant stoppe son mouvement pour ranger une pomme sur son étal. Il regarde Pepper d'un air peu commode, limite menaçant. Les poils de Carrot se hérissent._

_**Pepper :**_ "Dîtes, vous êtes sourd ?! Je vous ai demandé des noix de Kokoro ! Pourquoi vous ne réagissez pas ?!" 

_Le commerçant ne bouge toujours pas. De plus en plus agacée, Pepper se tourne vers les clients qui la regardent._

_**Pepper :**_ "QUOI ?! Qu'est-ce qu'il y a ?! Pourquoi vous me regardez tous comme ça ?! Pourquoi ces têtes d'enterrement ?! Pourquoi il manque des stands ?! Pourquoi on refuse de me servir ?! Enfin, vous n'avez jamais vu une sorcière au marché ?!"

_La foule s'agite alors d'un seul coup._

_**Passant :**_ "Je le savais ! Avec ce balai et ce chapeau, ça ne pouvait être qu'une sorcière ! Laissez-la moi !"

_**Autre passant :**_ "Mais oui, imbécile ! C'est Pepper de Chaosah, la gagnante du concours de potions ! Et je l'ai vue le premier ! C'est moi qui toucherai la prime !"  

_**Passante :**_ "Dans vos rêves ! Cette sorcière est pour moi !" 

_Pepper est maintenant apeurée._

_**Pepper :**_ "Mais... qu'est-ce qu'il vous prend, enfin ?!" 

_**Commerçant :**_ "Ma jeune sorcière, je te conseille de rentrer vite chez toi. Ils vont te poursuivre pour te capturer."

_**Pepper :**_ "Mais pourquoi ?! Qu'est-ce que j'ai fait de mal ?!"  

_**Commerçant :**_ "Tu n'es pas au courant ? Ce n'est pas si étonnant, moi-même je ne l'ai appris que ce matin..." 

_**Pepper :**_ "Appris quoi ?" 

_Les passants commencent à fondre sur elle._

_**Commerçant :**_ "Allez, file vite chez toi, et barricade-toi !! Je vais les ralentir." 

_**Pepper :**_ "Mais..."

_Pepper esquive l'un des passants qui s'est littéralement jeté sur elle. Le commerçant jette alors quelques fruits aux pieds de la foule en furie, pour en faire tomber un maximum. Pepper en profite pour se saisir de son balai et décoller._

_**Pepper :**_ "Mais qu'est-ce qu'il se passe, enfin ?! C'est quoi, cette mascarade ?!"

_**Une voix :**_ "Non ! PAS PAR LÀ !"

_Pepper n'a pas le temps de réagir à cette voix familière. Un filet tombe sur elle et la plaque au sol. Elle se retrouve empêtrée dans le piège, tout comme Carrot._

_**Pepper :**_ "Enlevez-moi ça tout de suite ! Laissez-moi partir !"

_**Autre voix :**_ "Oh non, je ne crois pas."

_Pepper lève la tête. Un vieil homme au regard perfide l'observe, entouré de gardes. Il réajuste son monocle._

_**Pepper :**_ "Vous êtes... qui êtes-vous ?"

_Tout le monde est atterré par l'ignorance de Pepper._

_**Garde :**_ "Espèce de sorcière ! Tu ne reconnais pas Lord Azeirf, au service de sa Majesté Acren ?!"

_**Pepper :**_ "Eh ! Je ne suis pas komonienne, moi ! Je ne suis pas censée connaître tout le gratin ! Et puis pourquoi "espèce de" sorcière ?! Espèce de garde !"

_**Garde** (brandissant sa lance) **:**_ "Sale petite..."

_**Lord Azeirf :**_ "Ça suffit ! Nous ne sommes pas des brutes ! Il est évident qu'il y a un malentendu. Gardes, enlevez ce filet."  

_**Garde :**_ "Mais, enfin, Lord..."

_**Lord Azeirf :**_ "Obéissez !"    

_À contrecœur, et avec une pointe de méfiance, les gardes débarrassent Pepper du filet, mais la tiennent quand même en respect avec leur lance. Celle-ci époussette ses vêtements, pendant que Carrot nettoie son pelage._

_**Pepper :**_ "Merci ! Ah... sans ce maudit filet, je vois plus clair ! Il me semble que je vous ai déjà vu..." 

_**Lord Azeirf :**_ "En effet. Nous avons participé tous deux au jury d'un concours de magie, ici." 

_**Pepper :**_ "Ah, oui, en effet ! J'ai l'impression que ça fait un siècle... bon, est-ce qu'on peut m'expliquer pourquoi on m'a agressée comme ça ?!" 

_**Lord Azeirf :**_ "Bien entendu. Mais trouvons un lieu plus adéquat pour cela. Gardes ! Allez calmer la foule. Je m'occupe de notre jeune amie." 

_**Garde :**_ "Monsieur ! C'est de la folie ! Nous ne pouvons vous laisser seul avec..." 

_**Lord Azeirf :**_ "Votre insubordination commence à m'agacer ! Je vous ai donné un ordre, garde ! Contentez-vous d'obéir !" 

_**Garde :**_ "À... à vos ordres, Lord !"

_Les gardes rejoignent le cœur du marché agité. Pepper lance un regard peu commode au lord._

_**Lord Azeirf :**_ "Veuillez me suivre, Mademoiselle. Je connais une boutique d'apothicaire qui sert également d'excellents thés." 

_**Safran :**_ "Évidemment, étant donné que c'est la mienne." 

_Pepper sursaute. Safran se tenait derrière eux avec son chat Truffel, et un panier rempli de feuilles de thé. Contrairement à d'habitude, elle ne porte pas son chapeau._

_**Pepper :**_ "Safran ! C'est bien ta voix que j'avais entendue tout à l'heure... merci de m'avoir prévenue, pour le piège..."

_**Safran :**_ "Mais encore une fois, tu fonces tête baissée sans réfléchir... tu es irrécupérable, ma pauvre amie. Bon, suivez-moi. Je pense qu'une explication s'impose, à présent."  

### Scène 4 - La nouvelle mesure

_Safran, Pepper et Lord Azeirf entrent dans la boutique de Safran. Pepper constate que sur la devanture, il n'est plus écrit "SAFRAN - SORCELLERIE", mais "SAFRAN - APOTHICAIRE ET MAISON DE THÉ".  
L'intérieur a beaucoup changé. Il y a effectivement beaucoup plus de tables qu'avant, et les étagères remplies d'ingrédients contiennent à présent des boîtes de thés. Les boules de cristal ont disparu, et Safran dispose maintenant d'un comptoir et d'un nombre incalculable de théières différentes, au fond de la pièce.  
Pepper et le lord s'assoient à une table._  

_**Safran :**_ "Bien... qu'est-ce que je vous sers ? La maison de thé vient d'ouvrir, je n'ai pas encore beaucoup de choix à vous proposer pour le moment, mais la liste va vite s'agrandir (de quoi remplir un palais !). J'ai des feuilles de Tomatcha, de Menthalist, de Darlingjee... enfin, tout est sur la carte. Pour vous, Lord, ce sera comme d'habitude ?"

_**Lord Azeirf :**_ "Pourquoi bousculer une bonne habitude qui ne nuit à personne ? Merci, Miss Safran."

_**Safran** (d'un air peu aimable) **:**_ "Et toi ?"

_Pepper regarde la carte avec incompréhension._

_**Pepper :**_ "*Le Primevère Ultime, l'Élégance mis en Bouteille... l'Exaltation Royale !* C'est quoi ces noms de thés ?!"

_**Safran** (agitant son poing) **:**_ "Un problème avec ma façon de nommer mes thés ?!"

_**Pepper :**_ "Non, non, non ! Rassure-toi, je ne faisais que lire... hé, hé ! Euh... eh bien, je vais prendre un thé Mil-Feuil."

_**Safran :**_ "On reste sur du classique... je vous apporte tout ça."

_Safran se dirige dans l'arrière-boutique en prenant une théière au passage._

_**Pepper :**_ "Bon... allez-vous enfin m'expliquer ce qui s'est passé ? Je n'aime pas me faire agresser sans savoir pourquoi ! Et même en sachant pourquoi, d'ailleurs !"

_**Lord Azeirf :**_ "Ah... non, c'est de ma faute. L'information n'est pas arrivée suffisamment rapidement à tout le monde, et vous êtes bien matinale pour une jeune sorcière."

_**Pepper :**_ "Je vous explique : là où j'habite, j'ai trois vieilles sorcières aigries sur le dos. Un des objectifs de ma vie est de minimiser le temps que je passe en leur compagnie, pour ma santé mentale et ma tension artérielle. Donc si je veux faire mes courses sans les avoir sur le dos et sans devoir leur rapporter dix mille produits parce qu'elles ont la flemme de bouger leurs fesses toutes ridées, je n'ai pas le choix : il faut que j'aille tôt au marché. Heureusement pour moi, ce sont de vieilles fainéantes qui dorment jusque tard dans la matinée. J'ai entendu dire que normalement, c'est l'inverse pour les personnes âgées ordinaires, qui ont tendance à se lever tôt. Un coup de chance dans ma situation !"

_**Lord Azeirf :**_ "Eh bien, vous m'avez l'air d'avoir une vie mouvementée avec ces sorcières ! Ah, c'est dommage... tellement dommage..."

_**Pepper :**_ "Quoi, dommage ?"

_**Safran :**_ "Chaud devant !"

_Safran leur apporte leur tasse sur un plateau. Pepper renifle son thé et fait une tête bizarre, que Safran punit d'un regard féroce._

_**Safran :**_ "Et voilà pour ton greffier. Il n'a pas intérêt à mettre le nez dans le bol de Truffel, où il aura affaire à moi !"

_Elle dépose deux bols de lait par terre. Carrot se dirige vers le sien en même temps que Truffel. Il essaie d'avoir l'air aimable envers elle, mais cette dernière l'ignore. Carrot léchouille son bol, un peu vexé. Pepper, elle, se contente de bien décortiquer son thé avec sa cuillère._

_**Safran :**_ "Alors, qu'est-ce que tu attends pour le goûter ? Qu'il fasse nuit ?!"

_**Pepper :**_ "C'est bon, c'est bon !"

_Pepper porte la tasse à ses lèvres et boit une gorgée. Elle affiche un air surpris._

_**Pepper :**_ "C'est... c'est hyper bon !"

_**Safran :**_ "Évidemment, c'est moi qui l'ai préparé ! Ce qui me chagrine, c'est que ça a l'air de te surprendre !"

_**Pepper :**_ "Ben... étant donné l'odeur que ça avait, je me disais... mais il est très bon !"

_**Safran :**_ "Peuh, l'odeur ! Ignorante un jour, ignorante toujours !"

_**Pepper :**_ "Justement, j'ignorais que tu savais faire d'aussi bons thés !"

_**Safran :**_ "C'est bien normal, lorsqu'on veut se reconvertir en salon de thé !"

_**Pepper :**_ "Reconvertir ? Pourquoi tu voudrais te reconvertir ? Tu es déjà élève à Magmah, et tu as ta troupe de danse !"

_Safran et Lord Azeirf s'échangent un regard éloquent._

_**Pepper :**_ "Eh ben, quoi ?"

_**Safran :**_ "Pepper... une nouvelle loi est passée hier, durant le conseil de Ah qui jugeait l'incident survenu à l'Arbre de Komona. Je pensais que tes marraines t'en avaient parlé..."

_**Pepper :**_ "Comme je disais à Lord Azeirf, je tente constamment de les éviter ! Je n'ai pas attendu leur retour pour aller me coucher, hier soir. Et qu'est-ce qu'elle dit, cette nouvelle loi ?"

_**Safran :**_ "C'est une loi anti-magie. À présent, toute activité de sorcellerie est prohibée et sévèrement punie."

_Un silence suit cette déclaration. Pepper a stoppé son mouvement pour reprendre une gorgée de thé. Non, quelque chose ne fonctionne pas dans sa tête. Elle repose lentement sa tasse et demande à Safran, avec un sourire :_

_**Pepper :**_ "Que... quoi ? Pardon, je n'ai pas bien compris ce que tu as dit."

_**Safran :**_ "Ne passe pas pour plus bête que tu ne l'es déjà, tu as très bien entendu ce que je viens de dire."

_Pepper, toujours souriante, tourne la tête vers Lord Azeirf._

_**Pepper :**_ "Lord Azeirf ? Pouvez-vous répéter ce que Safran vient de dire ? Je ne suis pas bien sûre d'avoir tout compris."

_**Lord Azeirf :**_ "Malheureusement, la sorcellerie est interdite, désormais. Interdiction de pratiquer la magie, de concocter quelque potion que ce soit, de vendre des ingrédients magiques, et, bien évidemment, d'exécuter le moindre sort. Les écoles de magie sont dissoutes, et toute activité relevant de la sorcellerie est punie d'emprisonnement. Les gens sont encouragés à dénoncer tout acte qui leur paraît suspect en vue d'une récompense, c'est pour cela que vous avez été secouée tout à l'heure au marché."

_La mâchoire de Pepper reste baissée. Elle n'arrive pas à réaliser ce qu'elle vient d'entendre._

_**Pepper :**_ "La magie... interdite ? Les écoles... dissoutes ? C'est une blague ? Vous êtes en train de me faire une blague, tous les deux, c'est bien cela ?"

_Aucun des deux ne répond. Safran s'est servie un thé et le déguste en détournant le regard, et Lord Azeirf continue de regarder Pepper avec des yeux graves._

_**Pepper :**_ "Pourquoi vous ne répondez pas ? Pourquoi ?! Enfin, C'EST PAS POSSIBLE !"

_**Safran :**_ "Chut ! On ne crie pas dans mon établissement."

_**Pepper :**_ "TON ÉTABLISSEMENT ?! MAIS QUEL ÉTABLISSEMENT, ENFIN ?! TU ES UNE SORCIÈRE ! UNE ÉLÈVE DE MAGMAH ! COMMENT ÇA PEUT ÊTRE TON ÉTABLISSEMENT ?! POURQUOI AVOIR SUPPRIMÉ "SORCELLERIE" DE TON ENSEIGNE ?!! NE ME DIS PAS QUE TU ES D'ACCORD AVEC ÇA ?!"

_**Safran :**_ "Que je sois d'accord ou non, c'est la loi, maintenant. Alors au lieu de geindre et de pleurer sur mon sort, j'ai pris les devants et je me suis reconvertie."

_**Pepper :**_ "MAIS COMMENT ?! Comment tu as pu te reconvertir ?! La loi a été votée hier soir, et lui dit que tes thés sont réputés !"

_**Safran :**_ "Déjà, je te prierais de bien vouloir te calmer. Ensuite, on ne désigne pas l'assistant personnel de son Altesse Acren d'une manière aussi disgracieuse. Et enfin, je n'ai pas attendu cette loi pour quitter Magmah."

_Se faire assommer aurait provoqué la même tête que celle de Pepper en ce moment._

_**Pepper :**_ "Q... quitter ? Tu as quitté Magmah ?"

_**Safran :**_ "La semaine dernière. La magie, les spectacles... pour moi, c'est terminé."

_**Pepper :**_ "Mais... mais pourquoi tu as fait ça ?! Tu adorais tes numéros sur scène ! Tu étais l'élève la plus douée de Magmah ! Ne me dis pas que tu as renoncé à tes rêves ?!"

_**Safran :**_ "Mais dans quel monde crois-tu que nous vivons, Pepper ?! Un monde fabuleux, où tout le monde est gentil et où les rêves de chacun s'exaucent ?! Réveille-toi, Pepper ! Pour survivre, il faut gagner sa vie ! Et ce ne sont pas les spectacles ou la magie qui vont m'aider ! J'ai pris mon avenir en main, Pepper, et je te conseille d'en faire autant ! Surtout maintenant que la magie est interdite."

_**Pepper :**_ "Mais... mais c'est pas possible ! Non, c'est un cauchemar ! C'est un cauchemar, et je vais me réveiller ! Dîtes-moi que c'est un cauchemar, je vous en supplie !!"

_**Lord Azeirf :**_ "Malheureusement, jeune fille, tout ceci est bien réel. Le Grand Juge et Maître de Ah a tranché hier soir, je peux en témoigner : les sorcières, la magie, c'est terminé. À présent que vous le savez, je serais beaucoup moins clément avec vous si je vous revois virevolter sur votre balai. Je vous conseille donc de rentrer chez vous, et de réfléchir à votre avenir."

_**Pepper :**_ "Réfléchir... à mon avenir ? Mais... mais c'est la magie, mon avenir ! J'ai toujours voulu être une grande sorcière de Chaosah !"

_**Lord Azeirf :**_ "Eh bien, il va falloir réfléchir à une reconversion, comme votre amie Safran. Sur ce, je vous laisse, j'ai encore des affaires à traiter, et des messages à diffuser un peu partout pour avertir la population de cette nouvelle mesure. Je vous souhaite une bonne journée. Ah, et bien entendu..."

_Lord Azeirf se saisit du balai de Pepper._

_**Lord Azeirf :**_ "Je confisque ceci. Au revoir, Mesdemoiselles. Mettez le thé sur ma note, Miss Safran."

_**Safran :**_ "Entendu, Lord Azeirf. Passez une agréable journée."

_**Pepper :**_ "Mon balai... NON ! RENDEZ-MOI MON BALAI !"

_Pepper tente de poursuivre le lord, mais Safran l'en empêche._

_**Safran :**_ "S'il te plaît, Pepper ! Calme-toi !"

_**Pepper :**_ "C'EST MON BALAI ! VOUS N'AVEZ PAS LE DROIT ! RENDEZ-MOI MON BALAI !"

_**Safran :**_ "PEPPER ! Tu vas me faire avoir des ennuis si tu continues à crier ! Alors pour l'amour du ciel, CALME-TOI !!"

_Pepper cesse de lutter. Résignée, elle s'assoit lourdement sur sa chaise, les yeux dans le vague, le visage terrifié. Carrot s'approche d'elle pour se frotter contre sa jambe, afin de la rassurer._

_**Pepper :**_ "Mon balai... non... c'est un cauchemar... Safran... pourquoi ? Pourquoi est-ce qu'on me fait ça ?!"

_**Safran :**_ "L'accident de l'Arbre de Komona était la goutte d'eau. Une réprimande pendait au nez des sorcières depuis des années, bien avant la guerre elle-même... on a voulu trop jouer avec nos pouvoirs, et voilà ce qu'on récolte."

_**Pepper :**_ "Mais je n'ai jamais rien fait de mal, moi ! Je voulais juste être une bonne sorcière..."

_**Safran :**_ "Écoute, Pepper... je comprends ta douleur. Nous ne sommes pas en très bon termes, toutes les deux. On s'est beaucoup chamaillées, on s'est même battues, mais on s'est toujours plus ou moins comprises. Je te mentirais si je te disais que quitter Magmah ne m'avait pas attristé. Mais ça fait partie de la vie, on n'y peut rien. Allez, finis ta tasse de thé et rentre chez toi. Je te l'offre pour cette fois. Rentre, et réfléchis sérieusement à ta reconversion. Je préfère te prévenir tout de suite : il est hors de question que j'héberge une fille qui n'a pas d'emploi stable ! En revanche, si tu veux un poste de serveuse, je t'avouerais que ça m'aiderait beaucoup. J'ai de plus en plus de monde, ici, et ça devient de plus en plus difficile de..."

_Pepper se lève d'un bond, le visage sombre._

_**Safran :**_ "Pepper... je connais ce regard, et je ne l'apprécie pas du tout ! Qu'est-ce que tu as l'intention de faire ? Pas de bêtises, au moins ?"

_**Pepper :**_ "Vous mentez."

_**Safran :**_ "Quoi ?"

_**Pepper :**_ "VOUS MENTEZ ! TOUS ! La magie ne peut pas être prohibée, ça n'a pas de sens ! Et je vais de ce pas demander à quelqu'un qui était à ce conseil de confirmer ça ! Merci pour le thé, Safran."

_Pepper sort en trombe de la boutique. Elle revient quelques instants plus tard._

_**Pepper :**_ "Tu... tu peux me prêter un balai ?"

_**Safran :**_ "Pardon ?! Mais c'est absolument hors de question ! Tu veux me faire descendre aux cachots ou quoi ?!"

_**Pepper :**_ "Mais comment je vais rentrer chez moi ?"

_**Safran :**_ "Comme tout le monde qui n'est pas sorcière : à pieds !"

_**Pepper :**_ "À pieds ?! Mais... j'en ai pour deux jours de marche !"

_**Safran :**_ "Tu n'avais qu'à respecter un peu plus tes marraines ! Elles t'auraient prévenue sur la situation, et tu ne serais jamais venue jusqu'ici ! Maintenant, du balai !"

_**Pepper :**_ "J'aimerais bien en avoir un, justement ! Tu es sûre que... même discrètement..."

_**Safran :**_ "DEHORS ! Et je te conseille d'enlever ton chapeau, si tu ne veux pas avoir d'autres surprises !"

_Pepper jette un dernier regard noir en direction de Safran avant de retirer violemment son chapeau._

_**Pepper :**_ "Allez, viens, Carrot ! On va tirer cette histoire stupide au clair !"

_Carrot tente une dernière approche envers Truffel, mais cette dernière grogne et tente de le griffer. Carrot s'enfuit à toutes pattes, suivant sa maîtresse marchant d'un pas décidé._

### Scène 5 - Reconversion

_Forêt de Bout-un-Cureuil, deux jours plus tard.  
À bout de force, Pepper ouvre la porte de sa maison. Elle porte Carrot sur une épaule, lui aussi exténué. Elle porte son chapeau à la main. Un nouveau trou s'est formé au niveau de l'endroit où elle le serrait avec son poing, démontrant une certaine colère de devoir le porter ainsi.  
Pepper ne tient plus et s'effondre dans le living._  

_**Pepper :**_ "Je... je suis rentrée ! Désolée d'avoir mis... aussi longtemps... mais j'ai eu... un... mais... mais qu'est-ce que vous faîtes ?!"

_Pepper se redresse d'un seul coup. Sur la table de la cuisine, plusieurs valises et mallettes sont entreposées, à moitié remplies, à côté d'un journal, où le gros titre stipule "FIN DE LA MAGIE", avec un petit article à côté : "LES AHRACHNERRES RÉCUPÈRENT LEUR INDÉPENDANCE". Elle constate que ses maîtres Thym, Cumin et Cayenne fouillent un peu partout, à la recherche de leurs affaires._  

_**Pepper :**_ "Vous partez quelque part ?"

_**Cayenne :**_ "Tiens ! Regardez qui est de retour !"

_**Cumin :**_ "Et dans quel état ! Où étais-tu passée ? On s'est inquiétées !"

_**Cayenne :**_ "Mouais... moi, je trouvais qu'on avait enfin la paix !"

_**Cumin :**_ "Cayenne, voyons !"

_**Pepper :**_ "J'étais au marché de Komona, et on a voulu me kidnapper ! Après je suis tombée sur Lord Azeirf, et il m'a dit que la magie était interdite, maintenant ! Il m'a pris mon balai, en plus ! C'est pour ça que j'ai mis autant de temps à rentrer... mais c'est pas vrai, n'est-ce pas ? Les écoles ne sont pas dissoutes, hein ? Ce n'est qu'une mauvaise blague ?"

_Ses deux marraines se regardent d'un air désolé. Pepper est terrifiée._

_**Pepper :**_ "Oh non... j'ai craint cette réaction pendant tout le voyage... par pitié, dîtes-moi que c'est faux ! Dîtes-moi que c'est un mauvais tour de Safran ! Dîtes-moi..."

_**Thym :**_ "Il n'y a rien à dire, Pepper."

_Thym apparaît derrière les deux autres marraines, les bras chargés de grimoires._

_**Pepper :**_ "Maître Thym ! Vous qui étiez au Conseil de Ah... vous n'avez pas laissé faire, hein ? Vous n'auriez pas pu les laisser faire passer une loi aussi absurde ?!"

_**Thym :**_ "Malheureusement, nous n'avons rien pu faire. Et je t'en ai déjà trop dit : ceux qui pénètrent l'Ascétribune sont liés par contrat magique, je n'ai pas le droit d'en dire plus ! Quelle ironie... la magie m'empêche de parler de ce qui nous prive de magie !"

_**Pepper :**_ "Mais... Maître Thym, ce n'est pas possible ! Pas vous ! Personne ne peut vous tenir tête ! Même Maître Cayenne n'y arrive pas, et c'est une dure à cuire !"

_**Cayenne :**_ "EH ! Toujours aussi insolente, à ce que je vois ! Un peu de respect pour tes Maîtres !"

_**Thym :**_ "Non, Cayenne. Il n'y a plus de respect à avoir. Nous ne sommes plus ses Maîtres."

_Thym s'approche de Pepper. Elle n'avait jamais vu Thym aussi abattue, même lorsqu'elle a appris la vérité sur Chicory._

_**Thym :**_ "Pepper, écoute-moi bien. La loi a été votée. La magie ne doit plus être pratiquée. C'est comme ça, il faut l'accepter."

_**Pepper :**_ "L'accepter ?! Vous baissez les bras ?! Non, il faut se battre ! Une vraie sorcière de Chaosah ne doit-elle pas jamais renoncer, quelle que soit la tâche qu'elle a entamée ?! Et mon initiation, alors ?"

_**Thym :**_ "Pepper... il n'y a plus de sorcière de Chaosah. Cherche donc d'autres mentors, fais autre chose de ta vie. Chaosah, c'est terminé."

_Paniquée par ces paroles, Pepper regarde tour à tour ses autres marraines._

_**Pepper :**_ "Vous, vous n'avez pas renoncé, Maître Cayenne ! Je le sais ! Vous êtes une battante, une coriace, je peux en témoigner ! Vous n'avez pas pu renoncer..."

_**Cayenne :**_ "Cesse de m'appeler Maître ! Tu n'as pas entendu ? Chaosah n'existe plus, comme les autres écoles ! Alors va trouver autre chose à faire et laisse-nous préparer nos affaires tranquilles !"

_Cayenne lui tourne le dos brusquement. Sa voix tremblait vers la fin.  
Pepper se tourne alors vers Cumin._

_**Pepper :**_ "J'ai toujours adoré vos cours, Maître Cumin ! Et vous adorez enseigner, je le sais ! Alors... vous ne voudriez pas..."

_**Cumin :**_ "Oh, ma pauvre petite Pepper... ce serait avec joie, mais... l'enseignement de la magie, c'est fini. C'est devenu illégal, et... on risquerait gros si on le faisait."

_**Pepper :**_ "On s'en fiche ! Les sorcières de Chaosah n'ont peur de rien ni personne !"

_**Cayenne :**_ "MAIS QUEL SOMMET TA STUPIDITÉ VA ATTEINDRE ?!!"

_Cayenne a fondu sur Pepper et la tient par les épaules. Ses yeux sont terrifiants._

_**Cayenne :**_ "Ce n'est pas un jeu, petite idiote ! Tu sais quel châtiment ils réservent à celles qui transgresseront cette loi ?! Tu tiens donc tant que ça à mettre fin à tes jours ?!!"

_Pepper est choquée par ces paroles. Cayenne s'en rend vite compte, et lâche Pepper avant de lui tourner le dos. Pepper aurait juré voir une petite larme couler sur sa vieille joue._

_**Cayenne :**_ "Ce n'est pas un domaine avec lequel on peut jouer. Il faut juste l'accepter, et aller de l'avant. C'est ce que je compte faire, et tu devrais en faire autant."

_Elle repart très vite dans une autre pièce._

_**Pepper :**_ "Maître Cayenne..."

_**Thym :**_ "Allez, laisse-nous, maintenant. On a encore beaucoup d'affaires à rassembler."

_Thym repart dans la bibliothèque._

_**Pepper :**_ "Mais... c'est d'une stupidité astronomique ! Si les magies ne sont pas pratiquées... le monde court à sa perte !"

_**Cumin :**_ "Ce n'était valable que si une magie n'était plus exercée alors que d'autres oui. Là, comme plus aucune magie ne sera pratiquée... il ne devrait pas y avoir de déséquilibre."

_**Pepper** (les yeux larmoyants) **:**_ "Alors... alors c'est fini ? Pour de vrai ? Vous n'allez pas vous battre ?"

_Cumin pose sa vieille mandoline et prend Pepper dans ses bras._

_**Cumin :**_ "Ma chère enfant... t'enseigner notre savoir fut une expérience exaltante. Tu n'étais pas toujours concentrée, tu as fait des bêtises, tu nous as tenu tête... mais personnellement, jamais je n'ai été aussi comblée. Cayenne et Thym ne l'avoueront jamais, mais elles pensent la même chose. Mais ce n'est pas parce que cette expérience est terminée qu'il n'y en aura pas d'autres ! C'est l'occasion de voir ce qu'il se fait ailleurs, Pepper. Ne pas se limiter à la magie ne peut être que bénéfique pour nous. Et toi, tu es encore très jeune. Tu as toute la vie devant toi, alors fais de nouvelles expériences ! Profite pour rencontrer de nouvelles personnes, voir de nouveaux métiers ! Essaie de voir le bon côté de cette loi injuste. Personnellement, c'est ce que je compte faire. J'ai toujours voulu me lancer dans la musique... ce n'est peut-être pas trop tard, qui sait ? Cayenne veut rentrer dans son pays, et Thym... elle n'a pas encore décidé, mais je pense qu'elle a plus d'une idée dans son sac. Allez, ne pleure plus. C'est une nouvelle vie qui s'offre à toi."

_Pepper repousse violemment Cumin._

_**Pepper :**_ "Alors vous avez abandonné... vous acceptez cette loi ignoble sans rouspéter... vous n'êtes que des lâches..."

_**Cumin :**_ "Pepper..."

_**Pepper :**_ "JE VOUS DÉTESTE !!!"

_Pepper fait volte-face et sort de la maison à toute vitesse. Carrot la regarde faire d'un œil attristé._

_**Cumin** (à Carrot) **:**_ "Va vite la trouver ! Empêche-la de faire une bêtise !"

_Carrot fait un signe de garde-à-vous pour signaler qu'il a compris, avant de poursuivre Pepper. Cumin le regarde s'éloigner d'un air inquiet._

_**Cumin :**_ "Pepper... je t'en supplie, sois prudente !"

_Pepper se contente de courir sans regarder devant elle. Aveuglée par la colère et la déception, elle s'éloigne le plus possible de sa maison, le plus possible de ses mentors qui l'ont déçue.  
Elle arrive aux abords d'une clairière. Les nuages sombres se lèvent. Les arbres qui l'entourent ressemblent étrangement à ses marraines, et ces gens du Conseil de Ah qui l'ont privée de son avenir._

_**Pepper :**_ "Plus de magie, hein ? PLUS DE MAGIE ?! VOILÀ CE QUE J'EN PENSE DE VOS LOIS ABSURDES !"

_Pepper lance plusieurs sorts explosifs d'affilé sur les arbres qui l'entourent, en poussant des hurlements, tandis que la pluie commence à tomber. Elle n'entend plus rien d'autre que sa colère. Même le grondement du tonnerre ne l'affecte pas, on dirait même qu'elle les provoque elle-même. Les sorts sont puissants, les bouts de bois arrachés volent partout..._

_**Pepper :**_ "UNE NOUVELLE VIE ?! ABANDONNER ?! JAMAIS, VOUS M'ENTENDEZ ! JAMAIS !"

_Carrot, abrité au loin, attend que l'orage passe, tremblant de tout son corps. Sous la pluie battante, voir sa maîtresse déverser autant de colère est bien plus terrifiant qu'il ne peut l'imaginer. Un véritable cauchemar.  
Bientôt, c'est comme si une tornade était passée dans la forêt : les arbres sont pliés, détruits, pulvérisés. Seule la fatigue oblige Pepper à s'écrouler dans l'herbe. Elle a à peine la force de frapper du poing au sol et de hurler :_

_**Pepper :**_ "C'EST PAS JUUUUUSTE !!!"

_Son cri résonne à travers la forêt. Quelques oiseaux s'envolent au loin, effrayés par autant de bruits. Carrot se précipite près d'elle alors que la pluie commence à tomber. Il tente de la relever, mais Pepper s'est tellement confondue en larmes qu'elle s'en est évanouie, ne sentant plus l'eau lui tomber dessus, au comble du désespoir.  
Plusieurs coups humides contre sa joue la font se réveiller. Elle se trouve à l'intérieur d'une souche d'arbre. Elle constate que Carrot, trempé jusqu'aux os, la léchouille pour la réveiller. La pluie a cessé, les rayons du soleil tentent de percer le feuillage dense des arbres restants._  

_**Pepper :**_ "Carrot... c'est toi qui m'as abritée ? Oh, je suis tellement désolée ! Je n'aurais pas dû partir comme je l'ai fait, c'était stupide..."

_Elle constate que Carrot l'observe en tremblant légèrement. Elle se rappelle alors ce qu'elle a fait, et regarde le reste des arbres qu'elle a détruit plus tôt._

_**Pepper :**_ "J'ai dû te faire peur... je n'aurais pas dû me mettre autant en colère. Je te demande pardon, Carrot. Je ne voulais pas que tu me vois comme ça. C'est fini, tu n'as plus rien à craindre. Viens par là, je vais te sécher."

_Carrot, rassuré, se dirige vers Pepper. Elle enlève son gilet déjà trempe pour tenter d'essuyer Carrot, avec une tristesse profonde._

_**Pepper :**_ "Carrot... qu'est-ce que je vais faire ? Qu'est-ce que je vais devenir ? Si la magie n'existe plus... c'était ma raison de vivre, ma raison d'être. Et je l'ai perdue."

_Carrot jaillit des bras Pepper et lui lance un regard horrifié._

_**Pepper :**_ "Quoi ? Ne me regarde pas comme ça, c'est la pure vérité ! Je n'ai plus aucune raison d'exister si on m'enlève la sorcellerie... et comme tout le monde semble avoir renoncé... je ne peux pas lutter toute seule. Alors à quoi bon..."

_Le collier de Pepper se met soudainement à briller. Pepper le retire en hâte pour l'observer. La moitié de cœur avec un "F" n'a pas bougé. Le fil, en revanche, diffuse une lueur apaisante. Réconfortante._

_**Pepper :**_ "Fairy... ce n'est vraiment pas le moment ! La situation n'a rien de drôle."

_La lueur disparaît. Pepper s'assoit._

_**Pepper :**_ "Je vais rester ici, et attendre. Va t'en, Carrot. Reprends ta vie de chat, sans souci, sans attache. C'est encore ce qu'il y a de mieux."

_Carrot bondit sur sa maîtresse et lui mord le bras._

_**Pepper :**_ "EH ! T'es fou ! Mais qu'est-ce qu'il te prend ?!"

_Carrot lui lance un regard rempli de colère et miaule avec énergie. Pepper est troublée._

_**Pepper :**_ "Mon ami... tu ne voudras jamais m'abandonner, n'est-ce pas ? Pauvre idiot... tu devrais trouver quelqu'un d'autre, qui a encore un rêve qui tient la route, qui n'a pas été pulvérisé par des gens qui n'y connaissent rien, qui se fichent du monde, qui..."

_Pepper se fige. Carrot l'observe. On dirait qu'elle vient de réaliser quelque chose d'important._

_**Pepper :**_ "C'est le Maître de Ah qui décide, non ? Et comme Wasabi n'est plus... c'est Shichimi qui a approuvé cette infamie ?! Non... pas elle ! C'est impossible, elle n'aurait pas pu nous faire ça ! Carrot ! Elle n'aurait jamais pu faire une chose pareille ! C'est notre am..."

_Pepper réalise autre chose, avec une tristesse bien plus profonde. Elle se lève, et sort de la souche. La clairière ressemble à un cimetière pour arbres. Ils sont coupés en deux, broyés, et seuls des troncs subsistent encore. Mais curieusement, elle est belle maintenant que la pluie a cessé._

_**Pepper :**_ "Elle tenait à son Maître plus que tout... et on l'a privée de tout espoir de la revoir... en rassemblant ces pièces d'amulette, en réveillant Maître Chicory, j'ai contribué à lui enlever celle qu'elle aimait le plus... elle doit me haïr plus que quiconque sur cette planète... Carrot, tu crois qu'elle aurait approuvé cette loi... pour me punir ? C'est ma punition pour l'avoir fait souffrir ? Elle n'aurait pas hésité à briser des centaines de vies, juste parce qu'elle savait que la sorcellerie était toute ma vie ?"

_Pepper se laisse tomber sur un amas d'herbes. Carrot bondit sur ses genoux, l'air toujours inquiet._

_**Pepper :**_ "Je ne peux pas y croire. Même si elle est très affectée par la perte de Wasabi... elle n'aurait pas pu aller aussi loin. Quelqu'un l'a sûrement forcée à répondre "oui". Je trouverai qui c'est, et je convaincrai Shichimi d'abolir cette loi ridicule ! Carrot, tout espoir n'est pas perdu ! Peu importe le temps que ça prendra, mais j'y arriverai ! Je la convaincrai ! Rassure-toi, je ne renoncerai pas !"

_La clarté diminue d'un seul coup. Pepper lève la tête : pourtant, aucun autre nuage en vue._

_**Pepper :**_ "Carrot ? Qu'est-ce qu'il se passe ? Je sens planer une menace autour de nous."

_Le chat reste aux aguets. Mais rien ne se manifeste à part le vent._

_**Pepper :**_ "Bon, il n'y a qu'un moyen de savoir ce qu'il se passe : la magie ! Voir au-delà..."

_Pepper se concentre. Le monde semble devenir un amas de particules qui s'agitent dans tous les sens. Mais des particules d'origine inconnue les encerclent, et sont bien plus actives que les autres._

_**Pepper :**_ "Mais que se passe-t-il, ici ?"

_Derrière le feuillage d'un arbre, sur une branche, elle le voit !_

_**Pepper :**_ "C'est quoi, ça, là-haut ?! CARROT ! ATTENTION !"

_Carrot saute au bon moment. Il évite un assaut de la menace. Pepper quitte sa vision de particules, et fait face à son agresseur._

_**Pepper :**_ "Mais... qu'est-ce que c'est que ça ?!"


## Acte 2 : Le nouveau repaire

### Scène 1 : L'être chaotique

_Pepper et Carrot font face à une créature des plus singulières. Celle-ci est entièrement noire, de forme à peine humaine, très petite, et..._

_**Pepper :**_ "On dirait que... son corps n'est composé que de particules en mouvements ! Tu as déjà vu un monstre pareil, Carrot ?"

_L'air bouche bée de Carrot en dit long sur sa réponse. La créature se contente de les observer, en lévitant au-dessus d'eux._

_**Pepper :**_ "Je n'arrive même pas à déterminer s'il a des yeux ou pas... peut-être qu'il peut parler... euh, hum... bonjour ? Je suis Pepper, une sorcière de Chaosah ! Et voici mon compagnon Carrot ! Que... qui es-tu, toi ? Et qu'est-ce que tu nous veux ?"

_La créature ne répond pas. Elle semble diffuser une sorte de râle, de respiration peu commode en guise de réponse, mais saccadée. Carrot hausse les épaules._

_**Pepper :**_ "Bon, visiblement, il ne comprend pas ce qu'on dit. Eh bien, si tu ne nous veux rien, au revoir ! Nous avons de la route à faire."

_Pepper se retourne. Elle sursaute aussitôt, car la créature est encore devant elle ! Carrot commence à grogner. Pepper fait volte-face, et la créature est de nouveau en face d'elle._

_**Pepper :**_ "Mais !"

_Pepper fait plusieurs volte-faces, mais à chaque fois, la créature est devant son visage, toujours aussi inexpressif, toujours aussi désordonnée. Carrot est désorienté._

_**Pepper :**_ "Mais... mais qu'est-ce que tu nous veux, enfin ?! Agis ou va t'en ! Nous sommes pressés, figure-toi ! On n'a pas le temps de jouer !"

_Un craquement sourd jaillit de la créature, et aussitôt, il fond sur Pepper dans un couinement suraigu.  
Pepper sent une force lui comprimer la poitrine. Elle essaie de s'en débarrasser, mais rien n'y fait. En ouvrant les yeux, elle ne se trouve plus dans la forêt, mais dans un espace où tout semble s'accélérer. Par flash, elle voit des dizaines d'images se suivre sans vraie cohérence. Elle voit des gens en train de lancer des pierres vers un groupe de personnes, un énorme monstre détruire une ville toute entière, quelques femmes portant des chapeaux et des balais, en train de lancer divers sorts pour abattre le monstre, une foule qui acclame ces sorcières, six cœurs qui jaillissent de ces sorcières pour ne former qu'un, et rayonner toujours plus fort au-dessus du monde...  
Enfin, elle voit le petit être apparaître devant elle. Toujours aussi chaotique et mystérieux. Il éclaire un chemin caché par les ténèbres. Pepper regarde cette route à suivre avec un large sourire rempli d'espoir._  

_**Pepper :**_ "Bien sûr ! C'est évident !"

_**Voix lointaine :**_ "Pepper !"

_Tout disparaît d'un coup, et Pepper sent qu'on lui arrache quelque chose de la poitrine, comme si on lui prenait son cœur de force. Elle se réveille en sursaut. Elle constate qu'elle est à nouveau allongée dans l'herbe. Carrot lui bondit dans les bras, et elle peut constater que Safran se trouve à côté d'elle, en compagnie de Truffel._

_**Pepper :**_ "Safran ? Mais... mais qu'est-ce que tu fais là ?"

_**Safran :**_ "Je te sauve la vie, triple andouille ! Je t'avais demandé de rentrer tranquillement chez toi et de réfléchir à ta reconversion, et je te retrouve dans la forêt en train de te faire attaquer par je ne sais quoi !"

_**Pepper :**_ "Ce n'était pas une attaque ! Mais... attends un peu ! Comment es-tu arrivée jusqu'ici, d'abord ? Je n'ai dit à personne où j'allais... et je ne t'ai pas demandé me suivre !"

_**Safran :**_ "Je sais, et je m'en serais bien gardé ! Mais une de tes marraines m'a demandé si j'avais des nouvelles de toi, et comme justement j'allais à la cueillette... j'en ai profité pour jeter un coup d'œil..."

_**Pepper :**_ "Tu es venue me trouver parce qu'une de mes marraines me cherchait ? Oh, Safran ! Tu es vraiment gentille !"

_Pepper se jette sur Safran pour la serrer contre elle. Carrot frotte son museau sur celui de Truffel, qui détourne les yeux... mais s'en trouve troublée._

_**Safran :**_ "Eh, doucement, idiote ! Tu vas me décoiffer !"

_Pepper relâche son étreinte. Safran en profite pour remettre ses vêtements en place._

_**Safran :**_ "Toujours aussi sauvage, hein ? Bon, tu peux me dire ce qui t'a conduit jusqu'ici ? Ta marraine m'a indiqué que tu étais toujours en colère lorsque tu as quitté ta maison."

_**Pepper :**_ "J'ai eu du mal à accepter la vérité, j'avais besoin de me défouler pour me changer les idées."

_**Safran** (observant les restes d'arbres) **:**_ "Je vois que c'est une réussite... mais dis-moi, tu comptes abattre tous les arbres de la forêt, ou bien tu as vraiment les idées en place, maintenant ? Parce que c'est mon gagne-pain que tu détruis, là !"

_**Pepper :**_ "Non, non, c'est fini ! Je viens d'avoir une révélation intéressante, à l'instant !"

_**Safran :**_ "Pendant que tu gigotais par terre comme un ver ? Tu as une drôle de façon de cogiter ! Et peut-on savoir ce qui... HIIIIII !!"

_Safran recule d'un pas en regardant au-dessus de Pepper d'un air terrifié. Pepper se retourne et remarque que la petite créature au corps chaotique est toujours là, avec sa respiration inquiétante._

_**Safran :**_ "Qu'est-ce que c'est que cette horreur ?!"

_**Pepper :**_ "Lui ? Aucune idée. Mais l'idée dont je te parlais, c'est lui qui me l'a donnée !"

_**Safran :**_ "Tu as des amis plutôt étranges... beurk ! Et que t'a-t-il suggéré ?"

_**Pepper :**_ "Au départ, je voulais aller trouver Shichimi pour lui faire changer d'avis sur la nouvelle loi."

_**Safran :**_ "Shichimi ? Ah, oui, c'est elle, le Maître de Ah, pour le moment... et donc, c'est elle qui a..."

_**Pepper :**_ "Oui. Mais je vois mal Shichimi approuver une telle abomination. Je suis convaincue qu'elle y a été forcée. Je m'étais mise en quête pour la trouver, mais... c'est inutile. Celui qui lui a forcé la main ne nous laissera pas faire. Donc, il faut qu'on arrive à changer sa mentalité sur les sorcières, et la loi sera abolie !"

_Safran la regarde avec des yeux ronds._

_**Safran :**_ "C'est pas vrai..."

_**Pepper :**_ "Ingénieux, hein ? Et pour ce faire, nous..."

_**Safran :**_ "Je ne savais pas que tu étais candide à ce point-là... on est sur un niveau de Maître !"

_**Pepper :**_ "Candide ? Pourquoi ?"

_**Safran :**_ "Tu vis réellement dans un monde enchanté, toi ! Tu crois qu'on peut changer les mentalités en claquant des doigts ? Nous sommes des sorcières, pas des faiseurs de miracle ! On ne changera jamais les mentalités de ceux qui détestent les sorcières !"

_**Pepper :**_ "C'est parce que tu n'y croies pas assez fort ! Moi, je suis convaincue qu'on peut faire bouger les choses, en aidant les gens dans le besoin, ou en accomplissant un travail d'intérêt public, à l'aide de la magie ! Les gens seront forcés de constater que nous pouvons être utiles, et au Conseil de Ah, ils supprimeront la loi ! C'est du pur génie !"

_**Safran :**_ "Mouais... et tu dis que c'est cette créature bizarre qui t'a suggéré cette idée ?"

_**Pepper :**_ "Oui ! Je crois que je vais l'appeler Messy. Il ne tient pas en place mais il m'a montré le chemin vers un avenir radieux."

_Safran se tourne vers Messy._

_**Safran :**_ "Messy. Tu es un abruti intégral."

_Messy répond par un râle._

_**Safran :**_ "Pepper. Pourquoi tu refuses d'accepter cette loi et de te reconvertir ?"

_**Pepper :**_ "Parce que si je le fais, ça voudrait dire que je suis d'accord à ce que le Conseil de Ah décide n'importe quoi ! Et je ne suis pas d'accord ! Et je veux que tout le monde sache que je ne suis pas d'accord ! Et je vais le prouver !"

_**Safran :**_ "Tu tiens vraiment à finir tes jours en prison ? Sombre idiote, va ! Va de l'avant, et fais comme moi ! Une vie sans magie n'est pas si désagréable ! En tout cas, ne compte pas sur moi pour t'épauler !"

_**Pepper :**_ "Oh, Safran ! Je t'en prie ! Je n'y arriverai pas seule ! J'ai besoin de ton aide !"

_**Safran :**_ "Hors de question ! Je ne veux pas être complice d'un acte illégal ! Non merci ! J'ai une boutique qui tourne, maintenant ! Je n'ai pas le loisir de jouer les hors-la-loi avec toi ! Non, non et non !"

_Carrot tend la patte à Truffel, mais cette dernière la rejette._

_**Pepper :**_ "Allez ! En plus, je sais que tu veux pratiquer la magie de nouveau ! Je sais que tu veux de nouveau danser sur scène ! Je sais que tu ne veux pas passer ta vie à servir du thé et... eh ! Attends un petit peu, toi !"

_Pepper se place devant Safran, les mains sur les hanches._

_**Safran :**_ "Eh bien, quoi ?"

_**Pepper :**_ "Comment ça, "quoi", madame l'apothicaire ! Tu m'expliques comment tu peux préparer et vendre tes potions sans magie ?"

_Pepper semble avoir touché un point crucial. Safran, les joues rosies, croise les bras et détourne les yeux._

_**Safran :**_ "Tu sais, le métier d'apothicaire ne consiste pas uniquement à vendre des potions magiques ! On peut produire toutes sortes de médicaments et de cosmétiques avec de simples plantes."

_**Pepper :**_ "De simples plantes, hein ? Arrête ! Je suis sûre que tu utilises des ingrédients magiques !"

_Safran reste silencieuse, le regard toujours détourné._

_**Pepper :**_ "Je le savais !"

_**Safran :**_ "Eh, tu crois que c'est facile, d'avoir autant de demandes de Filtre de Rajeunissement, et de ne pas pouvoir répondre à la demande à cause d'une loi ? Alors oui, j'utilise un peu de magie... mais j'en utilise moins qu'avant !"

_**Pepper :**_ "Moins qu'avant, hein ? Que ce soit vrai ou pas, tu es dans l'illégalité ! Alors évite de me faire des leçons de morale et rejoins ma cause ! Tu as besoin de la magie autant que moi !"

_**Safran :**_ "Maaaiiss je ne suis pas dans l'illégalité depuis si longtemps que ça, et... oh, et puis zut ! Tu m'énerves ! Bon, je veux bien t'accompagner au début, mais attention, hein ! C'est uniquement parce que je dois aller chercher des ingrédients un peu partout en secret, et que nous suivons le même chemin ! Je ne cautionne pas ton combat, d'accord ? Et j'ai une boutique à faire tourner, alors ne compte pas trop sur moi !"

_**Pepper :**_ "Safran, tu es géniale !"

_**Safran :**_ "Non, pas l'étreinte !"

_Trop tard. Carrot retend la patte. Truffel, le regard détourné, l'air résigné, tope dans la patte de Carrot, qui est ravi.  
Pepper relâche Safran, qui remet ses vêtements en place à nouveau._

_**Safran :**_ "J'espère que ça ne deviendra pas une habitude, sinon je te ferais payer la note de teinturier !"

_**Pepper :**_ "Qu'est-ce que que je suis contente de ne plus être seule ! Ce que tu fais signifie beaucoup pour moi, Safran !"

_**Safran** (rougissant légèrement) **:**_ "Bon, n'en fais pas trop non plus ! Je t'ai dit que je ne ferai que t'accompagner, je ne t'aiderai pas ! Je ne tiens pas à finir enfermée dans un cachot dégoûtant ! Ce que j'aimerais savoir à présent, c'est ce que tu comptes faire concrètement pour changer les mentalités."

_**Pepper :**_ "Wasabi avait inventé le métier d'Enchanteresse Errante pour m'entourlouper. Il s'agissait d'une aide magique itinérante à travers le monde. C'est ce que nous allons faire !"

_**Safran :**_ "Tu veux voyager à travers le monde ? Ah non, ça ne va pas être possible, ma grande ! Je ne peux clairement pas voyager aussi longtemps et aussi loin ! Et ma boutique alors ?"

_**Pepper :**_ "Hum... tu as bien de l'argent, non ?"

_**Safran :**_ "Oui, et pas qu'un peu ! Mais hors de question que je te débourse le moindre Ko !"

_**Pepper :**_ "Non, non, pas pour moi... mais tu peux te payer quelqu'un pour faire tourner ton commerce en ton absence, non ? Tu as bien le droit de prendre un petit congé ?"

_Safran réfléchit quelques instants._

_**Safran :**_ "Si nous ne partons pas longtemps, je pense pouvoir me faire remplacer durant une courte période. Certains ingrédients sur ma liste nécessitent d'aller plutôt loin, et... oui, un congé me semble une bonne idée."

_**Pepper :**_ "Super ! Je te laisse t'occuper des détails. Prétends que tu pars te reposer, que tu es malade, et retrouve-moi à l'entrée du Courant Express le plus proche de Komona. Nous allons montrer au monde qu'ils auront toujours besoin de sorcières. Prêt, Carrot ? Messy ?"

_Messy se contente d'un râle un peu craquant. Carrot semble emballé. Safran, un peu moins._

### Scène 2 - L'aide itinérante

_Pepper, Carrot, Messy, Safran et Truffel sont donc partis sillonner les routes afin de venir en aide à ceux qui en ont besoin, dans le but de redorer la côte des sorcières. Safran a préparé quelques provisions dans un sac. Mais la première étape pose déjà problème :_

_**Pepper :**_ "C'est pas possible !"

_**Safran :**_ "Je peux savoir à quoi tu t'attendais ?"

_L'entrée du Courant Express devant laquelle elles se trouvent est condamnée. Un immense panneau informe de son démantèlement prochain._

_**Safran :**_ "Plus de magie, Pepper ! C'est valable partout et pour tout le monde !"

_**Pepper :**_ "Pas de balai, pas de Courant Express... est-on condamnées à voyager à pieds toute notre vie ?!"

_**Safran :**_ "Mais comment crois-tu que les autres font ?"

_**Pepper :**_ "Ils utilisent le Courant Express, tout simplement ! Ils ne se rendent pas compte des conséquences de leur loi débile !"

_**Safran :**_ "En attendant, nous allons devoir utiliser nos jambes. Je sens que ce grand voyage dont tu parlais sera de courte durée..."

_**Pepper :**_ "Ne sois pas bête ! Ce petit contretemps n'a rien entamé de ma détermination ! En avant, les amis !"

_Pepper se remet en marche. Safran pousse un long soupir._

_**Safran :**_ "Je sens que notre expédition va être épique..."

_Truffel confirme avec un air aussi las que sa maîtresse.  
Les deux sorcières marchent pendant plusieurs heures. Le soleil tape de plus en plus fort, et le petit groupe arrive bientôt à bout de force._

_**Pepper :**_ "Quelle chaleur ! Qu'est-ce que je donnerais pour pouvoir mettre un chapeau..."

_Elle constate que Safran porte un large chapeau blanc, une ombrelle et se vaporise de l'eau avec un petit tube. Truffel se repose tranquillement sous l'ombrelle, sur l'épaule de Safran._

_**Pepper :**_ "EH ! Pourquoi tu as un chapeau, toi ?! C'est toi qui m'as conseillé de ne pas en porter !"

_**Safran :**_ "Je t'ai conseillé de quitter ton chapeau de sorcière uniquement ! Mais les autres chapeaux ne sont pas interdits, que je sache !"

_**Pepper :**_ "Tu aurais pu le préciser ! Et puis c'est bien de l'eau que tu te vaporises ?!"

_**Safran :**_ "Bien sûr, ce n'est pas du purin !"

_**Pepper :**_ "Tu aurais pu m'avertir, non ? Et m'en prêter un peu !"

_**Safran :**_ "C'est hors de question ! Tu es partie le nez au vent en pensant tomber miraculeusement sur des gens à aider ! Alors tu assumes, maintenant !"

_**Pepper :**_ "Prête-moi ton ombrelle alors !"

_**Safran :**_ "J'ai dit : non !"

_**Pepper :**_ "Mais tu as déjà un chapeau ! L'ombrelle ne te sert pas !"

_**Safran :**_ "Ne me sers pas ?! Et ma pauvre Truffel alors ? Tu veux que son pelage éclatant et fragile soit exposé aux rayons du soleil ?! Tu veux la tuer ?!"

_**Pepper :**_ "Mais Carrot se farcit déjà le soleil en pleine tête, et il ne s'en plaint pas ! Pas vrai, Carrot ?"

_La langue pendante, presque à terre, Carrot se contente de lever mollement une patte pour confirmer._

_**Pepper :**_ "Donne-moi ton ombrelle !"

_**Safran :**_ "Non !"

_**Pepper :**_ "Alors au moins ton chapeau !"

_**Safran :**_ "Et puis quoi, encore ? Tu veux que j'attrape des puces ?!"

_**Pepper :**_ "Alors là !"

_Les deux sorcières commencent à se chamailler. Carrot est trop fatigué pour participer. Truffel se pose tranquillement à l'ombre d'un cactus, en attendant que la dispute cesse. Messy, lui, se contente d'observer, sans autre réaction.    
Dans la bagarre, Pepper parvient à prendre le chapeau de Safran._

_**Pepper :**_ "Ha-ha ! Un point pour Pepper ! Mais..."

_**Safran :**_ "Rends-le moi immédiatement !"

_Pepper sent une douce brise fraîche envahir son corps au contact du chapeau._

_**Pepper :**_ "Mais il est magique, ce chapeau ! Il diffuse du vent frais !"

_**Safran :**_ "Oui, et il commence à faire chaud sous cette ombrelle, alors rends-le moi !"

_**Pepper :**_ "Espèce de tricheuse ! Tu me demandes de renoncer à la magie, mais tu es celle qui en utilise le plus !"

_**Safran :**_ "C'est bien le but de ce voyage, non ? Montrer que les sorcières ont leur place dans ce monde ! Alors à quoi bon limiter nos pouvoirs ?"

_**Pepper :**_ "Espèce de... !"

_Elles se disputent de plus belle. Mais la chaleur les arrête bien vite. Pepper s'effondre au sol et rampe vers le cactus, en compagnie de Safran, essoufflées._  

_**Pepper :**_ "C'est inutile... on n'a croisé personne de tout le voyage !"

_**Safran :**_ "Les gens ne sont pas stupides, Pepper. Par des chaleurs comme celle-ci, ils restent à l'abri chez eux ! Il faudrait être incroyablement sot pour prendre la route à cette heure-ci !"

_**Voix au loin :**_ "S'il vous plaît !"

_Pepper et Safran tournent la tête en direction des cris. Un jeune homme coiffé d'un galurin en paille leur fait des signes de bras. Les deux sorcières se dirigent vers lui._

_**Pepper :**_ "Oui ? Qu'est-ce qui vous arrive ?"

_**Jeune homme :**_ "Bonjour ! Je m'appelle Cletonal. Je suis désolé de vous importuner dans votre balade, mais... durant notre randonnée, ma compagne est tombée dans un trou, et je n'arrive pas à l'en sortir ! Pourriez-vous m'aider ?"

_**Pepper** (surexcitée) **:**_ "Je m'appelle Pepper ! Enchantée ! J'arrive tout de suite !"

_**Safran :**_ "Eh ! Tu n'es pas toute seule, je te rappelle !"

_Elles suivent le jeune homme un peu plus loin sur le chemin. Elles arrivent au bord d'une falaise, parsemée de crevasses._

_**Cletonal :**_ "Attention où vous mettez les pieds !"

_**Pepper :**_ "Ne vous inquiétez pas, on sait où on maaAAAHH !"

_Pepper se redresse juste à temps. Elle a failli glisser dans une crevasse._

_**Pepper :**_ "Ouf ! Ce n'est pas passé loin !"

_**Safran :**_ "Fais un peu attention, espèce de maladroite ! Tu ne pourras aider personne si tu te mets toi-même en danger !"

_**Pepper** (rougissant) **:**_ "Oui, c'est bon !"

_**Cletonal :**_ "La voilà ! Sha-One !"

_Une voix féminine s'élève à l'intérieur de la crevasse._

_**Sha-One :**_ "C'est toi, chéri ?"

_**Cletonal :**_ "J'ai trouvé de l'aide ! Tiens bon, on va venir te chercher !"

_**Sha-One :**_ "Oh ! Bonjour ! Je vous suis reconnaissante de m'aider. Je suis navrée de vous importuner à cause de ma maladresse !"

_**Pepper :**_ "Ne vous inquiétez pas ! Question maladresse, je m'y connais ! Nous allons vous sortir de là ! Attention, écartez-vous !"

_Cletonal et Safran s'écartent de Pepper, qui s'est mise à genoux._

_**Cletonal :**_ "Euh... elle compte y parvenir toute seule ? Elle dispose d'une corde ? La crevasse est très haute, moi-même je n'ai rien pu faire !"

_**Safran :**_ "Oh, la connaissant, vous risquez d'être surpris..."

_Cletonal reporte son regard sur Pepper, plutôt inquiet._

_**Pepper :**_ "Surtout, n'ayez pas peur, Madame ! Et laissez-vous guider !"

_**Sha-One :**_ "Je vous fais confiance, Mademoiselle."

_**Pepper** (à elle-même) **:**_ "Bon, ce n'est pas le moment de se tromper ! Un sort d'Anti-Gravitation devrait faire l'affaire..."

_Pepper fait claquer ses mains en fermant les yeux. Elle les pose ensuite au sol. Autour d'elle, tous les petits gravas et cailloux commencent à léviter, sous le regard horrifié de Cletonal, et blasé de Safran. Après quelques instants d'effort intense, le corps d'une femme commence à émerger de la crevasse, et flotte quelques centimètres au-dessus du sol. N'en pouvant plus, Pepper finit par lâcher le sort. Les pierres regagnent le sol. La femme retombe sur ses pieds avec une adresse remarquable._

_**Sha-One :**_ "Remarquable ! Vous avez réussi à me sauver ! Merci, jeune demoiselle ! Tout va bien ?"

_**Pepper** (essoufflée) **:**_ "Oui, oui, rassurez-vous ! J'ai juste besoin... de reprendre mon souffle !"

_**Cletonal :**_ "Mais... MAIS QU'EST-CE QUE C'ÉTAIT QUE ÇA ?!"

_**Safran** (vaporisant de l'eau sur son visage) **:**_ "Je vous avais prévenu que vous seriez surpris..."

_Le jeune homme accourt vers Sha-One en panique._

_**Cletonal :**_ "Tu vas bien ? Rien de cassé ?"

_**Sha-One :**_ "Je ne pense pas... tu as vu, cette jeune fille m'a sauvée d'une manière remarquable !"

_**Cletonal :**_ "Justement ! Je me demande bien ce que c'était !"

_**Sha-One :**_ "Aïe !"

_En voulant faire un pas, la jeune femme s'est affaissée, se tenant le pied._

_**Cletonal :**_ "Chérie ! Tu t'es blessée ?!"

_**Sha-One :**_ "Rien de grave, rassure-toi. Je me suis juste tordue la cheville..."

_**Cletonal :**_ "C'est à cause de cette fille, n'est-ce pas ?"

_Il pointe son doigt accusateur vers Pepper. Cette dernière lui rend un regard noir._

_**Pepper :**_ "Je n'ai rien fait de mal ! Je me suis contentée de sauver votre compagne !"

_**Cletonal :**_ "Contentée, oui ! Qu'est-ce que c'était que cette technique, hein ? Une pratique illégale, je suppose, si elle peut blesser ceux qui la subissent !"

_**Sha-One :**_ "Mais non, idiot ! Je me suis probablement foulée la cheville en tombant dans ce trou ! Tu devrais te montrer reconnaissant. Sans elle, je serais encore coincée !"

_**Cletonal :**_ "Mais, Sha-One..."

_**Safran :**_ "Ne bougez plus."

_Safran s'approche de Sha-One en fouillant dans son sac. Elle sort une petite fiole dont elle extrait une sorte de pâte qu'elle étale sur la cheville de Sha-One. Elle enroule ensuite la cheville dans un bandage au motif brodé._

_**Safran :**_ "C'est un anti-douleur. Il devrait vous permettre de poursuivre votre marche jusqu'à un guérisseur."

_**Sha-One :**_ "Oh, merci, jeune fille ! C'est incroyable, je n'ai déjà plus mal !"

_**Cletonal :**_ "Déjà ? Mais..."

_**Safran** (relevant ses cheveux) **:**_ "C'est bien normal, je suis une apothicaire de renom, après tout !"

_**Cletonal :**_ "Attendez, vous... d'abord cette technique étrange, puis cette pâte... apothicaire... dîtes-moi, vous n'êtes pas..."

_Il regarde Pepper et Safran d'un air livide, en tremblant._

_**Cletonal :**_ "...des sorcières ?"

_**Pepper :**_ "Bien sûr que si ! Des sorcières ! Et c'est grâce à nous que votre compagne est sauvée et guérie !"

_Cletonal se précipite vers Sha-One._

_**Cletonal :**_ "Viens, chérie ! Partons d'ici immédiatement ! Avant qu'elles ne nous changent en animal, ou qu'elles nous maudissent sur plusieurs générations !"

_**Sha-One :**_ "Mais, enfin... tu vois bien qu'elles ne sont pas méchantes ! Et elles ont droit à notre gratitude !"

_**Cletonal :**_ "C'est illégal, à présent ! Elles n'ont plus le droit de pratiquer la magie ! Il y a bien une raison à cela, non ?! Vite, rentrons au royaume prévenir le roi ! L'armée pourra nous protéger ! Vite !"

_**Pepper :**_ "Sombre crétin ! Pourtant, ça se voit que nous sommes pacifiques ! Nous voyageons dans le but de changer les mentalités pourries comme la vôtre à cause de cette nouvelle loi débile, en essayant d'aider un maximum de monde ! On vient d'aider votre compagne, vous devriez être reconnaissant !"

_**Cletonal :**_ "Reconnaissant ? Envers des hors la loi ?! Jamais de la vie !"

_**Sha-One :**_ "Cletonal..."

_**Cletonal :**_ "Nous partons ! Je vous conseille de rentrer vite chez vous, avant que je n'envoie l'armée à vos trousses !"

_**Pepper :**_ "Abruti !"

_**Safran :**_ "NON !"

_Safran retient Pepper de force, avant qu'elle n'exécute un autre sortilège. Cletonal recule d'un pas._

_**Cletonal :**_ "Je le savais ! Vous êtes des sauvages !"

_**Pepper :**_ "Lâche-moi ! Laisse-moi lui montrer que..."

_**Safran :**_ "Tu ne lui montreras rien du tout ! Calme-toi, je t'en conjure !"

_**Cletonal :**_ "Tu devrais écouter ton amie, sorcière ! J'ai des relations haut-placées, alors je vous garantis que vous ne me faîtes pas peur !"

_Aussitôt, Messy apparaît juste devant lui, et exécute un râle craquant et strident. Le jeune homme prend peur et commence à détaler comme un lapin._

_**Cletonal :**_ "AAHHH ! Ce sont des démons !!"

_**Safran** (à Pepper) **:**_ "Il faut que tu gardes ton sang-froid à tout prix ! Sinon, tu vas obtenir le contraire de ce que tu cherches à faire en entreprenant ce voyage !"

_**Pepper** (les yeux remplis de larmes de rage) **:**_ "Mais... mais... c'est vraiment pas juste !"

_**Sha-One :**_ "Jeune sorcière..."

_Elle prend Pepper par les épaules et se penche légèrement vers elle. Pepper peut remarquer alors à quel point la jeune femme est éblouissante : sa peau est extrêmement blanche, elle a les cheveux noirs tirés en arrière et soutenus par une queue de cheval grâce à des barrettes en bois, et son sourire est des plus rassurants. Elle ne saurait dire pourquoi, mais Pepper se sent troublée en sa présence._

_**Sha-One :**_ "Je te remercie du fond du cœur de m'avoir porté secours. Votre quête sera longue et difficile, mais je pense que vous y parviendrez. Ne croyez pas que le monde entier soit contre vous. Et c'est en montrant les merveilles que recèlent votre cœur que vous finirez par prouver à tous que la magie a toujours sa place ici."

_Pepper ne sait pas quoi répondre. Elle se contente d'essuyer ses yeux et de sourire._

_**Pepper :**_ "Merci de nous soutenir, Madame. Pardon de m'être emportée..."

_**Sha-One :**_ "Tu risques de tomber sur des réactions identiques au cours de ton voyage. Essaie de dominer tes émotions. Si tu te montres agressive, les gens ne changeront jamais d'avis sur les sorcières. Et personnellement, je tiens à ce que vous réussissiez. Cette loi doit être abolie au plus vite, avant que les tensions ne s'intensifient et que les fossés ne se creusent..."

_**Safran :**_ "Les gens sont donc tant effrayés par les sorcières ?"

_**Sha-One :**_ "J'ai pas mal voyagé, et les avis étaient mitigés. Mais cette loi a beaucoup plus d'impacts qu'on ne l'imagine. Ceux qui doutaient commencent à avoir peur. Ce que vous essayez de faire est remarquable et très courageux. Trouvez la bonne façon de vous y prendre, redonnez espoir aux gens. En tout cas, je suis une de vos plus grandes fans ! Je ferai tout pour vous aider. Ce sera ma modeste contribution pour vous remercier de m'avoir secourue."

_**Pepper** (en larmes, se jetant sur Sha-One) **:**_ "Si vous saviez à quel point ce que vous dîtes me redonne du courage ! Merci, Madame !"

_Sha-One est plutôt surprise par cet élan. Elle a l'air embarrassée que Pepper se soit pendue à son cou._

_**Safran :**_ "Pepper, enfin ! On ne se jette pas dans les bras des gens ainsi !"

_**Pepper :**_ "Oh, pardon !"

_Elle lâche immédiatement Sha-One._

_**Pepper :**_ "Je... je ne sais pas ce qu'il m'a pris... pardonnez-moi..."

_Sha-One reste perturbée quelques instants, les bras toujours en l'air._

_**Pepper :**_ "Vous... vous n'allez pas changer d'avis sur les sorcières, au moins ?"

_Aussitôt, Sha-One se met à rire._

_**Sha-One :**_ "Ha, ha, ha ! Non, rassure-toi, jeune sorcière ! J'ai juste été surprise, voilà tout !"

_Messy virevolte autour de Sha-One. Cette dernière le remarque enfin._

_**Sha-One :**_ "Drôle de créature qui vous accompagne..."

_**Pepper :**_ "Je sais ! Je l'ai appelé Messy, et c'est grâce à lui que nous nous sommes lancées dans notre quête ! Il a l'air de vous aimer, il n'a jamais autant virevolté !"

_**Sha-One :**_ "Messy... continue de veiller sur ces jeunes sorcières, tu veux ?"

_Messy émet un râle plus aigu que les autres fois. Sha-One sourit._

_**Sha-One :**_ "Bon, il faut que j'aille retrouver l'autre peureux, là-bas, pour essayer de le raisonner ! Encore merci pour tout ! Je suis persuadée que nous nous reverrons !"

_**Pepper :**_ "Au revoir, Madame Sha-One ! Et merci à vous !"

_**Safran :**_ "Et faîtes attention à votre cheville !"

_**Sha-One :**_ "Ne vous inquiétez pas, je ferai attention. Oh, j'allais oublier !"

_Elle jette un chapeau de paille à Pepper._

_**Pepper :**_ "Mais..."

_**Sha-One :**_ "Ce n'est pas bon de voyager tête-nue par une telle chaleur !"

_**Pepper :**_ "Je ne peux pas accepter ! Il est trop beau !"

_**Sha-One :**_ "Oh, j'en ai toute une panoplie ! Considère ceci comme une modeste récompense pour m'avoir sauvée. Et vous, madame l'apothicaire, je pense que ceci devrait vous intéresser !"

_Elle donne une plante très curieuse à Safran, dotée de six pétales de couleurs différentes._

_**Safran :**_ "Je n'ai jamais vu de plante pareille ! Qu'est-ce donc ?"

_**Sha-One :**_ "Aucune idée ! Elle vient de mon pays d'origine, mais je n'ai jamais réussi à percer son mystère... je pense que vous, vous y parviendrez !"

_**Safran :**_ "Bien entendu, je suis la meilleure apothicaire du pays ! Merci pour cette découverte, Madame."

_**Sha-One :**_ "À bientôt !"

_La jeune femme reprend la direction prise par Cletonal. Pepper enfile fièrement son nouveau chapeau, et Safran range la curieuse plante dans son sac._

_**Sha-One :**_ "Au fait !"

_Pepper et Safran se retournent._

_**Sha-One :**_ "Je ne vous ai pas demandé vos noms !"

_Avec un grand sourire, Pepper répond :_

_**Pepper :**_ "Je suis Pepper de Chaosah ! Et elle, c'est Safran !"

_Sha-One leur adresse un signe de main avant de reprendre sa route. Après quelques minutes de marche, elle exécute un sourire mélancolique._

_**Sha-One :**_ "Pepper... c'est un joli nom."

### Scène 3 - Le refuge

_La troupe poursuit son périple. Sur leur chemin, Pepper et Safran tentent de venir en aide à quelques personnes, mais globalement, les réactions sont plutôt négatives : entre les indifférents, les sceptiques, les anti-sorcières convaincus et les extrémistes qui tentent de les abattre à coups de fourches (Carrot a failli finir empalé), nos deux sorcières ne marquent pas beaucoup de points. Pire que tout, elles commencent à être connues et recherchées. Elles doivent constamment éviter les grandes places où des gardes ont été postés en renfort, ce qui limite fortement leur interaction avec le peuple, et surtout les commerçants : leur stock de vivres diminue dangereusement. Elles doivent commencer à devenir imaginatives pour obtenir des provisions (même si Pepper avait commencé à dresser Carrot pour voler furtivement de la nourriture au marché, Safran a mis immédiatement fin à ces entraînements secrets, ne voulant pas alourdir leur réputation, et s'est contentée de déguisements). Désormais, elles doivent vivre en fugitives.  
Fort heureusement, elles croisent parfois des personnes avec un bon cœur, trouvant aussi la nouvelle loi anti-magie absurde. Elles n'oublieront jamais (enfin, surtout Carrot) le repas copieux préparé par ce vieux paysan à moitié tordu, pestant contre tous les gouvernements possibles et imaginables, ni la marchande de fleurs qui les a dissimulés dans son étal pour éviter la garde. Safran en a profité pour acquérir de nouveaux ingrédients rares et précieux.  
Plusieurs jours ont passé. Elles se trouvent aux abords d'une forêt, non loin d'une ville lumineuse et bruyante appelée Qualicity. Pepper est debout devant Safran, Carrot, Truffel et Messy._  

_**Pepper :**_ "Bon ! Mes amis, il est temps de dresser un bilan de notre quête. Sur une cinquantaine de personnes secourues, nous..."

_Elle constate que Carrot tente d'attraper un fruit dans un arbre pour Truffel, qui soupire devant une tentative aussi lamentable de s'attirer ses faveurs, Safran déguste une sorte de sandwich composé de plantes, et Messy... se contente d'être Messy, c'est-à-dire immobile, en laissant échapper un râle craquant. Pepper s'agace._

_**Pepper :**_ "Dîtes, vous m'écoutez, au moins ?!"

_**Safran :**_ "Pourquoi faire ? Le bilan, il est vite fait : personne ne nous prend au sérieux à part deux-trois personnes. Notre stock de vivres continue de diminuer malgré nos ruses, et notre réputation empire au fur et à mesure que nous continuons notre "combat". La conclusion est sans appel : notre action ne sert à rien. Et avant de vraiment avoir des ennuis avec les gardes, on ferait mieux de rentrer chez nous. Personnellement, je pense que j'ai suffisamment abusé des services de ma remplaçante, et mon lit douillet me manque. N'est-ce pas Truffel ?"

_Truffel hoche la tête avec dédain._

_**Pepper :**_ "Tu... tu n'es pas sérieuse ? Tu ne vas pas renoncer maintenant ?!"

_**Safran :**_ "Pepper. En tant qu'ancienne élève assidue de Magmah, je sais pertinemment reconnaître une cause perdue lorsque j'en vois une. Grâce à moi, des dizaines d'élèves qui n'avaient rien à faire à Magmah se sont réorientées, et lorsqu'une tâche me paraissait impossible, je la mettais de côté. Crois-moi, en tant que commerçante, j'ai une vision bien plus précise d'un plan voué à l'échec qu'avant. Et ce que nous faisons est voué à l'échec. Alors je te remercie pour la balade, j'ai pu réunir des ingrédients intéressants, mais il faut que je m'occupe de ma boutique. Bonne chance."

_Safran finit son sandwich et se lève pour rassembler ses affaires. Pepper est tétanisée, et Carrot très attristé que Truffel s'en aille._

_**Pepper :**_ "Non... non ! Safran, tu ne peux pas faire ça ! Tu as oublié le vieux paysan ? La marchande de fleurs ? Et Sha-One ? Au moins trois personnes soutiennent notre cause ! Ce n'est pas ce que j'appelle un échec, ça ! C'est le début du succès ! Il faut redoubler d'effort, et pas se contenter de renoncer ! Je t'en prie, reste avec moi !"

_**Safran :**_ "Non, Pepper. J'ai pris assez de risques comme ça en t'accompagnant. Et avant que les autorités ne commencent à reconnaître ma tête malgré mes déguisements, il faut que je rentre chez moi. Si jamais je suis fichée comme fugitive, je peux dire adieu à mon commerce ! Alors ça suffit ! À la revoyure ! Enfin, si tu ne te fais pas arrêter avant ! Allez, viens Truffel !"

_Son chat blanc détourne la tête de Carrot et rejoint sa maîtresse d'un air hautain. Safran commence à s'éloigner de Pepper._

_**Pepper :**_ "Si tu quittes cette forêt, je te dénoncerai."

_Safran stoppe sa marche. Elle tourne la tête à moitié._

_**Safran :**_ "Pardon ?! Je crois que je n'ai pas bien entendu !"

_**Pepper :**_ "Tu as très bien compris ! Il est hors de question que je te laisse partir maintenant alors qu'on vient à peine de commencer ! Ce manque de courage me dégoûte au plus haut point ! Alors si tu oses avoir la lâcheté de rentrer chez toi parce que tu as la trouille de te faire prendre, je te dénoncerai ! Tout le monde saura que tu as transgressé les règles, et que tes potions miracles utilisent des ingrédients interdits ! Ce sera la fin de ta boutique, et tu seras bien obligée de me suivre pour espérer pratiquer la magie en toute liberté à nouveau !"

_Safran voit rouge. Elle se dirige vers Pepper à grands pas. Truffel et Carrot prennent peur et se cachent derrière un arbre._

_**Safran :**_ "Tu oses me menacer ?!"

_**Pepper :**_ "Considère cela comme une prise d'otage ! Et je te relâcherai lorsque la loi sera abolie !"

_**Safran :**_ "Une prise d'otage ?! Comment OSES-TU me menacer ?! Si c'est la guerre que tu cherches, tu vas la trouver !"

_Safran génère des boules de feu dans ses mains. Pepper n'est pas apeurée, et continue de la regarder avec des yeux mauvais._

_**Safran :**_ "Si jamais tu me dénonces, je dirai que tu m'as forcée à venir avec toi ! Ce qui n'est pas si loin de la vérité !"

_**Pepper :**_ "Ce sera ta parole contre la mienne. Je me demande laquelle de nous deux a le plus à perdre."

_Les boules de feu diminue d'ampleur. Safran est choquée de voir Pepper sous ce nouveau jour, avec si peu de pitié dans la voix. Mais Safran ne se laisse pas faire, et ses flammes regagnent leur vive lumière._

_**Safran :**_ "Et... et si je te réglais ton compte maintenant ?!"

_Pepper se retrouve aussitôt enveloppée dans une puissante aura chaotique qui fait trembler le sol. Safran ne peut plus cacher sa frayeur plus longtemps. Sa magie s'évapore._

_**Pepper :**_ "Tu es sûre ? Tu as déjà oublié l'issue de notre dernier duel ? Tu tiens vraiment à m'affronter de nouveau ?"

_Safran recule d'un pas. Elle vient de voir les yeux de Pepper. Ils sont devenus rouges, impitoyables. Pour couronner le tout, Messy se trouve juste au-dessus de sa tête, et son râle semble s'être intensifié._

_**Safran :**_ "Pepper... qu'est-ce qu'il t'arrive ?! Je ne te reconnais plus !"

_**Voix au loin :**_ "Qu'est-ce que c'est que ça ?"

_Safran sursaute. Pepper semble reprendre ses esprits, et le déluge de magie qui l'entourait disparaît d'un seul coup. Elle prend sa tête dans ses mains, comme si elle venait de se réveiller d'un cauchemar._

_**Pepper :**_ "Qu'est-ce que..."

_Mais des bruits de pas se dirigent vers elles. Aussitôt, des gardes en armure jaillissent des buissons derrière elles._

_**Garde :**_ "Elles sont là ! Attrapez-les !"

_**Safran :**_ "Cours, idiote !"

_Pepper suit Safran à travers la forêt. Carrot et Truffel les suivent de près. Messy se contente de prendre le même chemin, nullement affolé.  
Elles finissent par se retrouver bloquées devant un grand amas de branches et de racines, difficiles à escalader._

_**Safran :**_ "Zut, coincées ! Si seulement nous avions nos balais..."

_**Pepper :**_ "Attends, je vais utiliser un sort d'Anti-Gravitation !"

_**Safran :**_ "Trop tard ! Ils sont déjà là ! Venez !"

_Pepper et Safran se cachent derrière un tronc d'arbre. Les gardes arrivent et inspectent la zone._

_**Garde :**_ "Fouillez le secteur ! Elles ne peuvent pas être très loin..."

_Les gardes se déploient._

_**Safran :**_ "Grrr ! Ils sont collants ! Comment va-t-on s'en sortir ?"

_**Pepper :**_ "On pourrait leur jeter un sort ?"

_**Safran :**_ "Pour qu'ils aient une raison supplémentaire de nous arrêter ? Non merci ! Et puis, ils sont trop dispersés... il faut rapidement trouver une bonne planque, sinon ils vont finir par nous trouver !"

_Pepper réfléchit, alors que les gardes se rapprochent d'elles. Une planque bien cachée non loin d'ici... et soudain, le déclic._

_**Pepper :**_ "Je connais l'endroit idéal ! Ce n'est pas très loin. Mais il faudrait créer une diversion le temps de le rejoindre..."

_**Safran :**_ "Je m'en occupe ! Truffel, en position !"

_Truffel acquiesce d'un signe de tête et grimpe dans un arbre. Safran fait tournoyer une petite flamme dans sa main, et la jette d'un coup sec. Truffel bondit de l'arbre et prend appui sur la petite flamme (devenue une sorte de flèche) pour effrayer les gardes. Surpris, ils lèvent la tête, mais Truffel a disparu._

_**Safran :**_ "Maintenant !"

_Le groupe s'enfuit en suivant Pepper. Les gardes sont toujours perplexes._

_**Garde 1 :**_ "C'était quoi, ça ? Un chat ?"

_**Garde 2 :**_ "Les chats ne volent pas, voyons ! Allez, continue de chercher ces sorcières !"

_Contournant les gardes restants, Pepper et sa bande parviennent à sortir de la forêt._

_**Pepper :**_ "Ne viens pas me dire que tu as renoncé aux numéros scéniques, après ça !"

_**Safran :**_ "Oh, la ferme ! En attendant, ça nous a sorties d'affaire !"

_**Garde :**_ "Les voilà ! ELLES SONT ICI !"

_Pepper accélère la cadence._

_**Safran :**_ "Zut ! Ils en avaient laissé un à la lisière de la forêt ! Qu'est-ce qu'on fait ?"

_**Pepper :**_ "On y est presque ! Ne faiblissez pas ! On va y arriver !"

_Elles continuent de courir tant bien que mal avec leurs chats respectifs et Messy. Elles finissent par arriver près d'un marécage, plutôt sec par cette chaleur étouffante. Safran hésite à s'y jeter, en lançant un regard dégoûté au sol, mais Pepper lui prend la main pour la forcer à la suivre. Les gardes gagnent du terrain._

_**Safran :**_ "Ils sont bien plus entraînés que nous à la course ! Ils nous rattrapent !"

_**Pepper :**_ "Courage ! Nous y sommes !"

_Un des gardes jette sa lance en leur direction. Et lorsqu'elle atterrit au sol... elles ont disparu ! Les gardes s'arrêtent, surpris, et inspectent les lieux. Plus personne !_

_**Garde 1 :**_ "Mais... où sont-elles passées ?"

_**Garde 2 :**_ "Elles ont disparu sous nos yeux comme par magie !"

_**Garde 3 :**_ "Par magie tout court, oui ! Ce sont bien des sorcières ! Fouillez partout, elles ne peuvent pas être loin !"

_Les gardes se dispersent sur tout le terrain, tandis que le garde qui a jeté sa lance la reprend avec difficulté. Le marécage est peu dangereux étant donné la chaleur qui règne, mais le terrain reste mou par endroit. Elles ont bien disparu, mais ce que les gardes n'ont pas vu... se trouve à leurs pieds.  
En effet, nos sorcières ont emprunté un passage dans le sol, que Pepper a renfermé par la suite pour éviter d'être repérée. Elle tend l'oreille vers le haut._

_**Pepper :**_ "Ils s'éloignent... ouf ! J'ai bien cru que nous étions perdues !"

_**Safran :**_ "Mais NOUS sommes perdues ! À cause de ta maladresse, nous sommes tombées dans une crevasse !"

_**Pepper :**_ "Ce n'est pas une crevasse ! C'est la planque dont je t'ai parlée !"

_Safran jette un œil autour d'elle. La caverne est sombre et gélatineuse. Elle lance un regard incrédule vers Pepper, qui déclare avec joie :_

_**Pepper :**_ "Bienvenue chez les Ahrachnerres !"

### Scène 4 - Armoracia

_Safran fait quelques pas dans cet antre sous-terrain. Chacun de ses pas est ralenti par le sol gélatineux, qui tente de retenir son pied. Les bras écartés, elle essaie de ne pas tomber._

_**Safran :**_ "Je ne suis pas surprise de l'endroit que tu nous as dégoté, Pepper ! Venant de toi, je ne m'attendais pas à un palace ou un hôtel ! Beurk ! J'espère que ça s'enlève facilement, ce genre de taches ! Truffel, ne bouge pas !"

_Elle se dirige lentement vers son chat, qui tente de se débattre de la gélatine qui a pris ses pattes, alors que Carrot s'est déjà presque roulé dedans. Safran se saisit de Truffel, d'un flacon et d'un chiffon pour lui nettoyer les pattes._

_**Safran :**_ "J'espère que tu ne comptes pas nous faire dormir ici ! Parce que otage ou pas otage, c'est hors de question que j'y séjourne ne serait-ce qu'une après-midi !"

_**Pepper :**_ "Ne fais pas ta chochotte, Safran ! Ce n'est pas si terrible ! Et ici, au moins, on est sûres qu'ils ne viendront jamais nous chercher !"

_**Safran :**_ "Mais personne ne viendrait chercher quiconque dans cet endroit lugubre et écœurant !"

_**Pepper :**_ "Chut ! Tu vas vexer les Ahrachnerres !"

_**Safran :**_ "Je n'éprouve pas le moindre respect pour des araignées, qu'elles soient de verre ou pas ! C'est un signe de saleté, de négligence et de vétusté !"

_**Une voix :**_ "Saleté ?!"

_Les deux sorcières lèvent la tête. Une Ahrachnerre descend du plafond à l'aide d'un large fil gélatineux. Elle est plutôt de grande taille, son corps et ses huit longues pattes brillants d'un éclat bleuté._

_**Ahrachnerre :**_ "Négligence ?! Vétusté ?! Qui êtes-vous pour juger ainsi notre antre ?! Vous n'avez aucun droit de vous y trouver ! Les étrangers sont interdits ici !"

_**Safran :**_ "Parce que ça peut parler, ce genre de monstre, en plus ?! Sachez que je n'ai jamais voulu venir jusque dans votre dépotoir immonde, j'y ai été forcée ! Et je suis une sorcière respectable, propre et soignée, moi !"

_**Ahrachnerre :**_ "Une sorcière ?!"

_Il fait claquer ses pinces. Aussitôt, les sorcières sont encerclées par des dizaines d'Ahrachnerres, toutes de tailles différentes. Certaines sont énormes._

_**Ahrachnerre :**_ "Nous ne supportons pas les étrangers, et encore moins les sorcières ! Nous nous sommes battues pour obtenir notre indépendance, vous n'avez plus aucun droit sur cet endroit ! Alors du balai, avant qu'on ne vous dévore !"

_**Safran :**_ "Espèce de brute ! Je vais vous montrer qu'à moi, on me doit le respect !"

_Elle génère une flamme dans sa main. Les Ahrachnerres reculent, affolées._

_**Ahrachnerre :**_ "Du feu ! Vite, mettez-vous à l'abri ! Et allez prévenir la reine ! Et la princesse !"

_Les araignées s'agitent dans tous les sens dans une confusion paniquée._

_**Safran :**_ "C'est ça ! Craignez ma puissance, monstres ! Que cela vous serve de leçon ! Je vous apprendrai, moi, à me respecter comme il se doit !"

_**Pepper :**_ "Arrête immédiatement ! Elles ont une peur bleue du feu ! Et elles ne sont pas méchantes !"

_**Safran :**_ "Pas méchantes ?! Elles ont dit qu'elles détestaient les étrangers, et les sorcières encore plus ! Elles parlaient de nous dévorer !"

_**Pepper :**_ "Elles ne mangent que des minéraux, elles ne sont pas carnivores ! C'est uniquement pour nous effrayer qu'elles ont dit ça ! Il faut les comprendre : Wasabi les tenait en son pouvoir jusqu'à sa mort. Elles ont enfin pu obtenir leur indépendance, alors deux sorcières qui se pointent ici sans s'annoncer, il faut avouer qu'il y a de quoi flipper ! Comment aurais-tu réagi, toi, à leur place ?"

_Safran réfléchit quelques instants. Puis, elle éteint ses flammes._

_**Safran :**_ "Bien ! Puisque tu les connais si bien, je te laisse parlementer avec elles. Mais fais vite ! J'ai l'impression que cette matière visqueuse est en train de ronger mes chaussures !"

_**Pepper :**_ "Merci, Safran ! Ahrachnerres ! Vous n'avez plus rien à craindre ! Il y a eu un malentendu ! Je suis Pepper de Chaosah, vous vous souvenez ? Vous aviez tenté de me piéger sur ordre de Wasabi, mais nous sommes vite devenues amies ! Je... je suis en partie responsable de sa disparition, et... de ce fait, de votre indépendance !"

_Quelques Ahrachnerres montrent timidement leur tête._

_**Ahrachnerre 1 :**_ "C'est... c'est elle ? C'est vraiment Pepper ?"

_**Ahrachnerre 2 :**_ "Mais oui, c'est elle ! Je la reconnais !"

_**Ahrachnerre 3 :**_ "Et son petit chat orange... c'est bien Pepper !"

_Rassurées, les Ahrachnerres s'approchent de Pepper dans un brouhaha de cliquetis ravis. Safran lève les yeux au ciel._

_**Safran :**_ "Mais c'est pas vrai..."

_Carrot s'approche des Ahrachnerres et commence à jouer avec leurs pattes, sous le regard affolé de Truffel._

_**Pepper :**_ "Hé, hé, hé ! On se calme ! Moi aussi, je suis contente de vous voir ! Alors ça y est ? Vous avez récupéré votre territoire ?"

_**Ahrachnerre :**_ "Oh, oui ! Très récemment ! Et c'est grâce à vous ! On vous doit une fière chandelle !"

_**Pepper :**_ "Oh, ce n'était pas grand-chose ! Laissez-moi vous présenter mon amie Safran. Veuillez l'excuser, elle n'était pas au courant de tout ça, elle a simplement eu peur, et ne pensait aucun mot de ce qu'elle a pu dire."

_**Safran :**_ "Comment ça, "peur" ?! Jamais de la vie je n'aurais peur d'eux ! Ha ! Et je pensais chacun des mots que j'ai pu dire !"

_Se faire ainsi fusiller par des centaines d'yeux affecte davantage Safran qu'elle ne l'aurait cru elle-même. Elle détourne les yeux en croisant les bras pour ne pas perdre la face._

_**Ahrachnerre :**_ "Hum ! Bon, disons que les amis de Pepper sont nos alliés, donc vous pouvez rester. Nous ne vous ferons pas de mal. Mais faîtes attention, sorcière de flammes : au moindre mouvement suspect, nous vous attaquerons !"

_Des cliquetis d'approbation envahissent la caverne. Safran reste les bras croisés._

_**Safran** (à elle-même) **:**_ "Peuh ! Comme si j'allais rester, de toute façon..."

_**Ahrachnerre :**_ "Oh, j'y pense ! Il faut prévenir la princesse ! Elle avait hâte de vous rencontrer !"

_**Pepper :**_ "La princesse ? Qui est-ce ?"

_**Une voix :**_ "Les amies ! Le repas est prêt !"

_Au fond de la caverne, une sorte de rideau de gélatine commence à s'ouvrir, écartée par deux mains très fines._

_**Ahrachnerre :**_ "Princesse ! La sorcière Pepper est là !"

_**La voix :**_ "HHIII !!"

_Les petites mains disparaissent aussitôt, et le rideau se referme. Pepper lance un regard inquiet. Du coin de l'œil, Safran semble être intéressée par l'invitée mystère appelée "princesse"._

_**Pepper :**_ "Que se passe-t-il ? Un problème ?"

_**Ahrachnerre :**_ "Non, non, non ! Rassurez-vous ! Elle a dû être... euh... surprise, voilà tout ! Princesse ?"

_Une forte agitation peut s'entendre derrière le rideau. Des mouvements précipités, des bruits d'objets qui se brisent, et des petits "Vite, vite !". Quelques instants plus tard, le rideau s'ouvre à nouveau._

_**La voix :**_ "La sorcière Pepper ! C'est bien vrai ?! Que je suis contente !!"

_La source de la voix se précipite vers Pepper, alors que les Ahrachnerres sur son chemin s'écartent en s'inclinant légèrement. Pepper peut alors observer une jeune fille assez grande, d'un teint très, très pâle, enveloppée dans une longue cape rouge brodée. Ses cheveux châtains sont coiffés avec deux petites couettes de part et d'autre de sa tête, et une large mèche cache une grande partie de son front. Plutôt coquette à première vue (elle porte de longues boucles d'oreilles et s'est maquillée le visage), elle a le sourire jusqu'aux oreilles, et ses yeux brillant de joie contemplent Pepper comme si elle était en train de rêver. Pepper est très gênée par un tel accueil._

_**Jeune fille :**_ "C'est un honneur de vous rencontrer ! Mes amies m'ont tellement parlé de vous ! Oh, je peux vous serrer la main ? S'il vous plaît !"

_**Pepper :**_ "Euh... eh bien, oui, bien entendu, hé, hé !"

_La jeune fille serre la main de Pepper avec entrain, à la limite de lui casser le bras. Pepper peut constater que ses mains sont très, très fines, avec de très longs doigts. Safran observe tout ceci avec une pointe de jalousie, mais elle a remarqué quelque chose qui la fait tiquer._

_**Pepper :**_ "Merci pour votre enthousiasme, ça me fait plaisir ! Mais je ne sais toujours pas qui vous êtes ?"

_**Jeune fille :**_ "Oh, quelle sotte ! J'ai oublié de me présenter ! Veuillez pardonner ma maladresse !"

_Elle se recule et s'incline légèrement._

_**Armoracia :**_ "Je m'appelle Armoracia, et je vis avec mes amies les Ahrachnerres depuis la déclaration de leur indépendance !"

_**Pepper :**_ "Ah, c'est pour ça que je ne vous ai pas vue, la première fois que je suis venue ici... mais ils vous ont appelée "princesse" ?"

_**Ahrachnerre :**_ "C'est bien normal, vu qu'elle est..."

_**Armoracia** (donnant une tape discrète à l'Ahrachnerre) **:**_ "Ils sont vraiment trop mignons, voilà tout ! Ha, ha, ha ! Mais je n'en mérite pas tant, vous savez ! Je me contente d'essayer de leur faire des repas et des gâteaux ! Mais je ne suis pas très douée..."

_**Pepper :**_ "Au moins vous essayez, c'est le principal ! Et je suis sûre que votre cuisine est très bonne ! N'est-ce pas, les Ahrachnerres ?"

_**Ahrachnerre :**_ "Oh oui ! On adore quand c'est la princesse qui cuisine ! On se régale toujours !"

_Nouvelle vague de cliquetis. Armoracia cache son visage avec ses longues mains._

_**Armoracia :**_ "Oh, arrêtez ! Vous me gênez ! Je ne suis pas si douée que ça..."

_**Pepper :**_ "Ne vous rabaissez pas comme ça ! Ce que vous préparez doit être succulent !"

_**Armoracia :**_ "Oh, Pepper ! Venant de vous, ce compliment me va droit au cœur ! Vous... oh, je n'oserais jamais vous demander..."

_**Pepper :**_ "Ne soyez pas timide, demandez !"

_**Armoracia :**_ "Princesse Pepper ! Je serais honorée si... si vous acceptiez... de goûter mes pâtisseries !"

_Armoracia a eu extrêmement de mal à faire sa demande. Pepper cligne plusieurs fois des yeux._

_**Pepper :**_ "C'était juste ça que vous vouliez me demander ? Mais il ne fallait pas avoir autant peur ! Bien sûr que je veux y goûter ! Et surtout toi, Carrot, hein ? Petit morfale !"

_Carrot bondit partout avec appétit. Armoracia pousse un cri strident._

_**Armoracia :**_ "Oooohh !! C'est votre petit chat orange ! Il est si mignon !"

_Elle le prend dans ses bras et le câline. Carrot semble apprécier. Truffel lui lance un regard rempli de jalousie. Armoracia redonne Carrot à Pepper._

_**Armoracia :**_ "Il aura le droit à une grosse part de gâteau ! Attendez-moi quelques instants, je reviens vite !"

_**Safran :**_ "Un instant !"

_La princesse se retourne vers Safran. Son visage devient plus inquiet devant le regard accusateur de Safran. Truffel lui lance un regard noir, comme si elle lui en voulait d'avoir câliné Carrot et pas elle._

_**Pepper :**_ "Safran, reste calme..."

_**Armoracia :**_ "Euh... vous devez être une amie de Pepper ?"

_**Ahrachnerre :**_ "Ne faîtes pas de mal à la princesse ! Sinon..."

_**Safran :**_ "On se relaxe, tout le monde ! Je ne ferai pas de mal à votre petite princesse chérie ! Mais j'avais une question à vous poser."

_Elle l'inspecte en détail. Armoracia tremble comme une feuille, comme si elle avait peur que Safran remarque quelque chose qu'elle voudrait garder secret. Enfin, Safran désigne sa cape._

_**Safran :**_ "Où avez-vous trouvé ceci ? Je ne reconnais pas le textile, ni le design."

_**Armoracia :**_ "Euh... c'est moi qui l'ai tissé ! Pourquoi cette question ?"

_Safran observe plus précisément la cape. La tension est palpable dans toute la caverne. Les Ahrachnerres se tiennent prêtes à bondir sur Safran en cas de danger.  
Au final, la sorcière s'écarte et annonce à Armoracia :_

_**Safran :**_ "C'est du bon travail. Vous avez bon goût en matière de vêtement. Vous êtes très douée avec vos mains."

_Armoracia est surprise. Toujours un peu apeurée, elle baisse un peu la tête et répond un timide :_  

_**Armoracia :**_ "M... merci, madame la sorcière ! C'est gentil de votre part !"

_**Safran** (tendant la main) **:**_ "Appelez-moi Safran. Et voici Truffel. Je pense que nous avons des choses à nous dire, toutes les deux."

_**Armoracia** (serrant timidement la main de Safran) **:**_ "Ah ? Euh... eh bien... enchantée de vous rencontrer ! Vous voulez goûter à mes gâteaux, vous aussi ?"

_**Safran :**_ "S'ils sont aussi bons que votre cape est bien réalisée, alors je ne dis pas non. Auriez-vous une couverture pour que je puisse m'asseoir ? J'ai horreur de salir mes vêtements."

_**Armoracia :**_ "Je... je vous apporte ça !"

_La princesse retourne derrière le rideau à toute vitesse. Les Ahrachnerres sont surprises du comportement de Safran. Même Pepper va s'en décrocher la mâchoire._

_**Safran :**_ "Eh bien, quoi ?! Pepper n'est pas la seule étrangère sympa ! Vous le sauriez si vous passiez plus de temps hors de votre antre !"

_Les Ahrachnerres se regardent entre elles. Pepper s'approche de Safran._

_**Pepper :**_ "C'est... c'était plutôt gentil !"

_**Safran :**_ "Méfie-toi de cette princesse. Elle nous cache quelque chose."

_Puis elle s'éloigne dans un coin, en pestant contre la gélatine recouvrant le sol. Pepper se demande ce qu'elle a voulu dire._

_**Ahrachnerre :**_ "Bizarre, votre amie ! Elle se montre très froide envers nous, puis elle complimente la princesse !"

_**Pepper :**_ "C'est Safran, elle est comme ça ! Elle peut être gentille quand elle veut."

_**Ahrachnerre :**_ "L'autre sorcière ne me renvoyait pas une image aussi tranchée."

_**Pepper :**_ "L'autre sorcière ? Quelle autre sorcière ?"

_Elle a à peine fini sa question que l'Ahrachnerre s'est inclinée. Toutes les autres également. Pepper en déduit qu'Armoracia est revenue, mais elle révise sa déduction lorsqu'elle voit une Ahrachnerre beaucoup plus grosse surgir du fond de la caverne._

_**Safran** (se rapprochant de Pepper) **:**_ "Qui c'est, celle-là ? Une Ahrachnerre qui a besoin d'un régime ?"

_**Pepper :**_ "Non, c'est leur reine ! Et celle qui... non, c'est pas possible ! Je rêve !"

_Safran a des yeux ronds. En effet, une jeune personne se tient aux côtés de la reine des Ahrachnerres. Une jeune personne en robe de prêtresse, que Pepper et Safran connaissent bien._

_**Pepper :**_ "Shichimi ?! Mais qu'est-ce que tu fais là ?!"

_**Armoracia :**_ "C'est prêt, Princesse Pepper ! Et voici votre couverture, Dame Safran ! Vous m'en direz des n..."

_Armoracia stoppe son déplacement joyeux, les bras chargées d'assiettes, de pâtisseries, et d'une couverture brodée. Elle regarde vers la reine Ahrachnerre et Shichimi, puis vers Pepper et Safran, en ne sachant comment réagir._

_**Armoracia :**_ "Euh..."

_**Shichimi :**_ "Bon. Je pense que nous avons des choses à nous dire, mais ce sera autour d'une tasse de thé et de quelques biscuits."

### Scène 5 - La quête des Puissants

_Toutes les Ahrachnerres se sont rassemblées dans une galerie plutôt imposante, en forme de cercle, au cœur de leur antre. Au milieu de ce cercle, assises autour d'un rocher sur une couverture brodée, Pepper et Safran jugent Shichimi et la reine Ahrachnerre du regard. Armoracia se trouve debout, juste à côté, déposant ses pâtisseries sur le rocher, le visage inquiet, ne sachant quoi dire ni comment réagir. Un long silence pesant accompagne Shichimi, pendant qu'elle prépare un thé à l'aide de Safran, qui a reçu l'autorisation de la reine d'utiliser une toute petite flamme pour faire chauffer le thé. Le voici en train de bouillir, pendant que le silence règne en maître. Armoracia a bien du mal à le supporter._

_**Armoracia :**_ "Euh... donc, vous vous connaissez toutes ? Princesse Pepper, Dame Safran et Maître Shichimi ?"

_**Reine Ahrachnerre :**_ "Ce n'est pas ton Maître, Armoracia. Ne l'appelle pas ainsi. Nous n'avons plus de Maître, à présent."

_**Armoracia :**_ "Oh, pardon, ma Reine ! Je ne voulais pas..."

_Elle échappe son assiette des mains. Bien qu'elle ne l'ait pas rattrapée, aucun bris de verre ne se fait entendre. Elle ressort l'assiette, intacte, de sous sa cape._

_**Safran :**_ "Bien rattrapé. Vous êtes plutôt adroite. Même avec vos pieds."

_**Armoracia :**_ "Je... c'est souvent que j'échappe des assiettes, mais... je dois avoir de la chance, voilà tout !"

_**Reine Ahrachnerre :**_ "Ma petite Armoracia, aurais-tu l'amabilité d'aller me préparer ma crème gélatineuse ? Mes articulations me font souffrir."

_**Armoracia :**_ "Oh, tout de suite, ma Reine !"

_Armoracia file à toute vitesse._

_**Reine Ahrachnerre :**_ "Pauvre petite... la tension que vous générez est trop intense pour elle ! Pourriez-vous profiter de son absence pour régler vos affaires ? Je voudrais éviter de la mettre mal à l'aise à nouveau."

_Pepper et Safran se regardent, avant de regarder Shichimi. Cette dernière leur donne leur tasse de thé sans un mot. Elle prend une gorgée de thé dans un silence tendu._

_**Shichimi :**_ "Tu m'en veux, Pepper ?"

_Pepper recrache son thé dans sa tasse._

_**Pepper :**_ "Si tu pouvais éviter de dire des trucs pareils sans prévenir, j'éviterais de m'étouffer !"

_**Shichimi :**_ "Mais j'ai raison, non ? Je peux sentir tes reproches d'ici. La façon dont tu me regardes... tu avais le même regard haineux envers mon Maître, non ?"

_Pepper ne sait pas quoi répondre. Elle tourne la tête vers Safran, qui se contente de boire son thé._

_**Pepper :**_ "Je... je ne te déteste pas, Shichimi. Je sais que tu as été manipulée par Wasabi, et... je voudrais juste savoir pourquoi. Pourquoi tu as approuvé une loi aussi peu sensée ? Quelqu'un t'a forcé la main, c'est bien ça ?"

_Shichimi reprend une gorgée de thé, nullement perturbée._

_**Pepper :**_ "Réponds-moi, s'il te plaît ! J'ai besoin de savoir !"

_**Shichimi :**_ "Tu sais très bien que je ne peux rien dire. Ce qui se passe au sein de l'Ascétribune reste dans l'Ascétribune, c'est comme ça. On ne peut pas transgresser la règle comme tu es en train de transgresser la nouvelle loi depuis quelques jours."

_**Pepper :**_ "Tu crois sans doute que je transgresse par plaisir ?! J'ai un combat à mener, vois-tu ! Un combat contre cette horrible loi que tu as instaurée ! Je veux faire comprendre au monde à quel point elle est stupide, pour te forcer à la retirer ! Enfin, ce ne sera pas utile si tu acceptes de la retirer maintenant..."

_**Shichimi :**_ "Impossible. Il faudrait convoquer une autre assemblée pour cela. Et je n'ai aucune raison de le faire."

_**Pepper :**_ "Aucune raison ?! Alors que des milliers de sorcières ont perdu leur emploi ? Leur école ? Leur raison d'être ?! Ça ne te suffit pas ?!"

_**Safran :**_ "Baisse d'un octave, Pepper."

_**Pepper :**_ "Tu ferais mieux de me défendre, toi ! Tu n'es pas d'accord avec moi ?"

_**Safran :**_ "Hum, ce thé n'est pas aussi exceptionnel que la rumeur le laisse entendre. Je pensais que les sorcières de Ah savaient préparer les meilleurs thés du monde, que vous aviez un cours pour cela. Visiblement, tu n'étais pas très assidue... je peux faire largement mieux ! Je suis déçue..."

_**Pepper :**_ "Safran !!"

_**Safran :**_ "Je dois bien avouer que j'ai été surprise par cette loi. Mais si tu l'as approuvée, étant toi-même une sorcière, c'est que tu avais une bonne raison de le faire. Dis-moi si je me trompe, mais tu as des projets d'avenir, toi aussi, non ?"

_Shichimi ne répond pas. Elle se contente de finir son thé et de prendre une pâtisserie d'Armoracia._

_**Pepper :**_ "Safran t'a posé une question, Shichimi ! Tu n'aurais pas... approuvé cette loi pour ton profit personnel, non ? Ou pour me punir d'avoir provoqué la disparition de ton Maître ?"

_**Shichimi :**_ "Pepper... le monde est éternellement en train de changer. Depuis notre arrivée ici, sur Hereva, nous n'avons cessé de grandir et d'évoluer. C'est pareil pour les lois... la présence des sorcières provoque un déséquilibre depuis toujours. Nous portons des pouvoirs beaucoup trop grands. Imagine un peu la détresse des gens ordinaires, la peur avec laquelle ils doivent vivre tous les jours, en sachant notre existence et nos capacités, pouvant provoquer des guerres effroyables ! Et leur incapacité à pouvoir se défendre... est-ce juste, pour toi ? Que nous ayons un pouvoir aussi grand alors que d'autres non ?"

_Pepper ne sait pas quoi répondre._

_**Shichimi :**_ "Peut-être que c'est ce que ce changement veut nous faire comprendre. Moi aussi, je me sentais persécutée à tort lorsque cette loi a été proposée. Mais en y réfléchissant... c'est peut-être un juste réajustement du monde, non ? La catastrophe de l'Arbre de Komona nous a bien rappelé qu'il existe des forces bien au-delà de ce que nous sommes capables de contrôler. Que faire si nos pouvoirs nous échappent un jour ? Pour le bien du monde entier, et le nôtre aussi... peut-être que cette loi est la solution. Tu ne crois pas ?"

_Pepper finit son thé d'un seul coup, et le repose dans sa tasse._

_**Pepper :**_ "La magie nous entoure, Shichimi. Nous, sorcières, sommes capables de l'utiliser et de la maîtriser. Il doit bien y avoir une raison à cela, non ? Une raison pour laquelle nous sommes les seules à pouvoir le faire ? Et si, au lieu de détruire le monde, c'était justement pour le faire évoluer ? Le faire grandir, afin que tout le monde y trouve son compte ? Non... empêcher les sorcières de pratiquer la magie, c'est contre-nature. Bien sûr, il y aura toujours des Wasabi ou des Chicory pour tenter d'en abuser. Mais nous, nous resterons toujours intègres, et on sera toujours là pour empêcher les forces du Mal de détruire le monde ! Pas vrai, Safran ?"

_**Safran :**_ "Si jamais tu veux nous arrêter, Shichimi, sache que je suis son otage. C'est elle-même qui l'a dit. Je voulais rentrer chez moi, à la base."

_**Pepper :**_ "Pour utiliser tes ingrédients illégaux que tu as obtenus grâce à moi !"

_**Safran :**_ "J'étais partie pour aller les chercher moi-même, je te rappelle !"

_**Pepper :**_ "Ah, tu vois ! Tu avoues ton crime !"

_**Safran :**_ "Mais tu es bête ou q..."

_**Shichimi :**_ "Oh, ça suffit !"

_Pepper et Safran se taisent. Voir Shichimi faire preuve d'autorité est assez inédit._

_**Shichimi :**_ "Bon... si j'ai bien compris, tu comptes poursuivre ta quête pour abolir la nouvelle loi anti-magie ? Malgré les gardes postés un peu partout dans les cités les plus puissantes du monde ?"

_**Pepper :**_ "Oui ! Et toi ? Tu comptes me dénoncer ? Ou même m'emprisonner, vu que tu en as le pouvoir ?"

_Shichimi se redresse et observe le plafond, les mains dans le dos. La gélatine renvoie quelques rayons lumineux. Elle pousse un profond soupir._

_**Shichimi :**_ "Non, je ne vais pas t'arrêter, Pepper. Parce que ton combat est noble, et compréhensible. La magie, c'est ta vie, et tu veux la préserver, je peux comprendre. Je me demande juste ce que Maître Wasabi aurait fait à ma place..."

_Pepper est perturbée par le visage mélancolique de Shichimi. Pas Safran : elle est trop occupée à essayer d'améliorer le thé de Shichimi avec ses propres herbes !_

_**Shichimi :**_ "Mais tout ceci est inutile. Je suis encore Maître de Ah pour quelques jours, et ce rôle est un véritable fardeau pour moi."

_**Pepper :**_ "Quel fardeau ? Tu n'as plus d'élèves sous ton aile !"

_**Shichimi :**_ "Tu sais très bien qu'être Maître de Ah, c'est bien plus que diriger une école... c'est aussi devoir approuver des lois et juger des crimes, par exemple..."

_**Pepper :**_ "Je vois... je ne pensais pas que tu le vivais aussi mal... je suis désolée, Shichimi. J'ai été un peu dure avec toi..."

_**Shichimi :**_ "Ne t'excuse pas. Je comprends tout à fait. C'est juste que... prendre la place de Maître Wasabi sans la mériter... j'ai l'impression de lui manquer de respect. De salir sa mémoire."

_**Pepper :**_ "Elle t'a choisie, non ? Tu ne salis rien du tout ! Tu reprends son œuvre, et avec toi, au moins, on sait sur quel pied danser ! Je trouve que c'est une bonne chose ! J'espère que tu resteras Maître de Ah ! Et que tu pourras enseigner de nouveau à des élèves ! Enfin, quand j'aurai réussi ma mission ! Ce qui risque d'être compliqué... mais j'y arriverai !"

_**Shichimi** (avec un sourire) **:**_ "Hé, hé ! Tu es tellement motivée... ton ami a bien fait son travail."

_**Pepper :**_ "Et ouais ! Quand je me suis fixé un objectif, je mets au défi quiconque de... quoi ? Mon ami ? Tu parles de Carrot ?"

_**Shichimi :**_ "Non, je parle de la petite créature qui t'accompagne, et qui se cache dans l'ombre de l'antre. N'aie pas peur, petit monstre. Je ne vais pas te manger !"

_Accompagné d'un petit râle craquant, Messy descend du plafond et apparaît dans la lumière. Les Ahrachnerres s'agitent._

_**Ahrachnerre :**_ "Qu'est-ce que c'est ?! Un ennemi ?!"

_**Reine Ahrachnerre :**_ "Du calme, mes enfants, du calme ! Ce n'est pas un ennemi."

_**Pepper :**_ "Oui, c'est Messy ! Je ne sais pas trop ce que c'est, mais il m'a donné une vision de ce que je devais faire quand j'étais en proie au doute !"

_**Shichimi :**_ "Et il l'a bien fait. C'est comme si ce désir de rétablir la magie s'était inscrit dans ton âme... quelle créature étrange... ce n'est pas un esprit, mais c'est tout comme ! Bonjour, Messy. Je m'appelle Shichimi. Ravie de te rencontrer !"

_Messy répond par un râle, et s'approche de Shichimi pour mieux la regarder._

_**Pepper :**_ "Mais, Reine Ahrachnerre, vous venez de confirmer que ce n'est pas un ennemi... vous savez ce que c'est ?"

_**Reine Ahrachnerre :**_ "Pas vraiment. Mais de ce que je ressens... non, il n'est pas un ennemi. On dirait que son âme est pure, vide de toute mauvaise pensée."

_**Safran** (pour elle-même) **:**_ "Mais bien sûr... on voit tout de suite qu'il est très rassurant, comme bestiole..."

_Shichimi observe Messy quelques instants avec mélancolie._

_**Pepper :**_ "Bon, maintenant que tout est clair... qu'est-ce que tu vas faire, Shichimi ? Si jamais tu veux nous rejoindre, il y a toujours de la place dans notre groupe !"

_**Safran** (se levant d'un bond) **:**_ "Notre groupe ?! Non, non, non !"

_Safran se dirige vers Shichimi à grands pas, en ignorant la gélatine qui cherche à la ralentir. Elle la prend par les épaules et lui chuchote :_

_**Safran :**_ "N'accepte surtout pas son offre ! Sinon elle va te retenir en otage, tout comme moi ! Tu l'aurais vue quand j'ai essayé de partir... on aurait dit qu'elle allait détruire le monde entier ! Va t'en très vite et préviens les gardes ! Il faut l'enfermer au plus vite !"

_**Pepper :**_ "Qu'est-ce que tu lui racontes, Safran ?!"

_Shichimi éclate de rire et s'écarte de Safran._

_**Shichimi :**_ "Ha, ha, ha ! Tu sais ce que je pense, Safran ? Si tu avais vraiment voulu rentrer chez toi, tu y serais depuis longtemps !"

_Shichimi s'approche de Pepper, en laissant une Safran bouche bée, que Truffel tente désespérément de ramener au réel._

_**Shichimi :**_ "C'est d'accord, Pepper. Je veux bien vous aider !"

_**Pepper :**_ "Super ! Je retrouve la Shichimi que je connais !"

_Pepper prend Shichimi dans ses bras, sous les cliquetis joyeux des Ahrachnerres. Armoracia revient avec un bol._

_**Armoracia :**_ "Désolée, ma Reine, j'ai été un peu plus lente que prévue ! Oh, mais... vous vous êtes réconciliées ?"

_**Reine Ahrachnerre :**_ "Ne t'en fais pas, ma petite. Voir ces deux jeunes sorcières faire la paix m'a remis les os en place !"

_**Armoracia :**_ "C'est génial ! Je suis tellement contente ! Double ration de biscuits pour tout le monde !"

_Les Ahrachnerres sautent et claquent leurs pinces de joie. Armoracia leur jette des dizaines de gâteaux. Puis elle se dirige vers Pepper et Shichimi._

_**Armoracia :**_ "Un biscuit pour Seigneur Carrot ! Un biscuit pour Princesse Pepper ! Un biscuit pour Maître Shichimi !"

_Carrot se jette sur le gâteau et le dévore._

_**Pepper :**_ "Merci ! Mais arrêtez de m'appeler Princesse, c'est vous la princesse ici !"

_**Shichimi :**_ "Et je ne suis pas votre Maître."

_**Armoracia :**_ "Mais vous êtes tellement géniales ! Comment dois-je vous appeler ?"

_**Pepper :**_ "Et pourquoi pas tout simplement Pepper et Shichimi ?"

_**Shichimi :**_ "Cela me semble être une bonne idée."

_**Armoracia :**_ "Si ça vous fait plaisir, c'est d'accord ! HIIII !"

_Armoracia se recroqueville. Messy s'est approché d'elle avec un râle craquant._

_**Armoracia :**_ "Une vilaine bête veut m'attaquer ! Aidez-moi, Princesse Pepper ! Maître Shichimi !"

_**Shichimi :**_ "Rien à faire, elle veut nous appeler comme ça..."

_**Pepper :**_ "N'ayez pas peur, Princesse ! Ce n'est que Messy, un ami à nous !"

_**Armoracia :**_ "Oh, vraiment ?"

_Elle se redresse d'un coup, enjouée. Messy l'observe avec son râle habituel._

_**Armoracia :**_ "Est-ce que Maître Messy veut un biscuit, lui aussi ?"

_Messy regarde le gâteau, mais ne le prend pas._

_**Pepper :**_ "On ne sait pas trop de quoi il se nourrit... je ne l'ai encore jamais vu manger quoi que ce soit !"

_**Armoracia :**_ "Oh... vous ne savez pas ce que vous ratez, Maître Messy ! Et un biscuit pour Dames Safran et Truffel !"

_Elle se jette sur Safran, qui tombe presque par terre en la voyant arriver. Pepper laisse échapper un rire._

_**Pepper :**_ "Elle est amusante ! Allez, goûtons ce gâteau ma foi fort succulent !"

_Elle mange un morceau du gâteau. Aussitôt, elle sent sa respiration et son cœur s'arrêter. Elle regarde un peu partout, pour vérifier que personne ne la voit, puis elle crache discrètement le morceau de gâteau qu'elle avait en bouche._

_**Pepper :**_ "C'est... c'est..."

_**Shichimi** (mâchant avec plaisir) **:**_ "C'est bon, non ? Il paraît qu'Armoracia concentre la gélatine environnante avec l'eau présente dans le sol, pour les charger en minéraux ! Certains racontent même qu'elle met des morceaux d'araignées dedans !"

_**Pepper** (s'étouffant presque) **:**_ "Ah ? C'est... sympa, très sympa ! Tu... tu as l'air d'aimer, toi !"

_**Shichimi :**_ "J'en raffole ! Je m'en ferais sauter le ventre ! Tu ne finis pas le tien ?"

_**Pepper :**_ "Euh... non, je vais le garder pour plus tard, hein ? Au cas où... hé, hé !"

_Elle le range dans sa poche, en jurant sur sa propre vie qu'elle se jetterait un sort de Destruction sur elle-même si jamais l'envie lui reprenait d'en goûter un morceau. Elle ne préfère même pas penser au fait que Shichimi les trouve succulents.  
Pepper regarde autour d'elle. Les Ahrachnerres sont ravies, Carrot s'amuse comme un fou... Safran s'est aussi étouffée avec le gâteau d'Armoracia (je ne suis pas la seule, pense Pepper), alors qu'Armoracia prend plaisir à distribuer ses affreux gâteaux faits maison..._

_**Pepper :**_ "Ils ont l'air de bien s'amuser, ici."

_**Shichimi :**_ "Tu as le don d'attirer les ondes positives autour de toi, Pepper. Ce qui peut s'avérer extrêmement pratique dans ta situation."

_**Pepper :**_ "Oui, mais... je dois avouer que pour l'instant, toutes nos tentatives ne se sont pas trop soldées par une réussite. Beaucoup de personnes nous rejettent encore. À part persévérer, je ne vois pas trop quoi faire de plus... vu ta position, tu ne pourrais pas faire quelque chose, toi ? Genre nous débarrasser des gardes, ou... essayer de supprimer cette loi ?"

_Shichimi réfléchit._

_**Shichimi :**_ "Malheureusement, je n'ai qu'un rôle d'arbitre. Je ne fais que donner une décision finale à un problème. Comme je te l'ai dit, je ne peux pas abolir cette loi par ma simple volonté. Et les gardes doivent obéissance à leur roi, pas à moi. Je ne peux pas vous aider sur ce terrain-là. En revanche..."

 _Un petit sourire éclaire son visage._

_**Shichimi :**_ "...je pense savoir comment faire pour vous donner un sérieux coup de pouce."


## Acte 3 - Unitah

### Scène 1 - Infiltration

_Jamais l'antre des Ahrachnerres n'avaient accueilli autant de visiteurs, et encore moins des sorcières. Pourtant, ce sont bien trois sorcières qui se sont réunies dans un petit passage de l'antre, de sorte que seulement trois personnes puissent s'y tenir côte-à-côte, afin que certaines oreilles soient épargnées du plan top secret que s'apprête à dévoiler Shichimi._

_**Shichimi :**_ "Bon, autant vous prévenir tout de suite, mon idée reste dans le domaine de l'hypothèse."

_**Safran :**_ "Et autant vous prévenir tout de suite : la réduction de mon espace vital n'a rien d'hypothétique ! Alors si on pouvait écourter cette réunion secrète, je vous en serais reconnaissante !"

_**Pepper :**_ "Si tu interromps Shichimi toutes les cinq secondes pour te plaindre, on n'est pas sorties ! Vas-y Shichimi, expose-nous ton idée. Comment va-t-on faire pour que notre action donne des résultats ?"

_**Shichimi :**_ "Pour l'instant, vous avez aidé des gens au hasard, faisant partie du peuple, des gens sans aucune influence ni pouvoir. Exact ?"

_**Pepper :**_ "Hum... oui, nous n'avons aidé personne qui soit chef de quoi que ce soit. Tu confirmes, Safran ?"

_**Safran :**_ "Je confirme que cette proximité entre nous commence à m'oppresser ! Je vais attraper tous vos microbes si ça continue !"

_**Pepper :**_ "Elle confirme."

_**Shichimi :**_ "Bien. C'est une bonne chose : c'est en faisant circuler l'information qu'elle va prendre de l'ampleur."

_**Safran :**_ "...pour mieux nous identifier et nous arrêter. Quelle bonne idée !"

_**Pepper :**_ "Dis, tu vas te plaindre encore longtemps ?"

_**Safran :**_ "Tant qu'on sera entassées comme des sardines, oui !"

_**Shichimi :**_ "C'est une bonne chose dans le sens où nous allons devenir le centre des conversations, ce qui va forcément finir par remonter plus haut."

_**Safran :**_ "Et vu que les gens que nous avons aidés étaient plutôt contre nous, je ne suis pas convaincue que ce soit une bonne chose, moi !"

_**Shichimi :**_ "L'important, c'est qu'on en parle. Certes, le revers de la médaille, c'est que les gardes nous surveillent pour nous attraper. Mais au moins, les chefs et les dirigeants de tous les royaumes savent ce que nous faisons. Ce qui veut dire que maintenant, nos actions auront réellement de l'impact sur eux. C'est pour cela que si nous voulons vraiment faire bouger les choses et les mentalités, il faut viser les personnes qui ont la plus grande influence sur l'opinion publique : les dirigeants eux-mêmes !"

_**Pepper :**_ "Tu veux dire les rois et reines de tous les royaumes ?"

_**Shichimi :**_ "Pas forcément tous, mais les plus influents, oui."

_**Safran** (hilare) **:**_ "C'est ça, ton idée brillante ? Ha, ha, ha ! Nous exposer devant les rois et les reines de ce monde ?! Ha, ha, ha ! Si je n'étais pas aussi serrée, j'éclaterais de rire tellement c'est le meilleur plan du monde ! Ha, ha, ha ! Si jamais on veut se faire capturer à coup sûr ! Ha, ha ! Désolée, mais je dois sortir, c'est trop fort ! Ha, ha, ha !"

_Se tenant les côtes, Safran s'extirpe de la zone exiguë. Shichimi et Pepper la rejoignent assez vite._

_**Pepper :**_ "Dis donc, Safran, tu pourrais au moins respecter Shichimi ! Son idée n'est pas mauvaise, après tout !"

_**Safran :**_ "Non mais, se jeter dans la gueule du loup, quoi ! Ha, ha ! Merci, Shichimi ! Je n'avais pas autant ri depuis des lustres !"

_**Shichimi :**_ "C'est toujours un plaisir, Safran. Mais j'étais sérieuse. Venir en aide aux dirigeants est le meilleur moyen d'avoir une visibilité et une réputation rapides."

_**Safran :**_ "Et pour se faire jeter aux douves, oui ! Je vous préviens tout de suite : vous pouvez m'oublier dans vos petites combines à deux ronds ! Hors de question que je m'expose en criminelle devant les rois ! Déjà que je suis recherchée, si jamais ils me reconnaissent, je peux dire adieu à mon avenir !"

_**Pepper :**_ "Ouiii, ta précieuse boutique, on sait ! Mais il y a plus important !"

_**Shichimi :**_ "Surtout que je ne parlais de nous exposer en criminelles, mais bel et bien en aide. Il ne s'agit pas de faire les choses au hasard, le nez au vent, en espérant que ça passe. Non. Comme Safran l'a justement souligné, les risques encourus sont bien plus grands. Il faut faire un travail d'investigation d'abord, afin de connaître les vraies besoins des rois et des reines. Une fois identifiés, nous pourrons réfléchir à une façon efficace de les aider. Et quand ce sera fait, je pense qu'ils oublieront leur opinion négative sur les sorcières, et répandront la nouvelle à leur peuple. Les gens que vous avez aidés se souviendront de vous, et comme leur chef bien aimé aura vanté vos mérites, leur opinion personnelle va aussi changer. Si nous parvenons à faire changer d'avis quelques souverains influents, nous devrions avoir de puissants alliés de notre côté, suffisamment pour ré-ouvrir une réunion pour abolir cette loi anti-magie. Qu'en pensez-vous ?"

_**Pepper :**_ "Je pense que tu es un génie, Shichimi ! On va carrément faire comme ça ! Hein, Safran ?"

_**Safran :**_ "Mais vous faîtes bien ce que vous voulez, de toute façon je n'y participerai pas !"

_**Pepper :**_ "Merci pour ton soutien sans failles, Safran. C'est toujours un plaisir... mais j'y pense, Shichimi : comment allons-nous connaître les besoins les plus importants des souverains ?"

_**Shichimi :**_ "Grâce au réseau des araignées."

_Shichimi sort une boule de cristal de sa robe._

_**Pepper :**_ "J'ai déjà vu ce genre de boule de cristal ! J'avais suivi Maître Thym dans une grotte où une de ces boules était reliée à un fil, et elle donnait accès à plein de trucs super ! Des jeux, des images... des infos ! Et c'étaient des araignées qui contrôlaient le réseau !"

_**Shichimi :**_ "Exact. Et comme les Ahrachnerres sont des araignées..."

_**Pepper :**_ "On va pouvoir intercepter le réseau et tout savoir sur les rois !"

_**Shichimi :**_ "Tout, peut-être pas, mais au moins des indices sur ce dont ils ont le plus besoin."

_**Pepper :**_ "C'est trop génial ! Hein, Safran ?"

_**Safran** (brossant Truffel) **:**_ "Vous avez conscience que je ne vous écoute pas ?"

_Shichimi branche un des fils d'Ahrachnerre à sa boule de cristal. Cette dernière s'allume quelques instants, puis s'éteint aussitôt._

_**Pepper :**_ "...eh bien ? Elle ne s'allume pas ?"

_**Shichimi :**_ "Malheureusement, c'est du fil d'Ahrachnerre. Ce n'est pas très optimal comparé aux fils des araignées spécialisées là-dedans. Il faudrait que quelqu'un assure la compatibilité avec les Ahrachnerres, mais ces araignées spécialistes sont à des jours de marche... sans oublier les gardes qui nous guettent."

_Pepper réfléchit quelques instants, et un large sourire envahit son visage._

_**Pepper :**_ "Je ne connais qu'une personne capable d'assurer la compatibilité ! Princesse Armoracia !"

_Armoracia bondit d'un seul coup devant Pepper, le visage joyeux, une spatule à la main._

_**Armoracia :**_ "Oui, Princesse Pepper ?"

_**Pepper :**_ "Je pense que nous allons avoir besoin de vos talents de tisseuse."

_Quelques minutes plus tard, trois marchandes de fruits encapuchonnées avec leur panier en osier sortent d'un sol marécageux. Elles se dirigent vers la ville de Qualicity. À l'entrée de la ville, des gardes les stoppent._

_**Garde :**_ "Halte-là ! Déclinez votre identité et la raison de votre séjour à Qualicity !"

_**Marchande 1 :**_ "Nous sommes des marchandes de fruits itinérantes, et nous venons vendre notre marchandise dans votre ville !"

_**Garde :**_ "Bien. Présentez-moi votre permis et nous vous laisserons entrer."

_Les trois marchandes s'échangent un regard perplexe._

_**Garde :**_ "Alors, ce permis ? C'est pour aujourd'hui ou bien ?"

_**Marchande 2 :**_ "Oh, et puis zut !"

_La marchande ouvre un flacon et le pose sous le nez des gardes. Aussitôt, leurs paupières se referment, et ils s'écroulent par terre._

_**Pepper** (relevant sa capuche) **:**_ "Safran ! On avait dit qu'on restait discrètes !"

_**Safran :**_ "Mais c'est beaucoup trop long ! Et puis je t'avais dit de réfléchir avant de partir déguisée en marchande, mais tu ne m'as pas écoutée, une fois de plus !"

_**Shichimi :**_ "Arrêtez de vous chamailler ! On va attirer les regards. Safran, ils vont dormir combien de temps, ces gardes ?"

_**Safran :**_ "Je dirais une grosse demi-heure. Mon Filtre Somnifère n'a jamais été très régulier dans son effet."

_**Shichimi :**_ "Bien. Nous avons environ trente minutes pour exécuter le plan. Restez cachées sous votre capuchon. On continue comme prévu."

_Pepper tire la langue à Safran avant de remettre sa capuche. Elles entrent dans Qualicity.  
La cité est fidèle à elle-même. Toujours autant de buildings imposants, de maisons qui se déplacent toutes seules, de technologies avancées dans les vitrines, d'automates qui se baladent un peu partout... mais leur cible reste le vieux château qui s'élève sur une colline au loin, qui semble résister à l'appel du futur.  
Nos trois sorcières arrivent discrètement au château._

_**Safran :**_ "Bien entendu, ça grouille de gardes..."

_**Pepper :**_ "Étrange. D'habitude, ce sont des robots zombifiés qui assurent la sécurité. Ici, ce sont des gardes ordinaires..."

_**Shichimi :**_ "Peut-être que le roi s'est rendu compte que sa technologie n'était pas sans failles, et qu'il valait mieux se contenter de gardes de chair et de sang."

_**Safran :**_ "Ils ne sont pas sans failles pour autant. Bon, je les endors, et on entre."

_**Shichimi :**_ "Surtout pas ! Ces gardes-là se surveillent entre eux. Regarde au sommet des tours."

_Safran lève la tête. Il y a effectivement des sentinelles sur les toits._

_**Safran :**_ "Bien vu. Impossible d'endormir tout le monde lorsqu'ils auront donné l'alerte..."

_**Shichimi :**_ "Précisément. Il nous faut trouver une autre issue."

_**Pepper :**_ "Et pourquoi ne pas se faire prendre, après tout ? Le roi nous connaît bien ! Il nous libérera !"

_**Shichimi :**_ "Il a fait les frais des sorcières, lui aussi. Et même si sa fille en est une, rien ne nous dit qu'il n'a pas changé d'avis. Non, c'est trop risqué. Et je connais une autre issue."

_Shichimi désigne le sommet de la forteresse._

_**Shichimi :**_ "Le toit a bien été détruit lors du combat contre mon Maître ?"

_**Pepper :**_ "Oui... Wasabi t'a fait son compte-rendu, finalement... mais il a été reconstruit depuis."

_**Safran :**_ "Et il y a des gardes autour."

_**Shichimi :**_ "Reconstruit, oui. Par les ingénieurs de Qualicity. Cela reste donc le point d'entrée le plus vulnérable. Et connaissant leur fierté dans leur travail, ils n'auront pas posé de gardes à proximité. Ils ne s'attendent pas à ce qu'on entre par cet accès."

_**Safran :**_ "Ça se défend."

_**Pepper :**_ "Oui, tu as sans doute raison... tu es bien renseignée sur ces ingénieurs, dis-moi !"

_**Shichimi :**_ "Tout le monde sait que leur produit sont de piètre qualité, et qu'ils en sont fiers. Seuls Coriandre et son père se détachent du lot... bon, nous avons besoin de monter discrètement sur le toit. Pepper, tu penses pouvoir nous soulever jusqu'en haut ?"

_**Pepper :**_ "C'est quand même haut... et si jamais quelqu'un nous voit au loin... mais il faut essayer ! Je..."

_Une brindille craque derrière elles. Elles se retournent et se préparent à attaquer._

_**Pepper :**_ "Qui va là ?"

_**Voix :**_ "Ne m'attaquez pas ! Ne m'attaquez pas ! C'est moi, Armoracia !"

_Armoracia se dévoile derrière un arbre. Un peu honteuse, elle porte toujours sa cape brodée._

_**Pepper :**_ "Princesse ! Vous nous avez suivies jusqu'ici ?"

_**Armoracia :**_ "Je suis désolée, Princesse Pepper ! Mais j'étais tellement curieuse... et je voulais voir si mes tenues avaient fonctionné !"

_**Pepper :**_ "Euh... eh bien, disons que nous n'avons pas été repérées, alors on peut dire que c'est une réussite !"

_**Safran :**_ "Même si le déguisement n'était pas complet..."

_**Pepper :**_ Safran !"

_**Shichimi :**_ "En fait, Princesse, votre présence pourrait nous être d'un grand secours. Avez-vous du fil d'Ahrachnerre sur vous ?"

_**Armoracia :**_ "J'en ai toujours, au cas où il faille tisser en urgence !"

_**Shichimi :**_ "En avez-vous suffisamment pour dresser une corde jusqu'au sommet de ce château ?"

_Armoracia regarde le sommet. Elle reste bouche bée quelques instants._

_**Shichimi :**_ "Alors ?"

_**Armoracia :**_ "Euh... oui, oui ! Je devrais en avoir assez... euh... par contre, je vais devoir tisser la corde... euh... je reviens vite !"

_Armoracia se cache dans la forêt._

_**Pepper :**_ "Mais... pourquoi elle se cache comme ça pour tisser ? Elle a aussi refusé de nous laisser la voir tisser nos déguisements !"

_**Shichimi :**_ "Certains artistes ne supportent pas d'être vus en train de travailler. C'est une source de stress pour eux. Laissons-la tranquille et attendons qu'elle ait fini."

_**Safran** (d'un air suspect) **:**_ "Hum..."

_Les trois sorcières s'abritent en attendant qu'Armoracia revienne._ 

_**Safran :**_ "Hum... les gardes ont dû se réveiller, depuis le temps ! Il faudrait qu'elle se dépêche !"

_**Pepper :**_ "Respecte un peu son travail, Safran ! On ne peut pas lui demander de se dépêcher, quand même !"

_**Shichimi :**_ "Et puis les gardes vont d'abord se déployer en ville... ici, nous sommes en sécurité. Pour le moment."

_Quelques minutes plus tard, elle ressort de la forêt avec un grand sourire._

_**Armoracia :**_ "Désolée pour l'attente ! C'est terminé ! Est-ce que ça vous ira ?"

_Elle montre le bout d'une corde gélatineuse d'une longueur interminable ! Les trois sorcières sont impressionnées._

_**Shichimi :**_ "C'est parfait ! Vous avez fait un travail remarquable, Princesse ! Merci beaucoup, votre aide nous est très précieuse."

_Shichimi s'incline légèrement. Armoracia rougit._

_**Armoracia :**_ "Oh, arrêtez ! Ce n'est vraiment pas grand-chose..."

_**Pepper :**_ "Vous êtes géniale, Princesse !"

_Seule Safran la regarde d'un air suspect._

_**Pepper :**_ "Bon, il va falloir grimper maintenant. Attention !"

_Pepper prend la corde et la fait léviter avec un sort d'Anti-Gravitation. Elle parvient à l'attacher au sommet, d'un côté du château à l'abri des regards grâce aux arbres._

_**Pepper :**_ "Incroyable ! Ça se fixe bien !"

_**Safran :**_ "Surtout aux vêtements !"

_**Pepper :**_ "Plus qu'à grimper ! Attendez-nous ici, Princesse. Nous revenons vite !"

_**Armoracia :**_ "D'accord. Soyez prudentes, Princesse Pepper, Maître Shichimi, Dame Safran !"

_**Pepper :**_ "Bon. Safran, tu veux grimper la première ?"

_**Safran :**_ "Moi ? Grimper sur cette corde visqueuse ? Non merci ! Je préfère attendre ici avec la "princesse"."

_**Armoracia :**_ "Oh, j'avais oublié à quel point vous détestiez vous salir, Dame Safran ! Je suis désolée..."

_**Pepper :**_ "Safran... tu pourrais faire un effort ! Et ne pas faire culpabiliser la princesse !"

_**Safran :**_ "Faire un effort ? Mais je fais quoi, moi, depuis que tu m'as entraînée dans cette histoire ? Je dois prendre sur moi, habiter dans un trou couvert de substances peu ragoûtantes, jouer les fugitives, laisser mon magasin tourner tout seul, et crapahuter sur des châteaux maintenant ? Je pense que j'en ai assez fait, non ? Alors je reste là. Nous n'avons pas besoin d'être trois pour ce genre de mission ! Et quelqu'un doit veiller sur la princesse. Non mais !"

_**Armoracia :**_ "Dame Safran... vous voulez me protéger ? Vous êtes adorable !"

_Elle serre Safran dans ses bras. Celle-ci ne semble pas bien supporter cette étreinte._

_**Safran :**_ "Oui, doucement ! Nous n'avons pas gardé les dragons-vaches ensemble, que je sache !"

_**Armoracia** (se retirant aussitôt) **:**_ "Oh, pardon !"

_**Pepper :**_ "Bon... Shichimi, on y va ?"

_**Shichimi :**_ "Quand il faut y aller..."

_**Armoracia :**_ "Soyez prudentes !"

_Pepper et Shichimi entament donc l'escalade du mur, sous l'œil inquiet d'Armoracia et indifférent de Safran. Du moins, en apparence. Elle ne peut s'empêcher de jeter des coups d'œil furtifs au mur pour voir où elles en sont.  
Elles finissent par arriver en haut. Essoufflées, elles prennent une petite pause._  

_**Pepper :**_ "La vache ! C'est plus haut que je ne le pensais ! Heureusement que ce fil accroche bien !"

_**Shichimi :**_ "Oui, c'est dans des moments comme ça qu'on apprécie nos bons vieux balais ! Même si je préfère mon petit nuage..."

_**Pepper :**_ "Bon, par où on va entrer ?"

_**Shichimi :**_ "Hum..."

_Shichimi inspecte la zone. Effectivement, le toit où elles se trouvent est bien différent de la pierre qui entoure les autres toits adjacents. Il est entièrement lisse. Shichimi s'accroupit dans un coin. Elle a un petit sourire._

_**Shichimi :**_ "Oui, je savais bien que ce serait du travail bâclé. Regarde."

_Shichimi montre une faiblesse du toit dans un coin à Pepper._

_**Shichimi :**_ "On peut créer un passage ici. Attention !"

_Shichimi se concentre. Une ouverture se crée alors au-dessus d'elle, et un bras musclé transparent d'une taille colossale en sort. Il s'agrippe au morceau de toit, et l'arrache avec une facilité déconcertante ! Pepper est impressionnée. Le bras retourne dans l'ouverture et celle-ci disparaît._

_**Pepper :**_ "Mais..."

_**Shichimi :**_ "Les esprits sont toujours de précieux alliés, n'est-ce pas ? Attention..."

_Shichimi regarde par l'ouverture._

_**Shichimi :**_ "Non, comme d'habitude, la salle du trône est vide. Allons-y ! On va utiliser la corde pour descendre."

_Pepper se charge de remonter la corde pour la faire redescendre par l'ouverture créée par Shichimi. Elles s'emploient à descendre discrètement dans la salle du trône de Qualicity, déserte.  
Elles arrivent enfin en bas._  

_**Pepper :**_ "Ouf ! Je n'ai jamais autant fait d'escalade de ma vie !"

_**Shichimi :**_ "Ce sera au moins un avantage de cette loi : nous faire faire de l'exercice !"

_**Pepper :**_ "Pas faux... bon, où allons-n..."

_**Shichimi :**_ "Attention ! Quelqu'un vient !"

_Shichimi entraîne Pepper derrière le trône. Ce dernier ouvre un petit œil endormi, mais ne réagit pas plus que ça lorsqu'il aperçoit Pepper et Shichimi.  
La porte derrière le trône s'ouvre. Deux individus entrent. Pepper reconnaît l'un d'entre eux._

_**Coriandre :**_ "Inutile d'insister, Docteur Sandakropp ! C'est non !"

_**Dr Sandakropp :**_ "Je me permets tout de même d'insister, Princesse Coriandre !"

_L'homme qui accompagne Coriandre est grand, mince, et porte une sorte de blouse blanche. Sa longue frange flottante et sa petite barbichette soutenue d'une moustache fine le ferait presque passer pour un dragon._

_**Dr Sandakropp :**_ "Accepter mon offre est le meilleur moyen de rehausser l'opinion des gens sur les compétences de vos ingénieurs, je vous assure ! Ce n'est que du bonus pour vous ! Si vous réfléchissez bien, c'est la meilleure solution pour tout le monde. Vous ne pouvez que signer ce document, c'est le bon sens même !"

_Coriandre s'arrête de marcher. Elle croise les bras et lance un regard mauvais au docteur._

_**Coriandre :**_ "La fierté de Qualicity, ce n'est pas de construire les automates les plus parfaits du monde ! C'est de les construire soi-même, et de persévérer encore et toujours jusqu'à obtenir des résultats, et des résultats qui nous plaisent ! C'est l'amélioration continue, un cercle vertueux, et non pas un simple plan fourbe comme celui que vous me proposez ! Alors non, Docteur : je n'accepterai jamais votre offre ! Mes ingénieurs sont des êtres humains avec des rêves, pas de simples investissements sans aucune autre valeur que l'argent ! Je vous prierais de bien vouloir quitter ce château, et de ne plus jamais y remettre les pieds !"

_Le docteur la juge du regard. Pepper se cache derrière le trône : elle aurait juré qu'il a regardé dans sa direction ! Elle prie pour qu'il ne l'ait pas vue... mais le docteur prend une mine résignée et déclare :_

_**Dr Sandakropp :**_ "Très bien, je n'insisterai pas. Auprès de vous, en tout cas. Je me demande ce qu'en penserait votre père, maintenant que votre petit "plus" vous a été retiré... bien le bonjour, Princesse !"

_Coriandre serre les poings, tandis que le docteur prend la direction de la sortie. Avant de quitter les lieux, il déclare à nouveau :_

_**Dr Sandakropp :**_ "Vous verrez, vous regretterez de ne pas avoir accepté mon offre !"

_Puis il s'en va. Coriandre lui tire violemment la langue._

_**Coriandre :**_ "Bon débarras ! Vous pouvez sortir de votre cachette maintenant, les amies !"

_Pepper sursaute. Elle se dévoile, tout comme Shichimi, et s'approche de Coriandre, qui les accueille avec le sourire._

_**Coriandre :**_ "Ça faisait un bail, n'est-ce pas ?"

_**Pepper :**_ "Mais comment tu as su qu'on se cachait là ?"

_**Shichimi :**_ "On a oublié de ranger la corde. C'est un sérieux indice sur la présence d'étrangers dans le château, non ?"

_Pepper regarde la corde dont elles se sont servies pour entrer, pendouillant toujours depuis le toit._

_**Pepper :**_ "Ah, mince..."

_**Shichimi :**_ "Mais ça ne m'indique pas comment tu as su qu'il s'agissait de nous."

_**Coriandre :**_ "Hé, hé ! Cette fois-ci, c'est de mon fait !"

_Elle désigne un petit appareil en forme de mouche avec un seul gros œil, virevoltant autour de Pepper et Shichimi._

_**Pepper :**_ "Wah ! Tu as dressé une mouche pour nous espionner ? Je ne l'ai même pas entendue !"

_**Shichimi :**_ "Ce n'est clairement pas une mouche, Pepper."

_**Pepper :**_ "Ah bon ? Pourtant..."

_**Coriandre :**_ "Ha, ha ! C'est le signe que le camouflage est parfait ! Non, c'est un robot miniature que j'ai conçu. Le gros œil que vous voyez là est un capteur optique qui enregistre la lumière qu'il reçoit, et la retransmet via des ondes sur un écran installé dans ma chambre ! Comme ça, je peux observer ce qui se passe dans le château en ne bougeant pas de ma chambre !"

_**Pepper :**_ "Tu peux observer à distance ?! Incroyable ! Tu inventes toujours des trucs géniaux, Coriandre !"

_**Coriandre :**_ "Oh, ça m'occupe. Depuis que la magie est interdite, je peux me consacrer entièrement à l'ingénierie. J'ai plein d'idées en tête !"

_**Shichimi :**_ "En voilà une que la loi anti-magie n'a pas l'air de déranger !"

_**Coriandre :**_ "Eh bien, à titre personnel, non. Mais philosophiquement parlant, je trouve cette idée tout bonnement illogique. La magie est une ressource importante, c'est stupide de la laisser de côté."

_**Pepper :**_ "Tu as parfaitement raison !"

_**Coriandre :**_ "Mais bon, c'est la loi, il faut faire avec. C'est pour ça que je travaille corps et âme pour remplacer la magie par les nouvelles technologies. Ou du moins, pour combler son manque, je sais très bien que je n'arriverai jamais à la remplacer... mais ça ne veut pas dire que je n'ai pas mon lot de problèmes, comme cet énergumène que vous venez de voir..."

_**Pepper :**_ "Qui était-ce, d'ailleurs ?"

_**Coriandre :**_ "Le docteur Sandakropp. Il travaille dans un laboratoire secret (plus vraiment secret) où il mène des expériences à la limite de l'éthique. Il me sollicite sans cesse ces derniers temps pour acheter mes ingénieurs afin de les mettre à son service pour "les faire progresser", soi-disant. Mais tout le monde sait ce qu'il cherche à en faire : des esclaves à son service ! Il est hors de question que j'accepte de livrer mes ingénieurs à un type aussi ignoble ! Je préfère qu'ils gardent leur esprit critique, quitte à faire plus d'erreurs dans leur travail. C'est comme ça qu'on apprend, non ?"

_**Pepper :**_ "Tout à fait ! C'est à force de persévérance qu'on finit par obtenir ce qu'on veut !"

_**Coriandre :**_ "En parlant de ça, vous avez fait de sacrées acrobaties pour venir jusque là ! Pourquoi ne pas être passées par la porte d'entrée ?"

_**Pepper :**_ "C'est que... on est un petit peu recherchées..."

_**Coriandre :**_ "Hum, vous cherchez sans doute à abolir cette loi, non ?"

_**Shichimi :**_ "Tout à fait. Et pour y parvenir, nous avons besoin de tes services. Seulement, tu sembles très occupée, et venir avec nous... signifie coopérer avec nous, et donc..."

_**Coriandre :**_ "Je serais considérée comme criminelle à mon tour. Mais je me suis toujours évertuée à rester dans le droit chemin, et aller jusqu'au bout de mes convictions. Cette loi n'a pas de raison d'exister, et si vous faîtes quelque chose pour tenter de la supprimer, je veux en être. Il est vrai que l'absence de Zombification est un frein certain dans mon travail... et ça ne me dérange pas d'être une fugitive recherchée par mes propres gardes ! C'est même plutôt amusant ! Et je suis sûre que c'est ce que mon père voudrait que je fasse. Pour honorer grand-mère Apiacée, et..."

_**Pepper :**_ "Tu es géniale, Coriandre ! Merci !"

_Elle se jette dans ses bras. Coriandre sourit._

_**Coriandre :**_ "C'est bien normal de s'entraider entre amies ! Bon, je suppose que vous avez une planque secrète, et..."

_Elle observe la corde._

_**Coriandre :**_ "...je crois savoir où elle se trouve ! Mais hors de question de s'esquinter les bras à remonter sur le toit ! Place à la science !"

_Elle fait signe à Pepper et Shichimi de la suivre. Elles se dirigent sur le trône. Coriandre le tapote légèrement, et il ouvre un petit œil endormi._

_**Coriandre :**_ "Allez, mon gros ! Au boulot ! Accrochez-vous bien à lui."

_Pepper et Shichimi, nerveuses, s'accrochent du mieux qu'elles peuvent au trône, attendant de voir ce qu'il se passe. Le trône ouvre complètement son œil, et le sol sur lequel il est posé disparaît d'un coup ! Les voici qui tombent au ralenti dans une galerie souterraine, jusqu'à atteindre le sol, très, très profond._

_**Coriandre :**_ "Merci, mon grand !"

_Le trône referme son œil à moitié, et dès que les sorcières sont descendues de lui, il remonte très lentement vers le haut en utilisant ses "pattes". Une fois arrivé à destination, l'ouverture à présent minuscule par laquelle elles sont passées disparaît._

_**Coriandre :**_ "Ne vous inquiétez pas pour la corde, j'ai déjà envoyé des amis sur le coup. Nous y allons ?"

_**Pepper :**_ "Incroyable..."

_Pepper et Shichimi suivent Coriandre à travers les sous-sols du château._  

_De leur côté, Armoracia et Safran attendent, dissimulées derrière des arbres._

_**Armoracia :**_ "J'espère que Princesse Pepper et Maître Shichimi vont bien !"

_**Safran :**_ "Hum. Dîtes-moi, Princesse : comment êtes-vous tombée chez les Ahrachnerres ? Pepper a dit que vous n'étiez pas là lors de sa dernière visite..."

_**Armoracia :**_ "C'est normal, je ne suis chez eux que depuis très peu de temps ! J'étais perdue et effrayée, toute seule, et ils m'ont accueillie à bras ouverts ! Enfin... à pattes ouvertes ! Je m'efforce de payer ma dette tous les jours, en leur concoctant des repas et en les aidant du mieux que je peux !"

_**Safran :**_ "À bras ouverts... curieux, étant donné leur politique stricte concernant les étrangers, non ?"

_**Armoracia :**_ "Euh... oui, sans doute ! Mais... ils étaient peut-être dans un bon jour, qui sait !"

_**Safran :**_ "De là à vous appeler "Princesse"... j'avoue que quelque chose m'échappe, dans cette histoire !"

_L'air suspect de Safran effraie quelque peu Armoracia, qui ne sait plus où poser le regard._

_**Safran :**_ "En tout cas, vous avez des talents de tisseuse, c'est certain. Un hobbie ?"

_**Armoracia :**_ "Lorsqu'on vit au milieu d'araignées, le tissage est une activité quasi obligatoire ! Et je dois bien avouer que j'adore ça ! J'ai adoré faire vos costumes de marchandes !"

_**Safran :**_ "Oui... vous apprenez vite, c'est sûr. Vous n'avez pas dû tisser pour des humains depuis longtemps, en vivant au milieu d'araignées... votre corde était tout à fait impressionnante, aussi. En si peu de temps..."

_**Armoracia :**_ "M... merci ! Je savais que vous étiez pressées, alors j'ai mis les bouchées doubles..."

_**Safran :**_ "Oui... en parlant de double, c'est une sacrée corde ! Vous aviez vraiment autant de fils d'Ahrachnerre sur vous ?"

_**Armoracia :**_ "Je... j'en ai toujours ! Je ne me rends jamais compte de la quantité que je peux stocker sur moi ! Hé, hé..."

_**Safran :**_ "Il y en avait bien plus que vous ne pouviez en transporter. Bon, ça suffit, je vais être franche avec vous..."

_**Armoracia** (regardant autour d'elle) **:**_ "D'où vient ce bruit ?"

_Safran relâche son attention de la conversation pour aussi regarder autour d'elle. Non loin de leur position, le sol commence à trembler de plus en plus. Safran se lève d'un seul coup._

_**Safran :**_ "Venez derrière moi !"

_Armoracia se précipite derrière Safran, paniquée._

_**Armoracia :**_ "Que... que se passe-t-il, Dame Safran ?"

_**Safran :**_ "On ne va pas tarder à le savoir..."

_Safran prépare des flammes. Mais une immense masse émerge du sol d'un coup, en projetant Safran et Armoracia plus loin. Le sol s'arrête de trembler._

_**Armoracia :**_ "Dame Safran ! Vous n'êtes pas blessée ?!"

_**Safran :**_ "C'est pas vrai ! Ma jupe est toute sale, à présent ! Qui est le malotru qui a osé ?! Qu'il se désigne immédiatement !"

_**Coriandre :**_ "Pas d'inquiétude, Princesse Armoracia ! Quand Safran se plaint, c'est signe qu'elle va bien !"

_La princesse lève les yeux vers l'immense masse responsable de leur chute. Il s'agit d'une excavatrice imposante en ferraille, avec de grandes pinces, sur deux pattes, piloté par Coriandre, accompagnée par une sorte de petite poule noire, ainsi que Pepper et Shichimi qui leur font un signe de main._

_**Armoracia :**_ "Princesse Pepper ! Maître Shichimi ! Vous allez bien ! Et... vous êtes une amie ?"

_**Coriandre :**_ "Tout à fait ! Je m'appelle Coriandre, et voici mon ami Mango ! Enchantée !"

### Scène 2 - Chasse à la sorcière

_Les Ahrachnerres ont été affolées de voir un tel engin descendre dans leur antre. Elles ont cru que des humains venaient reprendre leur territoire ! Heureusement, Armoracia leur a expliqué qu'il s'agissait d'un nouvel allié pour les sorcières.  
Tout en pilotant, Coriandre a pu, à distance, envoyer un ordre aux gardes environnants afin qu'il se retirent de la zone près de l'antre. Pepper n'a jamais été aussi impressionnée par les talents de Coriandre. Et tout ça sans magie...  
Coriandre a rangé son excavatrice dans un coin de la grotte. Quelques Ahrachnerres curieuses se sont mises autour afin de l'observer. Pendant ce temps, Coriandre visite les lieux._  

_**Coriandre :**_ "Pas mal ! Ce sol naturellement gélatineux doit vous convenir parfaitement, votre Altesse !"

_**Reine Ahrachnerre :**_ "Nous vivons ici depuis des siècles. Lorsque les sorcières de Ah se sont accaparé nos terres, nous n'avons rien pu faire pour les en empêcher, et nous avons dû leur obéir sans rouspéter. J'avoue que laisser des sorcières utiliser mon antre comme cachette depuis notre indépendance me laisse perplexe... surtout la sorcière de flammes, là-bas..."

_**Coriandre :**_ "Ne vous inquiétez pas, Altesse. Je connais bien ces sorcières, et jamais elles ne vous ferons le moindre mal. Ni n'essaierons de vous reprendre ou brûler vos terres ! Ce sont de bonnes personnes, vraiment. Vous pouvez me faire confiance."

_**Reine Ahrachnerre :**_ "Je vous fais confiance, Princesse de Qualicity. Je sais que vous vous êtes battue pour que nous soyons libérées du joug de Ah... je vous en suis reconnaissante."

_**Coriandre :**_ "Oh, je n'ai pas fait grand-chose ! Et je suis désolée de vous envahir ainsi..."

_**Reine Ahrachnerre :**_ "Voyez."

_Elles regardent autour d'elles. Pepper et Shichimi discutent avec entrain avec des Ahrachnerres et Armoracia, tandis que Carrot s'amuse avec des Ahrachnerres plus petites. Safran, elle, lutte entre lancer un regard suspect vers Armoracia et ne pas salir ses vêtements avec la gélatine._

_**Reine Ahrachnerre :**_ "J'ai rarement vu mes enfants aussi joyeux de discuter avec des humains. Ne parlons pas d'Armoracia. C'est un véritable bonheur de la voir aussi souriante."

_**Coriandre :**_ "J'imagine que cela n'a pas été toujours le cas..."

_Safran tend subitement l'oreille._

_**Reine Ahrachnerre :**_ "Elle a un passé difficile avec les humains, qu'elle m'a raconté en détails. J'ai accepté de l'accueillir ici afin qu'elle puisse s'épanouir, mais je sens bien que la présence d'autres humains lui manque... vous êtes peut-être la clé de son bonheur."

_**Coriandre :**_ "Je l'espère de tout cœur, Altesse. Et nous allons l'y aider. N'est-ce pas, mon brave Mango ?"

_La petite poule noire de Coriandre caquette de joie en remuant les ailes._

_**Coriandre :**_ "Bon, veuillez m'excuser, Altesse, mais je crois bien que du travail m'attend !"

_**Reine Ahrachnerre :**_ "Faîtes, Princesse, faîtes. J'espère que nos toiles ne vous donneront pas trop de fil à retordre !"

_**Coriandre :**_ "Hé ! Vous êtes une rigolote, vous !"

_**Reine Ahrachnerre :**_ "Oh, vous savez, à mon âge, c'est bien tout ce qu'il me reste..."

_Coriandre lui adresse un sourire avant de se diriger vers Shichimi._

_**Coriandre :**_ "Bon ! Si tu me montrais ta boule de Cristalication ? Il est temps de pénétrer le réseau arachnide !"

_**Shichimi :**_ "Bonne idée ! Veuillez m'excuser."

_Elle rejoint Coriandre vers une galerie isolée. En marchant, cette dernière annonce qu'il serait judicieux de moderniser leur planque. Pepper sourit._

_**Pepper :**_ "Notre groupe prend forme ! Nous avons notre ingénieure, notre herboriste, notre infiltrée... et moi. Je n'ai pas vraiment de talent qui puisse aider, au final..."

_**Armoracia :**_ "Oh, si, Princesse Pepper ! Vous avez créé tout ceci ! Grâce à vous et votre détermination dans votre cause, j'ai pu vous rencontrer, ainsi que toutes vos formidables amies, et je vous en suis reconnaissante ! Un biscuit ?"

_**Pepper :**_ "Euh... peut-être plus tard, hein ? En tout cas, c'est incroyable que Coriandre ait pu vous ramener votre corde ! Et son écran de surveillance !"

_Pepper se rapproche d'un mur où Coriandre a fixé un grand tableau blanc, où sont projetées plusieurs petites images mobiles depuis un appareil branché plus loin à des fils d'Ahrachnerre, surplombé d'antennes._

_**Pepper :**_ "Je n'arrive pas à croire que c'est vraiment ce qui se passe dehors, en temps réel ! C'est comme si on avait un œil partout ! Coriandre est incroyable !"

_**Safran :**_ "Oui, c'est son truc, ces babioles ! Mais en avons-nous vraiment besoin ?"

_Safran s'approche de Pepper et Armoracia. Cette dernière a un regard inquiet en la voyant arriver._

_**Pepper :**_ "Comment ça ? Bien entendu qu'on en a besoin ! On peut surveiller les gardes !"

_**Safran :**_ "Coriandre leur a demandé de rentrer, ils ne sont pas près de revenir par ici, dans ce sol marécageux ! J'ai vraiment l'impression qu'elle s'installe, prend ses aises comme si elle était chez elle, alors que c'est NOTRE planque. Immonde. Recouverte de cette fichue gélatine..."

_**Pepper :**_ "Coriandre est des nôtres, maintenant ! C'est aussi chez elle ! Et même si ses outils prennent de la place, ils nous rendent bien service !"

_**Safran :**_ "Oui... sauf si c'est juste un moyen d'attirer l'attention sur elle..."

_**Pepper :**_ "À t'entendre, Safran, on dirait que tu es jalouse !"

_**Safran :**_ "Moi ? Jalouse de cette tordue du boulon ? Peuh ! Ridicule ! Mon art est bien plus utile et raffiné que ces morceaux de tôles moisis. Remarque, sur cette gélatine abominable, ça fait ton sur ton... beurk, beurk, beurk ! J'ai besoin de prendre l'air !"

_**Pepper :**_ "N'oublie pas que c'est grâce à Coriandre que tu peux sortir en utilisant sa plate-forme mobile plutôt que mon sort d'Anti-Gravitation !"

_**Safran :**_ "Zut ! Voilà ! Allez, viens, Truffel !"

_Truffel abandonne sa surveillance accrue de Carrot se roulant dans la gélatine pour suivre sa maîtresse sur la plate-forme mécanique en bois permettant d'accéder à l'extérieur, en levant le nez... pas trop, pour éviter de marcher dans la gélatine. Pepper a un petit sourire._

_**Pepper :**_ "Elle ne changera jamais... Princesse ? Tout va bien ?"

_Elle a remarqué l'air inquiet d'Armoracia en regardant Safran._

_**Armoracia :**_ "Euh... oui, oui, tout va bien ! Je vais... euh... préparer le repas ! Oui..."

_Elle file dans son coin cuisine. Pepper s'interroge._

_**Pepper :**_ "J'espère qu'elle ne se fait pas trop de souci..."

_Elle reporte son attention sur l'écran, avec un sourire mauvais. Carrot la rejoint._

_**Pepper :**_ "Mon petit Carrot... si avec ça, on n'arrive pas à leur faire entendre raison... je promets d'abandonner la sorcellerie ! On ne peut décidément pas perdre, avec une technologie comme celle-là !"

_Carrot n'approuve pas comme à son habitude. Il regarde l'écran d'un air horrifié._

_**Pepper :**_ "Eh bien, quoi ? Tu as vu un rat ?"

_Carrot lève la patte sur une des images. Pepper regarde, et prend un air horrifié à son tour._

_**Pepper :**_ "Des gardes ! Ils ne ressemblent pas à ceux de Coriandre... oh ! Et Safran qui est dehors ! Vite, Carrot !"

_Ils se précipitent vers la plate-forme pour remonter à la surface. Restant à genoux, Pepper regarde autour d'elle._

_**Pepper :**_ "Ils sont là-bas... heureusement, Safran est de l'autre côté..."

_Elle peut entendre les cris des gardes, au loin. Ils courent après quelque chose, mais pas dans leur direction._

_**Garde :**_ "Halte ! Halte, sorcière ! Au nom du roi Hartru !"

_**Pepper :**_ "Sorcière ?! Ils poursuivent une sorcière ! Vite, Carrot ! Va prévenir Safran et Truffel, qu'elles se mettent à l'abri ! Moi, je vais secourir cette pauvre sorcière !"

_Carrot fait un garde-à-vous avant de se diriger vers Safran. Pepper prend son élan et se dirige vers les arbres où les gardes courent.  
En s'approchant d'eux, elle remarque à quel point ils sont différents des gardes de Qualicity. Ils portent une véritable armure renforcée brillante, avec des épées impressionnantes, une sorte de lion en guise d'emblème sur le torse, des heaumes en forme de tête de lion et des boucliers portant le même emblème._

_**Pepper :**_ "On est sur une autre gamme de garde, là ! Il s'agit de faire attention..."

_Elle les suit en essayant de rester dissimulée derrière les troncs. Elle cherche désespérément la sorcière poursuivie, mais elle ne trouve personne._

_**Pepper :**_ "Elle doit se déplacer en balai... elle va tellement vite ! Hum... il s'agirait de ralentir ces satanés gardes !"

_Pepper se concentre, et prépare un sort de Désordre sur les arbres. L'effet escompté n'est pas là, mais les feuilles remuent suffisamment pour perturber les gardes._

_**Pepper :**_ "Je commence à être rouillée ! On tente le tout pour le tout !"

_Elle sort de sa cachette pour prendre de l'avance sur les gardes. Soudain, elle se sent soulevée dans les airs, puis stabilisée. Elle se rend compte trop tard qu'elle est tombée dans un piège sylvain, un filet composé de lianes très solides !_

_**Pepper :**_ "C'est pas vrai ! Ils sont si habiles que ça, ces gardes ?!"

_Elle remue dans tous les sens, mais impossible de s'en défaire._

_**Pepper :**_ "Oh non ! Il faut vite que je sorte avant que les gardes..."

_**Garde :**_ "Il y a quelque chose, là !"

_**Pepper :**_ "Et zut !"

_Les gardes se réunissent autour de Pepper. Elle sent son cœur battre à toute vitesse._

_**Garde :**_ "Non, ce n'est pas elle ! Peut-on savoir qui vous êtes, jeune fille ?"

_**Pepper :**_ "Et vous, vous êtes qui ?! Je n'ai jamais vu de gardes comme vous !"

_**Garde** (dégainant son épée) **:**_ "Contentez-vous de répondre à la question !"

_L'épée a l'air très tranchante. Pepper déglutit avec difficulté._

_**Pepper :**_ "Je... je..."

_**Autre garde :**_ "Attendez !"

_Il sort un parchemin de sa sacoche qu'il déplie. Plusieurs gardes se réunissent autour de lui, sauf celui qui tient Pepper en respect avec son arme. Ils donnent des coups d'œil alternatifs vers le parchemin, puis vers Pepper._

_**Garde :**_ "Quoi ? Qu'est-ce qu'il y a ?"

_**Autre garde :**_ "Elle ressemble à la sorcière dont notre prince a fait un portrait !"

_**Garde :**_ "Une sorcière ?!"

_Il approche son épée de la gorge de Pepper._

_**Garde :**_ "Est-ce que vous vous appelez Pepper ?! Répondez !"

_**Pepper** (terrorisée, tentant de faire bonne figure) **:**_ "O... oui, c'est bien moi ! Vous... vous croyez me faire peur, hein ?! Eh bien c'est raté ! Je peux savoir qui est votre prince ?! Oh !"

_Elle se souvient alors de Sha-One._

_**Pepper :**_ "C'est ce satané Cletonal, n'est-ce pas ? Sha-One n'a pas pu le raisonner, alors..."

_**Garde :**_ "Tu oses manquer de respect envers son Altesse Cletonal ?! C'étaient là tes dernières paroles, sorcière !"

_Pepper lui lance un regard méprisant. Il est hors de question de perdre la face devant ce garde, même si son heure est venue. Elle ne peut pas s'empêcher de trembler, mais elle ne veut montrer aucun signe de soumission ou de peur, tandis que le garde approche son arme un peu plus près de sa gorge.  
Il est interrompu par un cri. Il lève la tête, tandis que Pepper reprend sa respiration._

_**Garde :**_ "Qu'est-ce que c'était ?"

_**Autre garde** (en panique) **:**_ "Ygauvain a disparu d'un coup ! AAHH !"

_Il disparaît à son tour, comme aspiré sous terre. Le garde retire son épée de la gorge de Pepper et scrute le sol, l'épée brandit. Les autres gardes dégainent aussi, et observent leurs pieds attentivement._

_**Garde :**_ "Faîtes attention ! Il y a quelque chose dans le sol !"

_**Autre garde :**_ "AH !"

_Un autre garde se fait aspirer. Les autres prennent peur._

_**Autre garde :**_ "Il... il faut vite sortir de là ! Sinon on va tous y passer !"

_**Garde :**_ "Vous n'avez aucun courage, ou quoi ?! Vous faîtes honte à l'honneur du roi Hartru ! Trouvez et éliminez la menace, au lieu de fuir comme des lâches !"

_Un autre des leurs se fait aspirer. Les gardes s'enfuient à toutes jambes._

_**Garde :**_ "REVENEZ ICI IMMÉDIATEMENT ! Bande d'incapables ! OSE TE MONTRER, SORCIÈRE ! Je sais que c'est toi qui fais tout ça !"

_Pepper regarde autour d'elle, mais ne voit personne. Ayant une idée du responsable, elle lève les yeux vers les arbres. Et effectivement, elle voit une petite silhouette tapie dans le feuillage, accroupie, prête à bondir._

_**Pepper :**_ "Ne fais pas ça !"

_**Garde :**_ "Quoi ?!"

_La silhouette fond sur le garde à une vitesse inouïe. Le garde n'a pas le temps de réagir et recule. La sorcière pose sa main au sol, et une puissante énergie verte en émane. D'énormes racines jaillissent du sol, et projettent le garde sur un arbre, avant de l'attacher solidement. Son épée et son bouclier sont tombés par terre, et il tente de se débattre._

_**Garde :**_ "LÂCHE-MOI, MONSTRE ! Avant que la colère du roi Hartru ne se retourne contre t..."

_La sorcière utilise son autre main pour faire jaillir une plus petite racine, qui s'enroule autour de la bouche du garde, afin qu'il se taise. On n'entend plus que des gémissements de colère.  
La sorcière se retourne vers Pepper et exécute un geste cassant et bref, comme si elle tranchait l'air. Des feuilles sont projetées vers le haut du piège de Pepper à une vitesse fulgurante, et le rompent d'un seul coup net, comme l'aurait fait l'épée du garde. Pepper tombe au sol, et se relève avec difficulté._

_**Pepper :**_ "Aïe ! Je reconnais là tes manières peu délicates !"

_Elle se tient devant une sorcière au regard peu aimable, ayant la particularité de posséder une queue et des oreilles de raton-laveur._

_**Pepper :**_ "En tout cas, tu m'as sauvé la vie ! Je te remercie, Camomille."

_Le visage de Camomille se fronce de douleur. Elle tombe alors d'un coup sec au sol._

_**Pepper :**_ "Camomille ! Qu'est-ce que tu as ?!"

_Elle se précipite vers Camomille, qui respire avec difficulté. Elle constate que ses vêtements ont été déchirés d'un coup sec, et des tâches de sang les recouvrant peu à peu._

_**Pepper :**_ "Ils t'ont blessée, ces monstres !"

_En colère, Pepper regarde en direction du garde ligoté, et commence à rayonner d'une aura chaotique puissante._

_**Pepper :**_ "Je vais vous faire passer l'envie de nous faire du mal !"

_Une main tire sur son pied. Pepper cesse de rayonner. Elle voit que Camomille utilise le peu de force qu'il lui reste pour l'arrêter._

_**Camomille :**_ "Non... ne fais pas ça... tu vas... tu vas... comme à Hippiah..."

_Pepper a un horrible flash-back. Elle, perdant le contrôle de son trou noir, détruisant Hippiah... elle se reprend vite._

_**Pepper :**_ "Il faut vite te soigner ! Laissons cette pourriture sur son arbre ! Je vais t'emmener auprès de Safran..."

_Elle essaie de transporter Camomille, mais elle éprouve des difficultés à la déplacer._

_**Pepper :**_ "Ne t'inquiète pas, Camomille, on va y arriver !"

_Elle entend des bruits de pas se rapprocher._

_**Pepper :**_ "Non ! Pas encore ! Ne t'en fais pas, je vais te protéger ! À mon tour de te venir en aide !"

_Pepper se prépare à attaquer, mais elle entend une voix familière._

_**Coriandre :**_ "Pepper ! Où es-tu ?"

_**Pepper :**_ "C'est Coriandre ! Ouf ! Coriandre ! Ici ! Il y a un blessé !"

_Coriandre arrive en compagnie de Safran, Shichimi et Armoracia._

_**Coriandre :**_ "C'est... que s'est-il passé ici ? Mes mouches n'ont capté qu'une lueur menaçante au loin."

_**Pepper :**_ "Je vous raconterai plus tard ! Il faut à tout prix soigner Camomille ! Elle est blessée !"

_**Armoracia :**_ "Oh non ! Une autre amie à vous ? La pauvre !"

_**Safran :**_ "Laissez-moi regarder."

_Safran ausculte vite fait Camomille, qui respire de plus en plus lentement._

_**Safran :**_ "C'est très grave. Il faut la ramener dans la planque au plus vite ! Armoracia, peux-tu me donner un peu de fil ? Il faut stopper cette hémorragie..."

_**Armoracia :**_ "Tout de suite !"

_Elle fouille dans sa cape et sort du fil que Safran applique sur les blessures de Camomille en tant que bandage._

_**Pepper :**_ "Ça ira, tu croies ?"

_**Safran :**_ "Le temps de la ramener dans l'antre. J'ai un peu étudié cette gélatine, et malgré son aspect dégoûtant, c'est un coagulant plutôt efficace. Mais assez bavassé ! Aidez-moi à la soulever... doucement..."

_Toutes les sorcières et Armoracia aident Safran à transporter Camomille, qui gémit de douleur._

_**Safran :**_ "Et l'autre abruti, là-bas ? Je suppose que c'est lui qui vous a attaquées... on en fait quoi ?"

_**Coriandre :**_ "Laissons-le ici. Les racines me semblent solides. Je laisse une petite mouche ici pour le surveiller. Allez, allons-y ! Doucement !"

_La troupe transporte Camomille, en espérant ne pas arriver trop tard pour la soigner._

### Scène 3 - Plein les bras

_L'atmosphère est tendue dans l'antre des Ahrachnerres. Toutes les araignées attendent avec une angoisse certaine l'issue de l'intervention sur Camomille. Coriandre a fait hisser un rideau hermétiquement fermé afin que personne n'interfère. Safran et Armoracia sont les seules dans la pièce isolée à tenter de sauver Camomille. Pepper, Shichimi et Coriandre se tiennent devant le rideau, prêtes à obéir aux ordres de Safran._

_**Pepper :**_ "C'est tellement long ! J'ai si peur pour Camomille..."

_**Coriandre :**_ "Ne t'en fais pas, Pepper. Safran sait ce qu'elle fait. On ne devient pas apothicaire par hasard, il faut des bases de médecine pour pratiquer ce métier. Je suis sûre qu'elle va réussir. Elle a toujours été assidue dans tout ce qu'elle a fait. J'ai même entendu dire qu'elle avait appris à cautériser des blessures rien qu'en utilisant le feu ! Mais ce ne sont que des rumeurs... non, Pepper, elle ne va pas brûler Camomille ! J'aurais mieux fait de me taire..."

_**Pepper :**_ "Non, tu as raison : je suis trop peureuse ! Je devrais faire confiance à Safran : elle va réussir ! Quant à ces fichus gardes... qu'ils ne croisent plus jamais ma route, sinon..."

_**Shichimi :**_ "Du calme, Pepper. N'oublie pas que notre mission, c'est de redorer notre image auprès des puissants."

_**Pepper :**_ "MAIS COMMENT VEUX-TU REDORER NOTRE IMAGE FACE À DES BRUTES PAREILLES ?!!"

_Sa voix résonne dans la caverne. Carrot se recroqueville._

_**Pepper :**_ "Pardon... je ne voulais pas... mais je suis tellement en colère !"

_**Shichimi :**_ "Je comprends tout à fait, Pepper, je te jure. Moi aussi, je suis outrée qu'ils aient menacé vos vies à toutes les deux. Mais la haine ne fera qu'engendrer plus de haine... la vengeance n'est pas la bonne solution."

_**Coriandre :**_ "Allez, Pepper, calme-toi. Viens par là."

_Elle prend Pepper dans ses bras._

_**Coriandre :**_ "Respire à fond. Camomille va s'en sortir, et nous aussi. Tu verras."

_Pepper prend de grandes bouffées d'air. Sa rage bouillonne encore en elle, mais elle parvient à retrouver du calme. Elle s'écarte de Coriandre._

_**Pepper :**_ "Merci. Je me sens mieux."

_**Coriandre :**_ "Cette histoire va mettre nos nerfs à rude épreuve ! Il ne faut pas succomber à la colère. On n'en retirerait rien de bon. Il faut rester solidaire et s'entraider. Ensemble, on peut y arriver !"

_Pepper baisse la tête. Carrot bondit sur son épaule et la léchouille._

_**Pepper :**_ "Tu as raison. Avec des amis comme vous tous, rien n'est impossible !"

_Les Ahrachnerres claquent leurs pinces. On dirait une sorte d'applaudissement.  
Le rideau finit par bouger. Les trois sorcières se précipitent sur Safran. Son tablier est tâché de sang._

_**Pepper :**_ "Alors ?"

_**Safran** (retirant son masque) **:**_ "C'est bon, elle est tirée d'affaire. Il faut qu'elle se repose, maintenant. Donc le premier qui ira la déranger aura affaire à moi, c'est bien compris ?!"

_Malgré la menace, une vague de soulagement s'empare de la caverne._

_**Pepper :**_ "Ouf, quel bonheur ! Merci, Safran, du fond du cœur !"

_**Coriandre :**_ "Tu assures, comme toujours !"

_**Safran :**_ "Allons, allons, je sais que je suis géniale, mais quand même !"

_Armoracia se dévoile à son tour._

_**Pepper :**_ "Tout s'est bien passé, Princesse ?"

_**Armoracia :**_ "À merveille ! Dame Safran est un excellent guérisseur ! J'ai appris plein de choses sur le corps humain ! Et j'ai même pu participer aux soins de Dame Camomille !"

_**Safran :**_ "C'est elle qui a recousu ! Comme une pro, les yeux fermés..."

_**Armoracia :**_ "Ce n'est pas très différent du tissage, en fin de compte !"

_**Pepper :**_ "Bravo, Princesse ! Et merci, vous avez aidé à sauver notre amie ! Je ne l'oublierai jamais."

_**Armoracia :**_ "Je vous en prie, ce fut un honneur ! Princesse Pepper a beaucoup d'amies géniales ! Biscuits pour tout le monde !"

_Nouveaux cliquetis. Armoracia se précipite en cuisine._

_**Pepper :**_ "Oui... perso, je m'en passerais bien, hein, Safran ?"

_Safran continue de regarder Armoracia avec suspicion._

_**Pepper :**_ "Safran, pourquoi tu n'arrêtes pas de regarder Armoracia avec ces yeux-là ? C'est en rapport avec la mise en garde que tu m'as faite ?"

_**Safran :**_ "Hum ? Non, rien. Oublie. Tu verras bien, de toute façon !"

_Elle s'éloigne de Pepper pour prendre Truffel dans ses bras. Pepper reste pensive sur cette attitude._

_**Pepper :**_ "Bon... et notre prisonnier, il a bougé ?"

_Elle rejoint Coriandre qui observe l'écran de contrôle. Le garde est toujours attaché à l'arbre._

_**Coriandre :**_ "Non, il n'a pas bougé... satané roi Hartru, toujours aussi extrémiste !"

_**Pepper :**_ "Roi Hartru ? J'ai entendu les gardes parler de lui. Ce sont donc ses gardes..."

_**Coriandre :**_ "Leur blason ne laisse aucun doute."

_**Pepper :**_ "Pourriture... qu'est-ce que tu sais de lui ?"

_**Coriandre :**_ "C'est un véritable tyran. Il règne sur le royaume de Grala, un peu au-delà de l'Union des Technologistes. La seule chose qui l'intéresse, c'est le pouvoir, et rien d'autre. Ah, si : la reconnaissance du pouvoir ! S'il pouvait écraser le monde entier, il le ferait sans hésiter. Il déteste les sorcières depuis toujours : il trouve qu'elles ont trop de pouvoirs et que c'est injuste. Heureusement qu'il n'en a pas l'usage, sinon ce monde serait à feu et à sang ! Il veut imposer sa vision à tout le monde, et utilise toujours des méthodes radicales pour y parvenir. Il a même été jusqu'à demander audience à mon père pour qu'il m'abandonne !"

_**Pepper :**_ "Tu plaisantes ?!"

_**Coriandre :**_ "Oh que non. Et si mon père n'avait pas été diplomate, il y aurait longtemps qu'une guerre aurait éclaté entre nos deux royaumes ! Mais il l'a bien recadré, en précisant que ce n'est pas parce que sa femme et sa fille sont des sorcières qu'il en perd sa légitimité de souverain, et surtout que ça ne le concernait pas ! C'est ce jour-là que mon père a fait redoubler la sécurité autour de ma mère et moi. Malheureusement pour ma mère, ce ne fut pas suffisant..."

_**Pepper :**_ "Non... ne me dis pas que... qu'il a..."

_**Coriandre :**_ "Oh non, rassure-toi. Ma mère est la seule responsable de son malheur. Mon père ne pouvait pas la protéger contre elle-même, et... enfin..."

_Le visage de Coriandre s'assombrit fortement._

_**Pepper :**_ "En tout cas, je déteste ce roi Hartru ! Et ce prince Cletonal !"

_**Coriandre** (se reprenant) **:**_ "Ah ? Tu connais le prince de Grala?"

_**Pepper :**_ "On est tombées sur lui et sa compagne avec Safran durant notre voyage. Elle, elle a voulu nous aider, mais ce malotru..."

_**Coriandre :**_ "Oui... je ne l'ai jamais rencontré, mais les rumeurs vont bon train sur lui. Il est le petit frère du roi, et il se voit bien diriger à sa place... il fait tout pour plaire au roi, mais c'est un incorrigible trouillard ! Je ne suis pas étonnée qu'il ait envoyé ses gardes aussi loin pour poursuivre Camomille... peut-être pensait-il gagner les faveurs de son frère en capturant une sorcière... mais de là à la blesser..."

_**Pepper :**_ "Ils doivent payer ! On ne peut pas laisser passer ça !"

_**Coriandre :**_ "Pepper, tu as entendu Shichimi : la violence ne résoudrait rien... et c'est tout un royaume dont tu parles ! On ne peut pas se permettre de se mettre tout un peuple à dos !"

_**Pepper :**_ "Je sais, mais... grrr ! Je refuse d'aider une telle brute !"

_**Coriandre :**_ "Trouvons déjà comment calmer ses ardeurs envers les sorcières, on avisera après ! D'ailleurs, on pourrait peut-être interroger le garde que Camomille a fait prisonnier... hum, non, jamais il ne nous dirait quoi que ce soit... attendons que Camomille se remette. Elle pourra nous en apprendre plus."

_**Pepper :**_ "Oui.. tu as sans doute raison."

_Pepper n'en pense pas moins. Elle est toujours en colère contre le roi et le prince de Grala._

_**Coriandre :**_ "Alors, Shichimi ? Elle fonctionne, cette boule ?"

_Pepper et Coriandre se dirigent vers Shichimi, absorbée par la boule de Cristalication. Elle fait bouger ses doigts comme une véritable pro : les images dans la boule se succèdent à toute vitesse._

_**Shichimi :**_ "À merveille, Coriandre ! Tu es une véritable fée du réseau !"

_**Coriandre :**_ "Oh, ce n'est qu'un passe-temps ! J'avais une correspondante araignée qui m'a appris deux-trois choses, mais je suis loin d'être aussi douée qu'elle !"

_**Shichimi :**_ "Ce fut largement suffisant ! Merci !"

_**Pepper :**_ "Et sinon ? Tu trouves des infos ?"

_**Shichimi :**_ "Pas grand-chose. Le roi Hartru est doué pour ne laisser filtrer que ce qu'il veut que les gens voient. Mais j'ai une botte secrète pour en apprendre plus."

_**Pepper :**_ "Ah, c'est quoi ?"

_**Shichimi** (avec un clin d'œil) **:**_ "Secret !"

_**Pepper :**_ "Oh, allez ! Dis-le moi, je veux savoir !"

_**Shichimi :**_ "Non, non ! Tu verras bien !"

_Coriandre les regarde avec un sourire. La tension semble s'être dissipée. Même si la situation n'est pas glorieuse, tant qu'elles sont à l'abri...  
Le visage de Coriandre s'assombrit à nouveau. D'un seul coup, elle retourne dans son atelier. Mango la suit en caquetant.  
Safran sort de la chambre de Camomille et se dirige vers Pepper et Shichimi._

_**Safran :**_ "Dîtes ! Ça vous dirait de faire moins de bruit ? On a une patiente qui essaie de se reposer !"

_**Pepper :**_ "Oh, ça va ! On parle pas fort !"

_**Safran :**_ "Il est impératif qu'elle se repose ! Alors fort ou non, vous êtes priées de la fermer ! Sinon elle ne se remettra jamais !"

_**Shichimi :**_ "Elle a l'air bien remise, pourtant. Regarde."

_Elle désigne la plate-forme pour sortir de l'antre. Marchant avec difficulté d'un air déterminé, Camomille tente de s'échapper de l'antre._

_**Safran** (se précipitant sur elle) **:**_ "Non, non, non !! Tu restes allongée, toi ! Tu veux te tuer ?!"

_Camomille grogne en direction de Safran en montrant ses griffes. Safran s'arrête aussitôt._

_**Camomille :**_ "N'approche pas ! Ou ce sera toi l'amochée !"

_**Safran :**_ "Quelle sauvageonne !"

_**Pepper :**_ "Camomille ! Elle t'a sauvé la vie ! Tu pourras faire preuve de reconnaissance et écouter ses conseils !"

_**Camomille :**_ "Je n'ai rien demandé à personne ! Et je m'en serais parfaitement sortie toute seule si tu n'étais pas intervenue ! Tu ne sais toujours pas reconnaître un piège d'Hippiah, c'est pathétique !"

_**Pepper :**_ "Peu importe ! Tu étais poursuivie par des gardes, et tu étais blessée ! Tu comptais fuir encore combien de temps dans cet état ?!"

_**Camomille :**_ "Je me serais soignée toute seule une fois ces gêneurs semés ! Je n'ai pas besoin de vous, moi ! Alors je m'en vais d'ici ! Ce n'est pas ma tanière ! Ma place est dans la forêt !"

_**Pepper :**_ "Les gardes pourraient revenir ! C'est du suicide d'aller dans la forêt maintenant ! Je t'en prie, n'y va pas et reste à l'abri avec nous !"

_**Camomille :**_ "Non ! Bon vent !"

_**Coriandre :**_ "Attends !"

_Camomille stoppe son mouvement. Coriandre est sortie de son atelier._

_**Coriandre :**_ "Explique-nous au moins pourquoi les gardes de Grala te poursuivaient jusqu'ici ! Je te promets que nous ne te retiendrons pas plus."

_**Pepper :**_ "Coriandre, enfin !"

_**Safran :**_ "Elle risque de rouvrir ses blessures si elle part maintenant !"

_**Coriandre :**_ "Mais c'est sa décision. Personne ne vous a retenues lorsque vous avez décidé de vous lancer dans une quête contre la loi anti-magie ?"

_**Safran :**_ "Je suis otage, moi. On m'a forcée à venir, et je n'ai rien dit !"

_**Pepper :**_ "Tu n'as rien dit ? C'est la meilleure..."

_**Coriandre :**_ "Donc si Camomille veut repartir dans la forêt malgré nos mises en garde, libre à elle de faire ce qu'elle veut. Mais tes réponses pourraient grandement nous aider, Camomille. Tu veux bien nous éclairer ? Promis, on te laissera tranquille après."

_Camomille semble réfléchir._

_**Camomille :**_ "Bon. Si ça peut vous faire plaisir, moi je m'en fiche. Tant qu'on ne me retient pas ici."

_**Coriandre :**_ "Promis !"

_Armoracia jaillit de son antre à elle, avec deux assiettes remplies de gâteaux dans les mains._

_**Armoracia :**_ "À table !"

_Elle s'arrête d'un coup en voyant la situation et la tension qui règne dans la pièce._

_**Armoracia :**_ "Euh... tout va bien ? Je n'ai rien dit de mal, au moins ?"

_Quelques instants plus tard, Safran sert le thé à ses amies autour du rocher. Les Ahrachnerres sont toujours réunies, curieuses de connaître l'histoire de Camomille. Cette dernière se trouve face aux autres sorcières, qui la regardent d'un air grave._

_**Safran** (tendant une tasse à Camomille) **:**_ "Tiens. J'ai mis ton médicament dedans."

_Celle-ci reste les bras croisés, le regard détourné._

_**Safran :**_ "Pauvre idiote ! Comment veux-tu te rétablir si tu ne te soignes pas ?"

_**Camomille :**_ "Je n'ai pas besoin de toi et ton médicament pourri ! Je peux me soigner toute seule !"

_Cette remarque vexe profondément Safran._

_**Armoracia :**_ "Dame Camomille, je vous en conjure ! Acceptez ce thé ! Je serais bouleversée s'il vous arrivait malheur dans notre antre !"

_Camomille détourne un peu le regard vers la tasse tendue par Safran. D'un coup sec, elle s'en saisit et la boit d'un trait, avant de la reposer violemment._

_**Camomille :**_ "Voilà. Satisfaites ?"

_**Safran :**_ "EH ! Ma tasse en porcelaine ! C'est une pièce unique, fais un peu attention !"

_**Armoracia :**_ "Vous me faîtes tellement plaisir, Dame Camomille ! Merci !"

_Elle veut l'étreindre mais elle stoppe son mouvement._

_**Armoracia :**_ "Ah, oui... euh..."

_Elle se contente de déposer une assiette de gâteaux sur le rocher._

_**Safran :**_ "Eh ben, il y en a qui en ont, de la chance d'échapper à l'étreinte..."

_**Coriandre :**_ "Bon, Camomille. Raconte-nous ce qu'il s'est passé."

_Camomille soupire profondément. Puis elle se met à parler, sans regarder les autres._

_**Camomille :**_ "Il n'y a pas grand-chose à dire. Après que la loi soit votée, Hippiah a été dissoute. Je me suis mise à voyager, dans le but de visiter les forêts du monde entier, de ressentir l'âme végétale d'autres arbres que ceux que je connaissais, et mon périple m'a menée vers Grala. Dans la forêt de Lorbéciande, j'ai pu voir avec quelle brutalité les gardes de Grala traitaient les animaux qui leur barraient la route pour leur fichue cueillette ! J'ai tenté de les arrêter, mais ils s'en sont vite pris à moi. Leur fichu prince était avec eux, et il m'a vu me servir de ma magie. Il a ordonné qu'on me pourchasse. Je les ai guidés le plus loin possible de la forêt pour que les animaux puissent être tranquilles, et je voulais les amener jusque dans la forêt d'Hippiah, où j'avais installé tous mes pièges. Mais ils sont parvenus à me blesser, et... vous connaissez la suite..."

_Un profond choc saisit l'assistance._

_**Pepper :**_ "Je savais bien que c'étaient des monstres ! Oser s'en prendre aux animaux ! Quelle honte ! Je sais pas ce qui me retient de..."

_**Camomille :**_ "Arrête de faire semblant d'être choquée ! Tout le monde se fiche des animaux et des plantes, toi la première ! Tu sais de combien d'animaux tu as détruit le territoire, en broyant Hippiah avec ton trou noir ?! Sans parler de votre politique désastreuse d’enterrer vos échecs ! C'est une catastrophe écologique sans précédent ! Alors ne joue pas les écologistes, je sais très bien que tu t'en fiches, comme les autres !"

_**Pepper :**_ "C'est... c'est pas vrai ! J'ai réussi à faire prendre conscience à mes marraines qu'enterrer nos échecs était mal, et on recycle maintenant ! Enfin, on recyclait, tant que Chaosah existait... mais même ! Quant au trou noir... tu sais pertinemment que c'était un accident, et que je regrette profondément ce que j'ai fait ! Tu n'as pas le droit de dire que je me fiche du sort des plantes et des animaux !"

_Mais Pepper se rappelle bien le résultat de son excès de colère, juste avant de partir en voyage. Les arbres autour d'elle étaient détruits, et elle ne s'y est pas plus intéressée que ça..._

_**Camomille :**_ "Vous êtes tous pareils, de toute façon ! Vous, à Qualicity, vous êtes les plus grands pollueurs qu'on ait jamais vu ! N'est-ce pas, Coriandre ?!"

_Coriandre baisse la tête._

_**Coriandre :**_ "Les recherches scientifiques ont un sérieux coût environnemental, c'est vrai..."

_**Camomille :**_ "On ne parle pas de Magmah, bien sûr, qui a brûlé plus de forêts qu'on ne pourrait l'imaginer !"

_**Safran :**_ "Eh ! C'était avant, ça ! Nous ne sommes plus des pyromanes maintenant ! Et puis, de toute façon, j'ai quitté Magmah, alors..."

_**Camomille :**_ "Et les sorcières de Ah... elles essaient de se donner une bonne image, mais tout le monde sait que vos plantes tiennent debout grâce à vos esprits ! C'est contre-nature, ça m'écœure au plus haut point !"

_Shichimi continue de jouer avec sa boule et ne répond pas._

_**Camomille :**_ "Toutes, ici, vous vous fichez éperdument de l'environnement et de la nature... et vous voulez me faire croire que ma santé vous préoccupe ?! Foutaises ! Vous vous en fichez comme du reste ! Je sais très bien comment vous me voyez, comment vos regards me jugent lorsqu'ils voient ma queue d'animal ! Je ne suis qu'une bestiole parmi d'autres pour vous, une autre dont on se fiche ! C'est simplement pour vous donner bonne conscience que vous voulez m'aider ! Vous me faîtes VOMIR ! Toutes ! Alors si vous voulez bien m'excuser, je retourne chez moi, maintenant ! Et j'empalerai celui qui m'en empêchera avec une racine !"

_Impuissantes, les sorcières regardent Camomille se lever avec une grimace de douleur, boiter vers la plate-forme. Personne n'ose dire un mot.  
Puis soudain, Safran se lève._

_**Safran :**_ "Eh, Princesse !"

_Coriandre et Armoracia tournent la tête vers Safran, mais cette dernière s'adresse bien à Armoracia._

_**Safran :**_ "Attrapez !"

_Elle lance trois assiettes dans trois directions différentes. Paniquée, Armoracia attrape une, deux... et trois assiettes avec ses bras. Camomille a aussi regardé, et elle ne peut cacher sa stupéfaction._

_**Safran :**_ "Le moment semble bien choisi pour révéler votre supercherie, Princesse !"

_Toutes les sorcières se lèvent. Les Ahrachnerres commencent à bouillir._

_**Pepper :**_ "Une supercherie ? Mais de quoi tu parles, Safran ? Rattraper des assiettes avec ses mains et son pied n'est pas si bizarre ! Elle est juste adroite, c'est tout !"

_**Safran :**_ "Un peu trop, à mon goût ! Voilà plusieurs jours que je vous trouve suspecte, Princesse ! Vous vous cachez dans votre antre pour cuisiner et tisser, comme si vous vouliez cacher quelque chose... simple stress d'être observée ? Pas uniquement ! La corde pour grimper le château de Qualicity était bien trop longue pour avoir autant de fils d'Ahrachnerre sur vous pour la fabriquer ! À moins que vous soyez capable d'en générer vous-même... les Ahrachnerres vous accueillent à bras ouverts malgré leur haine envers les étrangers, curieux, n'est-ce pas ? Votre vitesse pour préparer les repas est ahurissante, et tout à l'heure, pour sauver celle-là, vous avez été capable de tenir les plaies à deux mains et me passer un scalpel ! Et même là, à l'instant ! Vous êtes arrivée avec deux assiettes en main, mais en voici trois qui sont vides ! Comment avez-vous pu en transporter une troisième ? Tout comme vous venez de rattraper les trois assiettes que j'ai lancées ! Allez, il est temps de vous dévoiler !"

_Safran claque des doigts. Une toute petite gerbe de flamme touche l'attache de la cape d'Armoracia, la faisant tomber à terre. Armoracia pousse un cri et laisse tomber les assiettes qui se brisent. Elle se protège avec ses bras._

_**Safran :**_ "Princesse des araignées !"

_Tout le monde, y compris Camomille, est choqué. Ils constatent qu'Armoracia se protège avec beaucoup plus de bras que prévu._

_**Pepper :**_ "Un, deux... trois, quatre... cinq, six ?!"

_**Coriandre :**_ "Quelle surprise ! Elle est à moitié araignée !"

_En effet, Armoracia possède six longs bras fins. Ce qui lui fait un torse plus grand que la moyenne._

_**Safran :**_ "Et ce n'est pas tout !"

_Elle s'approche d'Armoracia et soulève sa longue mèche sur son front._

_**Safran :**_ "Elle a quelques mirettes en plus !"

_En effet, deux autres paires d'yeux, restés fermés, sont présentes derrière la mèche écartée par Safran._

_**Safran :**_ "Elle a été capable de recoudre la plaie de Camomille tout en fermant les yeux par réflexe, parce qu'elle craint la vue du sang !"

_Elle relâche la mèche d'Armoracia. Cette dernière, en état de choc, regarde toutes les sorcières tour à tour, qui fixent ses bras et sa mèche. Ses yeux se remplissent de larmes._ 

_**Armoracia :**_ "Je vous en prie ! Cessez de me regarder comme ça !"

_Elle s'effondre en larmes, à genoux. Les Ahrachnerres font claquer leurs pinces avec rage._

_**Ahrachnerre 1 :**_ "L'horrible sorcière de flammes a fait pleurer la princesse !"

_**Ahrachnerre 2 :**_ "Notre princesse est splendide ! Arrêtez de la regarder comme un monstre !"

_**Ahrachnerre 3 :**_ "Ces sorcières ne font qu'apporter le désastre et le malheur sur nous ! Chassons-les !"

_Les Ahrachnerres commencent à avancer, menaçantes, vers les sorcières, qui reculent._

_**Pepper :**_ "Attendez ! On ne pense pas qu'elle est un monstre !"

_**Coriandre :**_ "Nous avons juste été surprises, c'est tout !"

_**Safran :**_ ""L'horrible sorcière de flammes" ! Eh, j'ai un nom, bande de malpropres !"

_**Pepper :**_ "Safran, ce n'est pas le moment de les insulter !"

_**Safran :**_ "Un pas de plus, et je vous fais tous fondre, c'est compris ?!"

_Elle menace les Ahrachnerres avec une flammèche. C'est suffisant pour les faire reculer un peu._

_**Pepper :**_ "Arrête tout de suite, Safran !"

_**Safran :**_ "Ce sont elles ou nous, Pepper ! Je te rappelle ce qui nous attend, dehors ? Hors de question qu'on se fasse jeter !"

_**Ahrachnerre :**_ "Vous êtes mauvaise ! Notre reine vous fera payer !"

_Un brouhaha commence à émerger des deux côtés. Pepper ne sait plus quoi faire pour arranger la situation._

_**Armoracia :**_ "ARRÊTEZ !"

_Le silence revient. Armoracia, toujours en pleurs, se relève._

_**Armoracia :**_ "Je ne veux pas que vous vous disputiez ! *Snif !* Pas au moment où je voyais une amitié naissante entre nous ! *Snif !* Je vous en prie, ne vous battez pas !"

_Safran éteint sa flammèche, bouche bée, la tête tournée vers Armoracia. Il semble que ce fut suffisant pour calmer les ardeurs. La princesse s'effondre, laissant tomber ses six bras sur ses genoux._

_**Armoracia :**_ "Les Ahrachnerres m'ont accueillie à bras ouverts... *snif !* ...parce que je suis en partie de leur sang ! *Snif !* Et parce que *Snif !* tout le monde à la surface *Snif !* me trouvait monstrueuse *Snif !* et avait peur de moi *Snif !* ! J'étais toute seule, perdue... *Snif !* Je ne veux plus qu'on me regarde comme ça ! *Snif !* J'ai tout fait pour paraître humaine, mais *Snif !* ça n'a pas été suffisant... *Snif !* Je... je ne me supporte plus ! *Snif !* Je voudrais tellement être humaine *Snif !*, rendre les gens heureux *Snif !* mais sous cette apparence, je ne peux rien faire pour eux ! *Snif !* J'ai honte de ce que je suis... *Snif !* je... je me déteste !!!"

_Elle part dans une crise de larmes intense, qui ne laisse pas nos sorcières de marbre. Safran est toujours choquée. Pepper voudrait tenter une approche pour la rassurer, mais elle a peur d'une réaction excessive._

_**Pepper :**_ "Princesse... je..."

_Mais aussi furtivement que le vent, une forme bondit sur Armoracia, et lui donne une violente claque qui la fait s'effondrer par terre. Tout le monde est choqué._

_**Pepper :**_ "Princesse Armoracia !"

_Les Ahrachnerres s'agitent, paniquées. Armoracia, une main sur la joue, regarde, surprise et confuse, le responsable de cette claque. Elle n'en revient pas qu'il s'agisse de Camomille. Debout, en colère, elle en a presque oublié sa blessure. Armoracia s'est arrêtée de pleurer._

_**Coriandre :**_ "CAMOMILLE ! Qu'est-ce qu'il t'a pris de faire ça ?!"

_**Camomille** (ignorant Coriandre) **:**_ "VOUS VOUS DÉTESTEZ ?! VOUS AVEZ HONTE DE VOUS ?!! Oui, vous pouvez avoir honte ! Mais pas de votre apparence, de votre comportement ! C'est totalement inacceptable d'entendre des bêtises pareilles !! Vous voulez être humain ?! Mais c'est LA PIRE ESPÈCE QUI EXISTE SUR CETTE PLANÈTE ! Ils sont vils, cruels, irrespectueux de leur environnement ! Et vous voulez devenir comme eux ?! Qu'est-ce qui ne tourne pas rond, chez vous ?! Qui voudrait être comme eux ?! Certainement pas moi ! Je suis différente, et alors ?! Je m'en fiche pas mal ! Au contraire, j'ai de la chance d'être différente ! Je remercie le ciel chaque jour de m'avoir faite comme je suis ! Je suis unique, et vous aussi, vous êtes unique ! Vous êtes un être vivant exceptionnel, chargé d'une moitié de compassion naturelle envers votre environnement ! Vous êtes gentille, souriante, dévouée envers vos amis !! Vous êtes une perle rare ! Alors qui s'en fiche de ce qu'on pense de vous, de votre apparence ?! C'est le cœur, le plus important, et le vôtre est plus gros que tous ceux qui se trouvent ici ! Je suis plus que ravie que vous ayez participé à ma guérison ! Vous êtes talentueuse, un être en or ! Alors pour rien au monde il ne faut souhaiter devenir aussi sombre, stupide et égoïste qu'un humain, c'est clair ?! Pour rien au monde il faut vous détester ! N'ayez pas honte de ce que vous êtes ! Parce que vous êtes un soleil pour votre entourage. Et personnellement, je vous trouve magnifique !"

_Les Ahrachnerres encouragent ces paroles._

_**Ahrachnerre 1 :**_ "Oui, elle a raison ! Vous êtes magnifique, Princesse !"

_**Ahrachnerre 2 :**_ "Vous nous apportez du bonheur tous les jours ! On est très contents de vous avoir avec nous !"

_**Ahrachnerre 3 :**_ "VIVE LA PRINCESSE ARMORACIA !"

_Les cliquetis se multiplient pour célébrer la princesse. Armoracia est si émue qu'elle ne sait même pas comment réagir._

_**Armoracia :**_ "Mes amies... vous... vous..."

_Camomille lui tend la main._

_**Camomille :**_ "Tenez-vous debout, soyez fière de ce que vous êtes ! Et ne baissez pas les bras ! Enfin, si jamais vous les baissez, vous en avez d'autres, vous, au moins !"

_Armoracia pousse un petit rire et se relève. Elle se met face à ses amies Ahrachnerres._

_**Armoracia :**_ "Mes amies... si seulement je pouvais exprimer la moitié de la reconnaissance qui m'anime en ce moment ! Je vous aime, toutes ! Merci de tout cœur !"

_Les Ahrachnerres sautillent de joie. La reine, restée au fond de la caverne, est satisfaite de l'issue qu'ont pris les événements. Les sorcières sont ravies, elles aussi. Surtout de l'intervention de Camomille, aussi inattendue qu'opportune._

_**Pepper :**_ "Finalement, Camomille n'est pas si froide envers tout le monde... regardez-la sourire !"

_**Coriandre :**_ "Oui... et mon petit doigt me dit que Safran a fait ça justement pour que Camomille reste avec nous un peu plus longtemps."

_**Pepper :**_ "Tu crois ? Elle savait que Camomille serait touchée par la situation de la princesse ? C'est vrai, Safran ? Safran ?"

_Safran est toujours bouche bée en regardant Armoracia._

_**Pepper :**_ "Eh bien, Safran ? Ne me dis pas que ta propre révélation te bouleverse !"

_Comme un boulet de canon, Safran se rue sur Armoracia. Celle-ci se protège avec ses bras._

_**Pepper :**_ "SAFRAN ! Qu'est-ce que tu fais ?"

_**Ahrachnerre :**_ "N'approchez plus de la princesse !"

_Les Ahrachnerres commencent à se ruer vers Safran._

_**Coriandre :**_ "Attendez ! Ce n'est pas ce que vous croyez !"

_Pepper lance un regard interrogateur vers Coriandre. Les Ahrachnerres aussi, qui ont stoppé leur course. Même Camomille, d'un geste, leur demande d'arrêter._

_**Pepper :**_ "Qu'est-ce que..."

_**Safran :**_ "Princesse."

_Armoracia baisse un peu les bras, en attendant la suite. Un large sourire se dessine sur le visage de Safran._

_**Safran :**_ "Mais vous êtes RESPLENDISSANTE, ma chérie !"

_Elle commence à regarder la tenue d'Armoracia en détail. Pepper et les Ahrachnerres sont dépitées devant une telle attitude. Coriandre étouffe un petit rire. Armoracia, confuse, ne sait pas quoi répondre. Elle essaie de retirer ses bras lorsque Safran passe à proximité._

_**Armoracia** (toute rouge) **:**_ "Euh... vous trouvez ?"

_**Safran :**_ "Absolument ! Je savais que vous aviez la fibre artistique en voyant votre cape, mais là, je dis oui, oui, et oui ! Regardez-moi ce chemisier magnifique, ce petit bas qui souligne la finesse de vos jambes ! Et ces rubans, quelle classe ! Quel raffinement ! Vous avez parfaitement su mettre en valeur votre particularité physique, darling ! Vous êtes un génie !"

_**Armoracia :**_ "Oh, Dame Safran ! Puis-je..."

_**Safran :**_ "Certainement pas ! C'est moi qui vous embrasse !"

_Safran prend Armoracia dans ses bras. Ravie, la princesse l'entoure de ses six bras. Les Ahrachnerres claquent des pinces, et les sorcières applaudissent._

_**Armoracia :**_ "Oh, c'est sans doute le meilleur câlin que j'ai eu de ma vie !"

_**Safran :**_ "Et moi le plus bizarre ! C'est perturbant qu'autant de bras vous enlacent ! On dirait qu'une créature va vous manger ! Mais non, je plaisante ! Vous êtes adorable, darling !"

_Tout monde rit. Même Camomille conserve son sourire._

_**Coriandre :**_ "Voilà une magnifique façon de clore le problème !"

_**Pepper :**_ "Oh oui ! On a gagné une alliée et la princesse se sent mieux dans sa peau ! C'est génial ! N'est-ce pas Carrot ?"

_Mais ce dernier est occupé avec Truffel. Visiblement, il a vanté sa beauté comme vient de le faire Safran avec Armoracia, et Truffel n'est pas insensible. Mango regarde tout ceci avec une certaine lassitude._

_**Shichimi :**_ "Tout est bien qui finit bien. Mais où est Messy ? Je ne l'ai pas vu depuis longtemps..."

_**Pepper :**_ "C'est vrai, ça ! Je l'ai oublié avec tout ça ! Messy ! Où es-tu caché ?"

_Un petit être chaotique descend du plafond où il s'est réfugié, avec son râle habituel._

_**Pepper :**_ "Toujours au même endroit, celui-là... il se fiche pas mal de ce qui se passe en bas !"

_**Coriandre :**_ "C'est quoi, ça ?"

_**Pepper :**_ "Celui qui nous a menées ici. Mais j'ignore tout de lui, ce qu'il est, ses pouvoirs... ce qu'il mange... je sais juste qu'il soutient notre cause et veut nous voir réussir !"

_**Coriandre :**_ "Curieux, comme bestiole ! Je n'en ai jamais vues, des comme ça..."

_**Pepper :**_ "Et pourtant, c'est un peu notre mascotte ! Bon ! S'il vous plaît !"

_La grotte retrouve son calme et écoute Pepper._

_**Pepper :**_ "Ce qui vient de se passer est très représentatif du monde que nous cherchons à développer ! Un endroit où tout le monde s'entraide, se soutient, et se respecte ! Et lorsque nous aurons réussi à rétablir la magie, nous nous occuperons de tous ceux qui cherchent à le corrompre par égoïsme, pour leur profit personnel ! Notre action ne fait que commencer ! Camomille. Je sais que nous ne sommes pas un exemple, mais ne veux-tu pas contribuer à construire un monde où la princesse Armoracia n'aura plus jamais peur de se montrer telle qu'elle est ?"

_Camomille réfléchit. Elle regarde Armoracia, qui lui lance un regard rempli d'espoir._

_**Camomille :**_ "C'est utopique, Pepper. Un tel monde n'existera jamais. Cependant... entre ne rien faire et agir, je préfère me battre pour ce que je crois. Alors j'accepte, Pepper. Mais attention ! Je le fais uniquement pour la princesse Armoracia, pas pour vous."

_**Pepper :**_ "Ça me convient ! Bienvenue parmi nous, Camomille ! Je propose une petite fête pour célébrer ton arrivée, et pour célébrer la princesse Armoracia aussi ! Et puis Coriandre, naturellement... enfin, tous, quoi !"

_Tout le monde semble d'accord. Armoracia, Safran et Camomille se hâtent en cuisine. Pepper les regarde faire, fièrement, en se disant que, après tout, elle arrivera certainement à faire revenir la magie avec des amies aussi géniales._

### Scène 4 - Soirée entre filles

_La fête bat son plein. Enfin libre de ne plus se cacher, Armoracia paraît encore plus détendue et joyeuse qu'avant. Elle put faire une démonstration de ses talents de tisseuse en direct, devant des sorcières impressionnées de voir avec quelle dextérité et vitesse elle utilise ses six bras. Elle parvint à créer des tenues de soirée pour toutes les sorcières, et même des accessoires pour chaque animal. Carrot fut émerveillé de voir à quel point Truffel porte bien les broches. La petite cravate de Carrot ne la laissa pas indifférente. Mango se battit toute la soirée avec son nœud papillon.  
Chacune pu mettre sa robe de princesse, et Safran poussa tout le monde à défiler sur une scène construite par Coriandre. Officiellement, pour s'amuser. Mais Pepper comprit assez vite qu'elle voulait prouver que sa tenue était la meilleure. Même Camomille défila, à contrecœur. Au final, c'est Armoracia qui remporta la victoire.  
Pepper n'avait pas autant ri depuis des jours. Elle se surprit à dire pendant la soirée :_  

_**Pepper :**_ "Ah... si seulement ça pouvait durer toujours !"

_Mais la loi revenait dans sa tête aussitôt, et calmait un peu son enthousiasme. Elle a une mission à accomplir. Et une fois son objectif atteint... pourrait-elle faire d'autres fêtes comme celle-ci, avec ses amies ?  
Mais ses pensées sont de courte durée. Safran propose un jeu de vérité._  

_**Safran :**_ "Bien ! On commence par Pepper ! Est-il vrai que tu as bu le flacon d'urine de ton chat par mégarde un jour ?"

_**Pepper :**_ "Mais... comment tu as su ça, toi ?!"

_**Safran :**_ "Je vois tout, ma petite Pepper ! Alors ? Tu préfères un gage ?"

_**Pepper :**_ "Non ! C'est... c'est bien la vérité ! Mais c'est ma marraine Thym qui a inversé les flacons par erreur !"

_**Coriandre :**_ "Beurk ! C'est pas de chance !"

_**Safran :**_ "Bon ! Au tour de Shichimi ! As-tu dévoré ton ami Yuzu en venant ici ?"

_**Pepper :**_ "C'est vrai ! Je n'ai pas vu ton petit renard depuis que nous sommes ici !"

_**Shichimi :**_ "Plutôt facile, celui-là ! Non, je l'ai simplement laissé à la maison. Je ne me voyais pas l'emmener jusqu'ici pour discuter avec les Ahrachnerres. Après, je ne me doutais pas que j'y resterais aussi longtemps..."

_**Pepper :**_ "D'ailleurs, tu venais leur parler de quoi ?"

_**Shichimi :**_ "Hep, hep, hep ! C'est plus d'une question, ça ! Passe ton tour !"

_**Pepper :**_ "Argh ! Attends un peu que ce soit mon tour de poser des questions !"

_**Safran :**_ "Camomille ! Ton père est réellement un raton-laveur ! Vrai ou faux ?"

_**Camomille :**_ "Laisse mes parents tranquilles !"

_Cela jette un froid. Camomille a répondu instantanément, alors qu'elle ne s'est même pas mise en cercle avec les autres. Personne n'ose regarder quiconque._

_**Camomille :**_ "Euh... je veux dire, je n'en sais rien. Je n'ai jamais connu mes parents..."

_**Safran :**_ "Ouh ! Il fait froid, tout à coup ! Réchauffons un peu tout ça ! Coriandre ! Tu vas te marier la semaine prochaine !"

_**Coriandre :**_ "QUOI ?! C'est totalement faux ! Jamais personne n'est venu me demander ma main !"

_**Pepper :**_ "Allez ! Il y a bien dû y avoir un joli prince qui a essayé de te conquérir, non ?"

_**Coriandre** (rougissant) **:**_ "Jamais, je vous dis !"

_**Safran :**_ "Ha, ha ! Elle rougit ! Mais la question a été posée, je laisse les autres compléter plus tard !"

_Safran se rassoit. Coriandre plonge son visage dans ses mains._

_**Armoracia :**_ "Dame Safran a l'air d'aimer jouer les maîtres du jeu !"

_**Pepper :**_ "Elle aime qu'on la regarde, mais elle ne veut pas l'avouer !"

_**Safran :**_ "Hep, hep ! Pas de messes basses, là-bas ! On dévoile tout devant tout le monde !"

_**Pepper :**_ "Justement ! Safran, c'est ton tour ! La scène te manque terriblement ! Tu meurs d'envie de refaire tes spectacles !"

_Safran détourne le regard._

_**Pepper :**_ "Aaah ! C'est la vérité !"

_**Safran :**_ "C'est totalement faux, ma petite Pepper ! J'ai un travail, maintenant ! Je n'ai plus le temps pour ces foutaises !"

_**Armoracia :**_ "Oh, j'aimerais tellement voir un de vos spectacles, Dame Safran ! Ce doit être magnifique !"

_Safran rougit._

_**Pepper :**_ "Hum, je ne suis pas convaincue par ta réponse, Safran ! Voici une autre question : es-tu amoureuse du prince Acren ?

_**Coriandre :**_ "Oh ! On va dans les gros potins, là !"

_**Safran :**_ "Quoi ?! Absurde ! Il s'agit de mon souverain, quand même !"

_**Armoracia :**_ "Ha, ha ! Dame Safran est aussi rouge qu'une flamme !"

_**Safran :**_ "Balivernes ! Je suis normale ! Et je ne suis pas amoureuse du prince Acren ! Fin de la discussion ! Et arrête de t'acharner sur moi, il y a d'autres participantes !"

_Les sorcières rient de bon cœur._

_**Armoracia :**_ "Eh, eh ! Princesse Pepper ! Vous pouvez me poser une question, à moi aussi ?"

_**Pepper :**_ "C'est vrai, Safran vous a oubliée ! Voyons... je suis sûre que vous aimez secrètement quelqu'un !"

_**Armoracia** (cachant son visage) **:**_ "Oh, non, Princesse Pepper ! C'est beaucoup trop gênant !"

_**Pepper :**_ "Ah, c'est donc vrai ! Qui est-ce ?"

_**Armoracia :**_ "Non, je ne peux pas !"

_**Pepper :**_ "Ah, c'est donc un gage ! Voyons... vous allez devoir jongler avec douze assiettes pendant une minute !"

_**Armoracia :**_ "Jongler ? Douze assiettes ?! Je n'y arriverai jamais !"

_**Pepper :**_ "Si vous vous défilez, vous devrez donner le nom de votre amoureux !"

_**Armoracia :**_ "Oh non !"

_Armoracia ramasse les assiettes, sous les encouragements des sorcières. Puis elle commence à en lancer une, puis deux... puis quatre, puis huit... puis douze, avec une dextérité incroyable, sans en croiser une seule. Pour finir, elle en attrape deux dans chaque main._

_**Armoracia :**_ "Ta-da !"

_Les sorcières applaudissent. Même Camomille, qui reste dans son coin._

_**Pepper :**_ "Une véritable artiste ! Vous pourriez avoir une place dans une troupe itinérante !"

_**Armoracia :**_ "Vous... vous croyez ? Mais les gens... ils fuiraient devant moi..."

_**Safran :**_ "Détrompez-vous, darling. Dans ce genre de troupe, les spectateurs se fichent totalement de l'apparence. C'est la performance qui les intéresse le plus. Vous auriez de l'avenir là-dedans."

_**Armoracia :**_ "Ah ! J'y réfléchirai, alors !"

_**Pepper :**_ "Ne viens pas me dire que la scène ne te tente plus, après ça, Safran !"

_**Safran :**_ "Sottises !"

_**Pepper :**_ "Bon, Coriandre ! Alors, ce prince ? C'est qui ?"

_**Coriandre** (le visage écarlate) **:**_ "Mais il n'y a pas de prince ! Et je suis beaucoup trop jeune pour me marier !"

_**Pepper :**_ "Allons ! Je suis sûre que ton père serait aux anges si tu te mariais ! Et ta mère l'aurait été, aussi !"

_Le visage de Coriandre s'assombrit._

_**Coriandre :**_ "Oui... enfin, si elle s'était intéressée à moi... excusez-moi un instant. Je dois... vérifier un truc."

_Elle se lève pour aller dans son atelier._

_**Pepper :**_ "Mince... je n'aurais peut-être pas dû..."

_**Safran :**_ "Pourquoi cette réaction ? Que s'est-il passé ?"

_**Pepper :**_ "Coriandre m'a confié que sa mère avait provoqué son propre malheur... étant donné la famille très orientée sur la recherche, je suppose que sa mère était aussi un génie, et... qu'une de ses expériences a mal tourné..."

_**Armoracia :**_ "Oh, pauvre Princesse Coriandre ! Elle qui est aussi douce qu'une maman..."

_**Pepper :**_ "Oui, c'est triste... mais elle a toujours son père, et elle a connu sa mère... moi, je ne les ai jamais connus, comme Camomille. Et toi, Safran ?"

_**Safran :**_ "Ma mère est partie très loin pour développer son business sur l'astrologie. Mon père... ce pauvre lâche l'a laissée tomber lorsqu'il a appris ses projets."

_**Pepper :**_ "Ouch. Dur !"

_**Safran :**_ "Qu'il ne croise pas ma route, c'est tout ce que je lui souhaite."

_**Pepper :**_ "Shichimi ?"

_**Shichimi :**_ "Hum ? Oh, mes parents se portent bien. Ils vivent une existence paisible, et... ma mère adorait me raconter des histoires quand j'étais petite. Mais... elle aime toujours en raconter, d'ailleurs ! Hé, hé... non, ils vont bien. Ils sont horlogers tous les deux."

_**Pepper :**_ "C'est un beau métier ! Mais ta maman n'était pas sorcière ?"

_**Shichimi :**_ "Elle a tout arrêté pour s'occuper de moi. Et aider papa dans son travail, qui la passionnait aussi. Le mécanisme d'une horloge a toujours été fascinant à décortiquer."

_**Pepper :**_ "Je veux bien te croire ! C'est bizarre, tu avais l'air si proche de Wasabi, je pensais que..."

_**Shichimi :**_ "Ce n'est pas parce que j'aimais bien mon Maître que je n'ai pas de famille, Pepper !"

_**Pepper :**_ "Oh, du calme, Shichimi, je ne voulais pas..."

_**Shichimi :**_ "Eh bien tu as ! Excusez-moi."

_Shichimi se lève et sort dehors._

_**Safran :**_ "Et une de moins... y a pas à dire, tu es douée pour te faire des amies, et rassembler les gens !"

_**Pepper :**_ "Oh, tais-toi, toi ! Madame Je-Veux-Paraître-Plus-Que-Ce-Que-Je-Suis-Et-Je-N'Assume-Pas !"

_**Safran :**_ "Bien, il se fait tard ! On devrait aller se coucher ! La fête est terminée !"

_Safran, vexée, prend Truffel, qui regardait Carrot faire quelques pas de danses, et retourne dans son espace personnel. Pepper contemple la salle, désormais vide. À part Armoracia, et Camomille dans un coin._

_**Pepper :**_ "C'est marrant... la seule sorcière qui reste est celle qui m'aime le moins ! Hein, Camomille ?"

_Camomille tourne une tête blasée vers Pepper, puis se lève lentement pour sortir dehors aussi._

_**Pepper :**_ "Et quatre sur quatre... bravo, Pepper. Toi qui voulais une équipe, c'est bien parti !"

_**Armoracia :**_ "Vous avez touché des points sensibles, Princesse Pepper. Vous devriez peut-être aller vous excuser. Ce n'est pas bon de laisser envenimer les choses. Je suis bien placée pour le savoir !"

_**Pepper :**_ "Tu as raison. Je présenterai mes excuses demain à tout le monde ! Allons nous coucher. Carrot ! On va dormir."

_Carrot se dirige vers Pepper pour aller dans leur espace personnel. Armoracia s'occupe de ramasser les assiettes de cette caverne qui s'est bien refroidie.  
Plus tard dans la nuit. Coriandre émerge de l'antre, avec une petite loupiote sur la tête. Elle s'étire et prend une grande bouffée d'air frais. Son visage est tout sale, et elle porte sa salopette de travail. Mango se trouve sur son épaule._

_**Coriandre :**_ "On est sur la bonne voie, Mango. Mais il faut savoir faire des pauses... oh ! Qui va là ?"

_Elle pointe sa loupiote vers la silhouette. Il s'agit de Camomille._

_**Camomille :**_ "Tu peux baisser ta lampe, s'il te plaît ?!"

_**Coriandre :**_ "Oh, pardon !"

_Elle baisse sa lampe. Elle s'approche de Camomille et s'assoit à côté d'elle._

_**Coriandre :**_ "Comment va ta blessure ?"

_**Camomille :**_ "Mieux."

_**Coriandre :**_ "Bien. Tu prends bien le traitement de Safran, au moins ?"

_**Camomille :**_ "Oui, oui."

_**Coriandre :**_ "Bon. Et sinon, ça va ? Tu t'intègres bien ? J'ai vu que tu restais dans ton coin, ce s..."

_**Camomille :**_ "Bon, tu as fini, avec tes questions ? T'es pas ma mère !"

_Un petit silence s'élève sur le marécage._

_**Camomille :**_ "Pardon."

_**Coriandre :**_ "Oh, ne t'inquiète pas, je comprends. J'ai tendance à trop m'inquiéter pour les autres. C'est amusant. Je me comporte comme une maman, alors que j'en ai pas eue..."

_**Camomille :**_ "Hein ? Mais... je croyais que..."

_**Coriandre :**_ "Je ne parle pas physiquement. Elle était bien là, mais... elle était ailleurs en même temps. Enfermée dans son propre monde. Elle faisait ses expériences, sans se soucier des autres. Mon père a dû l'enfermer dans un sous-sol pour qu'elle ne fasse de mal à personne. Il m'interdisait d'aller la voir, alors que... jamais elle n'a voulu être intentionnellement violente. Je ne sais pas ce qui est le pire : ne jamais avoir connu sa mère, ou... l'avoir connue, mais n'avoir jamais pu l'approcher."

_Camomille reste silencieuse. Le vent fait un peu bouger les plantes du marécage._

_**Coriandre :**_ "Je ne sais pas pourquoi je te raconte tout ça. Je dois t'agacer avec mes histoires, non ?"

_**Camomille :**_ "Euh... non, non ! C'est juste que... je ne sais pas trop quoi répondre... je n'ai jamais su quoi répondre, dans ces cas-là... même à Hippiah, je restais dans mon coin... je n'ai jamais apprécié... les autres."

_**Coriandre :**_ "Tu as été la cible de moqueries ? Par rapport à ton métissage ?"

_Camomille ne répond pas._

_**Coriandre :**_ "Je comprends ton ressentiment. Certaines personnes ne respectent pas les différences. Peut-être est-ce une crainte à un certain attachement qui pourrait se créer ? Une réaction de peur ? Je ne sais pas. Sache juste que tous les humains ne sont pas pareils, et qu'ici, personne ne te jugera. Tu as bien vu Armoracia, tout le monde l'a acceptée. Nous t'accepterons de la même façon. Si toutefois cela faisait partie de tes craintes !"

_**Camomille :**_ "Je... je ne sais pas. C'est vrai que je n'aime pas trop les humains, pour de multiples raisons, pas seulement leur regard sur moi... et que je sais que je ne devrais pas tous les mettre dans le même panier... mais je n'ai jamais... comment dire ? été à l'aise avec les gens..."

_**Coriandre :**_ "Peut-être que tu es plus à l'aise toute seule ? Ce n'est pas une tare, tu sais. Parmi mes ingénieurs, il y en a beaucoup que je ne peux pas faire travailler en groupe, parce qu'ils se débrouillent mieux lorsqu'ils sont tout seuls avec leurs pensées. C'est juste... un tempérament. Rien à voir avec ce que tu es physiquement. Je ne suis pas sûre que les autres comprennent bien, ça. Qu'on puisse avoir besoin d'être seul."

_**Camomille :**_ "Tu parles de Pepper ?"

_**Coriandre :**_ "Oui... elle a l'air de vraiment vouloir qu'on fasse équipe."

_**Camomille :**_ "Elle a dit que j'étais celle qui l'aimait le moins."

_**Coriandre :**_ "Et c'est vrai ?"

_**Camomille :**_ "Je ne sais pas. Ça dépend des moments. Des fois, je sens qu'elle est très gentille. Et d'autres... que son âme renferme une profonde haine envers tout le monde. Une sorte d'égoïsme et de méchanceté extrême nuisible pour tous. Qu'elle a déjà montrée, ce jour où elle a balayé mon école avec son trou noir, sans la moindre pitié... mais je dois trop me prendre la tête... je vois bien qu'elle essaie d'être sympa avec moi, qu'elle essaie de faire un effort, qu'elle regrette vraiment ce qu'elle a fait à Hippiah. Mais... il y a cette barrière qui m'empêche de l'apprécier. Une sorte d'instinct qui m'ordonnerait de me tenir loin d'elle. Et même... qui me fait avoir presque peur d'elle. Tu... tu penses que je suis folle ?"

_**Coriandre :**_ "Non, pas du tout. Se méfier, c'est normal. Surtout quand on ne connaît pas bien une personne. On se connaît toutes depuis quelques temps, maintenant, mais on n'a jamais vécues ensemble. Je pense qu'on va être surprises par ce qu'on risque de découvrir dans les prochains jours. Sur les autres, mais aussi sur nous-mêmes. Quelle qu'en soit l'issue, je pense que cette expérience sera intéressante pour tout le monde. Tu devrais te fier à ton instinct. Il est plus développé que le nôtre. Je pense qu'il pourrait t'être bien utile."

_Elles observent un temps de silence. Les étoiles scintillent au-dessus de leur tête._

_**Coriandre :**_ "Bon. Il va falloir que je retourne travailler. Merci pour la discussion, ça m'a permis de relâcher un peu de pression."

_Elle se dirige vers l'entrée de l'antre._

_**Camomille :**_ "A... attends !"

_Coriandre s'arrête un instant._

_**Camomille :**_ "Je... je voulais... euh..."

_Coriandre sourit._

_**Coriandre :**_ "De rien, ça m'a fait plaisir. Et n'hésite pas, si tu veux encore qu'on parle, toutes les deux. Je suis toujours disponible pour mes amis ! Bonne nuit, Camomille."

_Elle redescend. Un petit sourire se dessine sur le visage de Camomille._

_**Camomille :**_ "Merci, mon... amie."

### Scène 5 - Pour la forêt de Lorbéciande

_Le lendemain matin, Pepper fait le tour des chambres pour réveiller tout le monde._  

_**Pepper :**_ "Debout, là-dedans !"

_**Safran :**_ "Ferme-la ! Et laisse-moi dormir..."

_**Pepper :**_ "Allez ! On se lève ! Annonce importante !"

_**Shichimi** (à peine réveillée) **:**_ "Hum..."

_**Pepper :**_ "RASSEMBLEMENT !!" Où est Camomille ? Carrot ! Va la chercher, elle doit être dehors !

_Carrot s'y emploie. Armoracia sort lentement de son antre, en robe de chambre, encore décoiffée, s'essuyant tous les yeux avec tous ses bras en retirant ses trois masques pour dormir._

_**Armoracia :**_ "Princesse Pepper ? Vous êtes matinale ! Pourquoi ces cris ?"

_**Pepper :**_ "J'ai quelque chose d'important à dire à tout le monde ! Prépare le petit déj' ! DEBOUT TOUT LE MONDE !"

_**Safran** (jetant un objet, au loin) **:**_ "MAIS ELLE VA LA FERMER, OUI OU NON ?!!"

_Quelques minutes plus tard, toutes les sorcières sont réunies dans la pièce principale. Armoracia a exposé le petit déjeuner sur la table. Heureusement, Safran a trouvé une astuce pour que ce soit mangeable pour tout le monde, et l'a déposé à côté du plateau en prétendant qu'il s'agit d'une "petite sauce personnelle pour donner plus de goût". Un gâteau saucé dans le bec, Pepper se met face à ses amies._

_**Pepper :**_ "Bon, est-ce que tout le monde est là ? Un, deux, trois... quatre ? Où est la quatrième ? Coriandre ! CORIAAAANDRE ! RASSEMBLEMENT !"

_**Safran :**_ "Mais jamais elle s'arrête de hurler, celle-là ?!"

_Coriandre finit par émerger, en baillant lourdement. Totalement décoiffée, elle a des cernes impressionnantes, et marche comme un zombi. Mango, derrière elle, la surveille, de peur qu'elle s'effondre._

_**Coriandre :**_ "Bonjour... désolée, je n'ai pas beaucoup dormi..."

_**Safran :**_ "On peut voir ça ! Quelle horreur !"

_**Pepper :**_ "T'es sûre que tu n'as pas jeté le sort de Zombification sur toi-même ?"

_**Coriandre :**_ "Non, pas d'inquiétude, il ne fonctionne pas sur les êtres humains vivants. Alors, qu'est-ce que tu voulais nous dire ?"

_**Pepper :**_ "Bon. Pour commencer, je voudrais présenter mes excuses à chacune d'entre vous. Je n'ai pas été très sympa avec vous hier soir, et j'en suis désolée. S'il y a bien une chose qui est sûre, c'est que je n'ai jamais voulu vous heurter de quelque manière que ce soit ! Nous sommes une équipe, maintenant. Comme une famille ! Nous avons un objectif commun, et je tiens vraiment à ce qu'on s'entende bien ! Donc, je veillerai à faire attention à partir de maintenant, et je vais m'efforcer de devenir une meilleure amie pour vous."

_Les sorcières se lancent un regard entre elles, ne sachant que répondre. Coriandre est plutôt contente, Safran croise les bras, limite lassée, Camomille détourne les yeux, et Shichimi se contente d'écouter. Seule Armoracia semble vraiment ravie. Elle a un large sourire. Le large râle de Messy est le seul son qu'on puisse entendre._

_**Pepper :**_ "Bon... ceci étant dit, il est temps de parler boulot. Je vous rappelle notre objectif : tenter d'abolir la loi anti-magie, en redonnant la foi envers les sorcières aux gens. Shichimi a justement suggéré de plaire aux puissants du monde pour avoir plus d'influence et augmenter nos chances. Seulement, vous le savez, hier, l'armée du roi Hartru s'en est prise à l'une des nôtres. C'est totalement impardonnable ! Mais comme Shichimi l'a dit, nous en prendre à lui ne servirait à rien d'autre qu'à renforcer la haine. Il faut donc combattre la haine par la gentillesse. J'ai bien réfléchi cette nuit, et j'en suis parvenue à la conclusion que la haine du roi envers les sorcières venait bien de quelque part. Il faut trouver ce que c'est afin de changer son avis négatif sur les sorcières. Shichimi, tu es sur le coup ?"

_**Shichimi :**_ "Oui. J'attends que ma botte secrète se mette en place."

_**Pepper :**_ "Bon. Préviens-nous si tu as du nouveau. En ce qui nous concerne, nous devons nous tenir prêtes à toute éventualité. À l'heure qu'il est, la disparition du garde que nous avons fait prisonnier a dû être constatée. Donc, nous risquons de recevoir une mauvaise visite d'ici peu de temps. Si nous voulons pouvoir lutter contre une telle armée, il est impératif de se coordonner. Nous allons donc nous entraîner dans ce but. Et afin que les gens n'oublient pas que sommes ici, nous continuerons nos petites actions auprès du peuple, histoire de gagner des points et de nous faire connaître davantage. Et pour renforcer notre union, et notre cohésion de groupe, j'estime qu'il nous faut un nom pour notre association. J'ai donc décidé que nous nous appellerions Unitah."

_**Safran :**_ "Unitah ? C'est une blague ?"

_**Coriandre :**_ "Unitah ? Après tout, pourquoi pas ? C'est comme une nouvelle école qu'on viendrait de créer, et le titre est évocateur."

_**Camomille** (haussant les épaules) **:**_ "Unitah ? Bof."

_**Shichimi :**_ "Unitah ? Intéressant."

_**Pepper :**_ "Eh bien, cachez votre joie ! Je m'efforce pendant toute la nuit de nous trouver un nom, et c'est l'accueil que j'en ai ?!"

_**Safran :**_ "Il t'a fallu toute la nuit pour trouver ça ? Eh bien, il y a des signes qui ne trompent pas..."

_**Pepper :**_ "Ben quoi ? Qu'est-ce que tu lui trouves, à ce nom ? T'as mieux à proposer ?"

_**Safran :**_ "Dans l'immédiat, non. Mais tu aurais au moins pu nous demander notre avis avant de nommer notre groupe ! Si nous devons être unies, la moindre des choses, ce serait de prendre les décisions ensemble !"

_**Shichimi :**_ "J'approuve Safran. Encourageons l'esprit d'équipe en faisant participer tout le monde."

_**Coriandre :**_ "Je suis d'accord. Que tu sois un peu la chef de notre équipe, c'est normal, Pepper, c'est toi qui l'as créée."

_**Safran** (marmonne) **:**_ "Même si quelqu'un d'autre ici serait plus efficace dans cette fonction... hum, hum ! Non, non, rien, continue !"

_**Coriandre :**_ "Mais ce n'est pas une raison pour prendre les décisions toute seule. Si nous sommes vraiment une équipe soudée, des amies comme tu le voudrais, je pense que tu approuveras."

_Pepper regarde chacune des sorcières. Tout le monde semble d'accord._

_**Armoracia :**_ "Je... je suis d'accord aussi ! L'avis de tout le monde compte dans une équipe !"

_**Pepper :**_ "Princesse... bien, j'ai entendu ce que vous avez dit, et je suis d'accord. Désormais, prenons les décisions ensemble ! Donc, qui vote pour qu'on s'appelle Unitah ?"

_Pepper, Coriandre et Shichimi lèvent la main. Armoracia en lève trois avant d'en baisser deux, en rougissant. Camomille la lève qu'à moitié. Seule Safran ne lève pas la main._

_**Pepper :**_ "Bon, quatre voies et demi sur six, le nom est adopté !"

_**Safran :**_ "Génial..."

_Messy pousse un râle craquant. Est-ce sa façon de célébrer le nouveau nom de l'équipe ?_

_**Pepper :**_ "Nous sommes Unitah, l'école clandestine où toutes les sorcières s'unissent pour lutter pour une cause juste : rétablir le droit de pratiquer la magie ! Et pour cela, nous devons travailler en équipe pour redonner une image positive des sorcières aux gens ! Et accessoirement nous protéger contre d'éventuels gardes qui viendraient nous casser les pieds..."

_**Safran :**_ "Travailler en équipe ? Quelle plaie ! Attendez qu'ils arrivent dans la forêt, je vais leur faire passer l'envie de revenir avec un bon incendie maison !"

_**Camomille :**_ "Si jamais tu touches à une seule feuille de cette forêt, je t'empale au sommet d'une colline !"

_**Safran :**_ "J'aimerais bien voir ça ! Dis donc, le côté pacifique d'Hippiah a dû te passer sous le nez, tu es une vraie teigne !"

_**Camomille :**_ "Je le reste moins que toi !"

_**Coriandre :**_ "Allons, ne vous disputez pas... on pourrait poster des robots sentinelles dans la forêt, qui captureraient les ennemis qui s'y risquent..."

_**Safran :**_ "Et ce serait Madame Coriandre qui récolterait tous les lauriers ! Non merci, Princesse ! On se débrouillera sans votre technologie !"

_**Pepper :**_ "S'il vous plaît..."

_**Coriandre :**_ "Mais... on s'en fiche, des lauriers, c'est le résultat qui compte..."

_**Safran :**_ "Tu m'expliques l'intérêt de notre groupe si on laissait tes robots faire tout le travail ? Ce serait une pub ambulante pour la loi anti-magie ! "Pourquoi faire ce que des robots peuvent faire à notre place ?""

_**Coriandre :**_ "Je... je voulais juste aider..."

_**Safran :**_ "Aide-nous, mais avec ta magie ! Pas tes automates !"

_**Coriandre :**_ "Ah... et qu'est-ce que tu veux que je fasse avec un sort de Zombification, moi ?"

_**Safran :**_ "Ce n'est pas à moi de réfléchir à cette question ! C'est toi, la sorcière de Zombiah, chérie ! Ce n'est pas ma faute si vous ne savez lancer qu'un seul sort..."

_**Coriandre :**_ "Tu es vraiment injuste, Safran."

_**Pepper :**_ "S'IL VOUS PLAÎT !"

_Les deux sorcières s'arrêtent, mais se font clairement la tête._

_**Pepper :**_ "Je veux qu'on s'entende bien, vous vous rappelez ? Qu'on travaille en équipe ! Unitah !"

_**Safran :**_ "Je travaille parfaitement bien toute seule, et pas pour un groupe avec un nom aussi ridicule ! Et ce n'est pas le raton-laveur qui va me contredire, hein ?"

_**Camomille :**_ "Je te garantis qu'il va t'arriver des bricoles, toi !"

_**Safran :**_ "Ah oui ? Eh bien, vas-y ! Je n'attends que ça, l'infirme !"

_**Camomille :**_ "Là, on pourra pas dire que c'est ma faute ! Et ce n'est pas parce que tu m'as soignée que je vais te faire de cadeaux !"

_**Pepper :**_ "ON ARRÊTE !!"

_Les deux sorcières s'apprêtent à s'élancer l'une sur l'autre. Elles stoppent leur geste._

_**Pepper :**_ "Vous voyez, pourquoi on doit travailler notre esprit d'équipe, pourquoi je voulais qu'on s'appelle Unitah ? Si on se déchire entre nous, comment voulez-vous qu'on atteigne notre but ? On est du même côté, ne l'oubliez pas !"

_**Safran :**_ "Du même côté... un peu forcé, non ? Je suis en otage, je rappelle !"

_**Pepper** (plongeant son visage dans ses mains) **:**_ "Mais non, tu n'es pas en otage..."

_**Safran :**_ "Ah ? Je peux rentrer chez moi, alors ?"

_**Camomille :**_ "Oui, rentre chez toi, bonne idée !"

_**Safran :**_ "Moi, au moins, je le peux !"

_**Camomille :**_ "Je vais la démolir..."

_**Coriandre :**_ "Reste calme, Camomille. C'est de la provocation, rien de plus."

_**Pepper :**_ "Safran, s'il te plaît, arrête. C'est vraiment important, là. Il faut qu'on reste solidaires ! On a plus à gagner en jouant ensemble qu'en étant perso !"

_**Safran :**_ "Vous savez très bien ce que j'en pense, de votre plan."

_**Pepper :**_ "Je t'en prie, Safran. On a vraiment besoin de toi."

_**Camomille :**_ "J'en doute..."

_**Coriandre :**_ "Chut !"

_**Pepper :**_ "Si, Camomille. Au même titre qu'on a besoin de toi, de Coriandre, de Shichimi ! Regardez ! On a presque toutes nos écoles de représentées ! Il ne manque plus qu'Aquah pour avoir le triangle complet !"

_**Safran :**_ "Ah, attention, hein ! Si jamais il te prend le goût d'inviter un de ces poissons d'Aquah ici, je me tire ! Et pour de bon !"

_**Pepper :**_ "Je n'ai pas dit ça... je dis juste que nous avons une sorcière par école. C'est-à-dire des compétences uniques par personne ! Donc tout le monde est indispensable ici ! C'est ce qui fait la richesse de notre groupe, et notre force ! C'est ce qui donne tout son sens au nom d'Unitah ! Alors je ne vous demande pas d'être les meilleures amies du monde, juste de bien vous entendre et de jouer en équipe ! Ce ne devrait pas être au-dessus de vos forces, ça, non ?"

_**Safran :**_ "Faire équipe avec celle-là ? Ça me paraît difficile !"

_**Camomille :**_ "On se demande pour qui !"

_**Coriandre :**_ "S'il vous plaît, les filles..."

_Pepper ne sait plus quoi dire pour qu'elles s'entendent. Soudain, elle a une idée. Elle croise les bras et prend un air mesquin._

_**Pepper :**_ "Eh bien, Safran ? Je pensais que tu étais capable de tout faire ? Je suis déçue... tu ne peux même pas faire équipe avec quelqu'un ? Tu n'es peut-être pas aussi douée que je le pensais, alors..."

_**Safran** (tendant la main à Camomille, visiblement vexée) **:**_ "Faisons des étincelles, Camomille !"

_Camomille est surprise. Elle se tourne vers Coriandre, qui lui fait signe d'accepter. Sans comprendre, Camomille prend la main de Safran._

_**Camomille :**_ "Euh..."

_**Safran :**_ "Bien. Montrons à cette pimbêche à quel point nous sommes synchros, chez Unitah !"

_Pepper sourit. Coriandre lève le pouce en sa direction. Shichimi est toujours en train de pianoter sur sa boule de Cristalication. Armoracia revient avec d'autres gâteaux.  
Dans les jours qui suivirent, les sorcières d'Unitah commencèrent à s'entraîner. Coriandre fit installer un système d'alerte sur ses mouches, relié à un appareil qu'elle garde sur elle. Si quelqu'un de suspect pénètre la forêt, elle sera immédiatement avertie. Ce qui permet aux sorcières de s'entraîner en plein air, en toute sécurité. 
Safran et Camomille tentent des sorts combinés, mais comme leur magie est opposée... le feu de Safran englobe souvent les plantes de Camomille. Cette dernière finit par se plaindre que Safran le fait exprès, ce que Safran dément. Coriandre est rarement sur le terrain, elle passe beaucoup de temps à construire des machines, surtout la nuit (ce qui la pousse à se lever très tard les matins). Lorsqu'elle s'entraîne avec les autres, c'est pour tester une nouvelle machine pour permettre à Safran et Camomille de mieux se synchroniser. Mais bien souvent, c'est un échec. Le Fourneau à Plantes, devant booster les plantes par la chaleur du feu, finit broyé par les racines de Camomille, tandis que l'Arrosoir Chaleureux fut totalement brûlé par Safran. Pepper est exaspérée, mais ne compte pas abandonner. Armoracia suit leur progrès avec attention, et passe son temps à faire les repas et à tisser les vêtements abîmés par les entraînements un peu trop passionnés._  

_Armoracia et Safran s'associèrent pour fabriquer de nouvelles tenues pour les sorcières d'Unitah. Chacune d'entre elles reçut la même tenue, mais d'une couleur différente. La violette fut pour Safran, la rouge pour Coriandre, la verte pour Camomille, la blanche pour Shichimi et la noire pour Pepper. Il s'agit d'un ensemble comprenant un veston, un chemisier, des gants, une jupe et des bottes, brodés et fabriqués par Armoracia et imaginés par Safran, avec de petits motifs pour les rendre plus fashion. Sur chaque veston, Armoracia a brodé le symbole de la nouvelle école d'Unitah, voté à l’unanimité, soit deux bras qui se croisent, l'un portant une baguette, l'autre un flacon de potion, formant un "U". Et bien évidemment, chaque sorcière reçut un large chapeau caractéristique, afin de ne laisser aucun doute à quiconque les apercevrait. Toutes les sorcières apprécièrent leur nouvelle tenue, et l'enfila immédiatement. Sauf Coriandre, qui l'amena d'abord à son atelier avant de la mettre, et Camomille, qui eu du mal à appréhender sa tenue, étant donné ses mouvements assez spécifiques pour se déplacer. Ce fut la seule qui se débarrassa des gants, l'empêchant d'entrer en contact avec le sol.  
Pendant les pauses, Camomille aide Safran à récupérer du bois dans la forêt. À l'aide de Coriandre, elles réussirent à fabriquer de nouveaux balais aérodynamiques de toute beauté. À nouveau, les voilà qui peuvent se déplacer par la voie des airs. Seule Shichimi refuse qu'on lui fasse un balai : Armoracia l'aide à concocter un petit nuage de coton, qu'elle enchante pour retrouver son moyen de transport favori.   
Pepper finit par tenter de s'associer à Camomille et Safran pour faire cohabiter leur magie. Mais ça finit souvent par déraper : soit Safran reçoit une branche de Camomille par la tête, soit Camomille évite l'immolation de justesse à cause d'un envoi de flammes dans tous les sens suite à un sort de Désordre raté de Pepper._  

_Voyant que l'entraînement pour effectuer des combos ne donne rien, Pepper envoie les membres de son équipe une par une pour aider les gens dans le besoin. Coriandre a étendu la surveillance de ses mouches aux villes et villages à proximité de leur antre. L'écran retransmettant les images captées par les mouches s'est littéralement agrandi, et a été séparé en deux : une partie pour la surveillance rapprochée, une partie pour guetter les opportunités d'agir. Les sorcières d'Unitah n'ont qu'une règle : secourir en laissant la marque d'Unitah sur leur passage. Grâce à leur nouveau moyen de transport véloce, chacune d'entre elles eut l'occasion de s'illustrer auprès du peuple : Coriandre parvint à réparer (et animer) un immense four de boulanger qui ne fonctionnait plus ; Safran put soigner un enfant malade avec ses potions ; Shichimi apporta de la lumière dans une maison soi-disant hantée ; Pepper empêcha un gamin de se faire violenter par un autre groupe de garnements, en leur suggérant un peu violemment de réfléchir à deux fois avant de s'en prendre à plus petit que soi, et surtout à plusieurs. Camomille, elle, reste sur des missions forestières en aidant les animaux dans le besoin, et en chassant les braconniers. Elle n'a personne à qui laisser la marque d'Unitah, mais elle refuse encore d'intervenir pour aider les humains. Sa blessure finit par guérir complètement, lui permettant de retrouver l'intégralité de ses facultés._  

_Les journaux et les gens commencent à parler de plus en plus de ce groupe de sorcières qui agit pour aider le peuple. Les avis sont partagés, et les sorcières peuvent suivre certaines réactions en direct grâce au réseau de mouches et la boule de Cristalication. Concrètement, leurs actions ne font pas taire les anti-sorcières, et, au contraire, attisent encore plus leur haine, mais elles ont l'impression de gagner la sympathie de plus en plus de monde. Certains les voient comme des héroïnes qui viennent au secours des plus démunis, et d'autres comme des démons voulant se faire justice elles-mêmes sans se soucier de la loi, ici pour encourager le désordre et la ruine. La loi anti-magie est remise en question. Pepper n'a jamais été aussi fière, et ne se voyait pas diriger une telle équipe. Mais en tant chef tacitement désignée, elle parvient à arrêter les conflits quand ils éclatent et à trancher les décisions lorsque les avis sont trop partagés. Même si elle aurait voulu que son groupe soit plus soudé et fasse des actions en équipe, Pepper trouve qu'elle s'en sort bien pour un leader d'école clandestine débutant._

_Quant au garde fait prisonnier, les sorcières se relaient pour aller lui apporter de la nourriture. Elles essaient souvent de lui soutirer des informations, mais le garde reste de marbre. Aucun soldat ne vint le secourir durant ces jours d'entraînement et d'actions.  
Et un jour, pendant un entraînement où Safran essaie de donner des ailes enflammées à une plante carnivore générée par Camomille :_

_**Shichimi :**_ "Les filles ! J'ai trouvé quelque chose !"

_**Safran :**_ "Ma sauveuse ! On peut enfin arrêter de faire n'importe quoi !"

_**Camomille :**_ "Si tu ne faisais pas exprès de brûler mes plantes, on avancerait peut-être !"

_**Safran :**_ "Si tes plantes n'étaient pas aussi faibles..."

_**Pepper :**_ "Bon ! On arrête, les filles ! UNITAH, RASSEMBLEMENT ! Shichimi a une annonce à faire !"

_Elles redescendent dans l'antre. Carrot et Truffel font la sieste l'un à côté de l'autre, tandis que Mango visite la caverne en essayant de becqueter les restes de gâteaux tombés par terre. Armoracia s'est endormie, assise par terre, trois bras soutenant sa tête sur le rocher. Elle se réveille d'un coup._

_**Armoracia :**_ "Hum... oh, c'est vous ! Vous venez déjà manger ? Il n'est pas encore l'heure..."  

_**Pepper :**_ "Non, non, Shichimi a trouvé quelque chose ! Coriandre ! Viens vite !"

_Coriandre émerge de son atelier, le visage toujours aussi sale, un masque de protection sur la tête, qu'elle retire._

_**Coriandre :**_ "Ah ! On a enfin des info, non ?"

_Elles enfilent leur uniforme, et se réunissent quelques minutes plus tard autour de Shichimi dans l'espace nommé "Salle multimédia", où la boule de Cristalication est branchée au fil d'Ahrachnerre. Toutes les sorcières sont attentives._

_**Pepper :**_ "Alors, Shichimi ? Qu'as-tu découvert ?"

_**Shichimi :**_ "Bon. Mon agent infiltré m'a fait quelques révélations intéressantes sur le roi Hartru."

_**Safran :**_ "Un agent infiltré ? C'était ça, ta botte secrète ?"

_**Shichimi :**_ "Oui, il espionne le roi Hartru, et il a fait un rapport intéressant. Écoutez-bien : tout porte à croire qu'il cherche à percer le secret d'une machine antique, qui conduirait à une source de pouvoir infinie sur le monde. Coriandre, tu en as peut-être entendu parler ?"

_**Coriandre :**_ "Hum... effectivement, ça me parle. Une sorte de robot légendaire qui renfermerait le secret d'une source d'énergie colossale et illimitée. Mais à ce jour, personne n'a pris cette histoire au sérieux... pourquoi est-ce que le roi Hartru s'y intéresse tant ?"

_**Shichimi :**_ "On peut imaginer que c'est pour se protéger contre les sorcières. En effet, avec une telle puissance, il pourrait toutes nous éradiquer de sa propre main."

_**Pepper :**_ "Ah, non ! Hors de question qu'Unitah le laisse faire ! Il faut absolument l'empêcher de mettre la main sur ce robot !"

_**Coriandre :**_ "Ce n'est qu'une légende, Pepper. Ce robot n'existe pas."

_**Shichimi :**_ "Eh bien, d'après mon espion... le roi Hartru l'aurait bel et bien en sa possession."

_**Coriandre :**_ "Pardon ?! Mais... mais c'est impossible ! Ton agent a trop bu !"

_**Shichimi :**_ "Mon espion l'a vu de ses propres yeux, et il ne boit jamais pendant le travail."

_**Coriandre :**_ "Impossible... il existerait vraiment ? Non..."

_**Safran :**_ "C'est plutôt inquiétant. A-t-il trouvé le moyen d'exploiter ce robot ?"

_**Shichimi :**_ "Justement, mon agent m'a dit qu'il cherchait un moyen de l'activer. Enfin, qu'il faisait chercher. Il aurait besoin de l'aide d'un être d'exception qui vit dans la forêt de Lorbéciande, une sorte d'esprit de la forêt..."

_**Camomille :**_ "Quoi ? Il va s'en prendre à Seigneur Sylvagro ?!"

_**Pepper :**_ "Sylvagro ? Qui c'est, celui-là ?"

_**Shichimi :**_ "Ce n'est pas un esprit connu de Ah. J'ai cherché avec la boule, mais je n'ai rien trouvé à ce nom-là..."

_**Camomille :**_ "Ce n'est pas étonnant ! C'est une histoire qu'on me racontait à Hippiah étant enfant, la légende de l'esprit Sylvagro, qui veille sur les plantes du monde entier et fait en sorte de rendre la fertilité aux terres qui l'ont perdue. Il est connu pour punir ceux qui maltraitent la faune et la flore. Mais ce n'est qu'une légende... Pepper, tu as dû en entendre parler quand tu étais à Hippiah !"

_**Pepper :**_ "Maintenant que tu en parles, ça m'évoque vaguement quelque chose... il est représenté par une énorme bestiole à quatre pattes avec des bois de toutes les couleurs sur la tête, non ?"

_**Camomille :**_ "C'est une description très irrespectueuse pour Seigneur Sylvagro, mais c'est l'idée."

_**Safran :**_ "Un robot légendaire, une bête mythique... on reste sur de l'hypothèse ! Est-ce que ça vaut le coup qu'on y prenne au sérieux ?"

_**Camomille :**_ "Que nous y croyions ou non, le roi Hartru, lui, y croit ! Et il va dévaster la forêt dans le but de trouver cet esprit !"

_**Shichimi :**_ "En effet, il a envoyé toutes ses troupes en direction de cette forêt. C'est sûrement pour cette raison que personne n'est venu nous attaquer depuis des jours..."

_**Camomille :**_ "On ne peut pas rester sans réagir ! Il faut l'arrêter au plus vite !"

_**Pepper :**_ "Je suis d'accord ! Je sais qu'on devait donner une bonne image des sorcières au roi... mais j'estime qu'il est plus important de protéger la forêt et les animaux ! Unitah doit se ranger du côté de ceux qui sont dans le besoin avant tout ! Est-ce que vous êtes d'accord ?"

_Camomille a presque un regard reconnaissant envers Pepper. Shichimi, Safran et Coriandre confirment d'un signe de tête._

_**Shichimi :**_ "C'est évident."

_**Coriandre :**_ "Oui. Courber l'échine devant les tyrans, ça ne me plaît guère. Tant pis pour notre image, défendre la forêt est notre priorité ! La priorité d'Unitah, je devrais dire !"

_**Safran :**_ "Et si je croise ce malotru, je vous garantis que son popotin sentira le brûlé ! Ne t'inquiète pas, amoureuse des plantes : je ferai attention de ne pas faire voler de braise trop loin !"

_Les yeux de Camomille n'ont jamais été aussi teintés de joie. Elle détourne les yeux et marmonne :_

_**Camomille :**_ "M... merci."

_**Safran :**_ "Arrête, je vais finir par te trouver sympathique ! Bon, nous y allons ?"

_**Armoracia :**_ "Je peux venir avec vous ? S'il vous plaît ! J'aimerais tant vous voir en action en vrai, et non à travers un écran..."

_**Pepper :**_ "Ah, Princesse... euh, ça risque d'être dangereux..."

_**Coriandre :**_ "C'est une mission risquée."

_**Safran :**_ "On ne voudrait pas que la reine de la mode arachnéenne se fasse trop amocher !"

_Les six yeux d'Armoracia se remplissent de larmes._

_**Coriandre :**_ "Oh, Princesse, ne soyez pas déçue ! Nous rentrerons très vite, promis ! Et nous vous emmènerons lors d'un de nos petits sauvetages aux alentours ! Vous n'avez qu'à préparer un festin pour notre retour, d'accord ?"

_Armoracia sèche ses larmes et fait un grand sourire._

_**Armoracia :**_ "D'accord ! Soyez prudentes ! Et arrêtez-moi ce vilain roi !"

_**Pepper** (avec un clin d'œil) **:**_ "À vos ordres, Princesse ! Les animaux, vous veillez sur elle, d'accord ?"

_Carrot et Truffel font un clin d'œil synchronisé. Mango... continue de chercher ses morceaux de gâteaux. Messy descend de son plafond, plus agité et avec un râle bien plus craquant et aigu que d'habitude._

_**Pepper :**_ "Oh, oh ! Messy, tu as l'air de vouloir venir avec nous !"
  
_Messy semble confirmer en s'agitant davantage. Les sorcières se saisissent de leur moyen de transport favori, et se tournent vers la sortie de leur repaire d'un air déterminé._

_**Pepper :**_ "Allons montrer à ce malotru de quoi les sorcières d'Unitah sont capables !"

## Acte 4 - L'esprit de la forêt

### Scène 1 - Touchées en plein vol

_Les sorcières partent donc pour la forêt de Lorbéciande, en formation aérienne sur leur balai (et Shichimi sur son nuage). Avec Pepper en tête, elles essaient de voler très haut dans le ciel afin que personne ne les voit. Coriandre a activé un petit système de dissimulation, qui les recouvre de la même couleur que le ciel, mais l'effet reste à parfaire, et il faut vraiment être loin pour duper les yeux des gens.  
Elles parcourent une petite distance, en survolant une partie de l'Union des Technologistes, entourée d'une brume de pollution épaisse, idéale pour se cacher, mais moins pour la santé. Les sorcières s'étouffent, et Camomille est très en colère._

_**Safran :**_ "Quelle purée de pois nauséabonde ! C'est vraiment ta science qui provoque cette horreur, Coriandre ?"

_**Coriandre :**_ "Contente-toi d'enfiler le masque que je vous ai distribué !"

_**Safran :**_ "Hors de question que je porte un masque aussi disgracieux ! Et mon image, alors ?"

_**Pepper :**_ "Mais personne ne peut te voir, ici, alors on s'en fiche ! Mets ce foutu masque, si tu veux pas finir asphyxiée !"

_**Safran :**_ "Grrr ! Toujours à donner des ordres, hein ? Bon !"

_Elle enfile une sorte de masque à gaz que toutes les sorcières ont ajusté sur leur nez. Elle prend enfin une bouffée d'air... en conserve._

_**Safran :**_ "Hum... en fin de compte, je me demande si je ne préférais pas respirer le nuage toxique, là !"

_**Pepper :**_ "Arrête de raconter n'importe quoi ! Ne me perdez pas de vue, les filles ! On accélère !"

_Toujours en formation, elles parviennent à s'extirper de ce nuage polluant. Elles peuvent enfin retirer leur masque et prendre une vraie bouffée d'air pur. Camomille lance un dernier regard envers la ville industrielle, entourée de sa brume verte et poisseuse, avec un regard de dégoût._

_**Camomille :**_ "Pff... les humains ne respectent décidément rien !"

_Elles poursuivent leur périple au-dessus d'un grand lac. Pepper s'approche de Camomille._

_**Pepper :**_ "C'est de ce côté ?"

_**Camomille :**_ "Oui. La forêt de Lorbéciande se trouve juste derrière cette montagne !"

_En effet, le lac se trouve au pied d'une chaîne de montagnes plutôt imposante._

_**Safran :**_ "Sympa. J'espère que nous arrivons bientôt, je n'en peux plus de chevaucher ce balai !"

_**Pepper :**_ "Tu veux bien arrêter de te plaindre, deux minutes ?! On est parties il y a moins d'une heure !"

_**Safran :**_ "Je n'ai jamais chevauché de balai aussi longtemps ! Ce n'est pas bon pour l'entrejambe ni pour les articulations de rester sur son balai sur une durée aussi longue ! Toutes les vraies sorcières savent qu'il faut faire des pauses régulières toutes les trente ou quarante minutes max de vol ! Et nous avons déjà atteint cette limite..."

_**Camomille :**_ "On n'a pas le temps de faire une pause ! Le roi Hartru détruit la forêt en ce moment même ! Mais, bien entendu, toi, tu t'en fiches ! Il n'y a que ta petite personne qui t'intéresse !"

_**Safran :**_ "C'est faux ! Je ne fais qu'énoncer une vérité médicale qui nous concerne toutes ! C'est pour votre santé que je le dis !"

_**Camomille :**_ "On s'en fiche de notre santé ! Les animaux et les arbres de Lorbéciande se font sauvagement agresser, et ils sont sans défense ! On doit faire au plus vite !"

_**Safran :**_ "Dis, tu commences à m'agacer, l'écolo, à ne penser qu'à tes plantes et pas à nous ! On est censées être une équipe, et veiller les unes sur les autres !"

_**Coriandre :**_ "Les filles, allons ! Arrêtez de vous disputer tout le temps ! Nous sommes en mission, je vous rappelle !"

_**Safran :**_ "En mission, peuh ! Ça dépend pour qui ! Notre herboriste extrémiste n'a jamais su ce que ça voulait dire !"

_**Camomille :**_ "Quoi ?! Qu'est-ce que tu veux dire ?"

_**Safran :**_ "Tu n'as fait que rester dans ta petite forêt pour chasser des braconniers et soigner la pa-patte aux animaux, pendant que nous, nous faisions de vraies actions, auprès des gens, pour diffuser notre message ! Je commence à douter de ton implication et de ton utilité pour notre cause !"

_**Camomille :**_ "Mais... mais c'est pas vrai ! J'aide du mieux que je peux, et... et..."

_**Safran :**_ "Tu ne penses qu'à toi, Camomille, alors arrête de dire que je suis égoïste ! Parce que la plus égoïste ici, ce n'est pas moi !"

_**Coriandre :**_ "Safran, arrête ! Tu es injuste envers Camomille ! Elle essaie de faire de son mieux ! Et... et elle a du mal à être en présence d'autres êtres humains, c'est comme ça ! Alors au lieu de l'enfoncer, tu ferais mieux d'essayer de l'aider !"

_**Safran :**_ "Toi, la mécanicienne, on ne t'a pas sonnée ! J'ai vu que vous vous faisiez des petites séances de discussion perso, plutôt que vous joindre au groupe ! Niveau égoïsme, je ne viendrais pas trop la ramener, à ta place !"

_**Coriandre :**_ "Quoi ? Qu'est-ce que tu insinues ?!"

_**Safran :**_ "Absolument rien, je ne fais que constater ! Tu passes ton temps avec tes machines, tu n'es presque pas présente lors des entraînements et des missions ! Seuls tes joujoux mécaniques t'intéressent ! Tu n'en dors presque pas la nuit, alors évite de me faire la leçon ! Vous vous êtes bien trouvées, toutes les deux !"

_**Coriandre :**_ "Tu es vraiment méchante, Safran ! Tu devrais avoir honte de ce que tu es en train de faire ! Tu crois que tu es irréprochable ? À part critiquer, qu'est-ce que tu fais ? À part frimer devant tout le monde, est-ce que tu encourages l'esprit d'équipe ? Je ne crois pas ! L'égoïste, ici, ce n'est pas moi ! Ni Camomille ! Regarde-toi dans une glace avant de juger !"

_**Safran :**_ "Non, vous, regardez-vous dans une glace ! Je suis la seule qui fasse avancer les choses ici !"

_**Camomille :**_ "Menteuse ! Tu ne fais que me rabaisser, depuis que tu m'as sauvée ! Je t'ai déjà remercié, il me semble ! Je ne te dois plus rien !"

_**Safran :**_ "Espèce de teigne ! Je vais..."

_**Pepper :**_ "ÇA SUFFIT, MAINTENANT !"

_Pepper s'arrête d'un coup. Toutes les sorcières s'arrêtent aussi. Pepper est vraiment folle de rage. Une aura inquiétante l'entoure. Les autres sorcières prennent peur._

_**Coriandre :**_ "Pepper..."

_**Pepper :**_ "VOUS ÊTES TOUTES HORRIBLES ! J'ai créé Unitah parce que je pensais qu'on pourrait former une communauté de sorcières soudées, mais regardez-vous ! Vous vous déchirez les unes les autres parce que vous ne respectez rien ni personne ! Alors je vais vous dire ce que j'en pense, moi ! Safran ! Tu n'es qu'une petite peste qui cherche à tout prix à être vue et admirée par tout le monde, en traitant les autres comme des moins que rien ! Alors retourne sur scène faire tes spectacles, qu'on en finisse ! Coriandre ! Rester avec nous plutôt qu'avec tes outils, ça ne te ferait pas de mal ! Tu penses que ta grand-mère, ou même ta mère serait fière de te voir t'isoler comme ça ?! Et Camomille ! Tu détestes les humains, soit ! Mais malgré nos efforts pour t'intégrer à notre groupe, tu continues de n'accorder de l'importance qu'à ta forêt bien aimée ! Eh bien tu sais quoi ?! Si tu n'es pas fichue de nous donner notre chance, retourne dans la forêt ! Personne ne te retient, ici ! Je rejoins Safran sur un point : tant que tu garderas ton opinion déplorable sur les humains, tu ne nous sers à rien ! Le message d'Unitah ne se diffuse pas ! Alors si tu ne veux pas faire d'effort, tu sais ce qu'il te reste à faire ! Heureusement que Shichimi tient à peu près la route ! C'est bien la seule ici qui fasse vraiment avancer les choses !"

_Pepper reprend sa respiration. Elle ne s'est même pas entendue parler, comme si elle était dans un état second. La seule chose qu'elle ait perçue durant son discours, c'est le râle craquant de Messy, flottant au-dessus de sa tête.  
Pepper a l'impression de reprendre ses esprits. Elle regarde ses amies, et a l'impression qu'on lui fend le cœur avec une épée. Toutes la regardent avec frayeur. Coriandre est clairement inquiète, Safran profondément vexée, les larmes aux yeux. Shichimi a préféré détourner le regard._

_**Pepper :**_ "Je... je ne voulais pas vous le dire comme ça, mais..."

_**Safran :**_ "Mais tu l'as dit ! Espèce de monstre... tu avais exactement le même regard lorsque tu m'as prise en otage ! Et c'est ça qui est à la tête de notre groupe ? Pas étonnant que celles qui le composent soient aussi minables !"

_**Pepper :**_ "Safran... je... je suis..."

_**Coriandre :**_ "Pepper, tu es sûre que ça va ? Je ne t'avais jamais vue dans un tel état de rage..."

_**Safran :**_ "C'est un monstre ! Vous l'auriez vue, lors de notre duel, il y a quelques mois ! Elle n'a pas hésité à m'achever alors que j'étais en position de faiblesse !"

_**Pepper :**_ "C'est... tu as voulu faire pareil juste avant ! Et..."

_**Safran :**_ "Et elle a bien détruit Hippiah, aussi, alors le raton-laveur ne va pas me contredire, sur ce coup ! N'est-ce pas ?"

_Mais Camomille a baissé la tête. Tremblante de tout son corps, elle serre son balai avec hargne._

_**Coriandre :**_ "Camomille..."

_**Camomille :**_ "Je... JE VOUS DÉTESTE !!"

_Elle vire de bord et plonge vers la forêt bordant le lac._

_**Coriandre :**_ "CAMOMILLE ! REVIENS, C'EST DANGEREUX DE SE SÉPARER !"

_Coriandre se lance à sa poursuite._

_**Pepper :**_ "On doit la rattraper ! Elle risque sa peau en quittant la formation ! Safran, Shichimi ! Allons-y !"

_**Safran :**_ "Tu crois vraiment que je vais encore t'obéir après ce que tu as eu l'audace de me dire ?!"

_**Shichimi :**_ "Safran, il ne s'agit plus de ça. Les gardes sont partout, et s'ils voient Camomille, dans son état, ce sera un jeu d'enfant de la cueillir. Ensuite, ils pourront la faire parler pour remonter jusqu'à chacune d'entre nous. Si l'une de nous tombe, on tombe toutes. Ce n'est pas ce que tu veux, non ?"

_Safran détourne les yeux._

_**Safran :**_ "Je le fais uniquement pour que mon nom ne soit pas associé à ce groupe ! Pas pour obéir à ce monstre !"

_Elle désigne Pepper avec férocité. Cette dernière baisse les yeux._

_**Pepper :**_ "Je... je suis tellement..."

_**Shichimi :**_ "Allons-y vite. Nous devons régler cette histoire et reprendre notre route vers la forêt de Lorbéciande au plus vite."

_Shichimi rejoint Coriandre._

_**Pepper :**_ "Safran... je..."

_**Safran** (sèchement) **:**_ "Allons-y."

_Safran se lance aussi vers le sol. Pepper reste quelques instants en l'air, avant de pousser un cri en tapant sur son balai._

_**Pepper :**_ "MAIS QU'EST-CE QUE JE SUIS BÊTE !!"

_Elle suit Safran avec une grande colère contre elle-même. Messy la suit de très près._

### Scène 2 - Raven

_Pepper finit par atterrir au sol, en bordure du lac. Safran, Shichimi et Coriandre sont déjà descendues de leur balai, et scrutent quelque chose que Coriandre tient dans les mains, visiblement inquiète._

_**Pepper :**_ "Où est Camomille ? Qu'est-ce que vous avez trouvé ?"

_Safran lui lance un regard noir avant de tourner la tête, ce qui enfonce un peu plus le poignard que Pepper a l'impression d'avoir planté en plein cœur. Coriandre lui montre ce qu'elle tient avec frayeur._

_**Coriandre :**_ "C'est... c'est le balai de Camomille ! Elle l'a laissé derrière elle..."

_**Pepper :**_ "Oh non... c'est ma faute..."

_**Safran :**_ "Tu peux le dire, oui !"

_**Coriandre :**_ "Soit Camomille l'a laissé ici pour rejoindre la forêt... soit elle été... oh non, je suis vraiment inquiète, Pepper ! On doit absolument la retrouv..."

_**Shichimi :**_ "Derrière les arbres, vite !"

_Les sorcières se dissimulent derrière un tronc. Effectivement, deux gardes en patrouille surveillent les environs d'un pas lent._

_**Coriandre :**_ "Hum, ce sont bien les gardes du roi Hartru..."

_**Pepper :**_ "Ses gardes, ici ?! Mais je pensais qu'ils étaient tous dans la forêt de Lorbéciande !"

_**Coriandre :**_ "Le roi Hartru a toujours aimé agir plus loin que ses propres frontières ! Ces terres-là ne lui appartiennent pas... il tient vraiment à ce que personne ne vienne le déranger..."

_**Safran :**_ "Laissez-moi les incendier, ces pourritures de gardes !"

_**Pepper :**_ "Tu tiens vraiment à brûler la forêt ?"

_**Safran :**_ "Je peux contrôler mon pouvoir, moi ! Ce n'est pas le cas de tout le monde ici..."

_**Pepper :**_ "Hum !"

_**Shichimi :**_ "Ce serait trop risqué de les attaquer, même s'ils sont deux. D'autres pourraient nous prendre en chasse, et on ne sait pas s'ils ont capturé Camomille ou non... s'en prendre à eux pourrait menacer sa vie."

_**Safran :**_ "Grrr ! Alors, qu'est-ce que vous suggérez ?"

_**Pepper :**_ "Je vais m'aventurer dans la forêt pour retrouver Camomille."

_**Coriandre :**_ "C'est de la folie ! Non, Pepper ! Tu ne la retrouveras jamais là-dedans ! Elle a dû semer des pièges derrière elle... et s'il y avait d'autres gardes..."

_**Safran :**_ "Et puis, quoi ? Tu te crois meilleure que nous, avec tes crises de rage intempestives ?"

_**Pepper :**_ "Non, je ne me crois pas meilleure ! Au contraire, je suis minable, comme chef ! Je n'aurais jamais dû vous dire toutes ces méchancetés, c'était ignoble de ma part ! En tant que chef d'Unitah, j'aurais dû au contraire vous encourager, et vous pousser vers le haut... j'ai poussé Camomille à se mettre en danger, c'est à moi de réparer mes bêtises. Et puis, je suis la seule qui connaisse bien les pièges d'Hippiah. Je ne me ferai pas avoir deux fois ! Vous allez m'en empêcher ?"

_Les trois sorcières se regardent. Safran finit par croiser les bras et détourner les yeux._

_**Safran :**_ "Fais ce que tu veux ! Je m'en fiche, après tout !"

_**Coriandre :**_ "Sois prudente, Pepper. Que devons-nous faire, nous ?"

_**Pepper :**_ "La forêt de Lorbéciande a besoin de nous. Poursuivez votre chemin. Je vous rejoindrai avec Camomille plus tard !"

_**Coriandre :**_ "Bon, très bien. Revenez-nous vite, et en entier !"

_**Pepper :**_ "Ne t'inquiète pas, Coriandre. Les sorcières d'Unitah sont faites pour se retrouver !"

_Coriandre fait un sourire timide. Pepper voit bien l'inquiétude sur son visage, mais elle n'a pas le choix. Elle se saisit de son balai._

_**Pepper :**_ "Si je ne suis pas revenue dans une heure, faîtes sa fête au roi Hartru, rentrez à la planque, et continuez à lutter sans vous soucier de moi."

_**Coriandre :**_ "Impossible ! On ne peut pas vous abandonner !"

_**Pepper :**_ "C'est pourtant ce que vous ferez ! On ne peut pas perdre de temps à se chercher, alors que les gens autour de nous continuent de nous mépriser ! Nous avons de plus en plus d'influence positive sur les gens, il ne faut pas s'arrêter là !"

_**Coriandre :**_ "Mais tu es notre leader..."

_**Safran :**_ "Une équipe peut avancer sans leader ! Tant qu'on est toutes conscientes du rôle à jouer, aucun besoin de chef pour nous donner des ordres !"

_**Coriandre :**_ "Safran, allons ! Pepper, tu ne vas pas laisser dire ça !"

_Pepper observe Safran d'un air déterminé._

_**Pepper :**_ "Si. Je suis même plutôt d'accord. D'ailleurs, si je ne reviens pas, je veux que vous obéissiez à Safran. C'est elle, mon second. Allez, filez vite ! Je dois retrouver Camomille. Messy, avec moi !"

_Pepper enfourche son balai et fonce dans la forêt. Messy lui emboîte le pas dans un râle craquant. Les trois sorcières sont bouches bées face à ses dernières paroles. Surtout Safran, qui écarquille les yeux.  
Pepper s'enfonce dans la forêt, à toute vitesse, suivie par Messy. Elle est très concentrée. Elle utilise sa vision à particules pour repérer le chemin pris par Camomille._

_**Pepper :**_ "Je te connais, Camomille... je sais quelles traces tu laisses derrière toi... ah, non ! Pas cette fois-ci !"

_Elle fait un crochet pour éviter un des pièges de Camomille._

_**Pepper :**_ "Je suis sur la bonne voie !"

_En poursuivant son chemin, elle évite d'autres pièges du même genre. La forêt s'assombrit considérablement.  
Puis, Pepper s'arrête brusquement. À terre, elle aperçoit une jeune sorcière en tenue verte, à genoux, en train de s'occuper d'une biche blessée. Elle a posé un bandage de feuilles autour de sa cuisse._

_**Camomille :**_ "Voilà, tu devrais vite guérir. Allez, retourne près des tiens !"

_La biche se relève avec difficulté. Camomille lui donne un coup de main et elle finit par s'en aller en sautillant. Pepper prend quelques instants pour observer le visage de Camomille. Malgré les traces laissées par les larmes, elle n'a jamais été aussi détendue, aussi souriante... aussi heureuse. Pepper en est presque troublée, et sent son estomac se nouer. Quelles horreurs a-t-elle pu bien lui dire !_

_**Pepper :**_ "Camomille !"

_Celle-ci tourne subitement la tête vers Pepper. Elle prend peur et bondit aussitôt sur un arbre pour s'enfuir._

_**Pepper :**_ "ATTENDS ! Il faut que je te parle !"

_Elle poursuit Camomille avec son balai. Mais Camomille réussit l'exploit d'être plus rapide que Pepper sur son balai supersonique : elle saute d'arbre en arbre à une vitesse presque irréelle, en s'aidant de racines qu'elle fait pousser instantanément depuis le sol. Pepper n'arrive pas à la rattraper._

_**Pepper :**_ "CAMOMILLE !!"

_Cela ne la ralentit pas pour autant. Même, cela semble lui donner encore plus d'énergie ! Elle disparaît d'un coup !_

_**Pepper :**_ "Zut ! Je ne peux pas être aussi lente ! Mais... c'est moi ou je fais du sur-place ?!"

_En effet, le décor n'avance plus, expliquant comment Camomille a pu disparaître aussi vite. Elle a beau forcer sur son balai, Pepper n'avance plus du tout._

_**Pepper :**_ "Mais qu'est-ce qui se passe ?! Ce n'est pas un piège sylvain, pourtant ! Mais..."

_En regardant autour d'elle, elle s'aperçoit qu'elle voit trouble. Toutes les lignes des objets qui l'entourent sont ondulés, comme si elle était dans une bulle d'eau._

_**Pepper :**_ "C'est... c'est de la magie ?"

_**Une voix :**_ "Tiens, tiens, tiens... mais qu'est-ce que nous avons là ?"

_Pepper sursaute. Elle est aussitôt encerclée par des dizaines de gardes, la menaçant de leur épée. Derrière eux, un homme s'avance. Il porte une tenue différente des gardes : un haut blanc surmonté d'une fanfreluche, au motif de lion identique que sur les armures des gardes, et un pantalon serré au niveau des chevilles. Il porte une cape rouge soutenue par une médaille en or reprenant le symbole de Grala. Ses cheveux sont plutôt longs, épais, et de couleur or. La main sur le pommeau de sa longue épée, il se dandine jusqu'à faire face à Pepper, qui le reconnaît bien, même sans son galurin de paille._

_**Pepper :**_ "Prince Cletonal ! C'était vous !"

_**Cletonal :**_ "Et oui ! Vous pensiez que j'avais renoncé à vous poursuivre ? Vous avez eu tort, sorcières ! On ne trompe pas deux fois le prince Cletonal !"

_**Pepper :**_ "Qui vous a trompé ?! Je n'ai fait qu'aider votre compagne ! Vous n'êtes qu'un menteur !"

_**Cletonal :**_ "Vous lui avez fait du mal, je vous ai vues, toutes les deux, avec votre soi-disant amie apothicaire ! Votre magie est vile et sournoise ! Vous vouliez gagner ma sympathie pour mieux vous en prendre à moi ! Mais le prince Cletonal est plus fin que ça ! Vous ne m'aurez pas aussi facilement !"

_**Pepper :**_ "Vous racontez n'importe quoi ! Que dirait votre compagne Sha-One si elle vous entendait ?!"

_**Cletonal :**_ "Elle peut bien dire ce qu'elle veut, elle n'a plus aucune importance à mes yeux."

_**Pepper :**_ "Quoi ?!"

_**Cletonal :**_ "Tu as bien entendu ! Elle m'a trahie, cette peste ! Mais vous le savez mieux que moi ! C'est bien à vous qu'elle transmettait toutes sortes d'informations sur les plans de mon frère !"

_**Pepper :**_ "Mais qu'est-ce que... oh ! La botte secrète de Shichimi... c'était Sha-One ?"

_**Cletonal :**_ "Ha, ha ! On dirait bien qu'il y a encore des cachotteries parmi vous... mais c'est terminé ! J'ai vu clair dans votre petit jeu, sorcières ! Vous avez perverti ma compagne afin qu'elle vous donne des informations confidentielles dans le but de nous attaquer ! Fort heureusement, j'ai des relations fiables qui m'ont permis de pirater cette fichue boule de cristal et de remonter jusqu'à vous ! Je n'ai plus eu qu'à vous attendre, sachant ce que vous aviez appris, et à vous cueillir !"

_**Pepper :**_ "Oh non !"

_**Cletonal :**_ "Mais si ! Ça vous apprendra à essayer de vous en prendre à moi et mon frère ! J'ai tout de suite trouvé suspect toutes vos petites actions pour soi-disant sauver les gens, lorsque je les ai lues dans le journal ! Mais moi, le prince Cletonal, ne peut être dupé ! J'ai tout de suite compris ce que vous essayiez de faire : rallier le peuple à votre cause, tout comme moi il y a quelques jours ! Mais je ne vous laisserai pas les corrompre ! Je vous arrêterai, et dévoilerai votre face cachée au monde !"

_**Pepper :**_ "Ordure ! Vous n'avez rien compris ! Relâchez-moi tout de suite, ou..."

_**Cletonal :**_ "Ou quoi ? Pauvre sotte ! Tu ne t'es pas rendu compte que tu es dans une zone où tu ne peux pas utiliser la magie ?!"

_Pepper prend peur. Elle tente d’utiliser un sort de Destruction, mais rien ne se produit._

_**Pepper :**_ Mais..."

_**Cletonal :**_ "Ha, ha, ha ! Notre Zone Anti-Magie est un succès total !"

_**Pepper :**_ Vous... vous savez utiliser la magie ?!!"

_**Cletonal :**_ "Tu crois que ce privilège est réservé aux sorcières ?! POUR QUI TE PRENDS-TU ?! Pour la reine du monde ?! Non, NON !! Votre pouvoir est la pire injustice que ce monde ait connu, et mon frère et moi, nous allons corriger cette erreur !"

_**Pepper :**_ "Vous utilisez la magie alors que vous soutenez la loi qui l'interdit ! Vous êtes écœurant !!"

_**Cletonal :**_ "Oh, venant de ta part, je le prends comme un compliment ! Inutile de t'égosiller, Pepper de Chaosah ! Personne ne viendra te sauver, cette fois ! Ne compte pas trop sur ton amie animale !"

_**Pepper :**_ "Qu'est-ce que vous lui avez fait ?!!"

_**Cletonal :**_ "Prise au piège. Cette petite sotte était trop occupée à te fuir pour faire attention où elle mettait les pieds ! Elle doit être en route vers notre bien aimé roi Hartru, à l'heure où nous parlons !"

_**Pepper :**_ "Si jamais vous lui faîtes le moindre mal..."

_**Cletonal :**_ "Eh bien, quoi ? Que comptes-tu faire, dans ta position, hein ?! Tu ne peux rien faire pour m'arrêter ! Incline-toi devant ma puissance ! Mais ne te fais pas de souci, sorcière : tu vas bientôt la rejoindre, ton amie à la queue de raton-laveur... beurk, rien que d'y penser, j'en ai des nausées ! Comment des créatures aussi immondes peuvent exister..."

_**Pepper :**_ "C'est vous qui êtes immonde, pourriture !"

_**Cletonal :**_ "Je ne pense pas que le peuple soit de cet avis, quand je serai monté sur le trône ! Allez, emmenez-la au roi ! Offrez-lui une dernière balade avant le bûcher qui lui est destiné !"

_**Pepper :**_ "Le... le bûcher ? Vous comptez nous faire brûler ?!!"

_**Cletonal :**_ "Bien sûr ! C'est ainsi que les hérétiques sont traités, chez nous ! Ne sois pas jalouse, toutes tes amies auront le droit au même traitement, lorsque nous les aurons capturées !"

_**Pepper :**_ "Oh non ! Elles sont en danger... par ma faute..."

_**Cletonal :**_ "Ha, ha ! Réjouissez-vous, gardes ! C'est bientôt la fin d'Unitah !"

_Mais les gardes ne se réjouissent pas. Un gros coup de vent fait bouger le feuillage derrière le prince._

_**Cletonal :**_ "Hum ? Eh bien, qu'attendez-vous pour vous réjouir ?"

_Autre coup de vent, en face du prince. Celui-ci l'a remarqué._

_**Garde :**_ "Altesse, il y a quelque chose qui nous encercle !"

_**Cletonal :**_ "J'ai bien vu ! Gardes, surveillez les environs !"

_Les gardes se déploient, à l'affût du moindre mouvement. Le prince dégaine aussi, aux aguets._

_**Cletonal :**_ "Ce ne peut pas être le raton-laveur... mais alors..."

_Son visage se déforme par la colère. Il se met alors à hurler :_

_**Cletonal :**_ "SORS DE TA CACHETTE, RAVEN ! JE SAIS QUE TU ES LÀ ! AFFRONTE-MOI LOYALEMENT, POUR UNE FOIS !"

_Rien ne se produit. Pepper regarde autour d'elle, d'un air interrogateur._

_**Pepper :**_ "Raven ? Qui est-ce ?"

_Mais la forêt s'obscurcit d'un seul coup. Un puissant coup de vent entoure Pepper. Tout est si sombre et agité qu'elle ne peut rien voir, mais à l'abri dans son piège, elle n'en ressent pas les effets. Elle perçoit à peine les hurlements des gardes._

_**Pepper :**_ "Mais qu'est-ce qui se passe ?!"

_Soudain, un râle craquant se fait entendre. Pepper remarque Messy, juste au-dessus d'elle._

_**Pepper :**_ "Messy... c'est toi qui fais tout ça ?"

_Nouveau râle. Pepper ressent quelque chose de particulier. Non, ce n'est pas lui le responsable de cette tempête. Elle a l'impression que son cœur va exploser, tant il bat à toute vitesse._

_**Pepper :**_ "Mais..."

_D'un coup, une silhouette apparaît devant elle. Elle est imprécise, comme si ses contours n'étaient pas nets. Mais Pepper peut affirmer qu'il s'agit d'une femme, enroulée dans une sorte de kimono rouge et noir, portant des bottes ressemblant à des pattes d'oiseaux géantes. Mais le plus perturbant chez elle, c'est son masque. Il couvre entièrement son visage, et semble à mi-chemin entre un corbeau et un dragon. De longues plumes noires surplombent ce masque, et accompagnent une chevelure tout aussi noire, longue et très épaisse, qui flotte derrière elle. Pepper est bouche bée. Ce qu'elle ressent en sa présence la perturbe._

_**Pepper :**_ "Qui... qui êtes-vous ? C'est vous... Raven ?"

_**Femme :**_ "Je ne serai pas toujours là pour te venir en aide, Pepper."

_Elle tend une de ses mains. Une immense déchirure éclate derrière Pepper, la faisant sursauter. Derrière cette déchirure... tout semble chaotique. L'espace, le temps... Pepper est effrayée. Mais avant qu'elle n'ait pu faire le moindre mouvement, la femme pousse Pepper dans l'ouverture._

_**Pepper :**_ "NOOON !! ATTENDEZ !"

_L'ouverture l'aspire d'un coup. Messy saute dans la déchirure et suit Pepper.  
Ou plutôt, Pepper a l'impression qu'il l'enveloppe. Elle se sent tortillée, broyée, déchirée de toute part, comme si plusieurs personnes essayaient de lui arracher les membres et la peau. La douleur est insoutenable. Elle est incapable de crier : elle a l'impression de ne plus réussir à respirer. Elle n'ose pas ouvrir les yeux, de peur qu'ils sortent de leur cavité. Elle prie pour que ce voyage cesse au plus vite._  

_Son vœu est rapidement exaucé. Elle sent un sol sous ses pieds, mais elle n'a plus la force de se tenir debout. Elle tombe à terre, à moitié consciente. Elle sent quelqu'un s'approcher d'elle, mais elle n'en saura pas plus. Elle sombre dans l'inconscience._  

### Scène 3 - Le village Floréco

_Perdue dans les méandres du chaos, Pepper essaie de distancer la menace qui la poursuit. Mais elle est trop rapide, et gagne du terrain. Elle finit par l'encercler. Pepper est prise au piège._

_**Pepper :**_ "S'il vous plaît... laissez-moi sortir !"

_La menace se matérialise devant elle._

_**Pepper :**_ "Messy ? Qu'est-ce que..."

_Messy pousse un sifflement suraigu et bouillonne d'énergie. Il se transforme alors en une tête de dragon monstrueuse. Rien à voir avec la tête d'un vrai dragon : celui-là semble tout droit sorti du monde des morts, avec ses yeux révulsés et sa peau qui semble fondre. Il rugit et crache un déluge de flammes noires. Pepper hurle._  

_**Pepper :**_ "NON !!"

_Elle se réveille en sursaut. Elle essaie de reprendre sa respiration, mais il lui faut deux bonnes minutes pour comprendre qu'il s'agissait d'un rêve._

_**Pepper :**_ "Mais qu'est-ce qu'il s'est passé ? Où suis-je ?"

_Elle regarde autour d'elle. Elle est dans une petite tente faite d'immenses feuilles, allongée sur un drap de fleurs dorées._

_**Pepper :**_ "Comment ai-je atterri ici ? Oh ! C'est vrai..."

_Elle se rappelle la femme qui l'a jetée dans ce passage bizarre._

_**Pepper :**_ "Qui était-ce ? La Raven dont parlait le prince ? Elle connaissait mon nom... qu'est-ce que c'était, ce passage bizarre ? Et... où est Messy ? Messy ?!"

_Jamais râle ne lui parut aussi rassurant à entendre. Il s'était planqué dans l'obscurité de la tente._

_**Pepper :**_ "Messy ! Tu n'as rien... est-ce que tu sais ce qui est arrivé ?"

_Juste un râle._

_**Pepper :**_ "Je m'en doutais... il faudrait qu'on apprenne à communiquer, tu dois certainement en savoir plus que moi..."

_**Petite voix :**_ "Oh ! Vous êtes réveillée !"

_**Pepper :**_ "AH !"

_Pepper se redresse d'un coup, scrutant la tente. Vide._

_**Pepper :**_ "Qui est là ?"

_**Petite voix :**_ "Par ici ! Juste en bas !"

_Pepper regarde à ses pieds. Elle remarque alors un petit champignon bien agité. Elle s'agenouille pour le regarder de plus près._

_**Pepper :**_ "Est-ce que... est-ce que tu as parlé ?!"

_**Champignon :**_ "Oui, c'est bien moi qui parle ! Je m'appelle Champignax ! Enchanté de vous rencontrer, madame la sorcière !"

_**Pepper :**_ "Euh... moi de même ! Je m'appelle Pepper, et voici Messy. Dis-moi, petit Champignax, peux-tu me dire où je suis ?"

_**Champignax :**_ "Mieux ! Je vais vous montrer ! Suivez-moi !"

_Il guide Pepper à l'extérieur de la tente. Pepper laisse échapper une exclamation de stupeur.  
L'endroit où elle se trouve est gigantesque. Une cascade géante semble couler du ciel et alimente un cours d'eau très long et surtout très clair. L'endroit rayonne de lumière, et se compose d'une multitude d'arbres différents les uns des autres, donnant des fruits toujours plus inédits les uns que les autres. La végétation est dense, des fleurs que Pepper n'a jamais vues tapissent tout le lieu, et rayonnent de vie._

_**Pepper :**_ "Incroyable !"

_**Champignax :**_ "Bienvenue au village Floréco ! Laissez-moi prévenir tout le monde !"

_Le champignon sautille dans l'herbe, tandis qu Pepper observe un peu plus l'endroit. Elle remarque que beaucoup d'animaux, comme des oiseaux ou des poissons, peuplent le lieu. Mais elle remarque aussi que certains fruits bougent d'eux-mêmes, comme le petit Champignax. Tous regardent Pepper avec curiosité, et un peu de frayeur.  
Pepper porte son regard vers la cascade. Une immense statue d'animal orne le jardin. Il doit faire trente mètres de haut. Il s'agit d'un animal aux longues pattes fines, avec un long cou et une petite tête d'où jaillit une multitude de bois de toutes les couleurs. Elle semble transparente, comme faîte uniquement d'eau._ 

_**Pepper :**_ "Sylvagro..."

_Pepper remarque aussi plusieurs autres tentes comme la sienne._

_**Pepper :**_ "Curieux, comme endroit... j'ai l'impression d'avoir changé de planète ! Quel air pur ! Quelle brise agréable ! Et ces tentes... il y aurait d'autres gens ici ?"

_**Champignax :**_ "Eh ! La sorcière est réveillée ! Venez tous !"

_Aussitôt, d'autres êtres humains sortent des tentes. Beaucoup sont exclusivement vêtus d'habits fabriqués avec des feuilles et des fleurs. Ils s'approchent de Pepper avec une certaine appréhension._

_**Pepper :**_ "Euh... bonjour ! Je m'appelle Pepper, et je suis une sorcière qui... euh... s'est égarée ! Je vous suis reconnaissante de m'avoir secourue..."

_Les gens restent craintifs. Pepper se sent gênée._

_**Pepper :**_ "Euh..."

_**Champignax :**_ "Mère !! Venez vite !! La sorcière est réveillée !"

_**Pepper :**_ "Mère ?"

_D'une autre tente, une femme vêtue d'une robe formée de fleurs et de feuilles fait son apparition. Pepper s'en décroche la mâchoire. Elle reconnaîtrait cette chevelure rousse attachée d'un ruban de fleurs à des kilomètres._

_**Pepper :**_ "M... Maître Basilic ?! C'est bien vous ?!"

_La sorcière d'Hippiah met ses mains sur ses hanches._

_**Basilic :**_ "On dirait que tu ne peux pas te passer de moi, Pepper ! Et je ne crois plus être Maître de qui que ce soit ! Bref. Je pense que nous avons des choses à nous dire."

_Un peu plus tard, Pepper et Basilic se sont assises près du cours d'eau. Les autres personnes restent assises au loin, toujours un peu sur leurs gardes. Basilic remplit une sorte de nectar rose et parfumé dans un bol en bois, et la tend à Pepper._

_**Basilic :**_ "Tiens. Tu devrais apprécier."

_Curieuse, Pepper en prend une gorgée._

_**Pepper :**_ "C'est extra ! Sucré et doux à la fois ! Je... il me semble que j'en ai déjà bu."

_**Basilic :**_ "Oui. Tu ne t'en souviens peut-être pas, mais c'était ta boisson préférée étant petite. Du nectar de Rosébore. Tu en réclamais constamment."

_Pepper reste pensive._

_**Pepper :**_ "Oui, ça me rappelle quelque chose..."

_**Basilic :**_ "Tout ça me semble si loin, maintenant. Nos vies ont bien changé depuis. Tu continues à faire de la sorcellerie dans la clandestinité, et moi, je me suis retirée ici."

_**Pepper :**_ "Quel est cet endroit, d'ailleurs ?"

_**Basilic :**_ "Un lieu de retraite pour les êtres vivants qui en ont assez de l'égoïsme et du manque de respect des hommes envers la nature. Ici, l'eau n'a jamais été souillée, les plantes respirent de l'air pur... même certains végétaux ont pu se développer convenablement ! N'est-ce pas, Champignax ?"

_**Champignax :**_ "Oh oui, Mère !"

_**Pepper :**_ "Il vous appelle Mère..."

_**Basilic :**_ "Oui, j'ai contribué à lui donner une conscience."

_**Pepper :**_ "Vous... vous pouvez faire ça ? Je pensais que seule Zombiah pouvait..."

_**Basilic :**_ "Il ne s'agit pas d'animer des pantins sans âme ! Mais d'insuffler un désir de vivre à un être déjà vivant. Maintenant que la magie est interdite, je ne peux plus faire ce genre d'expérience là-haut. Au fond, c'est une bonne chose. J'ai enfin pu créer le paradis dont j'avais toujours rêvé."

_**Pepper :**_ "Là-haut ? Vous voulez dire que... nous sommes dans le sol là ?!"

_**Basilic :**_ "Oui."

_**Pepper :**_ "Incroyable ! On dirait qu'on est à l'air l... attendez, "créer" ? Vous voulez dire que vous avez créé cet endroit ?! Toute seule ?!"

_**Basilic :**_ "Je te l'ai dit, Pepper. Je ne suis pas une simple horticultrice. Et je ne l'ai pas entièrement créé. Je me suis servie d'une base. Vois-tu, ici, jadis, aux fins fonds de la forêt de Lorbéciande, le culte de Sylvagro, l'esprit de la forêt, était célébré. Mais lorsque je l'ai trouvé, ce n'était plus que des ruines, sûrement détruites par l'inconscience et le manque de respect de l'homme envers les sites sacrés. Seule cette statue était intacte... pour lui rendre hommage, j'ai décidé d'établir ici un lieu où la Nature reprendrait ses droits, inaccessible à quiconque n'étant pas un ami de la Nature. Et voilà ce que j'en ai fait. Le jardin que j'ai toujours voulu créer."

_**Pepper :**_ "Vos pouvoirs sont impressionnants ! Et eux, là-bas ? Vous les avez créés aussi ?"

_**Basilic :**_ "Bien sûr que non ! Je ne peux pas créer des êtres humains ! Enfin... euh... pas avec la magie, en tout cas ! Non... ce sont des âmes égarées qui ne supportaient plus que les humains souillent et polluent leur environnement. Je leur ai proposé de venir avec moi et d'enfin vivre en paix et en harmonie dans ce jardin isolé du reste du monde."

_**Pepper :**_ "Je vois... c'est pour ça qu'ils se méfient de moi..."

_**Basilic :**_ "Comprends-les. Se retrouver face à la sorcière qui a détruit Hippiah à elle toute seule, c'est plutôt effrayant."

_Pepper lance un regard de reproches vers Basilic._

_**Pepper :**_ "Vous leur avez raconté cet accident ?!"

_**Basilic :**_ "Ils doivent connaître les ennemis de la Nature. Sylvagro ne sera pas toujours là pour veiller sur eux."

_**Pepper :**_ "JE NE SUIS PAS UNE ENNEMIE DE LA NATURE !"

_Plusieurs animaux rentrent se cacher dans leur tanière. Les habitants du village se recroquevillent, plus effrayés qu'avant. Pepper se reprend._

_**Pepper :**_ "Je... je suis désolée. Je ne voulais pas vous effrayer."

_**Basilic :**_ "Voilà, c'est ça dont je parlais, Pepper. Je ne pense pas que tu sois une ennemie de la Nature. Mais ta colère t'en fait devenir une. Cette colère que tu n'as jamais su contrôler, même étant enfant. Cette rage incessante est un danger pour nous comme pour toi. C'est dans ce but que je les ai prévenus, et qu'ils ont peur de toi."

_**Pepper :**_ "Mais..."

_Elle regarde de nouveaux les habitants. Leur frayeur la touche profondément._

_**Pepper :**_ "Je..."

_**Champignax :**_ "Moi, je n'ai pas peur de la sorcière !"

_Il surprend tout le monde, surtout Pepper._

_**Pepper :**_ "Champignax..."

_**Champignax :**_ "Elle a sauvé plein de gens ! Elle est la chef d'Unitah ! Elle se bat pour une cause juste ! Elle ne peut pas être méchante !"

_Les habitants ne semblent pas convaincus, mais leur peur s'est transformée en curiosité._

_**Basilic :**_ "Tu vois, Pepper ? Les plantes, les fruits... ils ne connaissent pas la sournoiserie. L'envie. L'égoïsme. La colère, ou même la rancune. Ils sont naturellement vertueux. Ils ne chercheront jamais à faire le mal, à déclencher des guerres. La paix absolue... c'est pour cela que j'ai créé cet endroit. Une vague de paix et de tranquillité. C'est ce dont j'ai toujours rêvé... et ce n'est pas possible dans un monde où règnent la peur, l'avarice, ou la violence. Dans un monde d'hommes."

_Pepper reste silencieuse. Elle réfléchit quelques instants, le regard perdu dans l'immense prairie qui s'étend devant elle._

_**Pepper :**_ "On a l'impression d'être dans un rêve."

_**Basilic :**_ "En se donnant du mal, certains rêves se réalisent, Pepper."

_**Pepper :**_ "Mais c'est bien ce dont il s'agit. Un rêve. Une utopie. Maître Basilic..."

_**Basilic :**_ "Juste Basilic."

_**Pepper :**_ "Tout ceci n'est qu'un rêve. Tout porte à croire qu'il s'agit d'un lieu idéal, mais... vous n'avez fait que déporter le problème. Qui vous dit que ça durera éternellement ? Que personne ne viendra souiller votre précieux jardin ? Ce n'est qu'un endroit sur Hereva parmi d'autres, une cachette où vous vous voilez la face, mais ce n'est pas la réalité ! La réalité, c'est que les gens ont perdu la foi envers les sorcières et la magie ! Qu'il faut réagir pour leur faire retrouver espoir, et non pas se cacher dans une illusion comme vous le faîtes afin de continuer à utiliser la magie dans votre coin ! Il faut se battre pour obtenir ce qu'on veut, et pas se terrer comme un lâche !"

_Basilic lui lance un regard noir. Les habitants sont perplexes quant à ces paroles._

_**Basilic :**_ "Ne vous inquiétez pas, mes amis. Ne l'écoutez pas. Elle essaie de vous faire douter. Vous ne risquez rien en restant ici avec moi."

_**Pepper :**_ "Bien sûr que si ! Le roi Hartru est en quête pour déverrouiller une arme antique, et il cherche désespérément à utiliser la puissance de Sylvagro pour ça ! Si les gardes du roi Hartru trouvent ces ruines, vous ne pourrez rien faire ! Ils s'accapareront les pouvoirs de Sylvagro pour nous détruire ! Ils savent utiliser la magie, et pire : l'annuler ! Vous ne pourrez pas les protéger !"

_Nouvelle vague de panique._

_**Habitant 1 :**_ "Ils en veulent après Sylvagro ?"

_**Habitant 2 :**_ "Tu... tu crois qu'ils ont réussi à remonter jusqu'à nous ?"

_**Habitant 3 :**_ "Non... de toute façon, Sylvagro nous protégera contre les intrus ! N'est-ce pas, Basilic ?"

_Basilic est bien plus effrayée qu'elle ne voudrait l'admettre. Son visage s'enfonce dans le déni._

_**Basilic :**_ "C'est absurde ! Jamais ils ne trouveront ces ruines. Personne ne peut rentrer ici. L'entrée est verrouillée !"

_**Pepper :**_ "Ah oui ? Et d'après vous, comment j'ai pu entrer, moi ?"

_Les habitants s'agitent de plus en plus. Même Champignax et les autres fruits commencent à prendre peur._

_**Basilic :**_ "Ce serait plutôt à toi de me le dire, ça ! Champignax t'a trouvée inconsciente près de la statue de Sylvagro."

_**Pepper :**_ "Je... je ne sais pas. J'ai été capturée par les gardes du prince Cletonal, et... tout à coup, une femme mystérieuse avec un masque à plumes est apparue devant moi, et... elle a ouvert une sorte de déchirure, et m'a poussée dedans. Je me suis réveillée ici."

_**Basilic :**_ "Une femme avec un masque à plumes, tu dis ?! Une déchirure... non... pas elle..."

_**Pepper :**_ "Vous la connaissez ?"

_**Basilic :**_ "Pas personnellement. Elle est connue sous le nom de Raven. On raconte que ses pouvoirs sont très puissants, et qu'elle sévit un peu partout avec son groupe pour commettre des actes de vandalismes... mais ce n'est pas très important ! À part Raven, personne ne peut pénétrer ici ! Personne !"

_**Pepper :**_ "Et imaginez une minute que cette Raven collabore avec le roi Hartru ? Ou soit obligée d'utiliser ses pouvoirs pour son compte ? Envahir cet endroit reste une possibilité ! Vous ne pouvez pas rester sans réagir ! Il faut vous battre pour récupérer votre droit d'utiliser la magie ! Votre liberté ! Et espérer changer les mentalités sur la magie, sur l'environnement ! C'est en se battant pour ce qu'on croit qu'on obtient ce qu'on veut."

_Basilic reste silencieuse, les sourcils froncés. Le discours de Pepper ne semble pas lui plaire. Pepper la juge du regard, en essayant de ne pas ciller.  
Un des habitants s'avance, l'air assuré._

_**Habitant :**_ "Basilic. Laissez-nous combattre aux côtés de la jeune sorcière."

_**Basilic :**_ "Pardon ? Qu'est-ce que vous dîtes ?! Vous voulez vous battre ?!"

_**Habitant 2 :**_ "Oui ! Nous aussi, nous voulons changer les mentalités !"

_**Habitant 3 :**_ "Pendant que nous nous complaisons ici, les arbres, la végétation, les animaux continuent de souffrir, là-haut ! C'est intolérable ! On ne peut pas les laisser dans la douleur sans rien faire !"

_Chaque habitant se lève, et rejoignent les deux premiers. Ils ont l'air déterminé._

_**Basilic :**_ "Mes amis... la guerre et la violence sont les pires des fléaux ! Que pensez-vous qu'il va vous arriver, si vous sortez d'ici pour affronter le monde ? Vous vous ferez broyer en quelques instants ! C'est ce que vous voulez ?"

_**Habitant :**_ "La guerre est peut-être le prix à payer pour la paix. Nous voulons d'un monde paisible, pas seulement d'un jardin virtuel."

_Tout le monde semble d'accord sur ce point. Basilic incline la tête._

_**Basilic :**_ "Pauvres fous... vous avez déjà oublié la règle numéro un de cet endroit ?"

_Les habitants semblent bien embêtés._

_**Pepper :**_ "Quelle est cette règle ?"

_**Basilic :**_ "Quiconque pose le pied ici devra y mourir."

_**Pepper :**_ "Quoi ?! Vous plaisantez ?! Vous ne pouvez pas empêcher les gens de partir s'ils le souhaitent !"

_**Basilic :**_ "Question de sécurité. Je refuse d'ouvrir la porte à quelqu'un connaissant l'emplacement de ces ruines. Ce serait s'exposer au danger, ce fameux danger dont tu me rebats les oreilles depuis tout à l'heure ! En empêchant les gens de sortir, le risque est diminué."

_**Pepper :**_ "C'est... c'est affreux ! Ce n'est pas un jardin... c'est une prison ! Une prison dorée, où on choisit de se rendre, mais une prison quand même ! Nous n'avez pas le droit !"

_**Basilic :**_ "C'est ainsi ! Je ne peux pas prendre le risque de gâcher mon rêve !"

_**Pepper :**_ "C'est de l'égoïsme ! Vous aviez le titre de Maître, mais vous ne l'avez jamais été pour quiconque ! Laissez-nous sortir, maintenant ! Nous avons un combat à mener !"

_**Basilic :**_ "Je... je refuse !"

_Elle se dirige à l'écart, vers le champ de fleurs, et en cueille une. Le petit Champignax se dirige vers elle en sautillant._

_**Champignax :**_ "Mère... ce que vous avez créé ici est incroyable. Je vous suis reconnaissant de m'avoir donné la vie ! Mais vous ne pouvez pas contrôler celle des autres pour vos désirs personnels. S'ils veulent partir, vous ne pouvez pas les en empêcher !"

_Basilic réfléchit, profondément tourmentée._

_**Basilic :**_ "Non... ils détruiront mon rêve... je ne peux pas..."

_Au tour de Pepper de s'approcher de Basilic._

_**Pepper :**_ "Basilic. Je n'ai que quelques souvenirs de mon séjour à Hippiah. Je vous en ai toujours voulu d'avoir fait de moi une cible de moqueries, mais je me souviens que vous étiez une enseignante très patiente envers les autres sorcières qui se débrouillaient mieux que moi. J'étais peut-être une cause perdue, après tout... mais je ne garde pas l'image d'une sorcière égoïste, qui a peur. Je garde l'image d'une sorcière ouverte, qui veut insuffler le goût d'apprendre et d'aimer aux autres. Vous ne voulez pas poursuivre l'enseignement ? Il y a des gens là-haut qui manquent cruellement de compassion."

_Basilic écoute, mais reste dans le déni._

_**Basilic :**_ "Non, je ne peux pas... je..."

_**Pepper :**_ "Je vous en prie, Maître ! Aidez-moi à rétablir la magie ! Aidez-moi à sauver mes amies ! Elles sont en danger... et Camomille a été capturée par les gardes du roi Hartru !"

_Basilic a un sursaut, comme un électrochoc._

_**Basilic :**_ "Quoi ? Camomille ? MA Camomille ?! Elle a été..."

_**Pepper :**_ "Oui, ils l'ont attrapée ! Et peut-être que mes autres amies sont dans le même cas ! Et... et ils nous ont réservé un... un bûcher... je vous en conjure, Basilic ! Ouvrez cette porte, et aidez-moi à sauver Camomille ! Et les autres !"

_Basilic se redresse d'un coup. Elle se tourne vers la statue de Sylvagro, puis vers les habitants de Floréco. Ils affichent un visage déterminé._

_**Basilic :**_ "Mes amis... vous avez raison. Se terrer ici ne résoudra rien. S'il y a bien quelqu'un qui peut régler les problèmes de l'environnement, c'est bien vous ! Et je vais vous y aider ! Bien que la violence me révulse, il faut bien faire taire ceux qui l'utilisent à des fins égocentriques !"

_Les habitants sont ravis. Pepper est soulagée._

_**Basilic :**_ "Bien. Je vais ouvrir la porte. Restez bien en arrière !"

_Pepper s'écarte. Messy pointe le bout de son nez hors de la tente. Basilic tend une main vers la statue de Sylvagro et ferme les yeux. Elle entre en résonance avec la statue et s'illumine d'une lueur verte. Basilic effectue ensuite un geste vertical cassant. Deux lumières s'allument vers la tête de la statue, et un passage lumineux s'ouvre._

_**Pepper :**_ "Incroyable..."

_**Basilic :**_ "Allons-y vite, avant que..."

_**Cletonal :**_ "...quelqu'un ne s'y engouffre ? C'est un peu tard pour cela, sorcière."

_Pepper, horrifiée, regarde le prince Cletonal passer l'ouverture, avec une véritable armée derrière lui, transportant une prison similaire à celle où elle se trouvait, comprenant Camomille, Shichimi, Safran et Coriandre._

_**Pepper :**_ "Oh non ! Elles ont toutes été capturées..."

### Scène 4 - Contre-attaque

_Affichant un visage victorieux, le prince Cletonal se pavane tranquillement en regardant les lieux. Les habitants et les animaux (et les végétaux) sont terrifiés. Leur cauchemar d'être trouvés vient de se réaliser._

_**Cletonal :**_ "Hum... que voilà un jardin somptueux ! Il plaira beaucoup à mon frère !"

_**Pepper :**_ "Il n'est pas à vous ! Sortez d'ici tout de suite !"

_**Cletonal :**_ "Encore toi, bien sûr ! Tu m'expliques comment tu as pu t'échapper de ta cage ?! Oh, j'y suis... c'est encore un coup de cette satanée Raven... mais aucun risque qu'elle nous mette des bâtons dans les roues ici."

_Il se tourne vers la statue de Sylvagro avec un large sourire._

_**Cletonal :**_ "Le voici... aussi grand et imposant que mon frère me l'avait promis ! À présent, vous savez ce que vous avez à faire, gardes !"

_Les gardes se précipitent vers la statue. Ils se saisissent d'arcs et commencent à tirer des flèches enflammées vers la statue._

_**Basilic :**_ "QU'EST-CE QUE VOUS FAÎTES ?!! Arrêtez immédiatement ! Vous allez tout détruire !"

_**Cletonal :**_ "Désolé, sorcière d'Hippiah, mais les légendes sont claires ! Pour faire apparaître l'esprit de la forêt, il faut menacer ladite forêt ! Et détruire ces ruines où il était célébré est un bon début ! Allez, gardes ! Réduisez cet endroit à néant !"

_D'autres gardes prennent des flèches enflammées, et visent tout autour d'eux._

_**Basilic :**_ "Il est hors de question que je vous laisse faire !"

_**Pepper :**_ "Moi non plus !"

_**Cletonal :**_ "Oh, sûrement pas !"

_Il claque des doigts. Aussitôt, un garde avec une arme cylindrique tire en direction de Basilic et Pepper. Elles se retrouvent aussitôt enfermées dans une bulle anti-magie, comme les autres sorcières._

_**Pepper :**_ "C'est pas vrai !!"

_**Basilic :**_ "Mais... qu'est-ce qu'il se passe ?! Pourquoi ma magie est sans effet ?!"

_**Pepper :**_ "C'est la fameuse anti-magie dont je vous ai parlé ! Nous ne pouvons rien faire dans ces cages..."

_**Basilic :**_ "Oh non..."

_**Cletonal :**_ "Allez-y !"

_Les gardes poursuivent leur élan de destruction. Bientôt, le feu envahit le jardin, consume les fleurs, détruit les arbres. Les fruits sont acculés dans un coin, comme les animaux. Les habitants, terrorisés, se sont réfugiés dans un arbre que le feu n'a pas frappé, mais qui ne va pas tarder à le consumer. Le prince semble ravi._

_**Cletonal :**_ "Bien, bien ! Il ne devrait plus tarder à se montrer, maintenant !"

_**Basilic :**_ "ORDURE ! Comment osez-vous vous en prendre à la Nature ?!"

_**Cletonal :**_ "Qui se fiche de la Nature, de nos jours ? Seules mes ambitions comptent ! Je ne vais pas laisser trois carrés d'herbes me barrer la route, non ?"

_**Basilic :**_ "Cloporte ! Vous le regretterez !"

_**Cletonal :**_ "Vous voyez, ça, j'en doute !"

_Ce qui désole le plus Pepper, c'est que Camomille assiste à tout ceci. Elle la voit, dans sa bulle, avec des yeux remplis de colère et de haine. Jamais elle ne pourra refaire confiance à un être humain après ça... si jamais elles réussissent à s'en sortir vivantes. Pepper regarde autour d'elle, mais elle ne voit aucune échappatoire._

_**Pepper :**_ "Oh, Camomille... Basilic... je suis tellement désolée..."

_**Habitant :**_ "Nous ne vous laisserons pas détruire ce jardin sans réagir !"

_Toutes les sorcières se retournent, stupéfaites. Les habitants de Floréco sont descendus de l'arbre où ils s'étaient réfugiés, plus déterminés que jamais._

_**Cletonal** (d'un air moqueur) **:**_ "Vous ? Ha, ha, ha ! Et qu'est-ce qu'une poignée d'écolos compte faire pour m'arrêter ? Me jeter des fleurs au visage ? Marmonner des chants d'oiseaux ?"

_Les gardes se mettent à rire. Les habitants ne sont pas impressionnés._

_**Habitant :**_ "Nous ne pouvons peut-être rien contre vous, mais nous allons quand même tout faire pour vous arrêter ! Pour protéger le don qu'Hereva nous a fait ! Pour faire comprendre au monde qu'on ne peut pas détruire la faune et la flore sans conséquences !"

_Les habitants poussent un cri d'encouragement, qui déstabilise quelque peu le prince. Camomille est sous le choc. Même le petit Champignax s'est mis à bondir._

_**Champignax :**_ "Pour la Nature ! Pour la Nature ! Faîtes fuir les vilains gardes !"

_Le prince se reprend, plus en colère que jamais._

_**Cletonal :**_ "Vous l'aurez voulu ! Vous allez voir ce qu'il en coûte de s'opposer à la puissance de Grala ! Gardes ! Supprimez-moi ces présomptueux !"

_Les gardes s'arment de leurs flèches enflammées. Ce qui n'empêche pas les habitants de courir dans leur direction._

_**Basilic :**_ "ARRÊTEZ ! C'est de la folie !!"

_Les gardes tirent sur leur arc. Pepper ferme les yeux et prie de toutes ses forces._

_**Pepper :**_ "Je vous en prie, Raven ! Qui que vous soyez, on a besoin de vous MAINTENANT !"

_Mais les flèches ne partent pas. Chacun des gardes voit son arme être arrachée par une substance visqueuse, qui va s'écraser sur un arbre, et prend la forme d'une toile d'araignée. Gélatineuse._ 

_**Cletonal :**_ "Quoi ?! Quel est ce prodige ?!"

_Il se tourne vers la statue de Sylvagro. À son sommet, une jeune fille avec six mains sur les hanches, et six yeux en colère, juge le prince._

_**Pepper :**_ "Princesse Armoracia !!"

_Elle tourne subitement ses yeux agacés vers Pepper._

_**Armoracia :**_ "Princesse Pepper ! Dame Safran ! Princesse Coriandre ! PLUS JAMAIS vous ne me laisserez derrière vous sous prétexte de danger, c'est bien compris ?!"

_Pepper est déstabilisée de voir Armoracia en colère contre elle. Il en va de même pour Safran et Coriandre dans leur cage. Camomille est sous le charme._

_**Camomille :**_ "Une véritable héroïne..."

_**Safran :**_ "Oui, euh... vous savez, darling, on ne pensait pas que... enfin..."

_**Armoracia :**_ "Que je sois capable de me battre ?! Eh bien ne jugez plus jamais quelqu'un sur son apparence, et laissez-moi vous prouver que je sais me défendre !"

_**Pepper :**_ "C'est d'accord, Princesse Armoracia ! Mais... vous pouvez nous libérer ?"

_**Cletonal :**_ "Eh !! Vous le dîtes, si je vous dérange, hein ! AÏE !"

_Le prince se débat avec trois animaux qui lui griffent et picorent la tête. Il finit par leur faire lâcher prise, et constate qu'il s'agit de deux chats, l'un orange, l'autre blanc, et d'une poule noire._

_**Cletonal :**_ "Qu'est-ce que ça veut dire ?!"

_**Pepper :**_ "Carrot !"

_**Safran :**_ "Truffel chérie ?!"

_**Coriandre :**_ "Mango ! Mais que faîtes-vous ici ?!"

_Les trois animaux désignent Armoracia, qui tripote ses nombreux doigts entre eux, un peu honteuse._

_**Pepper :**_ "Princesse !!"

_**Armoracia :**_ "Je... je ne pouvais pas les laisser là-bas, tout seuls ! Et ils ont insisté pour venir m'aider..."

_**Cletonal :**_ "Il suffit ! Abattez-moi ce monstre ! Et ces maudits animaux !!"

_Carrot, Truffel et Mango esquivent une salve de flèches de justesse, et bondissent sur les gardes pour les attaquer. Les gardes qui tiraient sur la statue visent Armoracia. Cette dernière, avec grâce et élégance, esquive les flèches, utilise du fil d'Ahrachnerre pour descendre au sol, et pour en jeter au visage des gardes. En voilà la plupart piégés par le fil de la princesse._

_**Pepper :**_ "Génial !!"

_**Cletonal :**_ "Grrr... sale monstre ! Laisse mes gardes tranquilles !"

_Armoracia se tourne vers le prince. Les flammes la rendent encore plus grande et menaçante que d'ordinaire._

_**Armoracia :**_ "S'il y a un monstre ici, ce n'est certainement pas moi !"

_Cletonal prend peur. Il fait quelques pas en arrière, mais il est retenu par les habitants._

_**Habitant :**_ "Dîtes... vous ne nous avez pas oubliés, au moins ?"

_**Cletonal :**_ "À moi, la garde !!"

_Les gardes laissent tomber leur arc et dégainent leur épée. Armoracia profite de la confusion pour détruire l'arme qui a servi à générer les prisons d'anti-magie. Ces dernières tombent, et les sorcières sont de nouveau libres._

_**Pepper :**_ "Princesse ! Vous avez été héroïque !"

_**Shichimi :**_ "Oui, c'était bien joué !"

_**Safran :**_ "Classe et courageuse... vous êtes mon idole, darling !"

_**Coriandre :**_ "Félicitations ! Vous êtes adroite et puissante ! Vous nous avez sauvées !"

_Armoracia est gênée par autant de compliments. Camomille ne sait pas quoi dire._

_**Armoracia :**_ "Oh, je n'ai pas fait grand-chose... pour tout vous dire, j'étais morte de peur, sur cette statue ! Mais j'espère que maintenant, vous ne me verrez plus comme un être fragile !"

_**Pepper :**_ "Aucun risque ! Mais... comment vous nous avez suivies ?"

_**Basilic :**_ "Plus tard, la causette ! Nous avons des intrus à repousser !"

_**Camomille :**_ "Maître..."

_Camomille s'est tournée vers son ancien Maître, le visage mélancolique. Basilic sourit._

_**Basilic :**_ "Ma chère Camomille... je suis heureuse que tu te portes bien, et que tu sois aussi bien entourée. Mais remettons ces retrouvailles à plus tard. Pour l'heure, nous devons repousser les ennemis de la Nature. Ensemble."

_**Pepper :**_ "Oui ! Camomille, je regrette de t'avoir dit ces vilains mots. Je ne les pensais pas. Tu es quelqu'un de bien, qui aime être seule. Et je vois maintenant pourquoi tu détestes les humains ! Mais ne t'inquiète pas, je vais te montrer qu'il existe des humains qui s'intéressent au sort des plantes et qui feront tout pour les défendre ! Mes amies ! Montrons l'étendue de la puissance d'Unitah ! Pour Camomille !"

_Les sorcières poussent un cri d'encouragement. Des larmes perles sur les joues de Camomille._

_**Camomille :**_ "Mes amies... Maître... vous avez raison : faisons payer à ce prince son comportement !"

_Chacune des sorcières se prépare à attaquer. Les habitants commencent à être en difficulté face aux gardes et au prince. Les animaux se sont fait acculer dans un coin. Carrot s'est positionné devant Truffel et Mango pour les protéger, en grognant pour intimider les gardes. Le prince finit par s'approcher d'eux, avec un sourire diabolique._

_**Cletonal :**_ "C'est terminé pour vous, les matous et la poule ! Vous allez voir ce qu'il en coûte de s'en prendre à mon intégrité physique !"

_Il lève son épée. Carrot reste devant et ferme les yeux, prêt à accepter son sacrifice. Mais la lame ne tombe pas._ 

_**Pepper :**_ "Je te déconseille de toucher à mon ami, sans-cœur !"

_Le visage de prince s'est décomposé en voyant les sorcières se diriger vers lui. Il recule d'un pas._

_**Cletonal :**_ "Oh non... NON ! RETRAITE !"

_Il s'apprête à s'enfuir, mais Armoracia génère une grande toile pour le piéger, avec toute son armée, tout en évitant les habitants qui se mettent à couvert. Les chats et la poule sont désormais hors de danger._

_**Armoracia :**_ "Pas si vite, vilain !"

_**Cletonal :**_ "Lâche-moi, monstre !! JE T'ORDONNE DE ME RELÂCHER SUR-LE-CHAMP !"

_**Armoracia :**_ "Je ne reçois d'ordre que de ma reine, pas d'un méchant bonhomme qui ne se soucie que de lui-même !"

_Pepper s'avance vers Cletonal, qui cesse d'essayer de se dépêtrer de sa toile pour lui lancer un regard horrifié._

_**Cletonal :**_ "Non ! Pitié ! Je ferais ce que vous voulez !"

_**Pepper :**_ "Prépare-toi, ordure ! Tu vas recevoir la monnaie de ta pièce !"

_**Cletonal :**_ "NON !!"

_Avec un regard de dingue, Safran détourne l'intégralité du feu présent dans le jardin pour le faire tournoyer autour de son bras. Camomille et Basilic font naître une puissante plante carnivore du sol. Shichimi invoque un esprit pour le faire léviter. Coriandre zombifie la plante pour la rendre mobile. Ensuite, Pepper et Safran s'associent pour que la plante aspire le feu, et le recrache comme un puissant rayon renforcé par magie en direction de l'armée du prince Cletonal, qui ne peut que hurler. L'impact est suffisamment violent pour générer un souffle de la force d'une tempête. Lorsque la fumée provoquée par l'impact se dissipe, le prince Cletonal, tout tremblant, baisse ses bras pour constater ce à quoi il vient d'échapper : un énorme cratère fumant. La plante carnivore a disparu. Pepper s'approche à nouveau de lui, les bras croisés, le regard froid._

_**Pepper :**_ "Non, nous ne sommes pas des meurtrières. Mais si vous refusez de répondre à nos questions... nous viserons juste la prochaine fois. Est-ce clair ?!"

_Le prince est trop terrorisé pour répondre. Il regarde chacune des sorcières, les bras croisés, le regard mauvais dirigé contre lui. Ainsi que celui des habitants de Floréco, des animaux, des fruits. Même Champignax le fusille du regard. Et pis que tout, ce petit être chaotique au-dessus des sorcières, qui diffuse un râle inquiétant, comme s'il allait aspirer son âme pour le punir. Le prince Cletonal s'avoue vaincu._

_**Cletonal :**_ "Bien... vous avez gagné. Je répondrai à vos questions."

### Scène 5 - Empathie

_Un cri de joie parcourt le jardin. Les sorcières et les habitants de Floréco célèbrent leur victoire éclatante sur le prince Cletonal, toujours emprisonné dans la toile d'Armoracia avec ses hommes. Les autres animaux viennent près de Carrot, et l'acclament pour son héroïsme. Bien que déjà gêné par une telle ovation, ce ne fit qu'empirer lorsque Truffel lui fit cadeau d'une petite léchouille sur le museau pour le remercier de l'avoir défendue. Carrot tombe dans les pommes avec une expression de pur bonheur sur le visage, sous le rire des autres animaux et des fruits.  
Champignax est intenable : il saute partout autour des sorcières._

_**Champignax :**_ "Je le savais ! Je le savais que les sorcières n'étaient pas méchantes, Mère ! Elles nous ont sauvés !"

_**Basilic :**_ "Oui, mon brave Champignax. Elles sont épatantes pour des débutantes. Quelle action combinée remarquable parfaitement synchronisée ! C'est comme si vous n'étiez que la part d'une seule et même entité ! Vous avez dû travailler dur pour en arriver à un tel niveau."

_**Pepper :**_ "À vrai dire... c'était la première fois qu'on réussissait un sort combiné..."

_**Basilic :**_ "Ah... enfin, ça n'enlève rien à votre exploit. Et je vous remercie d'avoir préservé cet endroit. C'est très important pour moi."

_**Safran :**_ "De rien, darling ! Ce fut un plaisir ! Mais n'oublions pas que sans moi, votre jardin serait parti en fumée ! Heureusement que je suis là..."

_**Les autres sorcières :**_ "Safran !!"

_**Safran :**_ "Oh, là, là ! Un peu d'humour, que diable ! Nous savons tous qui est la véritable héroïne du jour ! Notre bien aimée (et très classe) princesse des arachnides !"

_Elle désigne Armoracia comme on dévoilerait une merveille du monde. Tout le monde l'applaudit alors, ce qui la fait rougir._

_**Habitant :**_ "Oui ! Elle a été héroïque ! Vous avez sauvé notre jardin... vous avez notre reconnaissance éternelle !"

_**Armoracia :**_ "Oh, chers habitants de Floréco... mes amies... ce n'était vraiment pas grand-chose !"

_**Safran :**_ "Ce n'est jamais grand-chose pour vous, darling ! Mais vous avez su mêler grâce et puissance... je suis très agréablement surprise ! Plus question de vous laisser sur le côté ! Vous êtes désormais membre à part entière d'Unitah ! Je suppose que cette fois-ci, personne ne va me contester, non ? De toute façon, j'enverrai quiconque s'y opposant valser avec mes flammes !"

_**Pepper :**_ "Inutile de nous menacer, Safran ! Bien entendu qu'elle fait partie de notre équipe ! Elle l'a toujours été... nous l'avons juste mal jugée, encore une fois... pardon, Princesse. Je vous promets que ça ne se reproduira plus."

_**Armoracia** (croisant les six bras, levant la tête) **:**_ "J'espère bien !"

_Elle ne tient pas longtemps dans cette pause. Tout le monde autour d'elle se met à rire. Elle décroise vite les bras et fait un sourire gêné._

_**Armoracia :**_ "Hé, hé... je ne suis pas crédible à ce point ?"

_**Pepper :**_ "La vexation vous va si mal, Princesse !"

_**Coriandre :**_ "Vous rayonnez tellement qu'aucun sentiment négatif ne vous convient ! Et on préfère vous voir sourire !"

_Nouvelle vague de rire. Pepper n'a jamais été aussi ravie. Elle a vraiment l'impression de faire partie d'une famille.  
Son regard se reporte sur le prince. Son visage s'assombrit._  

_**Pepper :**_ "À nous deux, Prince. On a quelques questions pour vous."

_**Cletonal :**_ "Oui, vous l'avez déjà dit, ça ! Qu'est-que vous voulez savoir ? Comment jeter une malédiction sur mon peuple ?!"

_**Pepper :**_ "Bien sûr que non ! Nous voulons savoir comment vous avez trouvé cet endroit ? Basilic avait dissimulé l'entrée !"

_**Cletonal :**_ "Hé, hé... vous êtes tellement pitoyables ! Vous êtes des sorcières et vous ignorez tout des secrets de notre monde ! Je ne suis pas étonné... les sorcières ont toujours méprisé tout et tout le monde, depuis la nuit des temps ! Pas de temps pour étudier l'histoire..."

_**Pepper :**_ "Nous ne sommes pas méprisantes ! On essaie d'apporter un peu de joie et de bonheur dans ce monde tristement sombre ! Et rétablir le droit de l'usage libre de la magie n'est qu'un début ! On compte bien aller plus loin, comme par exemple empêcher des fous dangereux dans votre genre de souiller un site sacré et de vous en prendre à la Nature !"

_**Cletonal :**_ "Peuh, c'est ça... vous pouvez dire ce que vous voulez, je ne vous crois pas."

_**Pepper :**_ "Vous verrez bien par vous-même ! Mais ça ne répond pas à ma question !"

_Cletonal soupire._

_**Cletonal :**_ "Ah... voilà à quoi j'en suis réduit : à instruire des sauvages ! Bon, très bien, vous voulez tout savoir ? Mon frère détient la clé de la libération de l'énergie du monde ! Elle est contenue dans un être mécanique qui, à ce qu'on raconte, était le gardien principal de l'Arbre de Komona, à l'époque !"

_**Pepper :**_ "L'Arbre de Komona ?!"

_**Cletonal :**_ "Oui, vous devez bien le connaître, vu que l'avez utilisé pour déchaîner votre magie maléfique à travers le monde ! Vous avez idée des dégâts que vous avez causés ?! Du nombre de familles que vous avez détruites, du nombre de toits que vous avez supprimés par votre malfaisance et votre égoïsme ?!!"

_Les sorcières baissent un peu la tête._

_**Pepper :**_ "Je... ce n'était pas ce que nous voulions..."

_**Cletonal :**_ "Bien sûr... voulu ou pas, votre pouvoir est dangereux pour le monde ! C'est pour cela que mon frère veut obtenir le contrôle de cet Arbre via le gardien mécanique ! Il pourra ainsi contrôler la source de votre pouvoir, le Réa, afin que vous cessiez d'en faire un mauvais usage ! Et pour cela... nous avions besoin du pouvoir de ce maudit esprit de la forêt... qui ne s'est pas montré, d'ailleurs ! Quel esprit de la forêt laisserait la sienne mourir sans rien faire ?! Vous vénérez vraiment un esprit de pacotille..."

_**Basilic :**_ "Pauvre idiot ! Sylvagro n'est qu'un mythe ! C'est un guide spirituel pour ceux qui en ont besoin ! Il montre le chemin vers un avenir radieux, où les plantes, les arbres, les animaux seraient enfin traités avec égard par les hommes. C'est cette foi inaltérable en un monde meilleur que représente Sylvagro ! Vous êtes fou si vous croyez qu'un tel être puisse exister..."

_Cletonal ne peut s'empêcher d'éclater de rire._

_**Cletonal :**_ "Ha, ha, ha ! Ignorante jusqu'au bout ! Et vous étiez Maître de l'école d'Hippiah, la magie tellurique au service de la Nature ?! Vous me faîtes tellement pitié ! Ha, ha, ha !!"

_**Camomille :**_ "Arrêtez de vous moquer !!"

_Cletonal cesse de rire. Tout le monde se tourne vers Camomille, qui semble très en colère._

_**Pepper :**_ "Camomille..."

_**Camomille :**_ "Vous êtes vil et méprisant ! Vous ne respectez ni la Nature, ni les gens, ni les sentiments ! Ce sont des humains comme vous qui me donnent envie de me replier sur moi-même et de vous détester, tous !"

_Pepper est très attristée par ces paroles. Les autres sorcières aussi, à en juger leur mine défaite. Ce qu'elle craignait est arrivé : Camomille va détester les humains pour le restant de ses jours, maintenant._

_**Camomille :**_ "Mais..."

_Pepper redresse la tête._

_**Camomille :**_ "Ce serait injuste pour ceux qui se sont battus pour protéger leur environnement, non ?"

_Elle se tourne vers les habitants du village._

_**Camomille :**_ "Vous n'avez pas hésité à risquer vos vies pour empêcher ces gardes cruels de tout détruire. Je... je n'avais jamais rencontré d'humains aussi proches de la flore..."

_Elle se tourne vers ses amies._

_**Camomille :**_ "Et vous... je vous ai ignorées. Je vous ai jugées. Je vous ai détestées. Et pourtant, vous êtes encore là. À vous battre pour ce qui est important pour moi, à mes côtés. Vous ne m'avez jamais laissée tomber. Je... je vous en suis reconnaissante, et... je... je n'ai jamais..."

_Elle fond en larmes. Le visage de Basilic s'illumine._

_**Camomille :**_ "Je n'ai jamais eu d'ami qui se souciait vraiment de ce que je ressentais ! Alors... merci ! Merci d'être là pour moi, pour la Nature ! Vous m'avez fait comprendre qu'il existait encore des cœurs généreux dans ce monde... et... je veux bien... enfin, si vous voulez..."

_Toutes ses amies la regardent avec tendresse. Armoracia a sorti un mouchoir brodé pour s'essuyer les yeux._

_**Camomille :**_ "Je voudrais vous aimer !"

_Aussitôt, un puissant rayon de lumière turquoise envahit le jardin. Tous se cachent les yeux. Les rayons viennent de la statue elle-même. En s'habituant à la lumière, Pepper remarque que la statue... s'est mise en mouvement !  
L'immense créature avance lentement vers le groupe, jugeant tout ce petit monde du regard. Ses bois se sont illuminés de toutes les couleurs. Le prince Cletonal est émerveillé._

_**Cletonal :**_ "Le voilà... Sylvagro... enfin..."

_**Habitant :**_ "C'est... c'est lui ?! C'est Sylvagro ?!"

_L'intégralité des habitants se mettent à genoux devant la créature. Champignax et les autres fruits s'inclinent devant la magnificence de l'esprit de la forêt, qui fait éclore des fleurs sur son passage, même là où la terre a été brûlée. Ne sachant que faire, Carrot s'incline aussi pour suivre le mouvement. Truffel fait une révérence gracieuse, et Mango se contente d'incliner la tête. Armoracia exécute sa plus belle révérence.  
Les sorcières sont sous le choc._

_**Pepper :**_ "Alors il existe... je pensais que c'était un mythe, Bas..."

_Mais cette dernière s'est mise à genoux !_

_**Pepper :**_ "Je vois... vous n'êtes pas mal pour retourner votre veste, vous !"

_**Coriandre :**_ "Euh... du coup, il faut s'incliner, nous aussi ?"

_**Safran :**_ "Devant un animal ? Non merci ! Je réserve mes révérences au prince Acren !"

_**Shichimi :**_ "Safran ! Tu crois que c'est le moment ?"

_Shichimi s'est inclinée sans hésiter. Avec une mine renfrognée, Safran s'exécute aussi. Coriandre et Pepper suivent le mouvement.  
Pepper remarque que la seule qui ne s'est pas agenouillée... c'est Camomille ! Elle est beaucoup trop sous le choc émotionnel pour avoir réagi. Elle contemple Sylvagro, sans parvenir à réaliser qu'il s'agit bien de lui.  
Sylvagro tourne sa petite tête vers Camomille. Cette dernière est paralysée._

_**Pepper :**_ "Oh, là, là, ça sent pas bon ! Camomille ! Mets-toi vite à genoux, avant qu'il se fâche !"

_Camomille réalise alors que tout le monde s'est incliné._

_**Camomille** (en s'inclinant à son tour) **:**_ "Oh ! Pardon, Seigneur Sylvagro ! Je ne voulais pas..."

_Sylvagro continue de s'approcher de Camomille. Pepper ne tient plus en place._

_**Pepper :**_ "Je refuse qu'il s'en prenne à Camomille !"

_**Basilic :**_ "Pepper ! Ne bouge pas !"

_Mais Pepper est stoppée net. Cette sensation... cette présence... elle l'a déjà ressentie. Mais où ?  
Messy se dresse juste devant elle._

_**Pepper :**_ "Oui... lorsque tu es entré dans mon esprit, j'ai ressenti la même chose, Messy..."

_Sylvagro est juste devant Camomille. Il est gigantesque ! Camomille n'ose pas lever le nez. Mais soudain, elle sent quelque chose lui toucher le haut du crâne. Camomille redresse timidement la tête, et elle aperçoit Sylvagro... enfin, sa tête, en gros plan, juste devant la sienne ! Elle pousse un cri et recule, toujours par terre. La créature a baissé sa tête jusqu'à Camomille. Mais elle ne semble pas agressive. Camomille s'approche timidement._

_**Camomille :**_ "Sylvagro... puis-je... ?"

_Sylvagro ne retire pas sa tête. Camomille pose délicatement ses mains dessus, et se met à le caresser. Un sourire apaisant se dessine sur son visage. Elle serre son museau contre elle, les yeux fermés._

_**Camomille :**_ "Merci... je ressens votre empathie jusqu'au plus profond de mon être..."

_Un éclat de lumière jaillit alors et entoure la sorcière et la créature. Pepper n'a jamais vu quelque chose d'aussi beau.  
L'intensité lumineuse diminue. Camomille et Sylvagro s'écartent l'un de l'autre, et se contemplent quelques instants.  
Soudain, Sylvagro disparaît d'un seul coup. Il est comme aspiré sur lui-même, et réduit en une petite orbe verte. L'orbe rejoint la main du prince Cletonal, qui s'en saisit avec un sourire malsain. Il est debout, libéré du filet._

_**Pepper :**_ "VOUS !"

_**Cletonal :**_ "Merci, sorcières ! C'est ce qu'il me fallait ! Bon vent !"

_**Camomille :**_ "CERTAINEMENT P..."

_Mais en voulant lui bondir dessus, elle se heurte à un nuage de fumée ténébreux, qui fait tousser tout le monde. Lorsqu'il se dissipe, le prince Cletonal a disparu._

_**Pepper :**_ "Grrr ! Il a réussi à s'enfuir !"

_**Camomille :**_ "Il faut le poursuivre ! Il a pris le seigneur Sylvagro en otage !"

_**Pepper :**_ "On n'a pas un instant à perdre ! Unitah, avec moi ! On fonce au château du roi Hartru ! Basilic..."

_Elle pose son regard vers son Maître d'autrefois. Basilic lui sourit._

_**Basilic :**_ "Allez-y. Ne vous en faîtes pas pour nous : sans anti-magie, je ne pense pas que ces gardes soient encore une menace. Et puis, j'ai une petite idée pour leur reconversion..."

_Les gardes, toujours piégés dans le fil d'Armoracia, s'échangent un regard inquiet._

_**Basilic :**_ "Je ne suis pas inquiète, vous réussirez. Vous avez une fâcheuse tendance à obtenir ce que vous voulez. Et avec vous, je sais que Camomille est entre de bonnes mains. Quant à nous..."

_Elle se tourne vers les habitants. Ils sont déterminés._

_**Basilic :**_ "Je pense qu'une mission nous incombe, là-haut. Une fois cet endroit rebâti, les vacances seront terminées !"

_**Pepper :**_ "Je suis heureuse de l'entendre. Bon courage, Maître Basilic. Vous en aurez besoin !"

_**Basilic :**_ "Vous aussi... bon, attention, j'ouvre le passage."

_Une seconde fois, Basilic ouvre le portail. Chaque sorcière se saisit de son balai (et Shichimi de son nuage). Camomille propose à Armoracia de l'accompagner._

_**Armoracia :**_ "Je n'ai encore jamais volé sur un balai ! Merci, Dame Camomille !"

_**Pepper :**_ "Unitah ! Départ !"

_Les sorcières foncent dans l'ouverture, en adressant un signe de main à Basilic et aux habitants de Floréco. Champignax saute sur place._

_**Champignax :**_ "Bonne chance, mesdames les sorcières ! J'espère qu'on se reverra !"

_Pepper et ses amies arrivent de l'autre côté du portail, dans la forêt de Lorbéciande. Elles se dirigent plus haut, dépassant la cime des arbres. Au loin, la ville fortifiée de Grala se dresse, avec son immense château au bord d'une falaise. Pepper regarde la ville avec détermination._

_**Pepper :**_ "Il est temps de régler nos comptes une bonne fois pour toute, Grala."

## Acte 5 - Le golem

### Scène 1 - Sous la falaise

_Les sorcières se rapprochent de la grande cité de Grala. Elle semble beaucoup plus grande que Qualicity, mais beaucoup moins moderne. Des remparts entourent le château et la ville, avec des sentinelles tout autour. Le château surplombe la ville, tout comme celui de Qualicity. Il est imposant, moins large que le château de Coriandre, mais plus haut, et plus sombre. Il possède d'innombrables tours, ainsi que l'immense blason de Grala qui recouvre une bonne moitié de sa façade : un lion prêt à rugir sur les ennemis qui s'attaqueraient au château. La mer agitée juste derrière la falaise renforce l'aspect sinistre et menaçant du château.  
Cependant, les sorcières ne semblent pas impressionnées. Elles foncent vers le château, déterminées à en découdre avec leur ennemi. Seule Armoracia semble inquiète._

_**Armoracia :**_ "Je... je voulais m'excuser auprès de vous."

_Pepper stoppe sa course. Ses amies s'arrêtent aussi._

_**Pepper :**_ "Pourquoi vous excusez-vous, Princesse ? Vous nous avez sauvées, je pensais qu'on était toutes d'accord là-dessus !"

_**Armoracia :**_ "Mais... le méchant prince s'est échappé à cause de moi ! Si mon fil avait été plus solide, il n'aurait pas pu en sortir, et nous ne serions pas en route vers ce château sinistre..."

_**Camomille :**_ "Princesse Armoracia ! Je vous interdis de vous en vouloir ! Le prince Cletonal est fourbe et cupide, mais il est aussi astucieux. Vous ne pouviez pas savoir qu'il serait capable de sortir de votre piège. Ce n'est pas votre faute ! Et je doute qu'il aille très loin sans armée ! Nous sommes libres et encore en course grâce à vous, ne l'oubliez pas ! Ne vous inquiétez pas, il finira par répondre de ses actes."

_Les autres sorcières confirment d'un signe de tête._

_**Armoracia :**_ "Je... je suis rassurée ! Je dois vous avouer que je me suis longtemps demandé si j'avais eu raison de vous suivre... si je n'allais pas faire de bêtises..."

_**Safran :**_ "Bien sûr que vous avez eu raison, darling ! Vous apportez du raffinement dans ce groupe de brutes !"

_**Coriandre :**_ "Safran !"

_**Pepper :**_ "Et vous n'avez pas fait de bêtises, soyez tranquille, au contraire ! D'ailleurs, vous ne m'avez pas répondu, tout à l'heure : comment avez-vous pu nous suivre ? Vous n'aviez pas de moyen de transport aérien..."

_**Armoracia :**_ "J'ai... euh... utilisé les petits appareils qui permettent de vous localiser, et... je suis passée par les voies souterraines. Je suis tombée sur le méchant prince et son armée qui vous avez capturé, et qui trafiquaient des drôles d'engins au cœur de la forêt... puis le portail vers le jardin s'est ouvert, et je vous ai suivies en cachette..."

_**Pepper :**_ "Attends une seconde... les petits quoi pour nous localiser ?"

_Coriandre regarde ailleurs._

_**Pepper :**_ "Coriandre !"

_**Armoracia :**_ "Pardon, Princesse Coriandre ! Vous m'aviez fait jurer de ne pas en parler !"

_**Coriandre :**_ "Pas de souci, Princesse. Elles auraient fini par le savoir..."

_**Pepper :**_ "Savoir quoi ?! Qu'est-ce que vous mijotez, toutes les deux ?"

_**Coriandre :**_ "Ne te fâche pas ! Shichimi m'a suggéré l'idée, et j'avoue qu'elle n'était pas si mauvaise... j'ai fait mettre de petits émetteurs dans vos costumes qui permettent de vous localiser. Au cas où l'une d'entre nous vienne à disparaître, les autres pourraient toujours la retrouver..."

_**Safran** (en panique, fouillant son costume) **:**_ "Des engins pour nous pister ?! Où ça, où ça ?!!"

_Coriandre désigne le blason cousu sur leur veston._

_**Coriandre :**_ "C'était le meilleur endroit pour les dissimuler..."

_**Safran :**_ "Bien entendu, à un endroit que nous ne pouvons pas enlever ! C'est absolument honteux, Coriandre !"

_**Pepper :**_ "Vous auriez pu nous en toucher deux mots, non ?! C'est une atteinte à la vie privée, ça !"

_**Coriandre :**_ "Je l'ai fait pour notre sécurité ! Et regardez, sans eux, la princesse Armoracia n'aurait pas pu nous trouver pour nous délivrer ! Vous devriez être contentes de les avoir !"

_Pepper et Safran réfléchissent. Camomille n'intervient pas. Elle lance des regards désespérés vers Grala._

_**Shichimi :**_ "Ne blâmez pas Coriandre. C'est moi qui ai eu l'idée. Maintenant que nous sommes une équipe, nous devons tout faire pour pouvoir se venir en aide mutuellement. C'est ce que j'ai trouvé de mieux. Pardon, j'aurais dû vous consulter avant, mais j'ai appris les plans du prince juste après, et..."

_**Pepper :**_ "Bon, on ne va pas vous en tenir rigueur pour cette fois. On a quelque chose à faire de plus urgent ! Mais bon sang, parlez-en à tout le monde, la prochaine fois ! On était d'accord pour que les décisions se prennent ensemble !"

_**Shichimi :**_ "Tu as tout à fait raison. Pardon, ça ne se reproduira plus."

_**Camomille :**_ "Bon, si vous avez terminé, Seigneur Sylvagro est toujours en danger, nous devons y aller !"

_**Pepper :**_ "Tu as raison. Allons-y !"

_**Coriandre :**_ "Attendez !"

_Tout le monde se retourne vers Coriandre, qui est horrifiée en regardant une sorte de petit écran portable._

_**Safran :**_ "Qu'y a-t-il ? On dirait que tu as vu un fantôme ! C'est quoi, ce truc ?"

_**Coriandre :**_ "Mon écran d'alerte... le garde qui était prisonnier... il n'est plus là !"

_**Pepper :**_ "Quoi ? Il s'est enfui, lui aussi ?!"

_**Coriandre :**_ "Il faut rentrer au repaire, maintenant ! Si le garde fait son compte-rendu, le roi Hartru va connaître l'emplacement de notre planque ! Les Ahrachnerres sont en danger !"

_**Armoracia :**_ "Oh non ! Mes amies... j'aurais dû rester là-bas pour les défendre !"

_**Pepper :**_ "Hum... non ! On continue vers Grala !"

_**Coriandre :**_ "Quoi ?! Non, on ne peut pas laisser les Ahrachnerres sans défense ! Elles nous ont accueillies..."

_**Camomille :**_ "Les Ahrachnerres sauront se défendre toutes seules ! Seigneur Sylvagro est en danger, lui aussi ! Si jamais le prince parvient à l'utiliser contre le gardien..."

_**Coriandre :**_ "C'est l'esprit de la forêt, il est mieux placé pour se défendre que les Ahrachnerres ! On doit retourner les aider !"

_**Camomille :**_ "Mais..."

_**Coriandre :**_ "Tu sais, Camomille, ce n'est pas parce qu'on t'a aidée dans ton combat qu'il faut te prendre pour le centre du monde ! L'amitié, ça marche dans les deux sens !"

_**Pepper :**_ "On se calme !!"

_Les sorcières se tournent vers Pepper. De gros nuages noirs laissent présager d'une tempête. Le regard qu'elle leur lance est terrifiant. Carrot, sur son épaule, n'est pas très rassuré._

_**Pepper :**_ "Si on règle leur compte au prince Cletonal et au roi Hartru, les Ahrachnerres n'auront rien à craindre. Suivez-moi."

_Messy, au-dessus de sa tête, laisse échapper un autre râle craquant. Pepper fait demi-tour et repart vers Grala._

_**Safran :**_ "Et puis, c'est juste à côté ! Hors de question de faire tout le chemin à l'envers ! Je vous rappelle qu'on ne devrait pas faire plus de trente minutes de vol..."

_**Shichimi :**_ "Oui, tu l'as déjà sortie pour justifier ta fainéantise, celle-là. Alors avance et tais-toi !"

_**Safran :**_ "Fainéantise ! De la part de celle qui est assise tranquillement sur son nuage douillet ! C'est trop fort !"

_Elles suivent Pepper. Coriandre est assez contrariée. Camomille passe devant elle._

_**Camomille :**_ "Ne t'inquiète pas, Coriandre. On parviendra à les protéger, si on arrête le prince et le roi de Grala maintenant !"

_Elle poursuit sa route. Armoracia lui adresse un sourire qui se veut rassurant, mais elle-même ne semble pas très rassurée. Essayant de se faire une raison, Coriandre les suit, toujours nerveuse quant au sort de leurs hôtes.  
Elles s'approchent des remparts._

_**Safran :**_ "Hum, il y a des sentinelles partout... comment allons-nous passer ?"

_**Pepper :**_ "En force. On neutralise quiconque essaiera de nous attaquer !"

_**Shichimi :**_ "C'est un peu trop radical. Ils rameuteraient toute l'armée... non, je connais une autre issue."

_**Pepper :**_ "Par le toit, encore ? Désolée, mais leur plafond n'a pas été détruit par une folle, que je sache !"

_Shichimi lui lance un regard noir. Mais elle passe outre._

_**Shichimi :**_ "Non, je ne parlais pas du toit. Regardez la falaise, là-bas. J'ai eu l'occasion de me rendre dans ce château pour rencontrer le roi, lors de ma nomination au rang de Maître. Ils m'ont fait visiter les souterrains, et il y a une issue par laquelle on peut rentrer, sur la falaise."

_**Camomille :**_ "Le château est si profond que ça ?"

_**Shichimi :**_ "Oui, et les rumeurs vont bon train sur ce que le roi Hartru tente de dissimuler dans ses sous-sols... je ne pense pas être allée aussi bas que possible lors de ma visite... c'est l'occasion d'aller vérifier par nous-mêmes."

_**Pepper :**_ "Bon. Allons-y."

_Les sorcières font un détour et se dirigent vers la falaise. La mer s'agite de plus en plus, et le tonnerre commence à gronder. Carrot, Truffel et Mango ne sont pas rassurés. Armoracia et Camomille ne sont pas très à l'aise non plus quant à l'orage qui se lève. Shichimi indique une petite fente dans la roche, qui semble avoir été sculptée pour fabriquer une fenêtre. Les sorcières s'engouffrent à l'intérieur. Armoracia doit se tortiller davantage à cause de ses bras encombrants, mais réussit à passer. Safran est outrée de voir que son costume a été sali lors de son passage par la fenêtre._

_**Safran :**_ "Oh non ! Encore des tâches d'humidité ! Sans rire, on ne pouvait pas passer par une entrée un peu plus large ?"

_**Pepper** (avec un sourire moqueur) **:**_ "Tu n'avais qu'à pas avoir d'aussi grosses fesses !"

_**Safran :**_ "PARDON ?! Je vais..."

_**Shichimi :**_ "CHUT ! Vous tenez à ce qu'on se fasse prendre ?! Ce n'est pas parce que nous sommes au sous-sol qu'il n'y a aucun garde ! Alors vous êtes priées de faire silence !"

_Pepper et Safran se taisent. Cette dernière se contente d'un regard mauvais avant de suivre Shichimi, le nez en l'air, visiblement vexée. Truffel hausse les épaules en direction de Carrot, qui le lui rend bien.  
Elles se trouvent dans une grande salle souterraine tout en hauteur. Au milieu, le vide intégral. On ne voit pas le fond. Seuls des escaliers permettent de descendre en toute sécurité, sur les côtés. Ils font tout le tour des murs, et montent jusqu'à une entrée à peine visible._

_**Safran :**_ "Ils sont vraiment malades, à Grala ! Ils tiennent tant que ça à mourir ?!"

_**Coriandre :**_ "J'avoue. Il y aurait beaucoup à dire sur les normes de sécurité de ce château..."

_**Pepper :**_ "Restez concentrées ! Le danger peut survenir de n'importe où."

_**Safran :**_ "Surtout du milieu ! Pourquoi on n'utilise pas nos balais pour descendre, au lieu de risquer de tomber bêtement ?"

_**Shichimi :**_ "Bonne idée pour faire une chute mortelle ! Tu n'as pas remarqué où nous étions ?"

_Safran regarde autour d'elle. Ces pierres sont décidément bien sombres... et trop ondulées. Comme les escaliers._

_**Safran :**_ "Encore une bulle d'anti-magie..."

_**Shichimi :**_ "Exact. Le roi tient réellement à ce qu'aucune sorcière ne rentre ici par effraction. Ce qui signifie que nous allons sûrement avoir un comité d'accueil en bas de cet escalier. Et seule la princesse Armoracia peut nous protéger."

_**Armoracia :**_ "Oh, là, là ! Je ne sais pas si je serai à la hauteur !"

_**Pepper :**_ "Bien sûr que si, vous l'avez déjà prouvé. Allez, continuons à descendre, nous verrons bien !"

_Les sorcières poursuivent leur descente en silence. Elles s'enfoncent encore davantage dans la falaise. Cet escalier semble être sans fin._

_**Coriandre :**_ "Hum..."

_**Shichimi :**_ "Oui, tu partages le même avis, Coriandre, n'est-ce pas ?"

_**Pepper :**_ "À quel sujet ?"

_**Safran :**_ "Que cet escalier est interminable ! Enfin, ils ont vraiment réquisitionné l'intégralité de la falaise pour terrer leurs secrets ?!"

_**Shichimi :**_ "Tu ne crois pas si bien dire, Safran. J'ai l'impression qu'on nous prend pour des idiotes depuis un moment."

_**Pepper :**_ "Comment ça ?"

_**Coriandre :**_ "Techniquement, on devrait être sous la mer, en ce moment."

_**Pepper :**_ "Quoi ?"

_**Shichimi :**_ "Tu devrais rester plus concentrée, Pepper. Ta colère t'aveugle. Tu n'as pas l'impression qu'on tourne en rond depuis tout à l'heure ?"

_Pepper regarde autour d'elle. Il est vrai que l'endroit n'a guère changé depuis qu'elles se sont mises à descendre. Puis..._

_**Pepper :**_ "Mais... la porte... elle est encore à la même place que tout à l'heure !"

_**Coriandre :**_ "Oui. J'ai comme l'impression qu'il y a un sortilège de Boucle Infinie dans ce sous-sol !"

_**Armoracia :**_ "De Boucle Infinie ?"

_**Pepper :**_ "Un sortilège qui permet de replier l'espace sur lui-même. Ceux qui sont dedans sont condamnés à refaire le même chemin, encore et encore, sans pouvoir en sortir... c'est un sort qu'on apprend à... à Chaosah..."

_Pepper est sous le choc._

_**Pepper :**_ "Non... ce serait... ce serait une de mes marraines qui aurait jeté ce sort ? Qui travaillerait... pour le roi Hartru ?!"

_**Coriandre :**_ "C'est très probable, malheureusement. Un kidnapping, sûrement. Je ne pense pas qu'une de tes marraines accepte de travailler pour quelqu'un, surtout qui déteste les sorcières à ce point."

_**Pepper :**_ "Détrompe-toi, elles en sont capables ! Elles... elles ont totalement renoncé à se battre, lorsque la loi est tombée... je... je leur en veux pour ça, d'ailleurs... si tu savais comme je leur en veux..."

_Les autres sorcières sentent le danger. Carrot aussi. Il essaie de calmer Pepper en se frottant à sa jambe._

_**Coriandre :**_ "Pepper... chacun réagit comme il l'entend, et... tu ne peux pas juger tes marraines pour avoir accepté la loi et être parties pour vivre une nouvelle vie..."

_**Pepper :**_ "Et tous leurs sermons, hein ? Leurs règles à la noix ! "Une vraie sorcière de Chaosah ne renonce jamais" ?! C'était du pipeau, hein ? Ou juste une nouvelle façon de me rendre la vie impossible ?!"

_**Coriandre :**_ "Pepper, tu ne peux vouloir de toutes les sorcières qu'elles rejoignent ta cause ! Chacun est libre de ses choix !"

_**Pepper :**_ "EH BIEN ELLES DEVRAIENT SE BATTRE ! ELLES N'ONT PAS D'HONNEUR SI ELLES FUIENT !! J'AVAIS DE L'ADMIRATION POUR ELLES, MOI ! ET ELLES M'ONT TRAHIE !! ELLES..."

_À force de s'agiter, Pepper fait perdre l'équilibre à Coriandre, qui tombe dans le vide._

_**Coriandre :**_ "AAAAHH !!"

_**Camomille :**_ "Coriandre !!"

_Elle saute aussi. Armoracia la suit, en bousculant Safran, qui tombe à son tour. Pepper plonge.  
La chute semble interminable. Pepper perd rapidement Safran de vue. Elle attend d'atterrir quelque part, mais aucun sol ne vient la réceptionner.  
Messy vient se positionner devant elle._

_**Pepper :**_ "Messy... aide-nous !!"

_Nouveau râle. Le noir du gouffre commence à s'illuminer par intermittence. Comme s'il était défectueux. Il clignote de plus en plus, et Pepper a l'impression qu'il finit par voler en éclat. Puis, elle sent sa chute ralentir, progressivement, comme si elle planait. Elle remarque que son balai la porte jusqu'au sol, où l'attendent ses amies._

_**Pepper :**_ "Vous êtes là ! Rien de cassé, ça va ?"

_**Coriandre :**_ "Tout va bien, rassure-toi."

_Carrot saute dans les bras de Pepper._

_**Pepper :**_ "Carrot ! Tu as l'air d'aller bien, toi aussi..."

_**Safran :**_ "Quelqu'un peut me dire ce que c'était ?"

_**Pepper :**_ "Hum... pour défaire le sort de Boucle Infinie, il faut "briser la répétition"... sauter dans le gouffre était sûrement la solution..."

_**Coriandre :**_ "Oui... et nous sommes sorties de la zone d'anti-magie. En un seul morceau, c'est un miracle !"

_**Camomille :**_ "Attendez ! Où est la princesse Armoracia ?!"

_Elles regardent au-dessus d'elles. Elles ne voient que le sous-sol d'où elles sont venues. Mais bien vite, elles aperçoivent une forme sombre qui rampe sur un mur et descend à toute vitesse. Lorsqu'elle est suffisamment proche du sol, elles se rendent compte qu'il s'agit d'Armoracia, qui descend le sous-sol sur le mur, la tête en bas, en agitant les bras à toute vitesse. Safran l'aide à rejoindre le sol._

_**Camomille :**_ "Princesse ! Vous êtes sauve !"

_**Armoracia :**_ "Oui ! Désolée de vous avoir inquiétées ! Je n'ai pas bien compris ce qui vient de se passer, mais j'ai pu m'accrocher au mur ! Où sommes-nous ?"

_Les sorcières regardent autour d'elles. Elles sont éclairées par des torches, et l'endroit ressemble à des cachots. Une grande porte se trouve devant elles. Messy apparaît lentement devant la porte. Il semble leur indiquer qu'il s'agit de la route à suivre._

_**Pepper :**_ "Allons par là !"

_Le groupe d'Unitah ouvre la porte, le cœur battant, avec le grondement de la mer frappant le rocher comme seule ambiance sonore._

### Scène 2 - Le secret du roi Hartru

_Le groupe passe discrètement la lourde porte. Elles arrivent dans une pièce circulaire immense et humide, où chaque étage comporte des cellules de prisonniers, vides. Mais c'est ce qui prend toute la place du centre qui attire leur œil : un immense corps métallique de plusieurs mètres de hauteur, en forme de trapèze, avec deux grands cercles en forme de roues crantées au sommet sur les côtés, tapissé de symboles étranges. Au sommet se trouve une petite excroissance plutôt carrée, ornée de deux traits bleus. Le corps semble être enterré profondément dans le sol. Les sorcières sont stupéfaites._

_**Pepper :**_ "Mais qu'est-ce que c'est que ça ? Le robot antique, vous croyez ?"

_**Safran :**_ "C'est à peu près l'idée que je m'en faisais, effectivement : grand, mystérieux, moche, tape à l'œil."

_**Camomille :**_ "J'avais une question pour toi, Safran : ça t'est déjà arrivé de respecter quoi que ce soit dans ce monde ?"

_**Safran :**_ "Quelle question ! Bien sûr que oui ! J'ai bien respecté tes arbres tout à l'heure, non ? Et maintenant, j'ai le droit à un câlin lumineux, moi aussi ?"

_**Camomille :**_ "Tu es vraiment..."

_**Coriandre** (toute excitée) **:**_ "C'est pas vrai c'est pas vrai c'est pas vrai !!"

_Elle se dirige vers le corps métallique. Messy s'agite soudainement et diffuse un râle inquiétant. Pepper accourt alors vers Coriandre pour l'arrêter._

_**Coriandre :**_ "Mais lâche-moi ! Il faut que je regarde cette merveille de plus près ! Le robot légendaire dont j'ai tant entendu parler, enfin !"

_**Pepper :**_ "Non, reste ici ! Nous ne sommes pas seules..."

_Pepper désigne les prisons en hauteur. Effectivement, en tendant l'oreille, elles entendent des voix._

_**Voix 1 :**_ "Vous devriez être honorée !"

_**Voix 2 :**_ "Honorée ? De quoi, travailler pour vous ? Mes rhumatismes se réveillent rien qu'en entendant ces mots !"

_Le cœur de Pepper s'arrête. Les sorcières montent discrètement d'un étage, suivies par Armoracia qui grimpe sur les murs, et se cachent grâce au corps métallique. En passant la tête discrètement sur le côté, elles remarquent qu'un homme portant une large cape ornée d'un lion avec de la fourrure, et une coiffure épaisse en or et en bataille, discute devant une des cellules, qui n'est pas vide. Malheureusement, Pepper reconnaît la toute petite mais énergique sorcière qui s'y trouve._

_**Pepper :**_ "Oh non, Maître Thym... pas vous !"

_Shichimi passe un index sur sa bouche. Toutes les sorcières (sauf Coriandre qui en profite pour observer le corps métallique de plus près) écoutent attentivement la conversation._

_**Roi Hartru :**_ "Oh, oh, oh ! Toujours aussi charmante, n'est-ce pas ! Mais vous devriez être fière : vous êtes officiellement la seule sorcière au monde à pouvoir pratiquer la magie ! Alors, qu'est-ce que ça fait, de se sentir toute puissante ?"

_**Thym :**_ "C'est censé me faire quelque chose ? Je n'ai pas accepté votre marché pour me sentir puissante ! Juste pour pouvoir continuer à parfaire mon art en toute tranquillité, c'est tout !"

_Pepper n'en revient pas. Thym était désespérée à ce point ?_

_**Roi Hartru :**_ "Pourtant, votre Zone Anti-Magie est un succès total ! Sans parler de votre télé-transporteur dans ces ruines... ce doit être jouissif, de maîtriser un tel pouvoir, non ?"

_**Thym :**_ "Pour la dernière fois, ce n'est pas du télé-transport, mais juste un révélateur d'accès dissimulé par magie ! Et l'anti-magie n'est qu'un dérivé de la micro-dimension, rien de bien sorcier ! Je me suis juste inspirée de la cage dans laquelle j'étais enfermée, dans l'Ascétribune. Mon Maître Chicory était capable de bien plus ! Elle pouvait créer de véritables vortex spatiaux de télé-transport, elle ! Dommage qu'elle ne fut qu'un imposteur..."

_**Roi Hartru :**_ "Elle a fait des dégâts, certes, mais elle vous a bien formée ! Votre sort de Boucle Infinie est tout bonnement exceptionnel !"

_**Thym :**_ "Là encore, rien d'exceptionnel..."  

_**Roi Hartru :**_ "Que vous dîtes ! Pour de simples êtres humains comme moi, votre art reste époustoufflant... je suis navré de vous offrir un confort si en deçà de votre talent, mais vous comprenez..."  

_**Thym :**_ "Oui, que penserait votre peuple si vous offriez une de vos chambres de luxe à une vieille sorcière rabougrie que vous détestez, je connais la musique... bon, vous êtes venu ici pour me cirer les pompes ou pour me demander autre chose ? Vous l'avez, maintenant, votre robot antique ! Qu'est-ce qu'il vous faut de plus ?"  

_**Roi Hartru :**_ "Je pense que c'est évident. Je n'ai pas fait tout mon possible pour que la loi soit adoptée pour me contenter d'un vieux tas de ferraille."

_Il fait les cent pas autour de la cellule. Thym l'observe avec méfiance._

_**Roi Hartru :**_ "Surprenant que ce bon vieux Lord Azeirf se soit présenté en faveur de cette loi, non ? À mon avis, il mijote quelque chose, lui aussi..."

_**Thym :**_ "Parce que vous non, peut-être ? Et vous ne devriez pas parler de ce qu'il s'est passé dans l'Ascétribune avec autant d'insouciance. Vous savez bien ce qui se passe si on révèle quoi que ce soit qui s'y est produit."

_**Roi Hartru :**_ "Ha, ha ! Ce fameux sort où il faut prêter serment ! Vous croyez qu'il me fait peur, votre sort de pacotille ? Non, pas avec ce que je m'apprête à acquérir..."

_**Thym :**_ "Vous avez gagné un "vieux tas de ferraille", pour reprendre vos termes. Wouhou. C'est le début du succès. Dans deux jours, vous devenez le maître du monde, non ?"

_**Roi Hartru :**_ Ha, ha, ha ! Décidément, le sarcasme vous va très bien ! Non, il ne s'agit pas de ça... il s'agit de plus, bien plus ! Les grimoires que vous m'avez prêtés ont été fort intéressants, et ont complété le savoir qui se transmet dans ma famille depuis des siècles..."

_**Thym :**_ "Ah bon ? Surprenant que vous y ayez compris quelque chose."

_**Roi Hartru :**_ "C'est au-delà de ça, au contraire ! Vous connaissez la légende de la création de la magie, non ?"

_**Thym :**_ "Légende ? Non, je ne connais qu'un vieux conte pour enfants, qui parle d'un dragon, d'une sorcière et d'un héros. Le conte indique que ce fut la première sorcière au monde à pratiquer la magie... elle aurait divisé son pouvoir en six créatures mythiques, en attente d'un héros suffisamment brave pour en être digne et détruire ce fameux dragon. Mais un tel dragon n'a jamais existé, et ne nous menace pas, donc niveau véracité, on fait mieux..."

_**Roi Hartru :**_ "Et pourtant... ces créatures existent bel et bien ! Et vous en avez une juste en face de vous !"

_Thym regarde le corps métallique de façon sarcastique._

_**Thym :**_ "Vous vous payez ma tête ?"

_**Roi Hartru :**_ "Absolument pas ! Bon sang... vous êtes des sorcières et vous ne vous rendez pas compte de ce qui se passe ? Les six créatures sont en attente d'un héros, ça ne fait aucun doute ! C'est ce que mon père m'a transmis... et je serai ce héros, capable de vaincre les dragons !"

_**Thym :**_ "Mais oui, mais oui... et où allez-vous le trouver, votre dragon noir ? Vous allez en combattre un au hasard ? Hé, hé... vous croyez que vous les intéressez ? Ils s'en fichent royalement de notre existence ! Nous sommes des fourmis pour eux."

_**Roi Hartru :**_ "Et je m'en vais leur faire une piqûre de rappel ! Lorsque les six créatures seront miennes, ils seront obligés d'admettre notre supériorité ! Le monde s'inclinera devant ma bravoure et ma puissance ! Je leur montrerai qui est le roi Hartru ! Je planterai mon nom dans leur mémoire comme une épée dans un rocher, et ils le craindront !"

_**Thym :**_ "Je vois... je n'étais pas si loin, avec mon "maître du monde", tout à l'heure... et dire que vous avez fait sauté la magie pour ça... c'est plutôt triste..."

_**Roi Hartru :**_ "Moquez-vous pendant que vous le pouvez, sorcière ! Bientôt, je vous surpasserai ! Et je mettrai fin à cette ignorance insupportable des dragons à notre égard ! Ils verront qui est l'espèce la plus puissante de ce monde !"

_**Thym :**_ "Eh bien j'ai hâte de voir ça !"

_**Roi Hartru :**_ "Vous verrez... en ce moment même, mon frère est en train de me rapporter la deuxième créature. Cet abruti pense que j'en ai besoin pour activer le robot, afin d'accéder aux pouvoirs de l'Arbre de Komona... quel imbécile ! Vous êtes bien placée pour savoir comment il s'active !"

_**Thym :**_ "Une amulette, oui... responsable des conséquences auxquelles nous devons faire face. Qui a disparu, d'ailleurs."

_**Roi Hartru :**_ "Précisément ! Mais cet idiot veut tellement ma couronne qu'il pensait me prendre de vitesse et s'accaparer le pouvoir de l'Arbre... hé, hé, hé ! J'ai hâte de voir sa tête lorsqu'il essaiera ! Au moins, il m'évite d'avoir à faire le travail, comme tous les imbéciles..."

_**Thym :**_ "J'avoue que vous avez un don pour exploiter les simples d'esprit pour votre profit personnel. Enfin, si ça vous amuse d'essayer de devenir le "héros" d'une histoire pour gamins... tant que vous me laissez faire mes expériences tranquille..."

_**Roi Hartru :**_ "Oui... profitez, tant que vous le pouvez encore..."

_**Pepper :**_ "VOUS N'ALLEZ RIEN FAIRE, ENCORE UNE FOIS ?!!"

_Ses camarades sont affolées, mais Pepper en a eu assez. Elle se dévoile. Le roi la regarde, mais ne semble pas surpris. Ce qui n'est pas le cas de Thym._

_**Thym :**_ "Pepper ?! Pauvre sotte, que fais-tu ici ?"

_**Pepper :**_ "CE QUE VOUS DEVRIEZ FAIRE, LÂCHE !! Vous êtes là, à vous prélasser, pendant que cette ordure fait mener un véritable chaos dehors ! Je n'en reviens pas que vous ayez fait des affaires avec ce type !!"

_**Thym :**_ "Cela ne te concerne pas ! Occupe-toi plutôt de ton équipe de bras cassés ! Vous avez créé une véritable pagaille, avec votre groupe, là ! Il paraît que vous vous appelez "Unitah" ? C'est déplorable !"

_**Pepper :**_ "Moi au moins, je n'ai pas renoncé ! Je me bats pour récupérer mes droits ! MAIS VOUS !! Vous pactisez avec l'ennemi ! C'est vous qui êtes déplorable !"

_**Thym :**_ "On fait ce qu'on peut avec ce qu'on a, Pepper ! Et puis je suis trop vieille pour laisser une gamine me faire la leçon ! Je dois toutefois admettre que te voir défaire mes sorts d'anti-magie et de Boucle Infinie m'impressionne. Tu n'es pas trop ratée, comme élève..."

_**Pepper :**_ "C'est vous qui êtes ratée comme Maître !"

_**Roi Hartru :**_ "Hop, hop, hop ! On se calme, Mesdames. Inutile de s'énerver !"

_**Pepper :**_ "INUTILE DE S'ÉNERVER ?! Après tout ce que vous nous avez fait subir ?! À nous les sorcières, mais aussi à l'environnement ?! Vous me demandez de ne pas m'énerver ?!"

_**Roi Hartru :**_ "Ce n'est pas ma faute si mes troupes sèment la pagaille ! Je n'ai fait que leur demander de me ramener l'esprit de la forêt..."

_**Pepper :**_ "Et prendre en chasse Camomille ? Ce n'était pas votre ordre, peut-être ?"

_**Roi Hartru :**_ "Réfléchis un peu, jeune sorcière. Je dois rester cohérent dans mes actes si je veux cacher mes véritables intentions ! Tout le monde sait que je suis contre les sorcières. Alors quand j'en vois une s'aventurer sur mes terres, je dois la faire pourchasser, c'est bien normal !"

_**Pepper :**_ "C'est bien normal... je me demande ce qui me retient de vous réduire en bouillie !"

_**Roi Hartru :**_ "Je te mets au défi d'essayer ! Vas-y, frappe !"

_Pepper n'a pas besoin d'une seconde invitation, elle lance un sort explosif sur le roi. Mais le sort n'explose pas, il semble... absorbé par le roi. Pepper est décontenancée._

_**Pepper :**_ "Mais..."

_**Roi Hartru :**_ "HA, HA, HA ! Surprise ? Je savais que des sorcières allaient venir essayer me mettre des bâtons dans les roues, alors je me suis protégé contre votre pouvoir démoniaque."

_Il montre la médaille dorée qui tient sa longue cape. Le roi semble onduler dans une sorte de bulle._

_**Pepper :**_ "L'anti-magie ! Maître Thym, vous lui avez donné cette protection ?!"

_**Thym :**_ "Un marché est un marché, Pepper. Il a été le premier à l'expérimenter, en bon cobaye !"

_**Pepper :**_ "Vous vendez vos sorts à ce roi cruel et sans scrupule... vous êtes décidément tombée bien bas ! Je suis fière de ne plus être votre élève !"

_Thym lui lance un regard noir._

_**Roi Hartru :**_ "Touchant. Mais au lieu de vous disputer, vous feriez mieux de collaborer avec moi, toutes les deux. Afin de m'aider à donner naissance à une nouvelle ère ! Une ère où les dragons reconnaîtront notre supériorité !"

_**Pepper :**_ "Vous êtes ridicule... les dragons n'ont jamais été violents ! Il est hors de question que je m'allie à vous !"

_**Roi Hartru :**_ "C'est dommage... je vais devoir t'éliminer, alors. Tu en sais beaucoup trop."

_Il dégaine une grande épée._

_**Pepper :**_ "Vous allez le regretter !"

_**Roi Hartru :**_ "Ha, ha ! Tu crois qu'une petite sorcière de rien du tout m'impressionne ?!"

_Un petit être chaotique détourne son attention. Il se met juste au-dessus de Pepper. Même Thym est surprise._

_**Pepper** (avec un sourire mauvais) **:**_ "Mais je ne suis pas seule ! Princesse !"

_Le roi n'a pas le temps de réagir. Une araignée un peu particulière fond sur lui et le prive de son arme d'une main, envoie un fil solide et gélatineux d'une autre, et attache le tout avec les quatre autres. Le roi est prisonnier. Les autres sorcières rejoignent Pepper._

_**Pepper :**_ "Bien joué, Princesse !"

_**Armoracia :**_ "À votre service, Princesse Pepper !"

_**Safran :**_ "Enfin ! Je ne supportais plus de l'entendre, celui-là, à jacasser et se prendre pour le boss ! Vaincre les dragons... quelle idée saugrenue !"

_**Shichimi :**_ "Profitons-en pour lui poser des questions sur ces créatures..."

_**Pepper :**_ "Nous allons le livrer à la justice, oui ! C'est l'occasion d'ouvrir un nouveau Conseil de Ah pour le juger, non ?"

_**Shichimi :**_ "On a trop peu d'éléments... mais s'il nous révèle des informations à propos des autres créatures mythiques..."

_**Pepper :**_ "MAIS IL TE FAUT QUOI DE PLUS ?! Il a avoué tous ses plans ! Il faut le juger, maintenant !"

_**Shichimi :**_ "Nous n'avons aucune preuve."

_**Pepper :**_ "Aucune preuve ? TU TE MOQUES DE MOI ?!! Et ce robot géant, là ?! Ce n'est pas suffisant ?!"

_**Shichimi :**_ "Pepper, ce n'est qu'un conte pour enfant. Ce robot pourrait n'être qu'un robot parmi tant d'autres. Il ne prouve rien..."

_**Pepper :**_ "Bon, peut-être, mais Sylvagro, on l'a bien vu, non ? Il n'est pas un mythe, non ?!"

_**Shichimi :**_ "C'est l'esprit de la forêt, Pepper. Rien à voir avec les six créatures du conte."

_**Pepper :**_ "Alors tu ne vas rien faire ?! À t'entendre, on a l'impression que tu es de son côté..."

_**Shichimi :**_ "Non, bien sûr que non. Je ne fais qu'énoncer les faits de façon rationnelle. Et le roi Hartru est un homme influent. Je sais qu'il fera jouer ces preuves-là, et le Conseil de Ah sera forcé de trancher en faveur de son innocence... je dis juste qu'il nous faut plus de preuves. Si les autres créatures existent, ce sera déjà plus solide. Et si elles permettent de transmettre les pouvoirs héroïques cités dans le conte..."

_**Pepper :**_ "Tu veux prendre le risque que ce monstre acquiert des pouvoirs surhumains pour le juger ?! TU ES FOLLE OU QUOI ?!"

_**Armoracia :**_ "Je vous en prie, Princesse Pepper, calmez-vous !"

_La voix effrayée d'Armoracia suffit à faire taire Pepper._

_**Pepper :**_ "Bien ! Votons, vu que c'est ainsi que nous fonctionnons ! Alors, qui est pour le plan risqué de Shichimi ?"

_Shichimi, Camomille, Safran et Armoracia lèvent la main. Coriandre est trop absorbée pour voter. Pepper grogne de plus belle._

_**Pepper :**_ "Bande d'ignorantes..."

_**Safran :**_ "Pourtant, elle a raison, Pepper. Si on veut des preuves suffisantes pour l'accuser..."

_**Pepper :**_ "PARCE QUE DÉTRUIRE DES RUINES ET S'EN PRENDRE À NOUS, CE N'EST PAS SUFFISANT ?!!"

_Elle donne un coup de poing vers la barrière qui entoure le robot. Elle a lancé un sort sans s'en rendre compte, et la barrière se détruit d'un coup ! Les autres sorcières sont paniquées. Sauf Coriandre._

_**Thym :**_ "Pepper..."

_Elle tourne sa tête agacée vers Thym. Son air effrayé est plutôt déroutant._

_**Thym :**_ "Qu'est-ce que c'est... que cette créature ?"

_Elle désigne Messy. Pepper lui lance un œil mauvais._

_**Pepper :**_ "Qu'est-ce que ça peut vous faire ?"

_**Thym :**_ "Il faut que tu t'en éloignes au plus vite !"

_**Pepper :**_ "Que je m'éloigne de celui qui m'encourage dans mon combat ?! Certainement pas ! C'est de vous qu'il faut que je m'éloigne ! Des lâches qui vendent leurs services et leur savoir aux ordures du monde pour être protégés !"

_**Thym :**_ "Mesdemoiselles... il faut que vous éloignez Pepper de ce monstre ! Il a clairement une mauvaise influence sur elle !"

_**Pepper :**_ "Si jamais la moindre d'entre vous ose s'approcher de moi, je la pulvérise, c'est compris ?!"

_Ses yeux redeviennent rouges. Aucune des sorcières n'ose s'approcher d'elle. Pepper fait quelques pas sur elle-même, en train de bouillir._

_**Roi Hartru :**_ "Vous êtes leur leader, et vous ne savez pas contrôler votre colère ? C'est pathétique."

_**Pepper :**_ "La ferme !"

_**Thym :**_ "Oui, je vous conseille de la fermer, Sire ! Vous ne savez pas ce dont elle est capable..."

_**Roi Hartru :**_ "La première des choses à faire lorsqu'on veut diriger une armée, c'est de paraître comme un véritable chef auprès de ses hommes ! Et ne montrer aucune faiblesse, aucune faille ! Se faire respecter, et travailler son image ! C'est la clé pour être aimé du peuple qu'on dirige, et ainsi pouvoir les diriger comme des marionnettes... mais vous n'arrivez même pas à assurer la cohésion dans votre petite équipe !"

_**Pepper :**_ "La ferme !!"

_**Shichimi :**_ "Roi Hartru, ne poussez pas le bouchon trop loin !"

_**Roi Hartru :**_ "Vous n'arrivez même pas à rallier vos amies à votre cause ! C'EST LAMENTABLE !!"

_**Pepper :**_ "LA FERME !!"

_Une aura destructrice s'empare de Pepper. Elle effraie tout le monde, surtout Carrot._

_**Shichimi :**_ "PEPPER ! CALME-TOI !!"

_**Pepper :**_ "Je ne suis pas un bon leader, hein ? Je ne sais pas rallier les autres à ma cause, hein ?! Eh bien vous allez voir ! Je vais les y forcer !"

_Pepper charge une puissance chaotique qui génère un énorme coup de vent. Aucune de ses amies ne peut la stopper. Coriandre regarde toujours le robot de près, et vient de découvrir quelque chose qui la laisse bouche bée._

_**Coriandre :**_ "Ce symbole... la signature de grand-mère ! Ce robot... il n'a rien de légendaire ! C'est un golem !!"

_**Pepper :**_ "Si je détruis ce robot, impossible pour vous de mener votre projet à bien, hein, Roi Hartru ?! EH BIEN REGARDEZ BIEN CE QUE JE VAIS FAIRE DE VOTRE RÊVE !!!"

_**Roi Hartru/Shichimi :**_ "NON !!"

_**Coriandre :**_ "NON, PEPPER !! Laisse-moi l'étudier !! Il a été conçu par Apiacée, ma grand-mère ! NE LE DÉTRUIS PAS !! J'en ai besoin !!"

_Mais Pepper lance son sort de Destruction sur le robot dans un excès de rage. Le tonnerre éclate._

### Scène 3 - L'éveil du golem

_Un immense fracas retentit dans le cachot. L'onde de choc est puissante. Elle entame les murs, provoquant des fissures de grandes tailles. Plusieurs pierres chutent. Les sorcières se sont recroquevillées. Quelques pierres se dirigent contre Armoracia et le roi. La princesse se protège avec ses six bras._

_**Camomille :**_ "ATTENTION !"

_Camomille forme une prison de racines qui englobe tout le monde. Les rochers s'éclatent contre cette paroi végétale, qui encaisse les coups. Les racines disparaissent._

_**Shichimi :**_ "Incroyable... elle a pu créer ses propres racines sans entrer en contact avec la terre... est-ce..."

_**Armoracia :**_ "Dame Camomille ! Vous m'avez sauvée !"

_**Camomille :**_ "Euh... on dirait bien ? Je ne pensais pas y arriver... ouh là !"

_Armoracia la prend dans ses six bras. Camomille est surprise, mais apprécie cette étreinte. Elle se tourne alors vers Safran._

_**Camomille :**_ "Tu as raison, c'est... assez particulier, comme étreinte !"

_Mais Safran vient d'apercevoir quelque chose qui la met en panique. Armoracia relâche Camomille._

_**Camomille :**_ "Quoi ?"

_**Safran :**_ "Coriandre !!"

_Elle court de l'autre côté des prisons détruites par le sort. Sous une grille, se trouve un corps inconscient._

_**Camomille :**_ "OH NON !"

_Toutes les sorcières et les animaux se précipitent vers Coriandre, qui ne bouge plus. Mango lui pique le visage avec son bec, mais elle ne réagit pas._

_**Armoracia :**_ "Princesse Coriandre ! Elle... elle n'est pas..."

_**Safran :**_ "Mais aidez-moi à lui enlever cette grille au lieu de regarder bêtement !"

_Les sorcières s'emploient à retirer la lourde grille qui lui est tombée dessus. Les six bras d'Armoracia se révèlent très utiles, mais la puissante envolée de la grille vers le haut les aide beaucoup. Elles constatent que Thym vient de faire léviter la grille par magie, et se rue vers Coriandre. Un filet de sang coule de sa tête._

_**Thym :**_ "Elle respire encore, mais elle a reçu un sacré coup. Emmenez-la hors d'ici en vitesse !"

_**Safran :**_ "Bien, aidez-moi à la soulever !"

_Thym claque des doigts. Le corps de Coriandre se soulève dans les airs._

_**Safran :**_ "...ou faîtes-la léviter, aussi..."

_**Thym :**_ "Pour un groupe de sorcières, vous oubliez souvent que vous avez des pouvoirs ! Allez, partez. Le sort devrait faire effet assez longtemps pour pouvoir l'évacuer."

_**Safran :**_ "Bien ! Les filles, enfourchez vos balais ! On sort de ces cachots humides et crasseux !"

_**Shichimi :**_ "Et le roi ? On le laisse là ?"

_**Safran :**_ "Qu'il pourrisse ici, c'est tout ce qu'il mérite !"

_**Shichimi :**_ "C'est un peu radical, non ? Même si c'est une ordure, il ne mérite pas de mourir écrasé ici..."

_Thym claque encore des doigts. Le roi est soulevé dans les airs comme Coriandre._

_**Thym :**_ "Pas le temps de discuter ! Votre amie a besoin de soins ! Filez !"

_**Shichimi :**_ "Bien... mais vous ? Vous restez là ?"

_**Thym :**_ "J'ai une correction à donner à une ancienne élève !"

_La fumée se dissipe. Elle révèle Pepper, de l'autre côté du robot, toujours entourée de la même aura chaotique. Messy se trouve juste au-dessus de sa tête. Carrot, sur l'épaule de Safran, lui lance un air inquiet. Les autres sorcières, en revanche, l'assassinent du regard._

_**Thym :**_ "PEPPER !"

_Pepper se tourne vers elle. Ses yeux sont toujours aussi rouges._

_**Thym :**_ "Ton insouciance et ton incompétence ont blessé une de tes amies ! Et tu te prétends un bon leader ?!"

_Le visage de Pepper s'adoucit. Elle remarque que Coriandre est blessée._

_**Pepper :**_ "Coriandre... je..."

_Elle fronce les sourcils. La colère revient au galop._

_**Pepper :**_ "Ce ne serait pas arrivé si vous m'aviez écoutées ! Vous pouvez ne vous en prendre qu'à vous-mêmes !"

_**Safran :**_ "Je ne sais pas ce qui me retient de l'incendier sur place !"

_**Camomille :**_ "Je la sens venir, la haine profonde que j'ai toujours redoutée... il faut fuir, tout de suite !"

_**Shichimi :**_ "Les filles..."

_**Thym :**_ "Oui, fuyez ! Je me charge d'elle ! Je vais lui apprendre deux ou trois choses à propos des conséquences de l'insouciance et du manque de travail sur soi-même !"

_**Pepper :**_ "Ho, ho ! Vous voulez encore jouer les Maîtres ?! Mais vous n'êtes plus rien à mes yeux, espèce de lâche ! Si c'est la bagarre que vous cherchez, vous allez la trouver, vieille bique !"

_**Thym :**_ "Tu crois m'impressionner avec ton insolence ?! Il y a bien longtemps que je t'ai cernée, ma jeune élève ! Voici ma dernière leçon !"

_**Shichimi :**_ "LES FILLES !!"

_Pepper et Thym se tournent vers Shichimi._

_**Thym :**_ "Mais vous êtes encore là ?! Je vous ai dit de filer ! Qu'est-ce que c'est que cette génération qui ne veut pas obéir ?!"

_Shichimi désigne le robot._

_**Shichimi :**_ "Vous n'avez pas remarqué qu'il se comporte de façon étrange depuis tout à l'heure ?!"

_En regardant le robot de plus près, elles remarquent que les symboles gravés dessus s'illuminent par intermittence. Comme s'il n'arrivait pas à démarrer._

_**Roi Hartru :**_ "Il commence à s'activer... votre amie a dû le mettre en route avec son sort explosif !"

_**Shichimi :**_ "Je suggère qu'on sorte toutes d'ici et qu'on règle nos comptes plus tard. Nous sommes en danger !"

_**Safran :**_ "Félicitations, Pepper ! Tu as réveillé ce tas de ferraille et tu as blessé la seule personne capable de comprendre comment l'arrêter ! Tu fais des merveilles en tant que chef, y a pas à dire !"

_**Pepper :**_ "Toi, la pimbêche, je te conseille de la mettre en sourdine !"

_**Safran :**_ "LA QUOI ?!"

_**Shichimi :**_ "S'il vous plaît ! Vous vous disputerez plus tard ! Il y a urgence !"

_**Safran :**_ "Deux secondes, on m'agresse, là ! Répète un peu pour voir ?!"

_**Pepper :**_ "Oui, une pimbêche, c'est ce que tu es ! Tu ne respectes rien ni personne parce que tu es frustrée de ne plus pouvoir te donner en spectacle ! Et tu n'assumes pas ! Alors évite de me faire des leçons à l'avenir !"

_Safran bouillonne. Camomille s'approche d'elle en panique._

_**Camomille :**_ "Safran... Coriandre est vraiment mal en point... nous devons partir !"

_**Safran :**_ "Bon... d'accord. Mais nous reprendrons cette discussion, Pepper ! Et on réglera nos comptes une bonne fois pour toute !"

_**Pepper :**_ "J'ai hâte !"

_**Safran :**_ "Partons !"

_Les sorcières enfourchent leur balai. Mais un énorme claquement mécanique retentit alors. Tous les symboles du robot s'éclairent en même temps. Les deux traits sur son excroissance au sommet s'illuminent plus intensément, comme s'il s'agissait de ses yeux. Dans un tintamarre mécanique assourdissant, les cercles crantés s'ouvrent, et déplient progressivement deux énormes bras de métal qui se terminent par des cylindres géants. Le robot utilise ses bras comme appui pour s'extraire du sol. Les sorcières remarquent alors qu'il dissimulait deux énormes jambes qui doivent bien faire deux fois la taille de son corps, qui se déplient pour qu'il se redresse. Ses mouvements lents sont tellement amples qu'ils détruisent davantage les murs déjà fragilisés. La structure commence à trembler._

_**Safran :**_ "Quel colosse !"

_**Camomille :**_ "Il faut le fuir immédiatement ! Sinon l'état de Coriandre risque d'empirer !"

_**Thym :**_ "Je vais le ralentir !"

_Thym déploie les bras. Elle forme une immense bulle d'anti-magie autour du robot, ce qui prend beaucoup de temps et d'énergie vu sa grande taille. Les sorcières ont même l'impression que Thym ne va pas y arriver. Mais elle parvient à terminer sa boucle. Le robot se met à onduler à l'intérieur. Thym essaie de le maintenir, mais l'effort demandé pour former la zone l'a beaucoup épuisée._

_**Camomille :**_ "Vous êtes sûre que ça ira ?"

_**Thym :**_ "Oui... impertinente... filez... vite..."

_Les sorcières prennent de la hauteur. Mais la tête du robot pivote d'un seul coup vers le petit groupe, et il abat son énorme bras dans leur direction pour les arrêter. De nouveaux gravas s'abattent au sol. Lorsqu'il retire son bras, un énorme trou dans le mur s'est formé. Heureusement, les sorcières l'ont esquivé._

_**Safran :**_ "Quelle force ! Et quel butor ! Non mais eh, le tas de ferraille ! On ne porte jamais la main sur des jeunes filles innocentes ! Et encore moins sur des beautés comme moi !"

_**Camomille :**_ "Je crois qu'il s'en moque complètement !"

_La bulle d'anti-magie disparaît. Thym est essoufflée._

_**Thym :**_ "Ce n'est pas... un simple robot... alimenté par magie... c'est... c'est un golem... l'anti-magie... ne l'affecte pas..."

_**Safran :**_ "Un golem ?!

_**Roi Hartru :**_ "Aucune magie ? Ce n'est donc pas une des créatures mythiques... ah, satanée Apiacée ! Elle nous a bien eus ! Ce n'était qu'un leurre..."

_La balai de Thym perd en altitude. Une liane la récupère et la ramène auprès du groupe._

_**Camomille :**_ "Nous allons vous faire sortir de là, ne vous inquiétez pas..."

_**Voix :**_ "Mon frère ! Mon frère ! Je l'ai !!"

_Des bruits de pas retentissent au milieu du chaos. Le prince Cletonal, tout enjoué, descend dans les cachots à toute allure, une orbe verte luminescente à la main._

_**Cletonal :**_ "J'ai réussi à capturer l'esprit de la forêt ! J'ai..."

_Puis il constate avec horreur que primo, son frère est attaché et en compagnie du groupe de sorcières, et que deuzio, un immense golem se dresse devant lui._

_**Cletonal :**_ "Mais... mais... MAIS QU'EST-CE QUI SE PASSE ICI ?!!"

_**Safran :**_ "Je vous déconseille de descendre plus bas, ignare."

_**Camomille :**_ "RELÂCHEZ SEIGNEUR SYLVAGRO !!!"

_Camomille fonce sur le prince. Ce dernier recule d'un pas et dégaine son épée._

_**Cletonal :**_ "Encore vous ?! C'est vous qui avez activé le gardien, n'est-ce pas ? Vous cherchez encore à ruiner mes plans !"

_**Camomille :**_ "Pauvre idiot ! Ce robot n'est pas le gardien de l'Arbre de Komona ! C'est un golem conçu par Apiacée, sans aucun pouvoir magique ! Votre frère vous a dupé !"

_**Cletonal :**_ "Peuh ! Sottises ! Mon frère n'oserait jamais me mentir à moi ! N'est-ce pas, mon frère ?"

_**Roi Hartru :**_ "Fuis, pauvre fou ! Avant de te faire broyer !"

_**Cletonal :**_ "Mais..."

_**Camomille :**_ "Donnez-moi cette orbe !"

_Mais un énorme bras métallique s'abat sur eux. L'impact est tout aussi violent, et d'autres pierres chutent encore._

_**Armoracia :**_ "DAME CAMOMILLE !!"

_**Safran :**_ "Pauvre idiote ! Elle devrait faire plus attention à elle, au lieu de vouloir récupérer son esprit chéri !"

_Mais Camomille émerge de la poussière en toussant. Elle revient vers son groupe._

_**Safran :**_ "Ouf ! Te voilà indemne ! Vite, sortons de son champ d'action, avant que l'édifice ne s'effondre !"

_**Camomille :**_ "Où est... le prince ? Et l'orbe ?

_**Safran :**_ "On s'en fiche ! La priorité est de sortir soigner Coriandre ! Allez !"

_**Armoracia :**_ "Où est Maître Shichimi ?!"

_Shichimi revient vers le groupe sur son petit nuage._

_**Shichimi :**_ "Je suis là ! L'impact m'a soufflé quelque peu. Allons-y vite !"

_**Armoracia :**_ "Et Princesse Pepper ?"

_Elles constatent que Pepper n'est pas avec elles. Mais un puissant sort explosif frappe le golem, ce qui le fait légèrement tomber en avant. Il se rattrape sur le mur, provoquant davantage de dégâts. Shichimi arbore un visage sombre._

_**Shichimi :**_ "Elle saura se débrouiller seule. Partons vite."

_Les sorcières se dirigent vers le haut pour atteindre la sortie. De son côté, Pepper, avec Messy, vole autour du golem, toujours enragée._

_**Pepper :**_ "Alors comme ça, tu veux nous écraser, hein ? Je vais te réduire en pièces !!"

_Pepper l'attaque à nouveau. Mais les dégâts provoqués par le golem deviennent critiques. Les murs commencent à s'effondrer. Pepper vole entre les pierres qui tombent pour ne pas se faire toucher. Puis, elle décide de battre en retraite et sortir par où elle est venue. En sortant de la falaise, elle voit une mer agitée, une pluie battante, et un orage violent, illuminant le ciel de sa foudre. Vu l'aura colérique que dégage Pepper, on aurait presque l'impression qu'elle génère les éclairs par la force de sa rage.  
Elle regarde la falaise s'effondrer sur elle-même. Elle voit le golem chuter dans l'eau, sans le moindre regret. Messy observe la catastrophe avec son râle habituel._

_**Pepper :**_ "Tu as raison, Messy. Faisons comme d'habitude. Après tout, ce maudit roi et ce satané prince méritent de voir leur château être détruit..."

_En effet, la structure de la falaise a été gravement affaiblie. Pepper voit que le château commence lentement à s'affaisser. C'en est presque fini du règne de la terreur du roi Hartru.  
Pepper ne le voit pas venir. Avec un puissant saut le faisant émerger de l'eau, le golem fond sur Pepper et tourne sur lui-même en déployant les bras. Pepper est projetée sur des dizaines de mètres, et atterrit violemment sur un rempart. Messy va la rejoindre sans se précipiter.  
Le golem atterrit lourdement sur la falaise déjà fragile. C'est le coup de grâce : une énorme fissure apparaît juste devant l'entrée du château. Des dizaines de gardes essaient d'évacuer les résidents, mais c'est trop tard : le morceau de falaise et le château tout entier commencent à basculer vers la mer agitée.  
Les habitants ont cessé leur activité. Tous regardent le monstrueux golem qui se dresse devant le château de leur roi, en train de s'effondrer. Un vent de panique intense s'empare de la foule lorsque le golem se met en marche en direction de la ville, vers l'endroit où Pepper a atterri. Ils se poussent, se bousculent violemment pour essayer de sortir de la cité de Grala, avant de se faire écraser pour le golem.  
Un énorme craquement retentit parmi la pluie démentielle. Le château commence à tomber vers la mer. Cependant, sa course est ralentie par une immense toile gélatineuse. Certains habitants ont stoppé leur course pour regarder ce qu'il se passe._

_**Habitant 1 :**_ "Mais... le château ne tombe plus ?"

_**Habitant 2 :**_ "Non ! Regardez, une immense toile d'araignée la retient !"

_Les habitants qui ont grimpé les remparts désignent quelque chose au loin._

_**Habitant 3 :**_ "Là ! Regardez ! Des sorcières !"

_Tous essaient de voir où le doigt de leur informateur se dirige. En effet, ils voient un troupeau de sorcières sur leur balai, autour du château. Ils voient aussi que quelqu'un essaie tant bien que mal de ralentir la chute du château avec sa toile._

_**Armoracia :**_ "Gnnn !! C'est... beaucoup trop lourd... et... je n'ai plus assez de fil !!"

_Les fils commencent à craquer._

_**Camomille :**_ "Tenez bon, Princesse ! Je vais vous aider !"

_Camomille invoque les racines les plus puissantes qu'elle puisse générer. Elles s'enroulent autour du château pour le maintenir en place. Mais il reste bancal._

_**Shichimi :**_ "Attendez !"

_Shichimi joint les mains. Une ouverture apparaît au-dessus d'elle, et un énorme bras musclé en sort. Il se positionne sous le château et tente de le maintenir. Les habitants poussent un cri de joie._

_**Habitant 1 :**_ "INCROYABLE ! Elles ont réussi à le rattraper !"

_**Habitant 2 :**_ "Elles sont vraiment épatantes, ces sorcières ! Je ne comprends pas ce que le roi avait contre elles..."

_**Habitant 3 :**_ "Je ne voudrais pas vous alarmer, mais le géant, là-bas, fonce toujours sur nous !"

_Ils réalisent que le golem marche toujours dans leur direction, provoquant des ondes de choc rendant le travail des sorcières difficile. Ils s'agitent à nouveau pour s'écarter de son chemin. Safran s'est posée sur les remparts avec les animaux, Coriandre et Thym, au bord de l'épuisement._

_**Safran :**_ "Fantastique ! Vous l'avez arrêté !"

_**Shichimi :**_ "Mais ce n'est pas suffisant... avec les pas de ce golem, ne nous tiendront pas longtemps ! Il faudrait qu'on puisse le remonter et le déplacer pour le mettre hors de danger !!"

_**Safran :**_ "Grrr ! Je n'ai pas de tels pouvoirs... et Coriandre aurait certainement pu arrêter ce golem !"

_**Thym :**_ "Si seulement... je n'avais pas utilisé... tout mon Réa... je pourrais... vous aider..."

_**Safran :**_ "Reposez-vous, vieille dame ! Vous êtes au bord de l'épuisement..."

_**Thym :**_ "VIEILLE DAME ?! Insolente... je vais..."

_Mais son poing retombe bien vite. Ses yeux se ferment._

_**Safran :**_ "Bon, au moins, elle se tiendra tranquille !"

_Elle se tourne vers ses amies. Elles sont de plus en plus en difficulté._

_**Safran :**_ "Qu'est-ce que je peux faire pour vous aider ?!"

_**Shichimi :**_ "Rien de plus, malheureusement ! Occupe-toi des blessées, elles en ont besoin ! Ce qu'il nous faudrait, c'est Pepper..."

_Safran baisse la tête. Carrot arbore un air inquiet, et cherche désespérément sa maîtresse des yeux, sans la trouver._

_**Safran :**_ "Pepper, oui... bon, allez, tenez bon, vous deux ! Je vais vous rafistoler !"

_Elle sort ses potions pour soigner Coriandre et Thym. Le roi Hartru, juste à côté, toujours ligoté, a le teint livide._

_**Roi Hartru :**_ "Si seulement j'avais su... ce robot, ce golem..."

_**Safran :**_ "Le pouvoir aveugle, Roi Hartru. Vous n'aviez qu'à voir la tête de votre frangin, là, lorsqu'il nous a attaquées ! À force de faire joujou avec des entités trop puissantes pour vous, voilà ce qui se passe ! Et vous vouliez affronter les dragons, ha, ha ! Vous êtes bien naïf ! Ce golem n'a pas le dixième de leur puissance !"

_**Roi Hartru :**_ "Pourquoi ? Pourquoi vous vous donnez tant de mal pour sauver mon château ? Après le tort que j'ai pu vous causer..."

_**Safran :**_ "Parce que c'est ça, Unitah ! Des sorcières unies dans l'unique but de venir en aide à tout le monde, quel qu'il soit ! C'est vrai, l'idée de départ, c'était simplement de redonner une bonne image des sorcières aux gens pour que l'usage de la magie soit de nouveau autorisé... mais j'ai bien compris que ce n'était pas le plus important. Ce qui compte, c'est d'utiliser la magie pour faire le bien autour de nous ! Pour rendre les gens heureux ! Rien d'autre ! Tant pis pour l'image ! Et puis, avec des sorcières aussi exceptionnelles que moi, ce n'est qu'une formalité marketing !"

_**Roi Hartru :**_ "Je vois... je suppose que je devrais vous remercier, alors ?"

_**Safran :**_ "Vous faîtes comme vous le sentez, mon bon souverain. De toute façon, ça ne changera rien à notre façon d'agir."

_**Roi Hartru :**_ "J'ai été idiot de croire que je pourrais être un héros ! Regardez-moi, attaché sur le rempart de ma propre ville, incapable de sauver mon peuple ! Je l'ai même mis en danger pour mes ambitions..."

_**Safran :**_ "Il n'est jamais trop tard pour changer ! Je suis navrée que personne ne puisse s'occuper de ce golem... mais il n'a pas l'air d'en vouloir à votre peuple. Ce qui l'intéresse, c'est... Pepper."

_Le golem arrive en ville. Il commence à détruire plusieurs habitations._

_**Roi Hartru :**_ "Il détruit ma chère Grala... si seulement on pouvait l'arrêter !"

_**Safran :**_ "Je m'y emploierais bien, mais... il faut que je soigne vite Coriandre, c'est la seule qui puisse nous aider !"

_Le roi regarde Coriandre, avec un air de regret._

_**Roi Hartru :**_ "Et dire que j'ai été jusqu'à demander au roi Einsheneim de l'abandonner parce que j'avais peur qu'il ne devienne trop puissant... ha, ha... Apiacée m'a bien puni, il semblerait ! Ma cité va être détruite sous mes yeux..."

_**Safran :**_ "Tout n'est pas perdu ! Il suffirait qu'elle se réveille..."

_Le golem est au milieu de la ville. Les habitants courent dans tous les sens pour éviter ses énormes pieds. Les autres sorcières et Armoracia arrivent à bout de forces : le château recommence à bouger vers le bas. La situation est critique...  
Au milieu du chaos, un puissant éclair noir jaillit du rempart. Safran et le roi se tournent vers son origine, tout comme Carrot, avec une lueur d'espoir. Pepper vient de se redresser, ses vêtements déchirés, le visage en sang, les yeux écarlates, dégageant une aura ténébreuse encore plus colérique qu'avant._

_**Safran :**_ "...ou qu'une véritable tempête se lève..."

### Scène 4 - Folie furieuse

_Pepper fait face au géant mécanique, les cheveux agités par la tempête. La foudre gronde au loin, donnant à la scène des allures d'apocalypse naissante. Jamais râle de Messy fut aussi profond._

_**Pepper :**_ "Allez. On va lui faire regretter de s'être activé !! YAAAAAHHH !!"

_Pepper s'élance à toute vitesse contre lui. Elle lui lance un sort explosif à bout portant, ce qui le fait reculer d'un pas lourd, provoquant encore plus de dommages dans la ville._

_**Safran :**_ "Mais... elle peut voler sans balai ?!!!"

_En effet, Pepper se maintient dans les airs grâce à son aura. Messy la suit de près, et il semble avoir grossi. Et se mélanger à cette aura ténébreuse qu'elle dégage.  
Le golem se reprend. Il dirige son canon vers Pepper. Ses symboles sur le corps s'illuminent. Un grondement mécanique retentit, comme si une énergie se chargeait._

_**Safran :**_ "PEPPER ! ATTENTION, IL VA TIRER !"

_Pepper s'écarte au dernier moment. Un colossal rayon d'énergie sort de son bras, et vient s'écraser contre le rempart, qui est pulvérisé au point d'impact. Les habitants se couchent à terre en poussant un cri. Safran fait tout ce qu'il faut pour protéger Coriandre et Thym des tremblements provoqués par l'explosion._

_**Safran :**_ "Quelle puissance... pourquoi Apiacée a créé un tel monstre ?!"

_**Roi Hartru :**_ "Certainement pour protéger l'emplacement de la véritable créature... vu que ce golem n'était qu'un leurre, son rôle devait être d'attaquer quiconque essaierait de le réveiller par la force pour acquérir son pouvoir..."

_**Safran :**_ "Inconsciente ! Elle ne rend pas compte de ce qu'elle a fabriqué ! Encore un peu, et il va réduire sa propre petite-fille en bouillie ! Tiens bon, Coriandre !"

_Mais cette dernière ne se réveille toujours pas. L'impact a beaucoup perturbé les autres sorcières._

_**Armoracia :**_ "HII !! Qu'est-ce que c'était ?!"

_**Shichimi :**_ "Le golem ! Il vise Pepper !"

_**Camomille :**_ "Pepper est ici ?! Il faut qu'elle vienne nous aider ! Sans ça..."

_**Shichimi :**_ "Impossible... le golem l'a prise en chasse ! On prendrait des risques si elle venait jusqu'ici..."

_**Camomille :**_ "Mais on ne va plus tenir très longtemps..."

_Effectivement, leur emprise sur le château diminue. Il continue de s'affaisser. Shichimi pousse un grognement._

_**Shichimi :**_ "Alors il va falloir tout donner pour le remonter nous-mêmes ! Vous êtes prêtes à donner toutes les forces qu'il vous reste ?"

_**Camomille :**_ "On n'a plus le choix..."

_**Armoracia :**_ "Je donnerai tout ce que j'ai dans chacun de mes bras pour sauver ce château et ses résidents !"

_**Shichimi :**_ "Bon ! À mon signal, vous tirerez le plus fort possible vers le haut ! On va essayer de le poser là-bas, contre les remparts ! Attention..."

_Avec des gestes tremblants, Shichimi essaie d'invoquer un deuxième esprit tout en maintenant la connexion avec le précédent._

_**Camomille :**_ "Tu veux invoquer un autre esprit ?! C'est de la folie, Shichimi, arrête !"

_**Shichimi :**_ "On n'a... plus le choix... préparez-vous !"

_Elle pose ses mains l'une contre l'autre. Une seconde ouverture apparaît, puis disparaît. Alternativement. Shichimi essaie de l'ouvrir, mais elle continue d'apparaître et de disparaître. Tandis que l'ouverture de l'autre esprit au bras musclé commence à faiblir. Le château perd de l'altitude._

_**Camomille :**_ "Shichimi..."

_Un violent impact la fait sursauter. Pepper et le golem sont en train de se livrer un combat acharné. La foule n'ose plus bouger, de peur de prendre un sort ou un rayon du golem. Ce dernier en a chargé un autre en direction de Pepper, mais l'a encore manquée. Il détruit une autre partie des remparts._

_**Safran :**_ "Si ça continue, les remparts vont s'effondrer... et je n'ai pas la force de transporter tout le monde ailleurs... allez, l'intello ! Réactive tes méninges, on en a besoin !"

_Malgré les soins, Coriandre ne se réveille toujours pas. Mango, très inquiet, continue de lui picorer le visage, mais aucune réaction. Truffel a aussi tenté de la léchouiller plusieurs fois, sans succès. Carrot... il est trop affolé par la violence de l'affrontement pour y prêter attention. Et par l'excès de rage de sa maîtresse...    
Les sorts de Pepper sont de plus en plus effrayants. Ils commencent à créer des distorsions spatiales plutôt dangereuses, qui détruisent de grands bâtiments au sein de la ville. Les habitants essaient d'éviter les gravats, voire les structures entières qui leur tombent dessus._

_**Pepper :**_ "Mais c'est qu'il commence à m'agacer, ce golem, avec ses bras !! Prends donc ça !"

_Elle jette un sort de Destruction sur ses articulations. L'un de ses bras tombe lourdement par terre._

_**Pepper :**_ "Ah, enfin !"

_Il utilise son second bras pour essayer de la frapper._

_**Roi Hartru :**_ "ATTENTION !"

_Mais il touche Pepper, qui est violemment projetée en l'air. Elle se stabilise vite, encore plus en colère.  
De son côté, Shichimi est en difficulté._

_**Camomille :**_ "Shichimi, on perd notre prise ! Arrête !"

_**Shichimi :**_ "NON ! Je suis... le Maître de Ah... je ne peux pas... échouer !"

_Elle pousse un grand cri. La seconde ouverture finit par s'ouvrir. Des nuages en sortent et soutiennent le château._

_**Camomille :**_ "Incroyable ! Elle a réussi..."

_**Shichimi :**_ "TIREZ !!!"

_Camomille, Armoracia et Shichimi tirent de toutes leurs forces. Peu à peu, le château remonte sur la falaise. Elles parviennent de justesse à le déplacer sur le côté, et le laissent lourdement tomber. Il est dans un piteux état, l'immense bannière de Grala sur sa façade est à moitié déchirée, mais il est sauf. Les trois jeunes filles s'effondrent._

_**Camomille :**_ "On... on a réussi..."

_**Armoracia :**_ "Maître Shichimi... vous êtes... merveilleuse !"

_**Shichimi :**_ "Non... vous, vous l'êtes... je ne pouvais simplement pas... décevoir mon Maître..."

_Des applaudissements retentissent. Les résidents du château sont sortis et acclament les sorcières._

_**Armoracia :**_ "Ils sont gentils... mais je n'ai pas la force... de les remercier !"

_**Shichimi :**_ "Nous les remercierons... plus tard..."

_Sur les remparts, le roi Hartru est impressionné._

_**Roi Hartru :**_ "Elles ont vraiment réussi à sauver mon château..."

_**Safran :**_ "Vous en doutiez ? Nous sommes Unitah, pas des débutantes ! Et avec un entraînement aussi efficace que le mien, comment croyiez-vous que tout ceci allait finir ?"

_**Roi Hartru :**_ "Elles auront au moins réussi à sauver les résidents du château... ma femme..."

_Les cris de la foule détournent leur regard. Pepper continue de se battre contre le golem._

_**Roi Hartru :**_ "Mais le principal problème n'est pas résolu..."

_Les autres sorcières regardent Pepper et le golem d'un air inquiet._

_**Armoracia :**_ "Vous pensez... que tout ira bien... pour elle ?"

_**Shichimi :**_ "Pepper s'en sort toujours... ce n'est pas ce qui m'inquiète le plus..."

_Puis Pepper se fait de nouveau frapper par le bras restant du golem. Aveuglée par la colère, elle se tourne encore vers lui._

_**Pepper :**_ "Tu vas mourir, tas de ferraille !!"

_**Habitants :**_ "ATTENTION !"

_Mais Pepper ne l'a pas vu venir. Les symboles du golem sont encore brillants. Le bras au sol du golem profitait du combat pour charger un autre rayon en sa direction. Il relâche toute sa puissance sur Pepper, qui est frappée de plein fouet. Elle s'écrase contre le rempart. La foule pousse un cri horrifié._

_**Armoracia :**_ "PRINCESSE PEPPER !!"

_**Shichimi:**_ "Non... NON ! C'est impossible ! Elle n'a pas pu..."

_**Safran :**_ "Oh non... elle n'a pas pu survivre à ça, vu les dégâts causés sur les remparts..."

_Carrot miaule à s'en rendre aphone. Truffel pose une patte sur son épaule. Pepper ne réapparaît pas. Il continue d'appeler sa maîtresse désespérément pendant plusieurs minutes, mais Pepper ne réapparaît toujours pas. Il se met alors à pleurer à chaudes larmes. Truffel et Mango essaient de le soutenir. Coriandre finit par réagir et ouvrir les yeux._

_**Coriandre:**_ "Hum... qu'est-ce qu'il s'est passé ?... oh, le golem ! Le golem ! Où est-il ? Où est le golem de ma grand-mère ?!"

_**Safran :**_ "Coriandre... ton golem, il a... il a..."

_Elle regarde le visage abattu de Safran avec un air horrifié. Elle voit alors le golem, avec un seul bras, son autre bras au sol encore fumant, ainsi que l'impact encore frais sur les remparts. Elle voit les dégâts provoqués dans toute la cité avec une grande frayeur._

_**Coriandre :**_ "Oh non... c'est lui qui a provoqué tout ça... quel désastre... quel malheur, pauvres gens... mais... où sont les autres ?!"

_Elle regarde vers la falaise et découvre Camomille, Shichimi et Armoracia, au sol, le visage aussi abattu que Safran._

_**Coriandre :**_ "Pepper... où est Pepper ?! Safran, réponds-moi ! Où est Pepper ?!"

_Mais Safran ne répond pas. Ne pouvant retenir une larme, elle se contente de lâcher :_

_**Safran :**_ "Pauvre, pauvre imbécile ! Tu ne pouvais pas te contrôler, pour une fois ?!"

_**Coriandre :**_ "Non... NON ! Elle n'est pas... elle n'est pas..."

_Mais la détresse de Carrot et la confirmation du roi Hartru répondent à sa question._

_**Roi Hartru :**_ "Elle a été frappée par l'un des rayons de cette chose... je suis navré pour vous."

_**Coriandre :**_ "NON !!! Oh, Pepper..."

_Elle fond en larmes, à genoux. Mango se dirige vers elle et pose sa petite tête sur sa main._

_**Coriandre :**_ "Grand-mère... pourquoi... pourquoi avoir créé un tel monstre de puissance ?! La technologie ne devrait pas servir à ça ! Non..."

_Une vague de tristesse a envahi la cité de Grala. Le golem semble s'être immobilisé, comme pour laisser le temps aux sorcières de pleurer leur amie disparue. Même ceux qui ne la connaissaient pas pleurent la disparition de la sorcière qui a tenté de les défendre.  
Au loin, en retrait dans la forêt de Lorbéciande, quelqu'un observe les événements en croisant les bras. Comme s'il attendait que quelque chose se produise.  
Soudain, un éclair de ténèbres jaillit du point d'impact. Le grondement qui en résulte force tout le monde à se boucher les oreilles. De puissantes vibrations font trembler le sol._

_**Safran :**_ "MAIS QU'EST-CE QUE C'EST ?!!"

_Lorsque le grondement cesse, tout le monde regarde le point d'impact. Une immense sphère de ténèbres s'élève dans les airs. En son centre, une sorcière, en proie à une crise de rage incontrôlable. Elle est difficile à reconnaître, mais..._

_**Armoracia :**_ "Princesse Pepper ?! C'est... c'est bien elle ?"

_Carrot lève les yeux. Il devrait être ravi que sa maîtresse soit encore en vie, mais... il ne reconnaît pas sa maîtresse. Il ressent une frayeur encore plus grande qu'avant. Comme toutes les sorcières présentes. Seuls les habitants poussent un cri de joie de voir que leur héroïne est toujours en vie._

_**Shichimi :**_ "Il s'agit bien du corps de Pepper, Princesse. Mais vous pouvez me croire : ce n'est clairement pas la Pepper que nous connaissons qui flotte là-haut. Je vous conseille de vous mettre à l'abri !"

_Shichimi et Armoracia, plus qu'inquiète, se déplacent avec le peu de force dont elles disposent. Mais Camomille est paralysée._

_**Shichimi :**_ "Viens vite, Camomille !"

_**Camomille :**_ "C'est... c'est elle ! Celle que j'avais sentie... la haine absolue... nuisible pour tous..."

_Shichimi essaie de traîner Camomille vers un abri.  
Pepper pose son regard écarlate vers le golem. Aussitôt, ce dernier remue. Mais Pepper pousse un hurlement presque animal, qui fait trembler toute la cité. Ses yeux deviennent incandescents, et rayonnent de rage. Une immense colonne de ténèbres la relie au ciel, qui n'en peut plus d'exploser. La véritable tempête commence.  
À une vitesse ahurissante, Pepper fond sur le golem et le frappe de toutes ses forces. Il n'a pas le temps de réagir : les coups sont d'une violence rare. Ils commencent à entamer sa structure métallique, et l'onde de choc des impacts affectent aussi les environs, provoquant encore plus de destruction que le golem en aurait été capable. Le rempart semble sur le point de s'effondrer, mais Safran et Coriandre se sont recroquevillées au-dessus des animaux et de Thym, toujours inconsciente, en attendant que la tempête cesse. En priant pour que Pepper arrête sa folie.  
Mais ce ne semble pas être dans les projets de Pepper. Elle continue de frapper le golem, qui commence à vaciller davantage, perdant de plus en plus de parties de lui-même. Pepper provoque une immense tornade d'énergie noire, qui enveloppe le golem. Tout le monde doit s'accrocher à quelque chose pour ne pas se faire souffler. La foudre électrifie la tornade, la rendant encore plus puissante. Un véritable déluge s'abat sur Grala, que personne ne peut observer à cause de l'eau qui est violemment projetée, et de la puissance du cyclone. Tout ce qu'ils peuvent faire, c'est attendre, et espérer que cela cesse vite._

_**Armoracia :**_ "Princesse Pepper ! Par pitié, arrêtez !!"

_Mais ce n'est pas suffisant pour la stopper. Shichimi et Camomille se sont réunies autour d'elle, à genoux, dans un abri près des remparts, à espérer que Pepper se calme.  
Le golem est quelque peu soulevé en l'air par la tornade. Lorsqu'il est suffisamment haut, Pepper prend énormément de distance, et fonce de toutes ses forces vers le golem, afin de lui donner un uppercut puissant, qui a raison des remparts et les coupent en deux. Le golem est projeté vers le ciel. Les habitants peuvent à nouveau observer la scène, maintenant que la tornade a cessé. Les sorcières également. Tous sont stupéfaits de voir que Pepper a réussi à projeter le golem aussi haut dans les airs.  
Mais ce ne semble pas être terminé. Les yeux toujours écarlates, Pepper pousse un autre hurlement animal. Elle écarte les bras, et rassemble toute son aura au-dessus d'elle. Elle provoque alors une ouverture immense, entièrement composée de ténèbres, faisant bien deux fois la taille du golem en rayon. Le trou noir le plus gros qu'elle n'ait jamais généré. Sa puissance d'attraction est trop forte. Les gens se mettent à hurler, mais ne peuvent lutter et se font aspirer. Le roi Hartru, Safran et Coriandre, ainsi que les animaux, commencent à être attirés par le trou noir. Shichimi a engagé une barrière pour éviter qu'Armoracia, Camomille et elle-même se fassent aspirer, mais elle a du mal à tenir.  
Fort heureusement, les habitants sont stoppés en l'air. Il semble qu'une immense barrière invisible empêche les gens de se faire avaler par le trou noir. En revanche, les débris de la ville commencent à disparaître au contact du trou noir. Les morceaux de remparts restants sont arrachés de la terre et aspirés par l'ouverture. Le château sauvé par les sorcières traîne au sol, mais se retrouve bloqué par une paroi toute aussi invisible.  
Bientôt, le golem commence à revenir des cieux, à toute vitesse. Il finit par tomber directement dans le trou noir, puis disparaître. Instantanément, le trou noir se referme. Le pouvoir d'attraction cesse, et les gens retombent au sol.  
Safran, Coriandre et les animaux vont s'écraser au sol également. Mais les habitants et les sorcières sont ralentis par Thym, qui s'est éveillée, et atterrissent en douceur sur le sol. De leur côté, Shichimi, Camomille et Armoracia sortent lentement de leur abri, toujours sous le choc.  
La protection invisible retenant les habitants disparaît. L'individu observant la scène au loin baisse son bras dirigé vers la cité. En rajustant son masque à plumes, il disparaît d'un seul coup.  
Les habitants reprennent leurs esprits peu à peu. Tous les regards se reportent sur la sphère ténébreuse de Pepper, qui a fortement diminué. Les yeux toujours aussi rouges, mais moins intenses, elle se pose sur un morceau de rempart encore debout, et hurle au peuple :_

_**Pepper :**_ "HABITANTS DE GRALA ! LA MENACE DU GOLEM N'EST PLUS ! VOUS ÊTES SAUFS ! ET C'EST UNITAH QUI VOUS A SAUVÉS !"

_Aussitôt, le peuple pousse une puissante acclamation de joie._

_**Habitants :**_ "OUI !! VIVE UNITAH ! VIVENT LES SORCIÈRES !"

_Pepper lève le poing au ciel, en poussant un cri de victoire. Seuls les membres d'Unitah, les animaux et le roi continuent de regarder avec effroi la responsable d'un tel élan de destruction._

### Scène 5 - La tête coupée

_Les sorcières d'Unitah s'associent aux habitants de la ville pour essayer de déblayer les gravats qui composent Grala, qui ressemble désormais plus à des ruines qu'à une cité. Elles recherchent des personnes qui seraient éventuellement bloquées sous les décombres. Quant aux blessés, Safran s'occupe de les prendre en charge, assistée par Camomille et Armoracia._

_**Homme blessé :**_ "Oh, trois jolies infirmières rien que pour moi ! J'ai vraiment de la chance d'être encore vivant !"

_**Safran :**_ "Bien vu, brave Gralien. Mais une des trois sort clairement du lot, non ?"

_**Camomille :**_ "Safran, enfin ! Sachez, Monsieur, qu'il y a trois files ! Alors décidez-vous auprès de qui vous allez vous faire soigner !"

_L'homme regarde les trois infirmières, et se dirige vers Armoracia._

_**Homme blessé :**_ "Hum, je vais prendre la jeune fille aux multiples bras ! Comme ça, j'aurai l'impression de me faire soigner par vous trois en même temps !"

_**Armoracia :**_ "Euh... par ici, Monsieur !!"

_Armoracia l'emmène de son côté. Safran est presque vexée de ne pas avoir été choisie._

_**Camomille :**_ "Concentre-toi, Safran ! On a encore beaucoup de monde à soigner !"

_**Safran :**_ "C'est bon ! Depuis que tu as décidé que les humains méritaient ton attention, tu es devenue encore plus désagréable qu'avant !"

_Camomille lui tire la langue et prend un enfant en charge, qui se dit impressionné par les racines qui ont retenu le château.  
En effet, en discutant avec les habitants, les sorcières reçoivent des compliments sur leur magie, des remerciements et de l'admiration. Pour eux, c'est une issue heureuse. Aucun d'entre eux n'a de rancœur envers les sorcières. Au contraire, ils les admirent. Surtout Pepper.  
Pepper se trouve dans une tente, cachée des habitants. Sous la surveillance de Thym, elle est enfermée dans une bulle d'anti-magie. Coriandre, la mine basse, est assise sur une chaise, juste à côté. Les caquètements de Mango ne semblent pas lui remontrer le moral._  

_**Pepper :**_ "Vous avez franchement intérêt à me laisser sortir d'ici maintenant, sinon..."

_**Thym :**_ "Sinon quoi ? Tu vas nous aspirer dans un autre trou noir ? Aucun risque, dans cette prison. C'est plus sûr pour tout le monde."

_**Pepper :**_ "Bon sang, MAIS JE VIENS DE SAUVER TOUTE UNE VILLE !!"

_**Thym :**_ "Si tu continues de hurler, je te lance un sort de Mutisme. C'est bien compris ?!"

_Pepper ne répond pas. Elle lui lance un regard mauvais. Carrot, à ses pieds, l'observe avec frayeur. Il n'ose pas l'approcher._

_**Coriandre :**_ "Grand-mère..."

_Thym jette un œil vers Coriandre._

_**Coriandre :**_ "Je pensais que vous étiez pour la paix... que vous n'utiliseriez jamais l'ingénierie à de mauvaises fins... suis-je... suis-je condamnée, comme maman... à engendrer encore plus de destruction ?"

_**Thym :**_ "Non, rassurez-vous, ma petite. Le cas de votre mère était particulier. Mais votre grand-mère n'a jamais œuvré pour le mal. Si elle a été jusqu'à construire une telle arme, c'était sans doute pour protéger quelque chose de très important."

_**Coriandre :**_ "Mais qu'est-ce qui peut justifier une telle violence ?!"

_Thym soupire. Elle prend une chaise et s'assoit à côté de Coriandre._

_**Thym :**_ "Je comprends ce que tu peux ressentir. Tu as peur d'être déçue par la femme qui t'a tout appris, et que tu admirais tant, n'est-ce pas ? C'est pour ça que tu te tortures l'esprit à essayer de comprendre ses motivations ?"

_**Coriandre :**_ "Mais... comment avez-vous su ?"

_**Thym :**_ "Parce que j'y ai été confrontée, moi aussi. J'admirais mon Maître Chicory plus que tout. J'étais convaincue que la moindre de ses actions, la moindre de ses réflexions avait pour but le bonheur commun et le progrès du monde de la magie. Mais j'ai finalement appris qu'elle s'était servie de nous... de moi... pour satisfaire ses propres désirs. Je... j'ai encore du mal à accepter ce qu'elle a fait. Que le Maître qui m'a enseigné tout ce que je sais n'était... qu'un mythe. Mais s'il y a une chose dont je suis sûre, c'est qu'Apiacée n'était pas égoïste. Loin de là."

_**Coriandre :**_ "Vous la connaissiez ?"

_**Thym :**_ "Peu. Nous n'avons jamais eu de vraie conversation ensemble. Mais je lui dois la vie. C'est elle qui m'a arrachée aux bras de la mort, ainsi que mes deux disciples. Est-ce qu'une personne égocentrique ferait ça ? D'accord, c'est cette satanée Wasabi qui le lui a ordonné, mais... après tout ce qu'elle a fait pour contribuer à l'évolution de notre espèce... pour les sorcières, mais aussi pour les humains... nous étions censées être de simples pantins guidés par la Zombification. Pourtant, nous nous sommes retrouvées à nouveau maîtres de nos mouvements et de nos pensées... même si elle prétend le contraire, je suis convaincue qu'Apiacée nous a volontairement redonné la vie."

_**Coriandre :**_ "Vous... vous croyez ?"

_**Thym :**_ "J'aime à le penser. Qu'elle n'ait pas révélé sa méthode afin qu'elle ne soit pas utilisée de façon abusive... ce serait bien son genre, d'apprendre de son erreur d'avoir diffusé la Zombification au monde. En tout cas, elle devait forcément avoir une bonne raison de créer ce golem. Nous n'aurions pas dû le réveiller... n'est-ce pas, Pepper ?"

_Pepper croise les bras et ne répond pas. Coriandre est attristée._

_**Coriandre :**_ "J'aurais tellement voulu l'étudier... il m'aurait sûrement apporté les réponses que je cherche. Sur grand-mère, mais aussi... sur maman..."

_**Thym :**_ "Hum. Viens avec moi."

_Coriandre suit Thym avec curiosité. Elles sortent de la tente où elles se trouvaient pour se diriger vers une autre, un peu plus grande, mais aussi mieux dissimulée derrière des débris._

_**Thym :**_ "Entre vite."

_Coriandre entre. Elle met ses mains devant sa bouche pour étouffer son exclamation de surprise. Sous cette tente repose un bras métallique un piteux état._

_**Coriandre :**_ "C'est... non !!"

_**Thym :**_ "J'ai pu le récupérer sans qu'on me voit. Il n'a pas été aspiré avec le reste du golem. Je me suis dit que, peut-être... la petite-fille d'Apiacée serait intéressée..."

_**Coriandre :**_ "Et comment ! Oh, merci, Maître Thym ! C'est très important pour moi !"

_Elle serre Thym en se mettant sur un genou._

_**Thym :**_ "Aïe, aïe ! Mon dos !!"

_**Coriandre :**_ "Oh, désolée !!"

_Elle la relâche. Thym remet sa robe en place._

_**Thym :**_ "Tu en feras un meilleur usage que moi. Je te suggère cependant de rester discrète à ce sujet."

_**Coriandre :**_ "Vous pouvez compter sur moi !"

_Coriandre se jette sur le bras pour l'observer de plus près. Le visage de Thym s'adoucit : elle est plutôt contente d'avoir redonné le sourire à la petite-fille de celle qui lui a redonné la vie._

_**Thym** (à elle-même) **:**_ "De rien, Apiacée..."

_Pendant le reste de la journée, les sorcières continuent d'aider les blessés de la catastrophe. Camomille en profite pour chercher du côté de la falaise si elle trouve l'orbe verte renfermant Sylvagro, mais c'est peine perdue. Shichimi lui conseille d'abandonner, que Sylvagro a dû être libéré. Camomille acquiesce, l'air abattu.  
Chaque habitant veut remercier les sorcières. Un herboriste fait cadeau de ses dernières plantes à Safran. Une couturière veut leur offrir de nouveaux accessoires de mode. Et mieux que tout, la reine de Grala en personne vint les remercier._

_**Reine de Grala :**_ "Chères sorcières, vous avez été héroïques ! Vous nous avez tous sauvés, ainsi que notre château ! Recevez notre reconnaissance éternelle !"

_Elle s'incline devant les sorcières._

_**Safran** (en donnant un coup de coude à Camomille) **:**_ "Elle, au moins, elle est gentille, pas comme son butor de mari !"

_**Camomille :**_ "Safran !"

_**Safran** (s'inclinant devant la reine) **:**_ "Oh, ce n'est pas grand-chose, vous savez ! C'est notre truc, de sauver des gens, chez Unitah ! Au fait, votre mari, on ne l'a plus vu depuis un moment."

_**Reine de Grala :**_ "Il est en pleine discussion avec l'une des vôtres... le Maître de Ah... Shichimi, c'est ça ? Ils sont dans la tente, là-bas."

_**Safran :**_ "Ah, elle se tape la discussion avec le big boss..."

_**Reine de Grala :**_ "Grâce à vous, aucun mort n'est à déplorer, à part notre regretté prince Cletonal, toujours porté disparu..."

_**Safran :**_ "Oui... enfin, sauf votre respect, Altesse, le prince Cletonal était vraiment un..."

_**Camomille :**_ "Safran, pour l'amour des plantes !"

_**Reine de Grala :**_ "On m'a rapporté ce qu'il a fait, malheureusement. Mais il ne méritait pas son sort pour autant. J'ai lancé plusieurs hommes à sa recherche. Je suis navrée qu'il vous ait causé du tort..."

_**Safran :**_ "C'est bon, c'est du passé, tout ça ! Et il a été plus que bien puni... ah, si seulement il avait été comme sa compagne !"

_**Reine de Grala :**_ "Sa compagne ?"

_**Safran :**_ "Mais si, vous savez bien ! Sha-One, la femme avec qui il se promenait !"

_**Reine de Grala :**_ "Sha... One ? Qui est-ce ?"

_Camomille lui lance un regard noir. Safran a un visage coupable._

_**Safran :**_ "Oups ! C'était une liaison secrète apparemment..."

_**Reine de Grala :**_ "Quoi qu'il en soit, vous êtes nos sauveurs. Et vous méritez ceci."

_Elle accroche une médaille représentant Grala à chacune des sorcières._

_**Armoracia :**_ "Oh ! Quelle jolie médaille !"

_**Camomille :**_ "Nous... nous ne pouvons pas accepter, Altesse !"

_**Reine de Grala :**_ "Elles vous reviennent. Sans vous, Grala n'existerait plus."

_**Safran :**_ "Bien sûr, Camomille ! Et si elle ne te plaît pas, tu peux toujours la revendre, tu sais ! C'est de l'or, je crois, non ?"

_**Camomille :**_ "Safran !!"

_**Safran :**_ "Oh, il serait temps que tu apprennes à te détendre, Princesse des Plantes ! L'humour, tu connais ?"

_La reine donne aussi une médaille à Mango, Truffel (qui l'arbore fièrement) et Carrot, qui est toujours aussi abattu._

_**Reine de Grala :**_ "Où est la sorcière qui s'est débarrassée du golem ?"

_**Camomille :**_ "Elle est... euh..."

_**Thym :**_ "Indisponible."

_Thym arrive derrière les jeunes sorcières. Elle emmène Coriandre avec elle, qui affiche un sourire éclatant._

_**Thym :**_ "Elle doit... euh... se reposer, Altesse. Son combat l'a fortement éprouvée."

_**Reine de Grala :**_ "Je comprends."

_Elle donne une médaille à Coriandre, qui a toujours un sourire aussi béat. Puis une à Thym, qui la refuse._

_**Thym :**_ "J'ai passé mon temps dans les vapes. Je n'ai rien fait qui justifie une médaille."

_**Reine de Grala :**_ "Vous la méritez, pourtant. Vous avez empêché une chute mortelle à la plupart des Graliens. Je vous en suis reconnaissante. Et vous étiez le mentor de la jeune fille qui a vaincu le golem, donc..."

_Thym se met sur la pointe des pieds. Elle a vraiment l'air peu sympathique._

_**Thym :**_ "Pas. De. Médaille."

_La reine est quelque peu déroutée. Les autres sorcières haussent les épaules._

_**Reine de Grala :**_ "Comme vous voudrez ! La jeune Shichimi a dû recevoir la sienne de mon mari. Pourriez-vous donner celle-ci à la jeune fille qui a bravement combattu ce géant mécanique ? Quel est son nom, déjà ?"

_Thym et les autres sorcières regardent la médaille d'un œil sombre. Thym la prend d'un geste sec et déclare en se retournant :_

_**Thym :**_ "Pepper. Son nom est Pepper."

_Quelques minutes plus tard, les sorcières ont demandé à sortir de la ville pour se réunir. Non loin de la forêt de Lorbéciande, près de la falaise où la mer s'est calmée (mais est toujours aussi agitée), les sorcières et Armoracia se sont rassemblées en cercle. Pepper est au centre, toujours prisonnière de sa bulle anti-magie._

_**Thym :**_ "Bon. Il faut que nous parlions de ce qui vient de se passer."

_**Pepper :**_ "Vous pouvez me faire sortir de là ?!"

_**Thym :**_ "Non, Pepper ! Tu es le principal sujet de discussion !"

_Elle regarde autour d'elle, et ne voit que des regards accusateurs pointés vers elle._

_**Pepper :**_ "Je vois... c'est un jugement, n'est-ce pas ? Comme un mini Conseil de Ah où je serais l'accusée, hein ? C'est une idée de vous, ou bien Shichimi vous a suggéré de rendre ça plus théâtral ?"

_**Shichimi :**_ "Pepper, s'il te plaît ! Essaie de rester silencieuse ! C'est très grave, comme affaire !"

_**Pepper :**_ "Oh, une affaire ? Tu vois, tu avoues toi-même que c'est un jugement ! Allons dans l'Ascétribune, ce sera plus approprié !"

_**Armoracia :**_ "Princesse Pepper, je vous en prie !!"

_Pepper tourne sa tête vers Armoracia, implorante. Elle croise les bras._

_**Pepper :**_ "De toute façon, je n'ai pas le choix, on dirait ! Je me doutais bien qu'il faudrait que je la boucle. Vous voulez me juger, et bien allez-y ! Balancez vos griefs !"

_Les sorcières s'échangent un regard évocateur._

_**Thym :**_ "Il me semble que c'est évident, non ?"

_Elle désigne les remparts de Grala, en lambeaux._

_**Thym :**_ "Vois par toi-même ! Vois où ta folie t'a menée ! À détruire une ville !"

_**Pepper :**_ "Détruire ?! Vous êtes sérieuse ?! Et le golem, alors ? Il était trop occupé à construire un nouveau resto, c'est ça ?!"

_**Safran :**_ "Elle ne parle pas de ça, idiote ! Elle parle des dégâts supplémentaires que ta colère incontrôlable a provoqués !"

_**Pepper :**_ "Vous êtes vraiment à côté de vos pompes ! Vous auriez préféré que je laisse le golem tout ravager sans réagir, peut-être ?"

_**Shichimi :**_ "Ce n'est pas ce qu'on a dit, Pepper. Mais il y avait sûrement une façon moins brusque d'y parvenir !"

_**Pepper :**_ "Moins brusque ! Ha, ha, ha ! Laissez-moi rire ! Si je vous avais laissé faire, effectivement, ça aurait été moins brusque, puisque vous n'auriez rien fait ! Vous étiez trop occupées à essayer de sauver un vieux château qui tombait en ruines pour sauver deux-trois aristocrates qui méprisent les sorcières ! Au lieu de vous concentrer sur la menace du plus grand nombre ! J'ai fait ce qui était le plus urgent, c'est-à-dire sauver le plus de vies possible !"

_**Shichimi :**_ "Mais qui es-tu pour juger si une vie vaut plus qu'une autre ? Nous avons sauvé ces personnes parce qu'il le fallait, c'est tout. Peu importe leur opinion ou leur rang social."

_**Pepper :**_ "Résultat : je me suis farcie le golem toute seule ! Et c'est ce qu'on me reproche, je vous signale ! J'ai sauvé des milliers de vies, et vous, à trois, seulement quelques-unes ! Et vous venez me faire la leçon ! Il faut vraiment réviser l'ordre de vos priorités !"

_**Safran :**_ "Pauvre imbécile ! Shichimi, Camomille et Armoracia ont tout donné pour sauver le château et ses résidents, et elles y sont parvenues sans ton aide qui aurait été plus que la bienvenue ! Un véritable leader les féliciterait !"

_**Pepper :**_ "Oui, parce que tu es bien placée pour parler, toi qui n'as absolument rien fait !"

_**Safran :**_ "Je... je m'occupais des blessées, figure-toi ! Blessées que tu as provoquées, je te rappelle ! Je ne pouvais pas les laisser à leur sort !"

_**Pepper :**_ "Facile de se justifier lorsqu'on se prend pour le meilleur guérisseur du monde, n'est-ce pas ? Résultat, ton action n'a absolument pas aidé à résoudre le conflit ! Elles pouvaient très bien attendre dans un abri, le plus urgent était d'aider les gens en danger !"

_**Camomille :**_ "Tu es injuste, Pepper ! Sans Safran, peut-être que Coriandre ne serait plus parmi nous ! Elle a fait un travail remarquable malgré les secousses !"

_**Pepper :**_ "Oui... je sais ô combien le sort de tes camarades t'intéresse, Camomille ! Tu n'étais pas ici uniquement pour retrouver ton ami Sylvagro, non, bien sûr !"

_**Camomille :**_ "Je..."

_**Pepper :**_ "Arrête ton numéro, Camomille ! Tu veux me faire croire que tu éprouves le moindre intérêt pour nous, mais je sais bien que ce n'est pas le cas ! J'ai vu tes yeux lorsque le prince a failli détruire Floréco ! Et ton petit cinéma avec ton esprit chéri ne change rien ! Tu détestais les humains, tu détesteras les humains, c'est ainsi ! Alors ne me fais pas croire que notre santé t'intéresse, subitement !"

_Camomille est bouche bée et profondément blessée. Coriandre se jette sur elle pour la prendre par les épaules._

_**Coriandre :**_ "Ne l'écoute pas, Camomille. Elle n'est pas dans son état normal."

_**Pepper :**_ "Bien sûr que si ! Qui es-tu pour dire que..."

_**Coriandre :**_ "LA FERME !!"

_Tout le monde est surpris. Personne n'avait jamais vu Coriandre se mettre en colère. Même Pepper est interloquée._

_**Pepper :**_ "Mais..."

_**Coriandre :**_ "TU TE TAIS ! J'en ai plus qu'assez de ta mauvaise foi ! Est-ce trop demandé pour toi de RECONNAÎTRE TES TORTS ?!! Tu ne vois pas ?!! TU NE VOIS PAS QUE C'EST TOI QUI AS CAUSÉ CETTE CATASTROPHE ?!!"

_**Pepper :**_ "Je... je n'ai rien causé, j'ai sauvé les..."

_**Coriandre :**_ "Tu as réparé tes bêtises, c'est tout ! Tu as déjà oublié ? Tu as oublié qui a réveillé le golem par sa colère ?! Qui n'a pas écouté ses camarades qui lui demandaient de ne pas y toucher ?! Qui a refusé de chercher des preuves pour régler l'affaire du roi Hartru sans violence ? QUI VIENT DE RASER UNE VILLE PARCE QU'ELLE NE SAIT PAS CONTRÔLER SA COLÈRE ?!! QUI A FAILLI FAIRE DISPARAÎTRE UN PEUPLE TOUT ENTIER ?!! Et tu te prétends le chef d'Unitah ?!! Un chef ne fonce pas tête baissée sans réfléchir ! Un chef écoute ses hommes et prend des décisions réfléchies, sans obéir à une impulsion ! Et quand un membre de l'équipe dit quelque chose, IL EST PRIS AU SÉRIEUX ! Il ne reçoit pas des réflexions désobligeantes ! Quand j'ai dit de ne pas toucher au golem, que je voulais l'étudier, ce n'était pas juste pour satisfaire mon ego ! J'ai vu la marque d'Apiacée, et je voulais tenter de comprendre comment il fonctionnait, parce que... il était à ma grand-mère... c'était une de mes dernières chances de la comprendre, j'avais besoin de ces réponses ! MOI TOI, TU T'EN FICHES, DE CE QUE PEUVENT RESSENTIR LES AUTRES ! Tu es tellement obsédée par ton objectif qu'il t'aveugle ! Et maintenant, c'est trop tard ! Tu as fait disparaître le golem pour toujours, alors que si tu m'avais écoutée, personne n'aurait été blessé, et la ville ne serait PAS UN TAS DE CENDRES !!!"

_Elle s'effondre, en larmes. Armoracia et Camomille se mettent autour d'elle pour tenter de la consoler, tout comme son brave Mango. Pepper ne sait pas comment réagir. Elle est vraiment peinée de voir Coriandre se mettre dans un état pareil._

_**Pepper :**_ "Coriandre... je... je ne savais pas... je..."

_**Shichimi :**_ "L'écoute. C'est le ciment qui maintient les fondations d'un groupe. Sans lui, l'édifice s'effondre. Si un chef n'écoute plus, c'est tout le groupe qui devient sourd. Pepper. Étant donné que ton manque d'écoute vient de provoquer un cataclysme sans précédent dont les Graliens mettront des jours à se remettre..."

_Elle soupire. Elle regarde Thym, qui est impassible, les bras croisés. Les autres sorcières font un signe de tête affirmé._

_**Shichimi :**_ "...nous avons décidé, à l'unanimité, de t'exclure d'Unitah."

_Pepper ne comprend pas. Son visage reflète une incompréhension, alors qu'elle a très bien compris pourquoi elle a l'impression de se faire poignarder en plein cœur._

_**Pepper :**_ "De quoi ? Vous... vous avez décidé quoi ?"

_**Shichimi :**_ "De t'exclure d'Unitah. Cette décision prend effet immédiatement."

_Seule Armoracia semble triste. Coriandre a du mal à cesser de pleurer, mais les autres la fusillent du regard._

_**Pepper :**_ "Mais... mais depuis quand vous prenez des décisions sans que tout le monde soit présent ? Depuis quand on peut exclure le chef d'un groupe ? HEIN ?! VOUS NE SERIEZ MÊME PAS ICI SANS MOI, ET VOUS ME JETEZ DEHORS ?!! Princesse !"

_Armoracia sursaute._

_**Pepper :**_ "Ne me dîtes pas que vous êtes d'accord avec ça ?!!"

_Armoracia baisse la tête._

_**Armoracia :**_ "Princesse Pepper... je... vous m'avez fait extrêmement peur, et... ce trou noir... non, je ne veux plus vous voir ainsi..."

_**Pepper :**_ "Princesse... alors vous avez rejoint leur camp, n'est-ce pas ? Je ne suis pas étonnée... vous vous êtes même attribuées une médaille derrière mon dos ! À moins que..."

_Elle regarde d'un peu plus près._

_**Pepper :**_ "Je comprends mieux... le roi Hartru vous a achetées, comme Maître Thym ! Et vous cherchez à m'écarter parce que vous saviez que je refuserais !"

_**Shichimi :**_ "Ne sois pas stupide, Pepper. C'est la reine qui a voulu nous remercier, et le roi me l'a confiée parce que nous discutions à propos des créatures mythiques..."

_**Pepper :**_ "Bien sûr... il vous a fait la même promesse qu'à mon ancien Maître, et vous avez accepté ! Félicitations, Maître Thym, vous les avez converties à votre cause ! Vous venez de gagner cinq pauvres lâches dans votre équipe ! Contente ?"

_**Thym :**_ "La colère... elle te fait raconter n'importe quoi, Pepper. Tu deviens paranoïaque ! Regarde plutôt ceci." 

_Elle montre la médaille que la reine voulait lui donner._

_**Thym :**_ "Tu comprends bien pourquoi j'ai refusé qu'elle te la donne. Elle voit en toi une héroïne, qui vient de sauver leur ville, comme tous ces pauvres Graliens... mais nous, nous savons ce que tu es et ce que tu as fait."

_**Pepper :**_ "Mais JE SUIS LEUR HÉROÏNE !! Je les ai tous sauvés ! Et la réputation d'Unitah va recevoir un énorme coup de boost grâce à moi ! Même si j'ai provoqué tout ça par erreur, je l'admets... JE RESTE LEUR SAUVEUSE !! Je mérite cette médaille dix fois plus que vous, et vous décidez de me la prendre ?!! Remarquez, ça ne m'étonne pas... la jalousie nous fait faire des choses stupides ! Et puis, vous voulez bien me montrer votre soumission à ce roi cupide et sans âme, j'ai bien compris le message... GARDEZ-LA, VOTRE MÉDAILLE ! JE REFUSE DE FAIRE PARTIE DU CAMP DES LÂCHES !"  

_**Thym** (ramassant la médaille) **:**_ "Je te conseille d'aller te faire soigner, avant de détruire une autre ville !"

_Pepper commence à bouillonner. Mais lorsqu'elle voit son chat... et surtout sa petite médaille..._

_**Pepper :**_ "Carrot ? Tu... tu les as rejointes aussi ? Tu es contre moi ? Non, pas toi ! Pas mon fidèle compagnon !"

_Carrot a l'air attristé et apeuré. Il rejoint Truffel._

_**Pepper :**_ "Je vois... tu t'es laissé séduire, et tu as mordu à l'hameçon... tu es bien naïf, mon pauvre ami ! Eh bien, vas-y ! Si tu tiens tant que ça à me trahir, tu n'es plus mon chat !"

_Carrot est choqué. Camomille aussi. Elle fusille Pepper des yeux._

_**Pepper :**_ "Et vous, toutes ! Vous avez choisi de fuir ? Très bien ! NE COMPTEZ PAS SUR MOI POUR VOUS SUIVRE ! MOI, JE VAIS ME BATTRE, JUSQU'AU BOUT ! Et je n'ai certainement pas besoin d'une bande de lâches comme soutien !"

_**Armoracia :**_ "Princesse Pepper... nous n'avons conclu aucun marché avec le roi Hartru, il faut nous croire ! Nous sommes toujours déterminées à nous battre pour rétablir la magie !"

_**Pepper :**_ "JE NE VOUS ENTENDS PLUS, D'ACCORD ?!! C'est terminé, vous ne faîtes plus partie d'Unitah, à mes yeux... ce n'est pas vous qui me virez, ça non ! C'EST VOUS QUI ÊTES VIRÉES ! TOUTES ! HORS DE QUESTION DE VOUS LAISSER SALIR LE NOM D'UNITAH AVEC VOTRE LÂCHETÉ ! N'OUBLIEZ PAS QUE C'EST GRÂCE À MOI SI LES HABITANTS DE GRALA CHANGENT D'AVIS SUR LES SORCIÈRES ! PARCE QUE JE ME SUIS BATTUE POUR QUE LEUR OPINION CHANGE ! UNITAH, C'EST MOI !"

_Coriandre se relève. Elle fait face à Pepper en lui lançant un profond regard de dégoût._

_**Coriandre :**_ "Un véritable leader nous aurait dit "Unitah, c'est nous"."

_**Shichimi :**_ "La condamnation est claire. C'est la majorité qui l'emporte, et le châtiment est ton exclusion d'Unitah. Nous gardons donc la jouissance du nom. Toi, tu repars dans l'anonymat, seule avec ton égoïsme et ta colère qui tu n'as jamais su dominer. Et bien entendu..."

_Elle exécute un geste avec sa main. Pepper voit alors son blason s'effacer de son veston. Elle bouillonne._

_**Shichimi :**_ "...hors de question que tu portes le symbole d'Unitah sur toi."

_**Pepper :**_ "Je te déconseille de croiser mon chemin à nouveau, Shichimi ! TU ES PIRE QUE TON MAÎTRE ! Je suis ravie que Chicory lui ait réglé son compte !"

_Shichimi lui envoie toute l'étendue de sa haine à travers ses yeux. Pepper ne peut s'empêcher d'être troublée : Coriandre et Shichimi, ses plus proches amies, qui la regardent avec autant de détestation... elle se rend compte qu'elle est isolée. Seule. Et sans Carrot.  
Un petit être chaotique apparaît alors dans la cage de Pepper. Cette dernière sourit._

_**Pepper :**_ "Tu es le seul qui ait jamais cru en moi, Messy... toi, tu ne me laisseras pas tomber."

_Ce râle particulier est vite devenu le son qu'elle apprécie le plus. L'entendre est un réconfort sans nom._

_**Shichimi :**_ "Bien. Afin d'éviter d'autres catastrophes, il est hors de question de te laisser vagabonder où bon te semble. Tu vas rester dans ta cage d'anti-magie jusqu'à une date ultérieure que nous fixerons. Ensemble."

_Pepper regarde l'ensemble des membres d'Unitah. Toutes semblent d'accord avec cette punition abusive. Armoracia et Carrot sont effondrés._

_**Pepper :**_ "Me priver de mon groupe ne vous suffit pas ? Vous voulez... VOUS VOULEZ AUSSI ME PRENDRE MA LIBERTÉ ??!! C'EST HORS DE QUESTION !"

_**Thym :**_ "Vite, écartez-vous !"

_Les sorcières reculent d'un pas. Le regard de Pepper est redevenu écarlate. Malgré l'anti-magie, son aura destructrice a repris le dessus, et avec un cri assourdissant, Pepper réussit à détruire la cage, sous le regard horrifié des autres sorcières et d'Armoracia. En leur jetant un dernier regard féroce, Pepper tend son bras et attire son balai à elle. Elle l'enfourche et s'éloigne à toute vitesse, suivie par Messy, avec un râle profond, toujours au-dessus de sa tête._

_**Safran :**_ "Elle... elle a pu annuler l'anti-magie ! Vite, rattrapons-la !"

_**Thym :**_ "Non. Il serait dangereux de la suivre, dans son état."

_**Shichimi :**_ "Est-il sage de la laisser partir avec cette créature ? Vous nous demandiez de les séparer, que son comportement était dû à ce petit monstre."

_**Thym :**_ "Malheureusement, ce n'était qu'un souhait de ma part. J'ai été surprise par cette créature qui me semblait venir de nulle part, mais j'avais tort. Il n'a jamais eu une quelconque influence sur Pepper. Bien que ça m'attriste profondément de l'avouer, tout ce que vous avez vu, tout ce qu'elle vient de dire, c'est ça, la véritable Pepper. Espérons qu'elle ne provoque pas d'autres catastrophes."

_Tous les membres d'Unitah portent leur regard sur Pepper, attristées de devoir admettre que celle qui était leur amie et leur leader possède un tel pouvoir de destruction. Carrot, impuissant, regarde sa maîtresse bien aimée s'éloigner de plus en plus de lui, le museau rempli de regrets et de tristesse.  
Pepper continue de s'éloigner de son rêve qui vient d'éclater en mille morceaux. De celles qu'elle croyait être de son côté. Et de son compagnon de toujours, une larme coulant sur sa joue, incapable de reconnaître la triste vérité._  

_Unitah, c'est terminé._

## Épilogue - L'Œil en Blanc

_Salle mystérieuse, au fin fond d'un château.  
Des dizaines de globes mystiques sont entreposés dans cette pièce beaucoup trop lumineuse pour un simple cachot. Sur une étagère, on trouve une sorte de triangle. Beaucoup trop impossible pour n'être qu'un simple objet de décoration.  
Mais, au fond, un énorme écran est suspendu au mur. Des dizaines de petites images sont diffusées via une sorte de projecteur relié à une mystérieuse boule de cristal. Ces images bougent, et montrent différents paysages d'Hereva. Sur l'un des écrans, on peut apercevoir une sorcière, sur son balai, accompagnée d'une masse chaotique informe, qui semble se séparer de ce qui lui était cher.  
En face de l'écran, une chaise. Sur la chaise, un homme, vêtu d'un costume entièrement blanc et d'une chemise noire, qui savoure un verre de vin qu'il fait tourner avec sa main, tout en tapotant sur l'accoudoir de sa chaise de l'autre, comme s'il jouait un morceau de piano. Lorsqu'il aperçoit la sorcière sur l'écran, ses pupilles se dilatent. Il prend une petite lampée de vin, et la savoure avant de l'avaler. Un léger sourire satisfait se dessine sur son visage._  

_**Homme en blanc :**_ "Hum... je sens qu'on va enfin pouvoir se divertir !"

**FIN**