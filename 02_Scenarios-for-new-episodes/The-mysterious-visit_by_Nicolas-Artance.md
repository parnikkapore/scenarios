The mysterious visit
========

* **Author:** Nicolas Artance <[nicolas.artance@gmail.com](mailto:nicolas.artance@gmail.com)>
* **License:** [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/)

## Page 1

**Panel 1:** Top view of a modern city, with giant buildings. We can see a huge crowd of people wearing curious clothes and accessories, especially on their head (helmets, solar panels, antennas, etc.). We can also see big, mechanical machines with several legs that try to move without crushing anyone (even a house is moving!). Countless pipes run in between buildings and to the ground, and it seems that a lot of energy is flowing through them. Despite the modern aspect of the city, we can make out, very very far from the focus point of the panel, a huge old castle, on the top of a steep slope.  
In the crowd two little witches are talking.  
_Pepper: Come on! Tell me who we are visiting!_  
_Coriander: I said you'll see once we're there._

**Panel 2:** Focus inside the crowd. Pepper is trying to follow Coriander, who's carrying a big bag on her shoulders. Pepper has to carry Carrot in her arms, because there are a lot of people around. And also a lot of mechanical beings, that seem to try to help people. One of them is displaying a mini map of the city to a tourist. Carrot is very curious, and looks around him.  
_Pepper: Why are you keeping this a secret? I am your friend, you can tell me!_  
_Coriander: You heard my father: nobody can know anything about the identity of my client._

**Panel 3:** Pepper and Coriander pass in front of a shop with animated ads... performed by mechanical beings. It is about a hi-tech new gadget, which seems to give the exact moment of the day... but which is very big! Carrot is attracted by a mechanical mouse, presented as a toy for cats.  
_Coriander: It is my client's request not to be known, and as my father often says: "the customer is king"._  
_Pepper: I know, I know, but I imagined that for me, you could make a little exception..._

## Page 2

**Panel 4:** Coriander stops walking and focuses on Pepper, who is surprised. She is not really angry, but forceful.  
_Coriander: And what do you imagine that I am doing, right now? You are here because you are listening at doors!_

**Panel 5:**
_Pepper: Yes... but I couldn't know that you would have a secret talk with your father at the same moment that I am coming to your castle..._  
_Coriander: That's why Father accepted that you can accompany me, but at the condition you ask NO questions. NO QUESTIONS, Pepper. So follow me in silence, please._

**Panel 6:** Pepper and Coriander enter a small alley. There is a beggar, seated on the floor.  
_Pepper: As you wish... but I am more curious than before! What does this client looks like? Tall, small? Are they aggressive, nice?_  
_Coriander: No comment._

**Panel 7:** A mechanical being, humanitarian specialist, gives some coins to the beggar! Carrot is amazed.  
_Pepper: OK! "No questions", I know! But you can tell me what you carry! That hasn't got anything to do with the client!_  
_Coriander: It HAS, actually. So no more questions. Ah, here it is._

## Page 3

**Panel 8:** They are in front of a big wall without any signs. It has some cracks. It seems to be an impasse.

**Panel 9:** Pepper is curious.  
_Pepper: Well... so we are visiting a... wall?_  
_Coriander: First rule of the city: don't rely on you first impression. It is not a simple wall. Hum-hum. Excuse me, sir._

**Panel 10:** The wall is moving! Pepper and Carrot are surprised. The cracks opens, and little eyes appears.

**Panel 11:** A bigger crack opens when he's talking.  
_Wall: Hum? Oh, it is you, princess. I suppose this is for the same business as usual?_

**Panel 12:**  
_Coriander: You are as perceptive as usual, sir._  
_Pepper: I get it! I see why you can't say anything about him, it is a very special client..._

## Page 4

**Panel 13:** The cracks disappear.

**Panel 14:** One new crack appears.

**Panel 15:** It becomes bigger, and slits the wall in two. 

**Panel 16:** Slowly, an opening appears.

**Panel 17:**  
_Coriander: That was not my client. Come on._  
_Pepper: Oh..._

**Panel 18:** Pepper follows Coriander inside the opening.

**Panel 19:** The opening is closing.

## Page 5

**Panel 20:** They are inside an old tunnel. Coriander turns on a light on her hat.  
_Pepper: I bet your client is even weirder than this wall! And in your city, I expect everything of..._  
_Coriander: Get down!_

**Panel 21:** Coriander jumps to the ground but Pepper is too slow. She disappears inside a blur of bats.

**Panel 22:** Pepper and Carrot are waving their arms/paws, but all the bats have gone. Except one, which is flying around her head.  
_Pepper: Help! Help!_  
_Coriander: It's over. Let's go on!_

**Panel 23:** They continue their walk. Pepper is grumpy, and Carrot, scared, looks carefully around him. The last bat of the previous panel landed on Pepper's hat, but nobody has seen it.  
_Coriander: It is a trap for intruders. My client doesn't welcome visitors, except me._  
_Pepper: Yes, that's why they attacked you too!_  
_Coriander: My client doesn't have the technology to distinguish between the two. That's something I am working on. Ah, here we are._

## Page 6

**Panel 24:** They are in front of a mechanical being, larger than the others they saw, with a helmet on its head. The helmet has a light. Next to the automaton, there is a wagon on rails. Behind it, a big console with many buttons, all with different colors. Some of them are turned on. There are also some levers, some activated, some not.  
_Coriander: Morning, sir. I am sorry to disturb you, but..._  
_M.Being: Don't say more, princess Coriander. I am used to your visit, now._

**Panel 25:**  
_Pepper: I don't see why your client is so secret. It looks just like the other mechanical beings we saw previously!_  
_Coriander: Him? No, he is just the manager of the underground rails network. He'll help us to reach our goal. Get in!_

**Panel 26:** Coriander steps in the wagon, followed by Pepper, disappointed again.  
_Pepper: I'm starting to believe that we'll never meet your client..._  
_Coriander: Be patient! We can go!_

**Panel 27:** The manager activates a lever. The cart starts moving.  
_Coriander: Maybe you didn't expect all these obstacles to go see a simple client._

## Page 7

**Panel 28:** The wagon is quick.  
_Coriander: That's normal!._

**Panel 29:** Shift!  
_Coriander: My client's life took an unexpected turn during their childhood._

**Panel 30:** Looping!  
_Coriander: They didn't know where to go!_

**Panel 31:** The wagon stops.  
_Coriander: If they couldn't find a house very quickly, my client's life would've stopped._

**Panel 32:** The wagon is pushed upward by a platform.  
_Coriander: Meeting my father was a springboard for my client!_

**Panel 33:** The wagon emerges from the floor.  
_Coriander: He gave them a hidden house, with some traps so they are not disturbed._

**Panel 34:** Pepper and Carrot are confused. Judging by the grass on the floor, they must be in some sort of garden. Coriander turns off her lamp and shows something with her hand.  
_Coriander: This is it! This is my client's house._

## Page 8

**Panel 35:** A small, classic house is in front of them. It is really far from the modern city. It looks like Pepper's house. We can see a forest behind the house, and a little river to its right. This river joins an ocean, not far from the house. A little beach can been seen, with many people on it.  

**Panel 36:** Pepper seems to be angry.  
_Pepper: After all these mysteries, don't go telling me that your client is just a normal person!_

**Panel 37:** Coriander points to Pepper's hat. Pepper is afraid.  
_Coriander: Absolutely not! You can judge for yourself, she is just here!_

**Panel 38:** Pepper and Carrot are surprised. The bat was here the whole time! It bat disappears in an intense magic light. Pepper and Carrot jump.

**Panel 39:** A deep dark veil is appearing around the house. We always can see outside the veil, but the light is drastically reduced. A dark silhouette appears, wrapped entirely in a dark cape they hold to hide their face.

## Page 9

**Panel 40:** Close-up on the silhouette. They ask with a disturbing voice.  
_Silhouette: You have it?_

**Panel 41:** Close-up on Coriander. She has a challenge face.  
_Coriander: Of course._

**Panel 42:** Pepper and Carrot are looking to the conversation with a worrying face. 

**Panel 43:** Final large panel. The silhouette throws their cape away, and reveals their identity. It is a young and tall girl, with a very pale skin, mid-long, dark hair, two long canine teeth. She wears a swimsuit, with a vampire on a beach towel, with sunglasses, below a parasol, sipping a glass of blood with a large smile, with a thumbs-up, drawn on it. She also wears sunglasses, and seems to be very pleased. Coriander has put her bag on the floor and is holding a potion with pride. Behind the veil, we can see other girls with swimsuit and beach stuff (buoys, surfboards, parasol, etc) who jump and shake their arms with big smile, looking at the scene. They seem to be friends with the girl who are talking to Coriander. Pepper and Carrot are looking at them with a big confusion. Projectors placed upward the front door enlighten the zone inside the veil.  
_Vampire girl: Oh, princess! You are amazing! You already finished it?!_  
_Coriander: For sure! This is the best vampire suncream my engineers could create! You can trust its efficiency, because we have tested it extensively, as you asked. The customer is king!_  
_Pepper: Don't rely on your first impression, you said? What a curious city!_

## *FIN*
