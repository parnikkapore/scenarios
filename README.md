## Formating style:

We use the **markdown** format. Check the other scenario's sources to have an idea of the formating style and how the website does the rendering. Don't worry too much about it; everything can be fixed before publishing on the website. But here is a list of tips:

1. No sex, no violence; write for all audience.
2. If you want to write for an episodes; keep it short (between 20 and 30 panels).
3. Try to be consistent with the world of Hereva, as written on the [Wiki](https://github.com/Deevad/peppercarrot/wiki).
4. Insertion of illustration and pictures is ok (must be CC-By compatible and use absolute links).
5. The main header h1 `# like that in markdown` has a special theme with a little separator under it. Use it for the top page title.
6. Text in italic `_like that in markdown_` after a picture or the main title(h1) will receive a special 'legend' theming.

## Filename

Example:
`Your-long-or-short-title_by_Your-name.md`

1. Use all letter small or capitalised and numbers, without accent or special characters.
2. `_` and `-` in filename will be replaced with space.
3. If you need a `,` insert two `--` 
4. Use `_by_` to write your author name, it is required: 

## License:

The authors of all modifications, corrections or contributions to this files accept to release their work under the license: [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/).
